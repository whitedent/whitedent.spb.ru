<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "В данном разделе Вы можете посмотреть фотографии наших работ по стоматологии, а так же увидеть сам рабочий процесс.");
$APPLICATION->SetPageProperty("keywords", "фото до и после протезирование зубов пластика");
$APPLICATION->SetPageProperty("title", "Фото клиники, рабочего процесса - До и после пластики и протезирования зубов");
$APPLICATION->SetTitle("Фотогалерея");
?> <?$APPLICATION->IncludeComponent(
	"whitedent:medialibrary.gallery.structure",
	"gallery.all",
	Array(
		"FOLDERS" => array(0=>"0",),
		"VAR" => "",
		"GALLERY_ID" => "",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"FOLDER" => "1"
	)
);?> 
<div class="do-i-posle-gal"> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"photos",
	Array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "7",
		"NEWS_COUNT" => "100",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_PICTURE",2=>"DETAIL_PICTURE",),
		"PROPERTY_CODE" => array(0=>"P_DOCTOR",),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);?> </div>
 
<!-- do-i-posle-gal -->
 <p>В этом разделе размещены фотографии, иллюстрирующие результат работы специалистов нашей клиники. Все фотографии сделаны до и после работы стоматологов «ВайтДент», размещены с согласия пациентов. Ознакомившись с фотографиями до и после протезирования, вы сможете оценить качество работы врачей клиники и сравнить его с работами специалистов других клиник. Все работы проводятся с использованием самого современного оборудования и сертифицированных препаратов, разрешенных к использованию на территории Российской Федерации. Врачи клиники выполняют самые сложные стоматологические операции, устанавливают протезы, импланты. На все виды работ распространяется гарантия.</p><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>