<?
$reviewsFilter = [];

if(!empty($_GET["doctor"])){
	$reviewsFilter["PROPERTY_O_DOCTOR"] = $_GET["doctor"];
}

if(!empty($_GET["service"])){
	$reviewsFilter["PROPERTY_SERVICE"] = $_GET["service"];
}

$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"reviews",
	Array(
		"IBLOCK_ID" => "9",
		"SORT_BY1" => "DATE_CREATE",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"PROPERTY_CODE" => array("NAME", "PUBLISHED"),
		"FIELD_CODE" => array("DATE_CREATE", "PREVIEW_PICTURE"),
		"FILTER_NAME" => "reviewsFilter",
		"NEWS_COUNT" => "5",

		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "content",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);