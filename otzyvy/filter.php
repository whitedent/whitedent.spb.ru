<div class="page-subsection">
	<div class="block-marked block-marked--shadow">
		<form class="grid grid--padding-y">

			<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
				<div class="form-input form-input--select">
					<select class="form-input__field" name="doctor" onchange="this.form.submit()">
						<option value="">Выберите врача</option>
                        <? foreach (getReviewsDoctors() as $doctor): ?>
                            <option
                                value="<?= $doctor["id"] ?>"
                                <?= $_GET["doctor"] == $doctor["id"] ? "selected" : "" ?>
                            ><?= $doctor["name"] ?></option>
						<? endforeach ?>
					</select>
				</div>
			</div>

			<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
				<div class="form-input form-input--select">
					<select class="form-input__field" name="service" onchange="this.form.submit()">
						<option value="">Выберите услугу</option>
						<? foreach (getReviewsServices() as $service): ?>
                            <option
                                value="<?= $service["id"] ?>"
								<?= $_GET["service"] == $service["id"] ? "selected" : "" ?>
                            ><?= $service["name"] ?></option>
						<? endforeach ?>
                    </select>
				</div>
			</div>

		</form>
	</div>
</div>
