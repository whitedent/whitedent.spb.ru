<div class="page-subsection">
	<div class="block-marked block-marked--arrow">
		<div class="grid grid--padding-y grid--justify-center grid--par-like">

			<div class="grid__cell grid__cell--l-9 grid__cell--m-9 grid__cell--xs-12">
				<p>Ваше мнение о работе влиники Вайтдент очень важно для нас! Любые отзывы, как положительные, так и критические, помогают совершенствовать наши услуги, обеспечивать удовлетворенность пациентов и поддерживать качество нашей работы на самом высоком уровне.</p>
			</div>

			<div class="grid__cell grid__cell--m-3 grid__cell--s-6 grid__cell--xs-12">
				<a class="button button--wide" href="/otzyvy/add/">Оставить отзыв</a>
			</div>

		</div>
	</div>
</div>