<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Спасибо за отзыв");
$APPLICATION->SetTitle("Спасибо за отзыв");
?>
<section class="page-section container">
	<div class="content">
		<div class="grid grid--justify-center">
			<div class="grid__cell grid__cell--l-6 grid__cell--m-8 grid__cell--xs-12 text-align--center">

				<h1>Спасибо за отзыв</h1>

				<div class="page-subsection">
					<p>Отзыв появится на сайте в течение двух рабочих дней.</p>
				</div>

				<div class="page-subsection">
					<p><strong>Посмотрите чем живет клиника, заходите в нашу группу</strong></p>
					<div class="grid grid--justify-center grid--par-like grid--padding-y">
						<div class="grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12"><!-- Put this script tag to the <head> of your page -->
							<script src="https://vk.com/js/api/openapi.js?168"></script><!-- Put this div tag to the place, where the Group block will be -->
							<div id="vk_groups"></div>
							<script type="text/javascript">VK.Widgets.Group("vk_groups", {mode: 4, height: 400, width: "auto"}, 64206144);</script>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");