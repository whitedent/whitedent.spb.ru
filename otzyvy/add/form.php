<form class="block-marked new-review validated-form submitJS">
	<div class="grid grid--padding-y">

		<div class="grid__cell grid__cell--xs-12">
			<div class="form-input form-input--textarea">
                <input type="text" name="req_name">
				<textarea name="text" class="form-input__field" required placeholder="Текст отзыва"></textarea>
			</div>
		</div>

		<div class="grid__cell grid__cell--xs-12">
			<div class="review-form-rating">
				<div class="review-form-rating__list">
					<div class="review-form-rating__star" data-rating="1"></div>
					<div class="review-form-rating__star" data-rating="2"></div>
					<div class="review-form-rating__star" data-rating="3"></div>
					<div class="review-form-rating__star" data-rating="4"></div>
					<div class="review-form-rating__star" data-rating="5"></div>
				</div>
				<div class="form-input form-input--hidden">
					<input class="form-input__field" type="hidden" name="rating">
				</div>
				<div class="review-form-rating__description">Пожалуйста, оцените насколько вам понравилось в нашей клинике.</div>
			</div>
		</div>

		<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
			<div class="form-input form-input--select">
				<select class="form-input__field" name="service">
					<option value="">Выбрать услугу</option>
                    <? foreach (getAllServices() as $service): ?>
                        <option value="<?= $service["id"] ?>"><?= $service["name"] ?></option>
					<? endforeach ?>
				</select>
				<label class="form-input__label">Укажите услугу, которую вы получили</label>
			</div>
		</div>

		<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
			<div class="form-input form-input--select">
				<select class="form-input__field" name="doctor">
					<option value="">Выбрать врача</option>
					<? foreach (getAllDoctors() as $doctor): ?>
                        <option value="<?= $doctor["id"] ?>"><?= $doctor["name"] ?></option>
					<? endforeach ?>
				</select>
				<label class="form-input__label">Укажите врача, к которому вы обращались</label>
			</div>
		</div>

		<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
			<div class="form-input form-input--text">
				<input name="author" class="form-input__field" type="text" required placeholder="ФИО">
			</div>
		</div>

		<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
			<div class="form-input form-input--email">
				<input name="email" class="form-input__field" type="email" placeholder="E-mail">
				<label class="form-input__label">Адрес используется для связи с Вами при обработке отзыва.</label>
			</div>
		</div>

		<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
			<div class="form-input form-input--tel">
				<input name="phone" class="form-input__field" type="tel" required placeholder="Телефон">
				<label class="form-input__label">Телефон используется для связи с Вами при обработке отзыва.</label>
			</div>
		</div>

		<div class="grid__cell grid__cell--xs-12">
			<div class="form-input form-input--checkbox">
				<input class="form-input__field" type="checkbox" required checked>
				<label class="form-input__label">Нажимая на кнопку, я даю свое согласие на обработку <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">персональных данных</a>.</label>
			</div>
		</div>

		<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
			<button class="button button--wide" type="submit">Отправить</button>
		</div>

	</div>
</form>