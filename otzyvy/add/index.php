<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Вы можете ознакомиться с отзывами наших клиентов в данном разделе сайта. Так же мы с радостью опубликуем Ваш отзыв на нашем сайте.");
$APPLICATION->SetPageProperty("title", "Оставить отзыв");
CJSCore::Init(array("popup"));
?>
<section class="page-section container">
	<div class="content">

		<h1>Оставить отзыв</h1>

		<? //include __DIR__."/description.html" ?>

		<div class="page-subsection">
			<? include __DIR__."/form.php" ?>
		</div>

	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");