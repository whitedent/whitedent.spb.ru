<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Вы можете ознакомиться с отзывами наших клиентов в данном разделе сайта. Так же мы с радостью опубликуем Ваш отзыв на нашем сайте.");
$APPLICATION->SetPageProperty("keywords", "отзывы стоматология услуги клиенты пациенты");
$APPLICATION->SetPageProperty("title", "Отзывы клиентов по стоматологии – клиника «ВайтДент»");
$APPLICATION->SetTitle("Отзывы");
?>
<section class="page-section container">
	<div class="content">
        <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
		<? include __DIR__."/button.php" ?>
		<? include __DIR__."/filter.php" ?>
		<? include __DIR__."/about.php" ?>
	</div>
</section>

<section class="page-section container">
	<div class="content">
		<? include __DIR__."/component.php" ?>
	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");