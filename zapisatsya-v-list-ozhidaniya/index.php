<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Записаться в лист ожидания");
?>          <p>Берегите себя, оставайтесь дома. Вы&nbsp;можете записаться в&nbsp;лист ожидания на&nbsp;любую услугу. <strong>Перезвоним вам, как только получим разрешение принимать клиентов, и&nbsp;согласуем дату визита.</strong></p>
          <h2>Как записаться?</h2>
              <h3 style="text-align: center;">Отправьте заявку через форму</h3>

<div class="feedback-form" id="enlist-form">
  <div class="mfeedback">

    <form method="POST" class="feedback-form zapisatsya-v-list-ozhidaniya validated-form submitJS">
      <table>
        <tbody>

        <input type="text" name="req_name" style="display: none;">

          <tr>
			  <td style="width: 180px;"><span style="color: tomato;">*</span> Имя</td>
            <td>
              <input type="text" name="name" class="validated required" pattern="^[а-яА-ЯёЁ ]+$" title="Допустимы русские буквы и пробел">
              <div class="validation-msg validation-msg-required">Обязательное поле</div>
            </td>
          </tr>

          <tr>
            <td><span style="color: tomato;">*</span> Телефон</td>
            <td>
              <input type="tel" name="phone" class="validated required">
              <div class="validation-msg validation-msg-required">Обязательное поле</div>
            </td>
          </tr>

          <tr>
            <td><span style="color: tomato;">*</span> Услуга</td>
            <td>
              <textarea name="service" class="validated required"></textarea>
              <div class="validation-msg validation-msg-required">Обязательное поле</div>
            </td>
          </tr>

          <tr>
            <td></td>
          </tr>

          <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Отправить" style="margin-left: 0;"></td>
          </tr>

        </tbody>
      </table>
    </form>

  </div>
</div>
            <p style="margin-top: 1.95rem; text-align: center;"><strong>или</strong></p>
           <h3 style="text-align: center;">Позвоните нам <span style="white-space: nowrap; font: inherit;">+7 (812) 385-01-51</span></h3><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>