<form class="form contacts validated-form submitJS">

    <input type="hidden"  name="secret" value="" class="secret">

    <div class="form-input form-input--text">
        <input name="name" class="form-input__field" type="text" required placeholder="Имя">
    </div>

    <div class="form-input form-input--email">
        <input name="email" class="form-input__field" type="email" required placeholder="E-mail">
    </div>

    <div class="form-input form-input--textarea">
        <textarea name="message" class="form-input__field" required placeholder="Ваше сообщение"></textarea>
    </div>

    <div class="form-input form-input--checkbox">
        <input class="form-input__field" type="checkbox" required checked>
        <label class="form-input__label">Отправляя эту форму Вы соглашаетесь с условиями Обработки персональных данных</label>
    </div>

    <button class="button" type="submit">Отправить</button>

</form>
