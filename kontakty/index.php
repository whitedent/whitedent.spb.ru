<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Наш адрес: г. Санкт-Петербург, ул. Нахимова, д. 7, корп. 2 (метро Приморская).");
$APPLICATION->SetPageProperty("keywords", "адреса контакты стоматология вайтдент приморская санкт петерург");
$APPLICATION->SetPageProperty("title", "Адреса стоматологии «ВайтДент» - клиника м. Приморская, Санкт-Петербург");
$APPLICATION->SetTitle("Контакты");
?>
<!--<?=$_SERVER['REQUEST_URI']?>-->
<section class="page-section container">
    <div class="content">

        <h1>Контакты</h1>

        <div class="page-subsection">
            <div class="grid grid--par-like grid--padding-y">
                <div class="grid__cell grid__cell--l-5 grid__cell--m-6 grid__cell--xs-12">
                    <div class="block-marked">
                        <div class="properties">
                            <div class="properties__item properties__item--contact properties__item--address">
                                <div class="properties__name">Адрес</div>
                                <div class="properties__value">г. Санкт-Петербург<br>ул. Нахимова, д. 7, корп. 2</div>
                            </div>
                            <div class="properties__item properties__item--contact properties__item--metro">
                                <div class="properties__name">Станция метро</div>
                                <div class="properties__value">м. Приморская (15 минут пешком)</div>
                            </div>
                            <div class="properties__item properties__item--contact properties__item--schedule">
                                <div class="properties__name">Время работы</div>
                                <div class="properties__value">пн.-пт. с 11 до 21, сб. с 11 до 18</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid__cell grid__cell--l-7 grid__cell--m-6 grid__cell--xs-12">
                    <div class="block-marked">
                        <div class="properties">
                            <div class="properties__item properties__item--contact properties__item--tel">
                                <div class="properties__name">Телефон</div>
                                <div class="properties__value"><span class="text-marked"><span class="phone">+7 (812) 612-97-92</span>, <span class="phone">+7 (812) 355-41-09</span></span></div>
                            </div>
                            <div class="properties__item properties__item--contact properties__item--email">
                                <div class="properties__name">E-mail</div>
                                <div class="properties__value"><a href="mailto:send@whitedent.spb.ru">send@whitedent.spb.ru</a><br>Сюда вы можете направить вопросы и пожелания о работе клиники</div>
                            </div>
                            <div class="properties__item properties__item--contact properties__item--vk">
                                <div class="properties__name">Соц. сети</div>
                                <div class="properties__value"><strong>10% скидка</strong> участникам нашей группы <a href="https://vk.com/spbdent" target="_blank">Вконтакте</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid__cell grid__cell--xs-12">
                    <div class="block-marked block-marked--icon block-marked--icon-teeth-help block-marked--icon-on-top">
                        <p class="text-marked">Гарантировано получение помощи при острой боли в момент обращения!
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-subsection" id="contacts-map">
            <h2>Карта проезда от метро Приморская</h2>
            <div class="grid grid--par-like grid--padding-y">
                <div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
                    <script
                        src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=1oZKRkLd7d1HAaTeaYinE17kiMFEouvM&amp;height=450"
                        data-skip-moving="true"
                    ></script>
                </div>
                <div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
                    <h3>Как добраться пешком</h3>
                    <p>От станции метро «Приморская» следуйте на юг. По улице Наличная идите в сторону наб. Новосмоленская/ул. Одоевского. Через 650 метров поверните направо на улицу Нахимова. Пройдите 270 метров, вновь поверните направо, продолжая движение по улице Нахимова. Затем плавный поворот направо надо сделать спустя 300 метров пути. Через 30 метров плавно поверните налево, а через 28 метров поверните направо. Пройдите 3 метра, поверните налево. Двигайтесь еще порядка 90 метров, пока слева не увидите здание клиники.</p>
                </div>
            </div>
        </div>

        <div class="page-subsection">
            <div class="grid grid--par-like grid--padding-y">
                <div class="grid__cell grid__cell--l-7 grid__cell--xs-12">
                    <h2>Наша клиника</h2>
                    <p>Клиника располагается в собственном 5-ти этажном здании с двумя операционными и новыйшим оборудованием.</p>
                    <div class="grid grid--padding-y grid--par-like">
                        <div class="grid__cell grid__cell--xs-12"><img src="/images/kontakty/office-1.jpg" alt="">
                        </div>
                        <div class="grid__cell grid__cell--xs-6"><img src="/images/kontakty/office-2.jpg" alt="">
                        </div>
                        <div class="grid__cell grid__cell--xs-6"><img src="/images/kontakty/office-3.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="grid__cell grid__cell--l-5 grid__cell--xs-12">
                    <h2>Карта метро</h2>
                      <div class="contacts-map">
                        <picture>
                          <source srcset="/images/kontakty/metro-map.webp, /images/kontakty/metro-map@2x.webp 2x" type="image/webp"/>
                          <source srcset="/images/kontakty/metro-map.jpg, /images/kontakty/metro-map@2x.jpg 2x"/>
                          <img src="/images/kontakty/metro-map.jpg" loading="lazy" decoding="async"/>
                        </picture>
                      </div>
                </div>
            </div>
        </div>

        <div class="page-subsection">
            <div class="grid grid--cols-12">
                <div class="grid__cell grid__cell--l-6 grid__cell--m-8 grid__cell--xs-12">
                    <div class="block-marked block-marked--shadow">
                        <h2>Написать нам</h2>
                        <p>Вы можете написать нам, используя эту форму, или позвонить по телефонам.</p>
                        <? include __DIR__."/form.php" ?>
                    </div>
                </div>
                <div class="grid__cell grid__cell--l-6 grid__cell--m-8 grid__cell--xs-12">
                    <h2>Реквизиты</h2>
                    <p>Полное название организации: ООО «Вайт Дент»<br>Юридический адрес: 197350, Санкт-Петербург, ул.Шаврова, д.17, лит.А, пом.1-Н<br>ОГРН: 1047823008562<br>Почтовый индекс: 199226<br>Почтовый адрес: ул. Нахимова, д.7, корп.2, литер А, г. Санкт-Петербург<br>БИК: 044030706<br>Расчетный счет: 40702810322110000242<br>Название банка: ФИЛИАЛ ПАО «БАНК УРАЛСИБ» В Г.САНКТ-ПЕТЕРБУРГ<br>Город банка: Г.САНКТ-ПЕТЕРБУРГ</p>
                </div>
            </div>
        </div>

        <div itemscope itemtype="http://schema.org/Organization">
            <meta itemprop="name" content="Вайтдент">
            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <meta itemprop="streetAddress" content="ул. Нахимова, д. 7, корп. 2 (метро Приморская)">
                <meta itemprop="postalCode" content="197350">
                <meta itemprop="addressLocality" content="Санкт-Петербург">
            </div>
            <meta itemprop="telephone" content="+7 (812) 612-97-92">
            <meta itemprop="telephone" content="+7 (812) 355-41-09">
            <meta itemprop="telephone" content="+7 (812) 346-80-14">
            <meta itemprop="email" content="w-dent@yandex.ru">
        </div>

    </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
