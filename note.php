<!DOCTYPE html>
<html>
<head>
    <link href="/bitrix/templates/wd-blue/template_styles.css" rel="stylesheet">
</head>
<body style="margin:0">
<script src="/bitrix/templates/whitedent/css-js/jquery-1.10.2.min.js" type="text/javascript"></script>
<? include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/wd-blue/include_areas/top/top_google_tag_manager.php'; ?>
<script>
    $(document).on("click", "#submit", function(){
        var error = 0;
        $('.error').detach();

        if($("#name").val() == "" || $("#name").val() == " ")
        {
            $("#name").after('<span class="error"><br>Поле "Имя" не заполнено</span>');
            $("#name").css("background", "tomato");
            error++;
        }
        else
            $("#name").css("background", "#fff");

        if($("#phone").val() == "" || $("#phone").val() == " ")
        {
            $("#phone").after('<span class="error"><br>Поле "Телефон" не заполнено</span>');
            $("#phone").css("background", "tomato");
            error++;
        }
        else
            $("#phone").css("background", "#fff");

        if ($("#name").val().match(/[^а-яА-Я ]/g)){
            $("#name").after('<span class="error"><br>В поле "Имя" допустимы только русские буквы и пробелы</span>');
            $("#name").css("background", "tomato");
            error++;
        }

        if ($("#phone").val().match(/[^0-9 ]/g)){
            $("#phone").after('<span class="error"><br>В поле "Телефон" допустимы только цифры и пробелы</span>');
            $("#phone").css("background", "tomato");
            error++;
        }

        if(error == 0)
        {
            $.ajax({
                url: "/bitrix/templates/wd-blue/ajax/ajax.note.php",
                type: "POST",
                data: {
                    "name":$("#name").val(),
                    "phone":$("#phone").val(),
                    "message":$("#message").val()
                },
                timeout: 5000,
                success: function(data)
                {
                    $(".msg-btn").html('<p style="color:blue;">Спасибо. Ваше сообщение отправлено, наш менеджер свяжется с Вами в ближайшее время.</p>');
                    yaCounter25626353.reachGoal('zapis');
                    ga('send','event','Form', 'Send', 'Запись на прием');
                }
            });



        }
    });
</script>
<style>
    .post-form {margin-bottom: 20px;}
    .post-form table {width: 100%;}
    .post-form td {padding-bottom:5px;}
    .post-form input[type="text"], .post-form input[type="password"] {width:100%;}
    .post-form textarea {width: 100%; height:150px;}
    .post-form .error {color:tomato;}
</style>
<div style="margin-bottom:8px">Вы можете отправить заявку нашему менеджеру с пожеланиями на дату и время записи. Наш менеджер свяжется с Вами в ближайшее время.</div>
<div class="post-form" id="note_page" style="margin-bottom: 0;">
    <table>
        <tbody>
        <tr>
            <td><span style="color: tomato;">*</span>&nbsp;</td>
            <td>Ваше имя</td>
            <td><input type="text" id="name" value=""/></td>
        </tr>
        <tr>
            <td><span style="color: tomato;">*</span>&nbsp;</td>
            <td>Телефон:</td>
            <td><input type="text" id="phone" value=""/></td>
        </tr>
        <tr>
            <td></td>
            <td>Сообщение</td>
            <td><textarea id="message"></textarea></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <label>
                    <input type="checkbox" id="agreement" checked required/>
                    Нажимая на кнопку, я даю свое согласие на обработку своих персональных данных.
                </label>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td class="msg-btn"><input type="button" id="submit" value="Отправить" style="margin-left: 0;"/></td>
        </tr>
        </tbody>
    </table>
</div>
<div style="display: none;">
    <? include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/wd-blue/include_areas/footer/footer_counters.php'; ?>
</div>
</body>
</html>