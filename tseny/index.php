<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "В данном разделе Вы можете ознакомиться с ценами на услуги по стоматологии. Стоимость так же можно уточнить у наших специалистов.");
$APPLICATION->SetPageProperty("keywords", "цена стоимость сколько стоит стоматология");
$APPLICATION->SetPageProperty("title", "Цены на стоматологические услуги - Васильевский остров,  Санкт-Петербург");
$APPLICATION->SetTitle("Цены");

$sectionCode = filterCode($_REQUEST["SECTION_CODE"]);
?>
<section class="page-section container">
	<div class="content">

		<h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>

		<div class="page-subsection">
			<div class="block-marked"> 
				<p class="text-marked"><strong>Прайс поменялся с 16 января 2023г. Цены на лечение уточняйте у администратора.</strong>
				</p>
				<p><strong>Cкидки не суммируются, на акционные позиции не действуют.</strong></p>
			</div>
		</div>

		<div class="page-subsection">
			<? include __DIR__."/menu.php" ?>
		</div>

        <? include __DIR__."/action.php" ?>
        <? include __DIR__."/search.php" ?>

		<div class="page-subsection">
			<div class="grid grid--padding-y">

				<div class="grid__cell grid__cell--l-9 grid__cell--xs-12">
					<? include __DIR__."/component.php" ?>
					<? include __DIR__."/call-back.php" ?>
				</div>

				<div class="grid__cell grid__cell--l-3 grid__cell--xs-12">
					<div class="grid grid--padding-y">
						<? include __DIR__."/features.php" ?>
					</div>
				</div>

			</div>
		</div>

	</div>
</section>

<? include __DIR__."/description.php" ?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
