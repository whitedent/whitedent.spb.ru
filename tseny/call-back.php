<div class="page-subsection">
	<div class="block-marked block-marked--arrow">
		<div class="grid grid--padding-y grid--justify-center grid--par-like">

			<div class="grid__cell grid__cell--l-9 grid__cell--m-9 grid__cell--xs-12">
				<p>Здесь представлены лишь некоторые базовые позиции.</p>
				<p>Более полную информацию о ценах вы можете узнать у администратора клиники.</p>
			</div>

			<div class="grid__cell grid__cell--m-3 grid__cell--s-6 grid__cell--xs-12">
				<a class="button" href="#" onclick="Comagic.openSitePhonePanel()">Заказать звонок</a>
			</div>

		</div>
	</div>
</div>

<div class="page-subsection">
	<p>После обследований и консультаций вам будет представлен полный план лечения, включающий перечень необходимых манипуляций, последовательность, финансовый и календарный план (в случае комплексной работы по нескольким направлениям).</p>
	<p>Точную стоимость лечения, а также этапы, альтернативные пути и гарантии вы сможете узнать только на консультации у специалиста.</p>
	<p><small>* Цены на услуги размещены с ознакомительной целью и не являются публичной офертой.</small></p>
</div>