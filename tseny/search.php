<? if(!empty($sectionCode)): ?>
    <? $APPLICATION->SetPageProperty("ADD_TYPE_AHEAD_ASSETS", true) ?>
    <div class="page-subsection">
        <div class="grid grid--cols-12">
            <div class="grid__cell grid__cell--l-6 grid__cell--m-8 grid__cell--s-10 grid__cell--xs-12">
                <div class="form-input form-input--search">
                    <input
                        class="form-input__field"
                        type="search"
                        placeholder="Поиск по прайс-листу"
                        id="price_title"
                    >
                </div>
            </div>
        </div>
    </div>
<? endif ?>