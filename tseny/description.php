<? if(empty($sectionCode)): ?>
    <section class="page-section container">
        <div class="content">
            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/tseny/description.html",
                )
            ); ?>
        </div>
    </section>
<? else: ?>
    <?
	$rsSect = CIBlockSection::GetList(
        ["ID" => "ASC"],
        ["CODE" => $sectionCode, "IBLOCK_ID" => ID_IBLOCK_PRICES]
    );
	$arSect = $rsSect->Fetch();
    ?>
    <? if($arSect["DESCRIPTION"]): ?>
        <section class="page-section container">
            <div class="content">
                <?= $arSect["DESCRIPTION"] ?>
            </div>
        </section>
    <? endif ?>
<? endif ?>