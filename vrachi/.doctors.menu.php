<?
$arOrder = ["SORT" => "ASC"];
$arFilter = [
	"IBLOCK_ID" => ID_IBLOCK_DOCTORS,
	"DEPTH_LEVEL" => 1,
	"ACTIVE" => "Y"
];
$bIncCnt = false;
$Select = ["ID", "IBLOCK_ID", "NAME", "SECTION_PAGE_URL"];
$NavStartParams = false;
$rsSections = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

$aMenuLinks = [];
while ($arSection = $rsSections->GetNext()) {
	$aMenuLinks[] = [
		$arSection["NAME"],
		$arSection["SECTION_PAGE_URL"]
	];
}