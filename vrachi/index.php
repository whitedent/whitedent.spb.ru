<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Сотрудники нашей клиники - это в первую очередь высококвалифицированные хирурги, имплантологи, терапевты, ортопеды и ортодонты. Высококвалифицированные врачи работают по всем направлениям современной стоматологии.");
$APPLICATION->SetPageProperty("keywords", "врачи хирурги стоматологии имплантологи терапевты ортопеды ортодонты");
$APPLICATION->SetPageProperty("title", "Врачи стоматологической клиники «ВайтДент»");
$APPLICATION->SetTitle("Врачи стоматологии ВайтДент");

$sectionCode = filterCode($_REQUEST["SECTION_CODE"]);
?>
<section class="page-section container">
    <div class="content">
        <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "doctors",
            array(
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => "5",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ACTIVE_FROM",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
				"SET_TITLE" => (bool)$sectionCode ? "Y" : "N",
				"PARENT_SECTION_CODE" => $sectionCode,

                "NEWS_COUNT" => "1000",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "D_SPECIALTY",
                    2 => "D_CERT",
                    3 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",

                "SET_STATUS_404" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "Y",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );
        ?>
    </div>
</section>
<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
