<section class="page-section container">
	<div class="content">

		<h2 class="home-subheader">Наши специалисты</h2>

		<div class="page-subsection">
			<div class="splide slider-doctors">
                <? include __DIR__."/doctors-component.php" ?>
			</div>
		</div>

		<div class="page-subsection">
			<div class="block-marked block-marked--arrow block-marked--shadow">
				<? include __DIR__."/doctors-description.php" ?>
			</div>
		</div>

	</div>
</section>