<? /* <p style="border: 1px solid #e0e0e0;padding-block: 0.5rem;margin-top: 1rem;text-align:center;">График работы в&nbsp;новогодние праздники: <strong>с&nbsp;31&nbsp;декабря по&nbsp;7&nbsp;января&nbsp;&mdash; выходной</strong></p> */?>
<div class="grid grid--padding-y grid--justify-center home-advantages">

    <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12">
        <div class="advantage-block advantage-block--top-10">
            <div class="advantage-block__text">
                Топ 10 клиник по имплантации <a href="https://www.spb.kp.ru/daily/26339.5/3222258/">по версии КП</a>
            </div>
        </div>
    </div>

    <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12">
        <div class="advantage-block advantage-block--lab">
            <div class="advantage-block__text">Собственная зуботехническая лаборатория</div>
        </div>
    </div>

    <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12">
        <div class="advantage-block advantage-block--tomography">
            <div class="advantage-block__text">Компьютерная томография зубов и пазух носа</div>
        </div>
    </div>

    <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12">
        <div class="advantage-block advantage-block--since-2004">
            <div class="advantage-block__text">Работаем с 2004 года<br>Мы помогли > 10 000 пациентов</div>
        </div>
    </div>

    <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12">
        <div class="advantage-block advantage-block--guarantee">
            <div class="advantage-block__text">
                <a href="/patsientam/garantii/">Гарантия</a> на все виды услуг
            </div>
        </div>
    </div>

</div>
