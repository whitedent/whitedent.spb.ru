<div class="page-subsection">
    <div class="grid grid--padding-y">

        <div class="grid__cell grid__cell--l-3 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden js-header-services"></div>

        <div class="grid__cell grid__cell--l-9 grid__cell--xs-12">
            <? include __DIR__."/top-advantages.php" ?>
        </div>

    </div>
</div>