<section class="page-section home-form container">
	<div class="content">
		<div class="grid grid--justify-center">

			<div class="grid__cell grid__cell--l-6 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden home-form__image">
				<picture>
					<source srcset="/images/home-form.webp, /images/home-form@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/home-form.jpg, /images/home-form@2x.jpg 2x"/><img src="/images/home-form.jpg" alt="Записаться на консультацию" title="Записаться на консультацию"/>
				</picture>
			</div>

			<div class="grid__cell grid__cell--l-6 grid__cell--m-8 grid__cell--s-10 grid__cell--xs-12 home-form__body">
				<h2 class="home-form__header">Записаться на консультацию</h2>
				<p>Вы можете написать нам, используя эту форму, или позвонить по телефонам.</p>
                <? include __DIR__."/consultation-form.php" ?>
			</div>

		</div>
	</div>
</section>
