<section class="page-section container">
	<div class="content">

		<h2 class="home-subheader">Наша клиника</h2>

		<div class="page-subsection">
			<div class="grid grid--padding-y">
				<div class="grid__cell grid__cell--l-3 grid__cell--m-3 grid__cell--xs-6 grid__cell--order-s-1 grid__cell--order-xs-1">
					<div class="gallery-item">
						<picture>
							<source srcset="/images/home-our-clinic-1_xs.webp, /images/home-our-clinic-1_xs@2x.webp 2x" type="image/webp" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-1_l.webp, /images/home-our-clinic-1_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)"/>
							<source srcset="/images/home-our-clinic-1_xs.jpg, /images/home-our-clinic-1_xs@2x.jpg 2x" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-1_l.jpg, /images/home-our-clinic-1_l@2x.jpg 2x" media="(min-width: 480px)"/>
							<img src="/images/home-our-clinic-1.jpg" alt="Коридор клиники" title="Коридор клиники" width="268" height="364" loading="lazy"/>
						</picture>
					</div>
				</div>
				<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12 grid__cell--order-s-3 grid__cell--order-xs-3">
					<div class="gallery-item">
						<picture>
							<source srcset="/images/home-our-clinic-2_xs.webp, /images/home-our-clinic-2_xs@2x.webp 2x" type="image/webp" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-2_l.webp, /images/home-our-clinic-2_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)"/>
							<source srcset="/images/home-our-clinic-2_xs.jpg, /images/home-our-clinic-2_xs@2x.jpg 2x" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-2_l.jpg, /images/home-our-clinic-2_l@2x.jpg 2x" media="(min-width: 480px)"/>
							<img src="/images/home-our-clinic-2.jpg" alt="Наша клиника" title="Наша клиника" width="568" height="364" loading="lazy"/>
						</picture>
					</div>
				</div>
				<div class="grid__cell grid__cell--l-3 grid__cell--m-3 grid__cell--xs-6 grid__cell--order-s-2 grid__cell--order-xs-2">
					<div class="gallery-item">
						<picture>
							<source srcset="/images/home-our-clinic-3_xs.webp, /images/home-our-clinic-3_xs@2x.webp 2x" type="image/webp" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-3_l.webp, /images/home-our-clinic-3_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)"/>
							<source srcset="/images/home-our-clinic-3_xs.jpg, /images/home-our-clinic-3_xs@2x.jpg 2x" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-3_l.jpg, /images/home-our-clinic-3_l@2x.jpg 2x" media="(min-width: 480px)"/>
							<img src="/images/home-our-clinic-3.jpg" alt="Административная стойка" title="Административная стойка" width="268" height="364" loading="lazy"/>
						</picture>
					</div>
				</div>
				<div class="grid__cell grid__cell--l-3 grid__cell--m-3 grid__cell--xs-6 grid__cell--order-s-4 grid__cell--order-xs-4">
					<div class="gallery-item">
						<picture>
							<source srcset="/images/home-our-clinic-4_xs.webp, /images/home-our-clinic-4_xs@2x.webp 2x" type="image/webp" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-4_l.webp, /images/home-our-clinic-4_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)"/>
							<source srcset="/images/home-our-clinic-4_xs.jpg, /images/home-our-clinic-4_xs@2x.jpg 2x" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-4_l.jpg, /images/home-our-clinic-4_l@2x.jpg 2x" media="(min-width: 480px)"/>
							<img src="/images/home-our-clinic-4.jpg" alt="Кабинет" title="Кабинет" width="268" height="364" loading="lazy"/>
						</picture>
					</div>
				</div>
				<div class="grid__cell grid__cell--l-3 grid__cell--m-3 grid__cell--xs-6 grid__cell--order-s-5 grid__cell--order-xs-5">
					<div class="gallery-item">
						<picture>
							<source srcset="/images/home-our-clinic-5_xs.webp, /images/home-our-clinic-5_xs@2x.webp 2x" type="image/webp" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-5_l.webp, /images/home-our-clinic-5_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)"/>
							<source srcset="/images/home-our-clinic-5_xs.jpg, /images/home-our-clinic-5_xs@2x.jpg 2x" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-5_l.jpg, /images/home-our-clinic-5_l@2x.jpg 2x" media="(min-width: 480px)"/>
							<img src="/images/home-our-clinic-5.jpg" alt="Оборудование" title="Оборудование" width="268" height="364" loading="lazy"/>
						</picture>
					</div>
				</div>
				<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12 grid__cell--order-s-6 grid__cell--order-xs-6">
					<div class="gallery-item">
						<picture>
							<source srcset="/images/home-our-clinic-6_xs.webp, /images/home-our-clinic-6_xs@2x.webp 2x" type="image/webp" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-6_l.webp, /images/home-our-clinic-6_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)"/>
							<source srcset="/images/home-our-clinic-6_xs.jpg, /images/home-our-clinic-6_xs@2x.jpg 2x" media="(max-width: 479px)"/>
							<source srcset="/images/home-our-clinic-6_l.jpg, /images/home-our-clinic-6_l@2x.jpg 2x" media="(min-width: 480px)"/>
							<img src="/images/home-our-clinic-6.jpg" alt="Врачи за работой" title="Врачи за работой" width="568" height="364" loading="lazy"/>
						</picture>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
