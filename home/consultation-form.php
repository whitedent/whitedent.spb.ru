<form
    class="home-form__form consultation-new-registration validated-form submitJS"
    method="post"
>

    <input type="hidden"  name="secret" value="" class="secret">

    <div class="form-input form-input--text">
		<input class="form-input__field" type="text" name="name" placeholder="Ваше имя" required>
	</div>

	<div class="form-input form-input--tel">
		<input class="form-input__field" type="tel" name="phone" placeholder="Ваше телефон" required>
	</div>

	<div class="form-input form-input--textarea">
		<textarea
			class="form-input__field"
			name="message"
			placeholder="Дата визита или другая информация"
		></textarea>
	</div>

	<div class="form-input form-input--checkbox">
		<input class="form-input__field" type="checkbox" name="agreement" id="home-form-agreement" checked>
		<label class="form-input__label" for="home-form-agreement">Отправляя эту форму Вы соглашаетесь с условиями Обработки персональных данных</label>
	</div>

	<button class="button home-form__submit" type="submit">Отправить</button>

</form>
