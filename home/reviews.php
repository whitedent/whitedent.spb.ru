<section class="page-section container">
	<div class="content">

        <? include __DIR__."/reviews-title.php" ?>

		<div class="page-subsection">
			<div class="splide">
				<? include __DIR__."/reviews-component.php" ?>
			</div>
		</div>

        <div class="page-subsection">
            <div class="block-marked block-marked--arrow block-marked--shadow">
				<? include __DIR__."/reviews-description.php" ?>
            </div>
        </div>


	</div>
</section>