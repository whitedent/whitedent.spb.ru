<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Вашему вниманию представлены стоматологические услуги клиники «ВайтДент». Если вас заинтересовала какая-либо из услуг, Вы можете ознакомиться с ней подробнее.");

if(!isCurPageUrlConsist('lechenie-desen')){
	$APPLICATION->SetPageProperty("keywords", "услуги стоматология вайтдент имплантация");
}

$APPLICATION->SetPageProperty("title", "Услуги стоматологии на Васильевском острове, Санкт-Петербург");
$APPLICATION->SetTitle("Стоматологические услуги клиники");

$elementCode = filterCode($_REQUEST["ELEMENT_CODE"]);
?>
<? if($elementCode): ?>
	<? include __DIR__."/component.php" ?>
<? else: ?>
<section class="page-section container">
	<div class="content">

		<h1>Услуги</h1>

		<div class="page-subsection">
			<div class="block-marked">
				<div class="grid grid--padding-y">
                    <? include __DIR__."/menu.php" ?>
				</div>
			</div>
		</div>

		<div class="page-subsection">
			<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/uslugi/description.html",
				)
			); ?>
		</div>

	</div>
</section>
<? endif ?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");