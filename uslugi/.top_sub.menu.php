<?
$aMenuLinks = [
	["Протезирование", "https://whitedent.spb.ru/protezirovanie-zubov/"],
	["Имплантация", "https://whitedent.spb.ru/implantatsiya/"],
	["Лечение зубов", "https://whitedent.spb.ru/terapiya/"],
	["Лечение десен", "https://whitedent.spb.ru/lechenie-desen/"],
	["Хирургия", "https://whitedent.spb.ru/hirurgiya/"],
	["Исправление прикуса", "https://whitedent.spb.ru/ispravlenie-prikusa/"],
	["Гигиена полости рта", "https://whitedent.spb.ru/gigiena-polosti-rta/"],
	["Рентгенодиагностика", ""],
	["Анестезия", ""],
	["Консультация", "/konsultatsii/"],
];