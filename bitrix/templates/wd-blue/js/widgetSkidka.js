jQuery(document).ready(function($){
    var widgetSkidka = $('#widgetSkidka');
    var widgetSkidkaLabel = $('#widgetSkidkaLabel');
    var widgetSkidkaClose = $('#widgetSkidkaClose');

    widgetSkidkaLabel.click(function(){
        widgetSkidkaToggleView(true);
    });

    widgetSkidkaClose.click(function(){
        widgetSkidkaToggleView();
    });

    $('body').click(function(e) {
        if($(e.target).closest(widgetSkidka).length == 0){
            widgetSkidka.removeClass('widgetSkidkaOpen');
        }
    });

    window.setTimeout(autoShow, 7000);

    $('.widgetSkidkaForm button').click(function()
    {
        var thisForm = $(this).closest('.widgetSkidkaForm');
        if(thisForm.length)
        {
            var nameVal   = $('input[type="text"]', thisForm).val();
            var emailVal  = $('input[type="email"]', thisForm).val();

            if(nameVal)  {$.cookie('sendBook_name',  nameVal,  {path: '/', expires: 30});}
            if(emailVal) {$.cookie('sendBook_email', emailVal, {path: '/', expires: 30});}
        }

    });

/* Functions */
function widgetSkidkaToggleView(isAutoShowed)
{
    widgetSkidka.toggleClass('widgetSkidkaOpen');
    if(isAutoShowed){$.cookie('isAutoShowed', true);}
}

function autoShow(){
    if(!$.cookie('isAutoShowed')){
        widgetSkidkaToggleView(true);
    }
}
/* /Functions */

});