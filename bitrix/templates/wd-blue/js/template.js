jQuery(document).ready(function($)
{
    $('.showmap').fancybox({
        'width'    : 615,
        'height'   : 420,
        autoSize   : false,
        'iframe': {'scrolling': 'no'}
    });
    $('.note').fancybox({
        'width'    : 615,
        'height'   : 420,
        autoSize   : true
    });

    $('.callback').fancybox({
        'width'    : 400,
        'height'   : 260,
        autoSize   : false
    });

    $(".image-open").fancybox({
        helpers : {
            title : {
                type : 'inside'
            }
        }
    });

    $('.services_slider.flexslider').flexslider({
        directionNav: false
    });

    $('.home_bottom_specialist .flexslider').flexslider({
        controlNav: false,
        itemWidth: 280,
        maxItems: 2,
        animation: "slide",
        slideshow: false
    });

    $('.home_bottom_review .flexslider').flexslider({
        controlNav: false,
        itemWidth: 150,
        maxItems: 6,
        animation: "slide",
        slideshow: false
    });

    $('.left_slider.flexslider').flexslider({
        controlNav: true,
        directionNav: false
    });

    $('.specialist_item_desc_fio').each(function () {
        this.innerHTML = this.innerHTML.replace( /^(.+?\s)/, '<span>$1</span>' );
    });

    if($(window).width() < 981) {
        $('.footer_top').css('background','none');
    }

    //  Event listener
    $('.ask_question a').click(function(){
        //yaCounter25626353.reachGoal('zadat-vopros');
    });

    $('.mfeedback form').submit(function(){
        //yaCounter25626353.reachGoal('obratnaja-svjaz');
       ga('send','event','Form', 'Send', 'Обратная связь');
    });
    // /Event listener

});

jQuery(document).ready(function($)
{
    $("#faq_question_form").on('submit', function(event){
        event.preventDefault();
        event.stopPropagation();

        var data = new FormData(this);

/*
        var reCaptchaResp = grecaptcha.getResponse();

        if (reCaptchaResp.length == 0) {
        	$('.g-recaptcha').css({
        		'border':'1px solid #f00'
        	});
        	return (false);
        }
*/

        $.ajax({
            url: '/includes/addQuestionHandler.php',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR)
            {
                if (data.success == "success")
                {
                    $(".ask-question-form").prepend("<font color='green'>Ваш вопрос был добавлен.<br/>Как только наши специалисты на него ответят, Вы получите уведомление на указанный адрес почты.<br/><br/>Также Вы можете записаться на приём по телефонам: +7 (812) 355-41-09, +7 (812) 346-80-14</font>");
                    $("#faq_question_form").hide();

                    //yaCounter25626353.reachGoal('vopros');
                    ga('send','event','Form', 'Send', 'Вопрос');
                }
                else
                {
                    $(".ask-question-form").prepend("<font color='red'>Произошла ошибка. Ваш вопрос не был добавлен. Вы можете задать его по телефонам: +7 (812) 355-41-09, +7 (812) 346-80-14</font>");
                    $("#faq_question_form").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                $(".ask-question-form").prepend("<font color='red'>Произошла ошибка. " + textStatus + "</font>");
                $("#faq_question_form").hide();
            }
        });
    });

    $(".ask_title").click(function(){
        $(".ask-question-form").slideToggle();
    });

});
$(document).ready(function(){
  var mobileMenuItems = document.querySelectorAll('.top-nav-mobile ul> li');
  for ( var i = 0 ; i < mobileMenuItems.length ; i++ ) {
    var topNavMobileButton = document.createElement('span');
    topNavMobileButton.className = 'top-nav-mobile-button';
    var item = mobileMenuItems[i];
    var childList = item.querySelector('ul');
    if ( childList ) {
      item.insertBefore(topNavMobileButton, childList);
    }
  }
  $(".top-nav-mobile-button").click(function(){
      $(this).next('ul').slideToggle(200);
      if ( !this.classList.contains('top-nav-mobile-button-active')) {
        $(this).addClass("top-nav-mobile-button-active")
      } else {
        $(this).removeClass("top-nav-mobile-button-active")
      }
  })
});

$(document).ready(function(){

    $('form.feedback-form-submitJS').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            handler: "addFeedbackHandler",
            popupTitle: 'Спасибо',
            popupText: 'Ваш отзыв отправлен администратору',
        });
    });

    $('form.zapisatsya-v-list-ozhidaniya').on('submitJS', function(event){
        ajaxFormSubmit({
            event: event,
            handler: "zapisatsyaVListOzhidaniyaHandler",
            popupTitle: "Спасибо за вашу заявку",
            popupText: "Перезвоним вам, как только получим разрешение принимать клиентов, и согласуем дату визита",
        });
    });

});
