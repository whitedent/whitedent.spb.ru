var oPopup = false;
window.BXDEBUG = true;
BX.ready(function()
{
    oPopup = new BX.PopupWindow(
        'orderPopup',
        window.body,
        {
            autoHide : true,
            offsetTop : 1,
            offsetLeft : 0,
            lightShadow : true,
            closeIcon : true,
            closeByEsc : true,
            overlay: {backgroundColor: 'black', opacity: '100'}
        }
    );

    oPopup.setContent(BX('orderPopup'));

    BX.bindDelegate(
        document.body, 'click', {className: 'css_popup' },
        BX.proxy(function(e){
            if(!e)
                e = window.event;
            oPopup.show();
            return BX.PreventDefault(e);
        }, oPopup)
    );

});



jQuery(document).ready(function($)
{
    $('.call_inner form').on('submit', function(event)
    {
        var orderForm = $(this);
        var result = '';
        $('.ajax_result').detach();

        event.preventDefault();
        event.stopPropagation();

        var data = new FormData(this);
        var url = '/bitrix/templates/wd-blue/include_areas/code/addQuestionHandler.php';

        jQuery.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR)
            {
                if (data.success == "success"){
                    result = 'Ваша заявка добавлена!';
                    orderForm[0].reset();
                    oPopup.show();
                }
                else{
                    result = '<strong>Произошла ошибка:</strong><br> ' + data.error;
                }
            },

            error: function(jqXHR, textStatus, errorThrown){
                result = '<strong>Произошла ошибка.</strong> ' + textStatus;
            },

            complete: function(jqXHR, textStatus){
                if(textStatus != 'success' || jqXHR.responseJSON.error){
                    textStatus = 'error'
                    orderForm.before('<div class="ajax_result ' + textStatus + '">' + result + "</div>");
                }
            }

        });

    });
});





