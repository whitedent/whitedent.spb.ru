<?
header("Content-type: text/html; charset=utf-8");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if($_POST["name"] != "" && $_POST["phone"] != "")
{
    $event = array();
    $event["NAME"] = htmlspecialcharsex(strip_tags($_POST["name"]));
    $event["PHONE"] = htmlspecialcharsex(strip_tags($_POST["phone"]));

    if(isset($_POST["message"]) AND $_POST["message"]){
        $event["MESSAGE"] = htmlspecialcharsex(strip_tags($_POST["message"]));
        CEvent::SendImmediate("NOTE", "s1", $event, "Y", 9);
    }
    else
    {
        CEvent::SendImmediate("CALLBACK", "s1", $event, "Y", 12);
    }
}
?>