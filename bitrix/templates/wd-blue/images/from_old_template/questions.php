<?
require_once "cons.php";
require_once "pages_module.php";

header( "Content-Type: text/html; charset: utf-8;");
header( "Last-Modified: ". gmdate("D, d M Y H:i:s", time() - 3600) ." GMT");

$id = checkVar( "id", "int", "get", 0);
$page = checkVar( "page", "int", "get", 1);
$show_form = checkVar( "show_form", "int", "get", 0);

if ( $id != 0 ) {
    $where = "WHERE aq.ID_SERVICES = ". $id;
}

$question_query = mysql_query( "SELECT aq.ID_AQ, aq.ID_DOCTORS, aq.answer, aq.question, d.name AS doctor_name, d.job AS doctor_job, d.src AS doctor_src FROM AQ AS aq LEFT OUTER JOIN doctors AS d USING( ID_DOCTORS ) {$where} ". getLimit( QUANTITY_AQ), $db);

$table_for_menu = "AQ";

$email_array = array( $manager_mail, "info@smartis.ru", "stas@smartis.ru");

$form_name = checkVar( "form_name", "string", "post");
$form_email = checkVar( "form_email", "string", "post");
$form_descr = checkVar( "form_descr", "string", "post");
$form_service = checkVar( "form_service", "int", "post");

function phpValidate() {
	global $error, $form_name, $form_email, $form_descr, $form_service;
	if ( $form_name == '') {
		$error = "<br><br>Ошибка! Пожалуйста, представьтесь.<br><br>";
		return false;
	}
	if ( $form_email == '') {
		$error = "<br><br>Ошибка! Пожалуйста, введите e-mail адрес.<br><br>";
		return false;
	}
	if ( preg_match( "/^[^а-яА-Я@\/.,:;]+@[^а-яА-Я@\/.,:;]+\.[^а-яА-Я0-9@\/,:;]{2,7}$/", $form_email) == 0) {
		$error = "<br><br>Ошибка! E-mail адрес введен некорректно.<br><br>";
		return false;
	}
	if ( $form_descr == '') {
		$error = "<br><br>Ошибка! Пожалуйста, введите текст вопроса.<br><br>";
		return false;
	}
	if ( $form_service == '') {
		$error = "<br><br>Ошибка! Пожалуйста, выберите услугу.<br><br>";
		return false;
	}
	if ( $_SESSION['answer'] != $_POST['code']) {
		$error = "<br><br>Ошибка! Код визуальной проверки введен неверно.<br><br>";
		return false;
	}
	return true;
}
if ( $_POST['g'] == 636123) {
	if ( $_SESSION['cms_actcode2'] == $_POST['actcode']) {
		if ( phpValidate()) {
			// запрос услуги
			$service_query = mysql_query( "SELECT name FROM services WHERE ID_SERVICES = ". $form_service, $db);
			$service_res = mysql_fetch_array( $service_query);

			$headers = "MIME-Version: 1.0\r\n"."Content-Type: text/plain; charset=\"windows-1251\"\r\nContent-Transfer-Encoding: 8bit\r\nFrom: ". $form_email;
			$feedbtxt = "Дата: ". date( "d.m.Y H:i:s") ."\n";
			$feedbtxt .= "IP: ". $_SERVER['REMOTE_ADDR'] ."\n\n";
			$feedbtxt .= "От: ". $form_name . "\n";
			$feedbtxt .= "E-mail: ". $form_email ."\n";
			$feedbtxt .= "Услуга: ". $service_res['name'] . "\n\n";
			$feedbtxt .= $form_descr ."\n\n";
			$feedbtxt .= "___________________________________________________\nОтправлено со страницы ". $_SERVER['HTTP_REFERER'] .".\n";
			$success = 0;
			foreach ( $email_array AS $address) {
				if ( mail( $address, $root_path .": сообщение посетителя.", $feedbtxt, $headers)) {
					$success ++;
				}
			}
			if ( $success == count( $email_array)) {
				$error = "Сообщение отправлено.";
			}
			unset( $form_name, $form_email, $form_descr, $form_service);
		} else {
		  $show_form = 1;
		}
	} else {
		unset( $form_name, $form_email, $form_descr, $form_service);
	}
}

$rand_code = md5( mt_rand());
$_SESSION['cms_actcode2'] = $rand_code;

$form_display = "none";
if ( $show_form == 1) {
    $form_display = "block";
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>WhiteDent</title>
	<link rel="stylesheet" href="style_whitedent.css" type="text/css">
	<!--[if lt IE 7]>
          <![if gte IE 5.5]>
          <script type="text/javascript" src="fixpng.js"></script>
               <style type="text/css"> 
                    .iePNG, .fon_text {filter:expression(fixPNG_scale(this));} 
                    IMG {filter:expression(fixPNG_crop(this));} 
                    .iePNG A {position: relative;}
               </style>
          <![endif]>
     <![endif]-->
     <script type='text/javascript' src='/js/cons.js'></script>
</head>

<body>
<?require_once "admin_menu.php";?>
<!-- Начало попапа, который содержит форму отправки -->
<div id='ask_form' class="ask_question" style='display:<?=$form_display?>'>
	<script type='text/javascript'>
		function getError( text) {
			alert( text);
		}
		
		function validate() {
			form = document.forms['form'];
			if ( form.elements['form_name'].value.length == 0) {
				getError( "Ошибка! Пожалуйста, представьтесь.");
				return false;
			}
			if ( form.elements['form_email'].value.length == 0) {
				getError( "Ошибка! Пожалуйста, введите e-mail адрес.");
				return false;
			}
			regExpStr = /^[^а-яА-Я@\/.,:;]+@[^а-яА-Я@\/.,:;]+\.[^а-яА-Я0-9@\/,:;]{2,7}$/;
			if ( !form.elements['form_email'].value.match(regExpStr)) {
				getError( "Ошибка! E-mail адрес введен некорректно.");
				return false;
			}
			if ( form.elements['form_descr'].value.length == 0) {
				getError( "Ошибка! Пожалуйста, введите текст сообщения.");
				return false;
			}
			return true;
		}
	</script>
	<p style='text-align:right; padding:5px 10px 0px 0px'><a href="questions.php?id=<?=$id?>&page=<?=$page?>&show_form=0" onclick="showElement( 'ask_form'); return false" style='color:black'>закрыть</a></p>
	<form name='form' method='post' onsubmit='return validate()'>
	<table class="tab_ask_quest">
		<tr>
			<td colspan="2" class="ask_td1"><h1 class="red">Задать вопрос</h1><?=$error?></td>
		</tr>
		<tr>
			<td class="ask_td2">Представьтесь:</td>
			<td class="ask_td3"><input type="text" class="inp_text" name='form_name' value='<?=$form_name?>'></td>
		</tr>
		<tr>
			<td class="ask_td2">Электронная почта:</td>
			<td class="ask_td3"><input type="text" class="inp_text" name='form_email' value='<?=$form_email?>'></td>
		</tr>
		<tr>
			<td class="ask_td2">Услуга:</td>
			<td class="ask_td3">
			     <select name='form_service'>
<?
            $service_query = mysql_query( "SELECT ID_SERVICES, name FROM services ORDER BY rating, ID_SERVICES DESC", $db);
            while ( $service_res = mysql_fetch_array( $service_query) ) {
                echo "<option value='". $service_res['ID_SERVICES'] ."'>". $service_res['name'] ."</option>";
            }
?>
                </select>
			</td>
		</tr>
		<tr>
			<td class="ask_td2">Текст вопроса:</td>
			<td class="ask_td3"><textarea class="textarea_text" rows="10" name='form_descr'><?=$form_descr?></textarea></td>
		</tr>
		<tr>
			<td class="ask_td2">Введите число:</td>
			<td class="ask_td3">
				<table border="0" width="90%" >
					<tr>
						<td width="20%"><img src='/captcha.php'  alt='код проверки' title='код проверки'></td>
						<td width="15"><h2>=&nbsp;</h2></td>
						<td><input class="fon" type='text' name='code' style='width: 100%;margin-top:10px;' maxlength='6'></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="ask_td2">
				<input type='hidden' name='g' value="636123">
				<input type='hidden' name='actcode' value='<?=$rand_code?>'>
			</td>
			<td class="ask_td3"><input type="image" src="img/send.jpg" width="94" height="22" border="0"></td>
		</tr>
		<tr>
			<td class="ask_td2"></td>
			<td class="ask_td3"></td>
		</tr>
	</table>
	</form>
</div>
<!-- Конец попапа -->
<div id="top">
	<div class="top_ie">
		<div class="articles_logo">
			<a href="/"><img src="img/logo.jpg" border="0"></a>
		</div>
		<div class="articles_name">
			<h1 class="blue">Клиника Эстетической<br>
			Стоматологии</h1>
		</div>
	</div>
</div>
<div class="menu_articles">
	<?include('incl_menu.php')?>
</div>
<div class="questions">
	<div class="questions_ie6">
<?
        $cms_array = getAdminMenu2( "AQ");
        echo $cms_array['js'];
        echo $cms_array['conf'];
?>
		<h1 class="red"><?=$cms_array['add']?>Вопрос-ответ</h1>
		<div class="questions_text_ico">
			<div class="questions_text">	
				<?include('incl_menu_question.php')?>
			</div>
			<div class="questions_ico">
				<table class="tab_banner">
					<tr>
						<td class="art1_1"><img src="img/art_tl.jpg" width="10" height="10" border="0"></td>
						<td class="art2"></td>
						<td class="art1_1"><img src="img/art_tr.jpg" width="10" height="10" border="0"></td>
					</tr>
					<tr>
						<td class="art3"></td>
						<td class="art6"><a href="questions.php?id=<?=$id?>&page=<?=$page?>&show_form=1" onclick="showElement( 'ask_form'); return false"><img src="img/ico_quest.jpg" width="65" height="65" border="0" align="middle" style="margin-right:7px;"></a><a href="questions.php?id=<?=$id?>&page=<?=$page?>&show_form=1" onclick="showElement( 'ask_form'); return false" class="red">Задать&nbsp;вопрос</a><br><?=$error?></td>
						<td class="art4"></td>
					</tr>
					<tr>
						<td><img src="img/art_bl.jpg" width="10" height="10" border="0"></td>
						<td class="art5"></td>
						<td><img src="img/art_br.jpg" width="10" height="10" border="0"></td>
					</tr>
				</table>
			</div>
		</div>
<?
		echo getPageStr( "AQ", $PHP_SELF ."?id={$id}&page=*", "SELECT aq.ID_DOCTORS, aq.answer, aq.question, d.name AS doctor_name, d.job AS doctor_job, d.src AS doctor_src FROM AQ AS aq LEFT OUTER JOIN doctors AS d USING( ID_DOCTORS ) {$where} ", QUANTITY_AQ);
        while ( $question_res = mysql_fetch_array( $question_query) ) {
            $cms_array = getAdminMenu( "AQ", $question_res['ID_AQ']);
            echo <<<HERE
		<div class="quest_answer">
			<div class="quest">
				{$cms_array['edit']}{$cms_array['del']}{$question_res['question']}
			</div>   
			<div class="answer">
				<table class="tab_answer">
					<tr>
						<td class="td_tab_text">
							<table class="tab_text">	
								<tr>
									<td class="text_td1"><img src="img/answ_tl.jpg" width="30" height="30"></td>
									<td class="text_td2">&nbsp;</td>
									<td class="text_td3">&nbsp;</td>
								</tr>
								<tr>
									<td class="text_td4">&nbsp;</td>
									<td class="text_td5" style="vertical-align:top;">
                                        {$question_res['answer']}
									</td>
									<td valign="top" style="background:url('img/kostyl32.jpg') 0% 0% repeat-y"><img src="img/kostyl.jpg" width="30" height="50"></td>
								</tr>
								<tr>
									<td><img src="img/answ_bl.jpg" width="30" height="30"></td>
									<td class="text_td7">&nbsp;</td>
									<td><img src="img/answ_br.jpg" width="30" height="30"></td>
								</tr>
							</table>
						</td>
						<td class="td_tab_doctor">
							<table class="tab_doctor">
								<tr>
									<td class="doc_td1">&nbsp;</td>
					