
<div class="footer_top_copyright">&copy; 2008&ndash;<?= date('Y') ?> Клиника Вайтдент. Все права защищены.</div>
 
<div class="footer_top_license">Лицензия на осуществление медицинской 
  <br />
 деятельности № 78-01-005523 выдана 09.02 2015 
  <br />
 комитетом по здравоохранению г.Санкт-Петербурга</div>
<div class="footer-terms">
  <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">Соглашения на обработку персональных данных</a> 
  <br> 
  <a href="/pravila-ispolzovaniya-sayta/">Правила использования сайта</a>
</div>
