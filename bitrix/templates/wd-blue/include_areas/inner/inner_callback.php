<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?><script src="<?= SITE_TEMPLATE_PATH ?>/js/order_form.js" type="text/javascript"></script>
<? CJSCore::Init(array("popup")); ?>
<div class="call_inner">
    <form enctype="multipart/form-data" method="post" class="group">

        <div class="call_inner_input">
            <label for="name">Имя</label>
            <input type="text" name="name" id="name"/>
        </div>

        <div class="call_inner_input">
            <label for="phone">Телефон</label>
            <input type="text" name="phone" id="phone"/>
        </div>

        <div class="call_inner_time">
            <div class="time_desc">
                <p>Укажите, в какое время вам перезвонить?</p>
                <p>
                    Напоминаем, что администратор сможет вам перезвонить<br>
                    только в часы работы клиники с 11:00 до 21:00
                </p>
            </div>
            <input type="text" name="time" id="time"/>
        </div>
        <label class="small form-agreement">
          <input type="checkbox" id="agreement" checked="" required="">
          Нажимая на кнопку, я даю свое согласие на обработку своих персональных данных.
        </label>

        <input type="submit" name="submit" value="Получить обратный звонок"/>

    </form>
</div>

<div id="orderPopup">
    <div class="orderPopupInner">
        <p>
            <strong>Спасибо <br/>
            Мы получили заявку и скоро вам перезвоним!</strong>
        </p>
        <p class="red">Хотите Получить Скидку в 10%<br>на все услуги?</p>
        <p>Подпишитесь на группу прямо сейчас<br> и скидка ваша:)</p>
        <p><a href="https://vk.com/spbdent" class="subscribe" target="_blank"></a></p>
    </div>
</div>