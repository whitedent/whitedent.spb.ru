<h2>Вопросы</h2>
<div class="inner_questions">
    <div class="inner_questions_question">
        <div class="inner_question_title">Юля</div>
        <div class="inner_question_text">Добрый день! подскажите, пожалуйста, лечила глубокий кариес в клинике по страховке. В первый же день зуб стал болеть, через пару дней боль утихла, но все равно возникала в процессе пережевывания пищи. Обратившись через 3 недели в клинику, мне сказали, что нужно удалять нерв и ставить вкладку стоимостью 14 тысяч. Подскажите, пожалуйста, пульпит появился в результате осложнения после неправильного лечения кариеса и могу я в данном случае настаивать на бесплатном лечении по страховке? Заранее спасибо</div>
    </div>
    <div class="inner_questions_answer">
        <div class="inner_question_title">Ответ</div>
        <div class="inner_question_text">Очень сложно дать однозначный ответ по вашему вопросу без осмотра и снимков до, в процессе и после лечения! Но скорее всего появление боли после лечения глубокого кариеса может быть связано с тем фактом, что пульпа зуба( нервная ткань) расположена очень близко к области лечения кариеса! Такое бывает. Касательно лечения по страховке – эту информацию сможет предоставить Вам только Ваша страховая компания! Очень много страховых компаний, а у каждой компании – страховых программ!По какой застрахованы Вы, и что по ней проходит, мы, к сожалению, не знаем, поэтому не можем Вас проконсультировать!</div>
    </div>
</div>