<h2>Отзывы пациентов</h2>
<div class="inner_otzivy">
    <div class="inner_otzivy_inner group">
        <div class="inner_otziv">
            <div class="inner_otziv_fio">Людмила Владимировна</div>
            <div class="inner_otziv_text">Пользуюсь клиникой с момента ее образования. Отличный сервис, высокий профессионализм врачей,современное оборудование и очень бережное и уважительное отношение к пациенту. В этом заслуга коллектива и лично главврача Вадима Семеновича. Спасибо!</div>
            <div class="inner_otziv_date">26 июня 2014</div>
        </div>
        <div class="inner_otziv_to_all"><a href="">Все отзывы</a></div>
    </div>
</div>