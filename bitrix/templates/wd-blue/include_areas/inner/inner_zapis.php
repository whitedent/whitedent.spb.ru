<div class="inner_zapis">
    <div class="inner_zapis_inner">
        <p>Запишитесь на прием по телефону</p>
        <p><span class="phone">(812) 579-33-33</span></p>
        <p>или заполнив форму <strong>online</strong></p>
    </div>
    <p class="inner_zapis_note">Администратор свяжется с Вами для подтверждения записи.</p>
    <a href="#" id="inner_zapis_link">Запись на прием</a>
</div>