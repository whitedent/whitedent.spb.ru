<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!empty($_POST))
{
	//init data
	$name = htmlspecialcharsbx($_POST['name']);
	$phone = htmlspecialcharsbx($_POST['phone']);
	$time = htmlspecialcharsbx($_POST['time']);

	$date = date('Y-m-d');
	$dateFull = date('Y-m-d-H-i-s');
	$iblockId = 14;

    CModule::IncludeModule('iblock');
    if($name){
        $topic = $name.'_'.$dateFull;
    }else{
        die(json_encode(array('error' => 'Обязательное свойство "Имя" не заполнено')));
    }

    if(
        empty($phone)
        OR empty($time)
    ){
        die(json_encode(array('error' => 'Мы сможем ответить если укажите телефон и время звонка')));
    }

	//code generation
	$arParamsCode = array(
	   "max_len" => 255,  
	   "change_case" => "L",  
	   "replace_space" => '-',  
	   "replace_other" => '-',   
	   "delete_repeat_replace" => true
	);

	$code = CUtil::translit($topic, "ru", $arParamsCode);

	$element = new CIBlockElement;
	
	$elementProps = array();
	$elementProps['PHONE'] = $phone;
	$elementProps['TIME']  = $time;

	$arElementFields = Array(
		"IBLOCK_ID" => $iblockId,
		"PROPERTY_VALUES"	=> $elementProps,
		"NAME" => $name,
		"CODE" => $code,
		"ACTIVE" => "Y"
	);
	if ($PRODUCT_ID = $element->Add($arElementFields))
	{
	    $data = array("success" => "success");

        $order_link  = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
        $order_link .= '?IBLOCK_ID='.$iblockId.'&type=content&ID='.$PRODUCT_ID;

        $arEventFields = array(
            "NAME" => $name,
            "DATE" => $date,
            "ID" => $PRODUCT_ID,
            "PHONE" => $phone,
            "TIME" => $time,
            "ORDER_LINK" => $order_link
        );
        $resSend = CEvent::Send("NEW_ORDER", "s1", $arEventFields);
    }
	else{
		$data = array("error" => $element->LAST_ERROR);
	}
	echo json_encode($data);
} else {
	LocalRedirect('/');
}

?>