<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(empty($_POST)) {
	die("No data in request");
}

$siteCode = "s1";
$emailEventName = "NEW_ZAPISATSYA_V_LIST_OZHIDANIYA";

//init data
$name = htmlspecialcharsbx($_POST['name']);
$phone = htmlspecialcharsbx($_POST['phone']);
$service = htmlspecialcharsbx($_POST['service']);
$reqName = htmlspecialcharsbx($_POST['req_name']);

if(empty($name) || empty($phone) || empty($service)){
	die(json_encode([
		"result" => "error",
		"message" => "Some of input fields is empty"
	]));
}

if(!empty($reqName)){
	die(json_encode([
		"result" => "error",
		"message" => "Ошибка данных формы"
	]));
}

$dateForUser = date("d.m.Y");
$dateForCode = date("Y-m-d-H-i-s");

if($GLOBALS["prod"]) {

	$arEventFields = array(
		"DATE" => $dateForUser,
		"NAME" => $name,
		"PHONE" => $phone,
		"SERVICE" => $service,
	);

	$resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
	$returnMsg .= "; Message send: " . $resSend;
}

die(json_encode([
	"result" => "success",
	"message" => $returnMsg
]));