<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(empty($_POST)) {
    die("No data in request");
}

$iblockId = ID_IBLOCK_FEEDBACK; // Инфоблок "Отзывы"
$siteCode = "s1";
$emailEventName = "NEW_FEEDBACK";

//init data
$name = htmlspecialcharsbx($_POST['name']);
$email = htmlspecialcharsbx($_POST['email']);
$message = htmlspecialcharsbx($_POST['message']);
$reqName = htmlspecialcharsbx($_POST['req_name']);

if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['message'])){
    die(json_encode([
        "result" => "error",
        "message" => "Some of input fields is empty"
    ]));
}

if(!empty($reqName)){
	die(json_encode([
		"result" => "error",
		"message" => "Ошибка данных формы"
	]));
}

$dateForUser = date("d.m.Y");
$dateForCode = date("Y-m-d-H-i-s");

CModule::IncludeModule('iblock');

$topicInit = 'Отзыв от ';
$topicForUser = $dateForUser;
$topicForCode = $topicInit.'_'.$dateForCode;

//code generation
$arParamsCode = array(
   "max_len" => 255,
   "change_case" => "L",
   "replace_space" => '-',
   "replace_other" => '-',
   "delete_repeat_replace" => true
);
$code = CUtil::translit($topicForCode, "ru", $arParamsCode);

$element = new CIBlockElement;

$elementProps = [
    "NAME" => $name,
    "EMAIL" => $email,
    "MESSAGE" => $message,
];

$arElementFields = Array(
    "IBLOCK_ID" => $iblockId,
    "PROPERTY_VALUES" => $elementProps,
    "NAME" => $topicForUser,
    "PREVIEW_TEXT" => $message,
    "CODE" => $code,
    "ACTIVE" => "Y",
    "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL")
);

$newIblockItemId = $element->Add($arElementFields);

if (!$newIblockItemId) {
    die(json_encode([
        "result" => "error",
        "message" => $element->LAST_ERROR
    ]));
}

$returnMsg = "New Iblock Item successfully added (id: $newIblockItemId)";

if($GLOBALS['prod']) {
    $backend_link = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
    $backend_link .= '?IBLOCK_ID='.$iblockId.'&type=consultation&ID='.$newIblockItemId;

    $arEventFields = array(
        "NAME" => $name,
        "DATE" => $dateForUser,
        "ID" => $newIblockItemId,
        "EMAIL" => $email,
        "COMMENTS" => $message,
        "BE_LINK" => $backend_link
    );

    $resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
    $returnMsg .= "; Message send: " . $resSend;
}

die(json_encode([
    "result" => "success",
    "message" => $returnMsg
]));