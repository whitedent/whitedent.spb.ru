<link href="<?= SITE_TEMPLATE_PATH ?>/css/widgetSkidka.css" rel="stylesheet">

<div id="widgetSkidka">
	<div class="widgetSkidkaForm">
		<div class="widgetSkidkaFormTitle">Получите скидку 10% прямо сейчас!</div>
        <div class="widgetSkidkaFormBody">
            Подпишитесь на аккаунт Инстаграм<br>
            и получите скидку на лечение в клинике<br>
            (не действует на предложения по акциям).
            <a href="https://www.instagram.com/whitedent_spb/" class="widgetSkidkaSubscribe">Подписаться</a>
        </div>
	</div>
	<div id="widgetSkidkaClose"></div>
	<div id="widgetSkidkaLabel"><span>Скидка 10%</span></div>
</div>