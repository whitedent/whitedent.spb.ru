<div class="left_slider flexslider">
<ul class="slides">

        <li> <a href="/akcii/#akcii-item-3" class="services_slider_link"></a> 

		<div class="slider-akcia">
  <p class="slider-akcia__title">Компьютерная 3D томография</p>
  <p class="slider-akcia__price">
    за 2000 руб. 
  </p>
			<p class="slider-akcia__old-price">вместо <del>2300 руб.</del></p>
</div></li>
        <li> <a href="/akcii/#akcii-item-10" class="services_slider_link"></a> 

		<div class="slider-akcia">
  <p class="slider-akcia__title">Брекет системы</p>
  <p class="slider-akcia__price">
    от 17000 рублей 
  </p>
  <p><span class="small">Консультация врача ортодонта по вторникам</span><br>
	  <span class="small"><strong>всего 450 рублей</strong></span>
  </p>
</div></li>
        <li> <a href="/akcii/#akcii-item-11" class="services_slider_link"></a> 

		<div class="slider-akcia">
  <p class="slider-akcia__title">Имплант с установкой</p>
  <p class="slider-akcia__price">
    всего за 15 000 рублей 
  </p>
  <p><span class="small">только по средам</span><br>
  <p><span class="small">при дальнейшем протезировании в нашей клинике <strong>гарантии на работы 1 год</strong></span>
  </p>
</div></li>

</ul>
<? /* <div class="left_slider flexslider">
	<ul class="slides">

        <li> <a href="/akcii/#akcii-item-5" class="services_slider_link"></a> 

		<div class="slider-akcia">
  <p class="slider-akcia__title">Компьютерная 3D томография</p>
  <p class="slider-akcia__price">
    за 1600 руб. 
  </p>
			<p class="slider-akcia__old-price">вместо <del>2300 руб.</del></p>
</div></li>

<li> <a href="/akcii/#akcii-item-3" class="services_slider_link"></a>
 <div class="slider-akcia slider-akcia_happy-hours">
   <div class="slider-akcia__top">
    <div class="slider-akcia__header">
     <p class="slider-akcia__title">Счастливые часы</p>
     <p class="slider-akcia__subtitle">понедельник, среда, суббота</p>
    </div>
   </div>
   <div class="slider-akcia__middle">
    <ol class="slider-akcia__conditions">
      <li>Консультация терапевта - БЕСПЛАТНО</li>
      <li>Лечение со скидкой 15%</li>
      <li>Чистка - 2500 рублей вместо 3800 руб.</li>
    </ol>
   </div>
  <p class="slider-akcia__time">
    СЧАСТЛИВЫЕ ДНИ С 5.12.2016*<br>
    (понедельник, среда, суббота)<br>
    <span class="small">*при наличии свободного времени у врача</span>
  </p>
	</div></li> */ ?>
</div>
