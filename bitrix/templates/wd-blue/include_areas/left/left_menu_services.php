<div class="menu_services">

    <div class="menu_services-desktop s-hidden">
      <? $APPLICATION->IncludeComponent(
          "bitrix:menu",
          "menu_services_tree",
          array(
              "ROOT_MENU_TYPE" => "left",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(
              ),
              "MAX_LEVEL" => "2",
              "CHILD_MENU_TYPE" => "left_sub",
              "USE_EXT" => "N",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "N"
          ),
          false
      ); ?>
    </div>

    <div class="menu_services-mobile xl-hidden l-hidden m-hidden">
      <? $APPLICATION->IncludeComponent(
          "bitrix:menu",
          "menu_services_tree",
          array(
              "ROOT_MENU_TYPE" => "left_sub",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(
              ),
              "MAX_LEVEL" => "1",
              "CHILD_MENU_TYPE" => "",
              "USE_EXT" => "N",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "N"
          ),
          false
      ); ?>
    </div>

</div>