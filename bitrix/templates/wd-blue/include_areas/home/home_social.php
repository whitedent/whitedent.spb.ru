<div class="social_network">
 <!--noindex--> <?$APPLICATION->IncludeComponent(
	"primepix:vkontakte.group",
	"",
	Array(
		"HEIGHT_FORM" => "310",
		"ID_GROUP" => "64206144",
		"TYPE_FORM" => "2",
		"WIDTH_FORM" => "310"
	)
);?> <!--/noindex-->
</div>
<div class="left-articles">
	<div class="left-art-h">
		Полезные статьи
	</div>
	<ul class="menu-ll">
		<li><a href="/articles/stati-nashikh-spetsialistov/korotkaya-uzdechka-yazyka-i-defekty-fiktsii-vse-prosto/">
		Короткая уздечка языка и «Дефекты фикции». Все просто? </a></li>
		<li><a href="/articles/stati-nashikh-spetsialistov/restavratsiya-zubov-pokazaniya-i-protivopokazaniya/">
		Реставрация зубов: показания и противопоказания </a></li>
		<li><a href="/articles/obshchaya-informatsiya/kt-i-implanty/">
		Можно ли делать компьютерную томографию (КТ) с имплантами, штифтами, коронками? </a></li>
		<li><a href="/articles/obshchaya-informatsiya/chto-luchshe-kt-ili-mrt-nosovyh-pazuh/">
		Что лучше показывает КТ или МРТ носовых пазух? </a></li>
		<li><a href="/articles/obshchaya-informatsiya/rekomendatsii-po-povedeniyu-posle-operatsii-v-polosti-rta/">
		Рекомендации по поведению после операции в полости рта </a></li>
		<li><a href="/articles/obshchaya-informatsiya/kompyuternaya-tomografiya-pri-gaymorite/">
		Компьютерная томография при гайморите </a></li>
	</ul>
</div>