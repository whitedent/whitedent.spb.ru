<? /*<div class="home_title">Отзывы пациентов</div>
*/
?><div class="home_title">Отзывы пациентов<a style="font-size: 1rem; float: right" href="https://whitedent.spb.ru/otzivi/#feedback-form">Оставить отзыв</a></div>

<!--test-->
<? 

if(false) {
    $APPLICATION->IncludeComponent("sr.bitrix:medialibrary.view", "home_review", Array(
        "FOLDERS" => "12",  // Папка медиабиблиотеки
        //"FOLDERS" => "003",	// Папка медиабиблиотеки
        "VARIABLE" => "",	// Название переменной
        "COUNT_IMAGE" => "22",	// Количество картинок
        "RANDOM" => "N",	// Случайный порядок
        "PAGE_LINK" => "",	// Ссылка на страницу
        "PAGE_LINK_TEXT" => "",	// Текст ссылки
        "ADD_TITLE" => "",	// Добавка к заголовку
        "TITLE" => "N",	// Устанавливать заголовок
        "LOAD_JS" => "Y",	// Загружать Fancybox
        "CACHE_TYPE" => "A",	// Тип кеширования
        "CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "RESIZE_MODE" => "Y",	// Сжатие изображений на лету
        "RESIZE_MODE_W" => "145",	// По ширине
        "RESIZE_MODE_H" => "108",	// По высоте
        "WATERMARK_SETTING" => "Y",	// Включить водяные знаки
        "PAGE_NAV_MODE" => "N",	// Включить постраничную навигацию
        "ELEMENT_PAGE" => "13",	// Количество изображений на странице
        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        "PAGER_SHOW_ALWAYS" => "N",	// Всегда показывать навигацию
        "PAGER_TITLE" => "",	// Подпись постраничной навигации
        "PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
        ),
        false
    );
}

?>

<p style="clear:both"></p><?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "otzyvy",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array("ID", "CODE", "XML_ID", "NAME", "TAGS", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PICTURE", ""),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "9",
        "IBLOCK_TYPE" => "content",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "N",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "13",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array("NOT_DISPLAY_ANONS", ""),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
);?>