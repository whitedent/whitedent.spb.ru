<div itemscope itemtype="http://schema.org/ImageObject">
	<a href="/images/map/map_01.jpg" class="image-open"> <img src="/images/home/home_middle/img-2.jpg" itemprop="contentUrl" width="800" title="Удачное расположение" alt="Удачное расположение" draggable="false"  /> </a> 
	<div class="home_middle_item_content"> 
	  	<div class="home_middle_item_title" itemprop="name">
			Удачное расположение
		</div>
	 	<span itemprop="description">
			К нам удобно добираться как на метро <br />
	 		(м. Приморская) из любой точки города, <br />
	 		так и на автомобиле. <br />
	 		От метро 10-15 минут пешком.
		</span>
	</div>
</div>