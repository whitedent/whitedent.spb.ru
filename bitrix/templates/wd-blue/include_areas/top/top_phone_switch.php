<?php
if($_COOKIE['cookie_url_key'] AND $_COOKIE['cookie_url_key'] !== 'null')
{
    includePhoneSwitch($_COOKIE['cookie_url_key']);
}
elseif(strpos($_SERVER['REQUEST_URI'], 'utm_medium=cpc'))
{
    includePhoneSwitch('346-80-14', true);
}
else
{
    //includePhoneSwitch('+7 (812) 355-41-09', true);
}

function includePhoneSwitch($newPhone, $setCookie = false)
{
    if($newPhone == '+7 (812) 346-80-14') $newPhone = '346-80-14';
    $result = "var phoneSwitchBlocks = $('.phone');\n";
    $result .= "phoneSwitchBlocks.each(function(){\n";
        $result .= "$(this).html('".$newPhone."');\n";
    $result .= "});\n";

    if($setCookie){
        $result .= "\n";
        $result .= "$.cookie('cookie_url_key', '".$newPhone."', {\n";
        $result .= "expires: 14,\n";
        $result .= "path: '/'\n";
        $result .= "});\n";
    }

    $result = "jQuery(document).ready(function($){\n\n".$result."\n});\n";
    $result = "<script type='text/javascript'>\n".$result."</script>\n";
    echo  $result;
}