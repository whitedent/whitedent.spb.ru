<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

            <? if($numbPage == 'all'): ?>
                </div>

                <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4  block-wrap__item_s-width6 left-sidebar">
                  <? include($includeAreasPathRoot.'left/left_yandex_search.php'); ?>
                  <? include($includeAreasPathRoot.'left/left_menu_services.php'); ?>
                  <? $APPLICATION->IncludeFile($includeAreasPath.'left/left_slider.php'); ?>
                  <? $APPLICATION->IncludeFile($includeAreasPath.'home/home_social.php'); ?>
                </div><!-- .left-sidebar -->

              </div>
            <? endif; ?><!-- .block-wrap -->

          </div><!-- .content -->
		</div><!-- .container-->


	</div><!-- .middle-->

</div><!-- .wrapper -->

<footer class="footer">

    <div class="bottom_menu_wrapper container">
        <div class="bottom_menu pageBlockInner content">
            <? include($includeAreasPathRoot.'top/top_menu.php'); ?>
        </div> <!-- bottom_menu -->
    </div> <!-- bottom_menu_wrapper -->

    <div class="footer_top container">
      <div class="content">
        <div class="footer-top-inner">
        <div class="footer_top_left">

            <div class="footer_top_links">
                <? $APPLICATION->IncludeFile($includeAreasPath.'footer/footer_links.php'); ?>
            </div>

            <? $APPLICATION->IncludeFile($includeAreasPath.'footer/footer_copyright_license.php'); ?>

        </div> <!-- footer_top_left -->

        <div class="footer_top_right">
            <? $APPLICATION->IncludeFile($includeAreasPath.'footer/footer_phones_address.php'); ?>

            <div class="footer_top_soc">
                <? $APPLICATION->IncludeFile($includeAreasPath.'footer/footer_soc.php'); ?>
            </div>

        </div> <!-- footer_top_right -->
	  </div>
      </div>
    </div> <!-- footer_top -->
    <div class="footer_bottom pageBlockInner container">
      <div class="content">
        <div class="footer_bottom_left">
            
            <? $APPLICATION->IncludeFile($includeAreasPath.'footer/footer_promotion.php'); ?>
        </div>
        
        <div class="footer_bottom_right" style="display: none;">
            <? $APPLICATION->IncludeFile($includeAreasPath.'footer/footer_counters.php'); ?>
        </div>
      </div>
    </div> <!-- footer_bottom -->

</footer><!-- .footer -->

	<?
		 $APPLICATION->IncludeFile($includeAreasPath.'code/widgetSkidka.php');
	?>

	<script src="<?= SITE_TEMPLATE_PATH ?>/js/widgetSkidka.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/form_validation.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/ajax_form.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/template.js"></script>

</body>
</html>
<!-- Agent7276s -->