<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?><div class="mfeedback">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

<form action="<?=$APPLICATION->GetCurPage()?>" method="POST" class="post-form">
<?=bitrix_sessid_post()?>

  <input type="text" class="fieldRequired" name="fieldRequired"> <? // anti-spam field ?>

  <table>
		<tr>
			<td style="width: 180px"><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span style="color: tomato;">*</span><?endif?> <?=GetMessage("MFT_NAME")?></td>
			<td><input type="text" name="user_name" value="" pattern="^[а-яА-ЯёЁ ]+$" title="Допустимы русские буквы и пробел"></td>
		</tr>

		<tr>
			<td><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span style="color: tomato;">*</span><?endif?> Электронная почта:</td>
			<td><input type="email" name="user_email" value=""></td>
		</tr>

		<tr>
			<td><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span style="color: tomato;">*</span> <?endif?><?=GetMessage("MFT_MESSAGE")?></td>
			<td><textarea name="MESSAGE"><?=$arResult["MESSAGE"]?></textarea></td>
		</tr>

		<?if($arParams["USE_CAPTCHA"] == "Y"):?>
		<tr>
			<td><input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">&nbsp;&nbsp;&nbsp;Секретный код:</td>
			<td><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA"></td>
		</tr>
		<tr>
			<td><span style="color: tomato;">*</span> Введите секретный код:</td>
			<td><input type="text" name="captcha_word" size="30" maxlength="50" value=""></td>
		</tr>
		<?endif;?>
		<tr>
        <td></td>
        <td>
          <label>
            <input type="checkbox" id="agreement" checked="" required="">
            Нажимая на кнопку, я даю свое согласие на обработку своих персональных данных.
          </label>
        </td>
    </tr>

		<tr>
			<td></td>
			<td><input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" style="margin-left: 0;"></td>
		</tr>
	</table>
</form>
</div>