<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";
	
$strReturn = '<div class="wd_blue_breadcrumbs"><ul>';

$num_items = count($arResult);
for($index = 0, $itemSize = $num_items; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    $name = '<span itemprop="title">'.$title.'</span>';
    $link = $arResult[$index]["LINK"];
    $aLink = '<a href="'.$link.'" title="'.$title.'" itemprop="url">'.$name.'</a>';
	$microDataPosition = '<meta property="position" content="'.($index + 1).'">';


	if($link <> "" && $index != $itemSize - 1) {
    $strReturn .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		 $strReturn .= $aLink;
		$strReturn .= $microDataPosition.'</li>';}
	else{
    $strReturn .= '<li>';
		$strReturn .= $title;

		$strReturn .= $microDataPosition.'</li>';}
}

$strReturn .= '</ul></div>';

return $strReturn;
?>