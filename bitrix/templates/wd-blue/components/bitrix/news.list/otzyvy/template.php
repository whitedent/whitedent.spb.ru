<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<div class="slider_content_wrapper flexslider">
    <?php 
        //print_r($arResult['ITEMS']);
            $images = array();
            foreach ($arResult['ITEMS'] as $key => $item) {
                if($item['PROPERTIES']['NOT_DISPLAY_ANONS']['VALUE_XML_ID'] == "") {
                    $img = array();
                    $img["URL"] = $item['PREVIEW_PICTURE']['SRC'];
                    $img["RAND_ID"] = "link_" . rand(00000, 99999);

                    $imgT = CFile::ResizeImageGet( $item['PREVIEW_PICTURE']['ID'],Array("width"=>200, "height"=>100),BX_RESIZE_IMAGE_PROPORTIONAL_ALT,false,false,false,65 );

                    $img["URL_THUMB"] = array();
                    $img["URL_THUMB"]["src"] = $imgT['src'];
                    $img['DESCRIPTION'] = $item['PREVIEW_PICTURE']['DESCRIPTION'];
                    $images[] = $img;
                }
            }

    ?>

   <ul class="slider_content slides">
        <? foreach($images as $img) { ?>
            <li class="slider_item">
                    <div class="slider_item_img" itemscope itemtype="http://schema.org/ImageObject">
						<? /* <a href="<?=$img["URL"]?>" rel="image_group" class="image-open" id="<?=$img["RAND_ID"]?>">
                            <img src="<?=$img["URL_THUMB"]["src"] ?>" id="img_<?=$img["RAND_ID"]?>" title="<?=$img["DESCRIPTION"];?>" alt="<?=$img["DESCRIPTION"];?>"/>
                        </a> */ ?>
                        <a href="<?=$img["URL"]?>" data-fancybox="gallery" class="image-open" id="<?=$img["RAND_ID"]?>" itemprop="contentUrl">
                            <img src="<?=$img["URL_THUMB"]["src"] ?>" id="img_<?=$img["RAND_ID"]?>" title="<?=$img["DESCRIPTION"];?>" alt="<?=$img["DESCRIPTION"];?>" itemprop="thumbnail"/>
                        </a>
                    </div>
                </li>
        <? } ?>
    </ul> 

</div>
