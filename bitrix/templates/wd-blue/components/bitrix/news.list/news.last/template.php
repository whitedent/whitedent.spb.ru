<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><div class="news news-table" style="margin-bottom: 0px;">
<?$i = 1;?>
<?foreach($arResult["ITEMS"] as $arItem):
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	
    /*if($i == 1)
        echo '<tr>';*/
    ?>
			<div class="news-table__item">

				<div class="news-item-img"><?if($arItem["PREVIEW_PICTURE"]):?><div><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"/></div><?endif;?></div>
        <div class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                <p><?=$arItem["PREVIEW_TEXT"];?></p>                
            </div>
        </div>
      </div>
    <?
    /*if($i == 2)
    {
        echo '</tr>';
        $i = 1;
    }
    else
        $i++;*/
    ?>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>