<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?><div class="slider_content_wrapper flexslider">
    <ul class="slider_content slides">
        <? foreach($arResult["ITEMS"] as $arItem):
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            $arItemThumb = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width" => 98, "height" => 116));
        ?>
            <li class="slider_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>"  itemscope itemtype="http://schema.org/ImageObject">

                <div class="slider_item_img">
                    <img
                        src="<?=$arItemThumb["src"]?>"
                        alt="<?=$arItem["PREVIEW_PICTURE"]["NAME"]?>"
                        itemprop="contentUrl"
                    />
                </div>

                <div class="slider_item_desc" itemprop="caption">
                    <div class="specialist_item_desc_fio" itemprop="name"><?= $arItem["NAME"]; ?></div>
                    <div class="specialist_item_desc_spec" itemprop="description">
                        <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                            <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                                <?
                                    if(count($arProperty["DISPLAY_VALUE"]) > 2)
                                    {
                                        $arProperty["DISPLAY_VALUE"] = array_slice($arProperty["DISPLAY_VALUE"], 0, 2);
                                    }
                                ?>
                                <?=strip_tags(implode('<br>', $arProperty["DISPLAY_VALUE"]), '<br>'); ?>
                            <?else:?>
                                <?=strip_tags($arProperty["DISPLAY_VALUE"]); ?>
                            <?endif?>
                        <?endforeach;?>
                    </div>
                    <div class="specialist_item_desc_readmore"><a href="/doctors/#doctor-<?=$arItem['ID']?>">Подробнее</a></div>
                </div>
            </li>
        <? endforeach; ?>
    </ul> <!-- slider_content  -->
</div> <!-- slider_content_wrapper -->
