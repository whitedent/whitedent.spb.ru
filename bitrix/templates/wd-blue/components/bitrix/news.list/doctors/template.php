<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><div class="doctors">
    <?foreach($arResult["ITEMS"] as $arItem):
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    	?>
        <div class="doctor-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" itemscope itemtype="http://schema.org/ImageObject">
            <h2 itemprop="name" id="doctor-<?=$arItem['ID']?>">
                <?=$arItem["NAME"]?>
            </h2>
                <span itemprop="description" class="doctor-subHeader">
                    <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
						<?if($pid != "D_CERT"):?>
							<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
								<?=strip_tags(implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]));?>
							<?else:?>
								<?=strip_tags($arProperty["DISPLAY_VALUE"]);?>
							<?endif?>
						<?endif?>
            		<?endforeach;?>
                </span>
            <div class="doctor-item__inner">
            <div class="doctor-item__img"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["DESCRIPTION"]?>" itemprop="contentUrl" /></div>
			<div class="doctor-item__description"> 
            <?=$arItem["PREVIEW_TEXT"];?>
			<?if(count($arItem["DISPLAY_PROPERTIES"]["D_CERT"]["FILE_VALUE"])):?>
				<p>Сертификаты:</p>
				<p>
				<?foreach($arItem["DISPLAY_PROPERTIES"]["D_CERT"]["FILE_VALUE"] as $i=>$c):?>
					<a href="<?=$c["SRC"]?>" class="link-dashed image-open">Сертификат №<?=($i+1)?></a>
					<?if(isset($arItem["DISPLAY_PROPERTIES"]["D_CERT"]["FILE_VALUE"][$i+1]["SRC"])):?><br/><?endif;?>
				<?endforeach;?>
				</p>
			<?endif;?>
            <div class="clear"></div>
        </div>
        </div>
        </div>

    <?endforeach;?>
</div>
<div class="clear"></div>