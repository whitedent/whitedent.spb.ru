<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
$arSpecialty = array();
$arSize = array("width" => 270, "height" => 170);

foreach($arResult["ITEMS"] as $arItem):
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    
    $arDoctor = $arDoctorSpec = $specialty = array();
    if($arItem["DISPLAY_PROPERTIES"]["P_DOCTOR"]["VALUE"] > 0)
    {
        $arDoctor = CIBlockElement::GetByID($arItem["DISPLAY_PROPERTIES"]["P_DOCTOR"]["VALUE"])->GetNext();
        if($arDoctor["PREVIEW_PICTURE"] > 0)
            $arDoctor["PREVIEW_PICTURE"] = CFile::GetByID($arDoctor["PREVIEW_PICTURE"])->Fetch();
        $r = CIBlockElement::GetProperty($arDoctor["IBLOCK_ID"], $arDoctor["ID"], array(), array("CODE" => "D_SPECIALTY"));
        while($p = $r->Fetch())
        {
            if($p["VALUE"] != "" && count($arSpecialty) == 0)
            {
                $arSelect = Array("ID", "NAME");
                $arFilter = Array("IBLOCK_ID" => $p["LINK_IBLOCK_ID"], "ACTIVE" => "Y");
                $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1000), $arSelect);
                while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    $arSpecialty[$arFields["ID"]] = $arFields["NAME"];
                }
            }
            $specialty[] = $arSpecialty[$p["VALUE"]];
        }
    }


    $arItemThumbBefore = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], $arSize);
    $arItemThumbAfter  = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], $arSize);

	?>    
    <div class="table-photo" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div>
            <div class="table-photo__title">
                <h2 class="red"><?=$arItem["NAME"]?></h2>
            </div>
            <div class="doctor-info">
                <?if(is_array($arDoctor["PREVIEW_PICTURE"])):?>
                    <div class="doctor-img-small"><a href="<?=$arDoctor["DETAIL_PAGE_URL"]?>"><img src="/upload/<?=$arDoctor["PREVIEW_PICTURE"]["SUBDIR"]."/".$arDoctor["PREVIEW_PICTURE"]["FILE_NAME"]?>" alt=""/></a></div>
                <?endif;?>
                <div style="float: right;">
                    <?if($arItem["DISPLAY_PROPERTIES"]["P_DOCTOR"]["DISPLAY_VALUE"]):?>
                        <span class="doctor-name"><?=$arItem["DISPLAY_PROPERTIES"]["P_DOCTOR"]["DISPLAY_VALUE"]?></span><br />
                    <?endif;?>
                    <?if(count($specialty)):?>
                        <span class="doctor-specialty"><?=implode(", ", $specialty)?></span>
                    <?endif;?>
                </div>
            </div>
        </div>

        <div class="table-photo__row">

            <div class="table-photo__before">
            	<div class="img-before" itemscope itemtype="http://schema.org/ImageObject">
            		<a href="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="image-open" itemprop="contentUrl">
            			<img src="<?= $arItemThumbBefore["src"] ?>" alt="" itemprop="thumbnail"/>
						</a>
					</div>
				</div>

            <div class="table-photo__arrow"><img src="<?=SITE_TEMPLATE_PATH?>/images/rarr_sm.png" width="32" height="32" alt=""/></div>

            <div class="table-photo__after">
            	<div class="img-before" itemscope itemtype="http://schema.org/ImageObject">
            		<a href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" class="image-open" itemprop="contentUrl">
            			<img src="<?= $arItemThumbAfter["src"] ?>" alt="" itemprop="thumbnail"/>
						</a>
					</div>
				</div>

        </div>
    </div>
    
<?endforeach;?>
