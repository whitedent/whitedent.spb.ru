<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><div>
<!--<div class="questions_ico" style="padding-right: 5%;padding-bottom: 20px;">-->
    <div style="padding-right: 5%;padding-bottom: 20px;">
    <a href="/ask/"><img src="<?=SITE_TEMPLATE_PATH?>/images/ico_quest.jpg" width="65" height="65" style="margin-right:7px;float:left;"/></a><a href="/ask/" style="line-height: 67px;" class="red">Задать&nbsp;вопрос</a>
    <!--
    <table class="tab_banner">
        <tbody>
            <tr>
                <td class="art1_1"><img src="<?=SITE_TEMPLATE_PATH?>/images/art_tl.jpg" width="10" height="10" border="0"></td>
                <td class="art2"></td>
                <td class="art1_1"><img src="<?=SITE_TEMPLATE_PATH?>/images/art_tr.jpg" width="10" height="10" border="0"></td>
            </tr>
            <tr>
                <td class="art3"></td>
                <td class="art6"><a href="/ask/"><img src="<?=SITE_TEMPLATE_PATH?>/images/ico_quest.jpg" width="65" height="65" style="margin-right:7px;float:left;"/></a><a href="/ask/" style="line-height: 67px;" class="red">Задать&nbsp;вопрос</a><br/></td>
                <td class="art4"></td>
            </tr>
            <tr>
                <td><img src="<?=SITE_TEMPLATE_PATH?>/images/art_bl.jpg" width="10" height="10" border="0"></td>
                <td class="art5"></td>
                <td><img src="<?=SITE_TEMPLATE_PATH?>/images/art_br.jpg" width="10" height="10" border="0"></td>
            </tr>
        </tbody>
    </table> -->
</div>
    <?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
    <?foreach($arResult["ITEMS"] as $arItem):
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        
        $arDoctor = $arDoctorSpec = $specialty = array();
        if($arItem["DISPLAY_PROPERTIES"]["F_DOCTOR"]["VALUE"] > 0)
        {
            $arDoctor = CIBlockElement::GetByID($arItem["DISPLAY_PROPERTIES"]["F_DOCTOR"]["VALUE"])->GetNext();
            if($arDoctor["PREVIEW_PICTURE"] > 0)
                $arDoctor["PREVIEW_PICTURE"] = CFile::GetByID($arDoctor["PREVIEW_PICTURE"])->Fetch();
            $r = CIBlockElement::GetProperty($arDoctor["IBLOCK_ID"], $arDoctor["ID"], array(), array("CODE" => "D_SPECIALTY"));
            while($p = $r->Fetch())
            {
                if($p["VALUE"] != "" && count($arSpecialty) == 0)
                {
                    $arSelect = Array("ID", "NAME");
                    $arFilter = Array("IBLOCK_ID" => $p["LINK_IBLOCK_ID"], "ACTIVE" => "Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 1000), $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $arSpecialty[$arFields["ID"]] = $arFields["NAME"];
                    }
                }
                $specialty[] = $arSpecialty[$p["VALUE"]];
            }
        }
	?>
    
    <div class="quest_answer" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="quest"><!--<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">--><?=$arItem["PREVIEW_TEXT"];?><!--</a>--></div>   
        <div class="answer">
            <div>
                <div class="small_doc2">
                    <?if(is_array($arDoctor["PREVIEW_PICTURE"])):?>
                        <div class="doctor-img-small"><a href="<?=$arDoctor["DETAIL_PAGE_URL"]?>"><img src="/upload/<?=$arDoctor["PREVIEW_PICTURE"]["SUBDIR"]."/".$arDoctor["PREVIEW_PICTURE"]["FILE_NAME"]?>" alt=""/></a></div>
                    <?endif;?>
                </div>
                <div class="small_doc1">
                    <?if($arItem["DISPLAY_PROPERTIES"]["F_DOCTOR"]["DISPLAY_VALUE"]):?>
                        <?=$arItem["DISPLAY_PROPERTIES"]["F_DOCTOR"]["DISPLAY_VALUE"]?>
                    <?endif;?>
    				<p class="small_doc1" style="padding-right: 0 !important;"><?if(count($specialty)):?><?=implode(", ", $specialty)?><?endif;?></p>
                </div>
            </div>
            <div class="clear"></div>
            <div style="vertical-align: top;">
                <?=$arItem["DETAIL_TEXT"]?> 
            </div>
        <!--<table class="tab_answer">
            <tbody>
                <tr>
				    <td class="td_tab_text">
				        <table class="tab_text">	
								<tbody><tr>
									<td class="text_td1"><img src="<?=SITE_TEMPLATE_PATH?>/images/answ_tl.jpg" width="30" height="30" alt=""/></td>
									<td class="text_td2">&nbsp;</td>
									<td class="text_td3">&nbsp;</td>
								</tr>
								<tr>
									<td class="text_td4">&nbsp;</td>
									<td class="text_td5" style="vertical-align:top;">
                                        <?=$arItem["DETAIL_TEXT"]?> 
									</td>
									<td valign="top" style="background:url('<?=SITE_TEMPLATE_PATH?>/images/kostyl32.jpg') 0% 0% repeat-y"><img src="<?=SITE_TEMPLATE_PATH?>/images/kostyl.jpg" width="30" height="130" alt=""/></td>
								</tr>
								<tr>
									<td><img src="<?=SITE_TEMPLATE_PATH?>/images/answ_bl.jpg" width="30" height="30" alt=""/></td>
									<td class="text_td7">&nbsp;</td>
									<td><img src="<?=SITE_TEMPLATE_PATH?>/images/answ_br.jpg" width="30" height="30" alt=""/></td>
								</tr>
							</tbody></table>
						</td>
						<td class="td_tab_doctor">
							<table class="tab_doctor">
								<tbody><tr>
									<td class="doc_td1">&nbsp;</td>
									<td class="doc_td2">&nbsp;</td>
									<td><img src="<?=SITE_TEMPLATE_PATH?>/images/answ_tr.jpg" width="30" height="30" alt=""/></td>
								</tr>
								<tr>
									<td class="doc_td3">&nbsp;</td>
									<td class="doc_td4" valign="top">							<table class="tab_small_doc">
								<tbody><tr>
									<td class="small_doc1">
										<?if($arItem["DISPLAY_PROPERTIES"]["F_DOCTOR"]["DISPLAY_VALUE"]):?>
                                            <?=$arItem["DISPLAY_PROPERTIES"]["F_DOCTOR"]["DISPLAY_VALUE"]?>
                                        <?endif;?>
										<p class="small_doc1" style="padding-right: 0 !important;"><?if(count($specialty)):?><?=implode(", ", $specialty)?><?endif;?></p>
									</td>
									<td class="small_doc2">
                                        <?if(is_array($arDoctor["PREVIEW_PICTURE"])):?>
                                            <div class="doctor-img-small"><a href="<?=$arDoctor["DETAIL_PAGE_URL"]?>"><img src="/upload/<?=$arDoctor["PREVIEW_PICTURE"]["SUBDIR"]."/".$arDoctor["PREVIEW_PICTURE"]["FILE_NAME"]?>" alt=""/></a></div>
                                        <?endif;?>
                                    </td>
								</tr>
							</tbody></table>									</td>
									<td class="doc_td5">&nbsp;</td>
								</tr>
								<tr>
									<td class="doc_td4">&nbsp;</td>
									<td class="doc_td6">&nbsp;</td>
									<td><img src="<?=SITE_TEMPLATE_PATH?>/images/answ_br.jpg" width="30" height="30" alt=""/></td>
								</tr>
								<tr>
									<td><img src="<?=SITE_TEMPLATE_PATH?>/images/answ_mt.jpg" width="18" height="30" alt=""/></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>-->
			</div>
		</div>
<?endforeach;?>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
</div>