<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;
?>
<div class="doctors">
  <? foreach($arResult["ITEMS"] as $arItem):
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

    if(!empty($arItem["PROPERTIES"]["PUBLISHED"]["VALUE"])){
      $propPublished = $arItem["PROPERTIES"]["PUBLISHED"]["VALUE"];
    }else{
      $propPublished = "Y"; // для старых Отзывов (без свойства о публикации)
    }

    // Не выводим неопубликованные Отзывы для Гостей
    if($propPublished != "Y" && !$USER->IsAdmin()){
      continue;
    }
    ?>
    <div
      class="
        doctor-item
        <?= ($USER->IsAdmin() && $propPublished != "Y") ? "unpublished " : "" ?>
      "
      id="<?= $this->GetEditAreaId($arItem['ID']) ?>"
    >

      <h2>
        Отзыв от <?=$arItem["NAME"]?>
      </h2>

      <? if(!empty($arItem["PREVIEW_PICTURE"])): ?>
        <div class="doctor-img">
          <a href="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="image-open">
              <img
                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                width="250"
                alt="<?=$arItem['PREVIEW_PICTURE']['DESCRIPTION']?>"
              />
          </a>
        </div>
      <? endif ?>

      <?= $arItem["PREVIEW_TEXT"] ?>

      <? if($arItem["PROPERTIES"]["NAME"]["VALUE"]): ?>
          <p><?= $arItem["PROPERTIES"]["NAME"]["VALUE"] ?></p>
      <? endif ?>

      <div class="clear"></div>

    </div>
  <? endforeach ?>
</div>

<div class="clear"></div>