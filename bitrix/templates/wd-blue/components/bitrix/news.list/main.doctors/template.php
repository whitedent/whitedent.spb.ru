<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><div style="margin-bottom: 10px;">
    <?foreach($arResult["ITEMS"] as $arItem):
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    	?>
        <div style="width: 198px;float:left;margin:0 10px 10px 0;" id="<?=$this->GetEditAreaId($arItem['ID']);?>" itemscope itemtype="http://schema.org/ImageObject">
            <a href="/doctors/#doctor-<?=$arItem['ID']?>">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" width="200" itemprop="contentUrl" />
            </a>
            <div style="height: 50px;" itemprop="caption">
                <a href="/doctors/#doctor-<?=$arItem['ID']?>" itemprop="name"><?=$arItem["NAME"]?></a><br />
                <span style="color: #93968A;font-size: 85%;" itemprop="description">
                    <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>    
                        <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
                            <?=strip_tags(implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]));?>
                        <?else:?>
                            <?=strip_tags($arProperty["DISPLAY_VALUE"]);?>
                        <?endif?>
                    <?endforeach;?>
                </span>
            </div>
        </div>
    <?endforeach;?>
    <div class="clear"></div>
</div>
<p><a href="/doctors/">Все доктора</a></p>