<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <?if(is_array($arResult["DETAIL_PICTURE"])):?>
        <div itemscope itemtype="http://schema.org/ImageObject"><img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" class="detail" itemprop="contentUrl"/></div>
   	<?endif?>
   	<?=$arResult["DETAIL_TEXT"];?>
