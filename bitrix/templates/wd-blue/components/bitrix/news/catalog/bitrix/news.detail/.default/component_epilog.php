<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(strpos($APPLICATION->GetCurUri(), 'PAGEN')){
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: ".$APPLICATION->GetCurPage());
    exit();
}

$detailText  = $arResult["DETAIL_TEXT"];

$firstPBegin = strpos($detailText, '<p>');
$firstPEnd   = strpos($detailText, '.');
$firstP      = substr($detailText, $firstPBegin, ($firstPEnd - $firstPBegin));
$firstP      = strip_tags($firstP);

$APPLICATION->SetPageProperty("description", $firstP);

$APPLICATION->AddHeadString('<meta property="og:type" content="article" />');


if($arResult["DETAIL_PICTURE"])
{
    $ogImage = 'http://whitedent.spb.ru'.$arResult["DETAIL_PICTURE"]["SRC"];
    $APPLICATION->AddHeadString('<meta property="og:image" content="'.$ogImage.'" />');
}
else
{
    $firstImg = '';
    $output = preg_match_all('/< *img[^>]*src *= *["\']?([^"\']*)/i', $detailText, $matches);
    $firstImg = $matches[1][0];

    if($firstImg){
        $ogImage = 'http://whitedent.spb.ru'.$firstImg;
        $APPLICATION->AddHeadString('<meta property="og:image" content="'.$ogImage.'" />');
    }
}


