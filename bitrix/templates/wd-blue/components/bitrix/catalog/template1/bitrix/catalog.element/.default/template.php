<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="question_item" itemscope itemtype="http://schema.org/Question">
	<p class="topic"><span>Тема: </span><?=$arResult["NAME"]?></p>
	<p class="question" itemprop="name"><span>Вопрос: </span><?=$arResult["PREVIEW_TEXT"]?></p>
	<p class="date"><span>Дата: </span><?=$arResult["DATE_CREATE"]?></p> <p class="name"><?=$arResult["PROPERTIES"]["NAME"]["VALUE"] ?></p>
	<p class="answer" itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer">
        <span>Ответ: </span>
        <span style="font-weight: normal" itemprop="text"><?=$arResult["DETAIL_TEXT"]?></span>
    </p>
	<?
		if (!empty($arResult["DISPLAY_PROPERTIES"]["RESPONDENT"]["VALUE"]))
		{
			$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PREVIEW_PICTURE");
			$arFilter = Array("IBLOCK_ID"=>"5", "ID"=>$arResult["DISPLAY_PROPERTIES"]["RESPONDENT"]["VALUE"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement())
				$arFieldsResp = $ob->GetFields();
	?> 
	<img src="<?=CFile::GetPath($arFieldsResp["PREVIEW_PICTURE"])?>" alt="">
	<?
		}
	?>
	<p class="answerer"><span>Отвечает:</br></span><?=$arResult["DISPLAY_PROPERTIES"]["RESPONDENT"]["DISPLAY_VALUE"]?></p>
	<p class="link_questions"><span>Похожие вопросы:</span></p>
	<ul>
	
	<?
		$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","NAME","DETAIL_PAGE_URL");
		$arFilter = Array("IBLOCK_ID"=>"12","ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","SECTION_ID"=>$arResult["IBLOCK_SECTION_ID"], "!ID"=>$arResult["ID"], "PROPERTY_STATUS"=>4);
		$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter, false,  Array("nTopCount" => 5), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFieldsLink = $ob->GetFields();
	?>
	<li><a href="<?=$arFieldsLink["DETAIL_PAGE_URL"]?>"><?=$arFieldsLink["NAME"]?></a></li>
	<?
		}
	?> 	
		
	</ul>
	
	<?
		if(!empty($arResult["DISPLAY_PROPERTIES"]["SERVICE"]["VALUE"]))
		{
			$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PREVIEW_TEXT");
			$arFilter = Array("IBLOCK_ID"=>"13","ID"=>$arResult["DISPLAY_PROPERTIES"]["SERVICE"]["VALUE"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFieldsServ = $ob->GetFields();
			}
	?> 
		<p class="link_item"><span>Услуга: </span></p>
	<ul>
		<li>
			<a href="<?=$arFieldsServ["~PREVIEW_TEXT"]?>"><?=$arFieldsServ["NAME"]?></a>
		</li>
	</ul>
	<?
		}
	?>
</div>
