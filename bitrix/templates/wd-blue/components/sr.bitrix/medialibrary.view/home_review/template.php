<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<!--test-->
<div class="slider_content_wrapper flexslider">
    <? if(count($arResult["FOLDER_IMAGE"]) > 0): ?>
    <ul class="slider_content slides">
        <? foreach($arResult["FOLDER_IMAGE"] as $img): ?>
        <li class="slider_item"> <!--test-->
            <div class="slider_item_img" itemscope itemtype="http://schema.org/ImageObject">
                <a href="<?=$img["URL"]?>" data-fancybox="home-review-gallery" class="image-open" id="<?=$img["RAND_ID"]?>" itemprop="contentUrl">
                    <img src="<?=$img["URL_THUMB"]["src"] ?>"<?=$img["SIZE"]?> id="img_<?=$img["RAND_ID"]?>" title="<?=$arResult["DESCRIPTION"][$img["ID"]];?>" alt="<?=$arResult["DESCRIPTION"][$img["ID"]];?>" itemprop="thumbnail" />
                </a>
            </div>
        </li>
        <? endforeach; ?>
    </ul> <!-- slider_content -->
    <?endif;?>
</div> <!-- slider_content_wrapper -->
