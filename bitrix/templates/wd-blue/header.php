<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?><!DOCTYPE HTML>
<html lang="ru" >
<head>
    <meta name="viewport" content="width=device-width">

    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <title><? $APPLICATION->ShowTitle() ?></title>

  <?
  $baseUri = ($_SERVER["HTTPS"]) ? "https://" : "http://";
  $baseUri .= $_SERVER["SERVER_NAME"];
  $curUri = $baseUri.$APPLICATION->GetCurPage();
  ?>

  <? $APPLICATION->AddHeadString('<meta property="og:site_name" content="whitedent.spb.ru" />'); ?>
  <? $APPLICATION->AddHeadString('<meta property="og:type" content="article" />'); ?>
  <? $APPLICATION->AddHeadString('<meta property="og:image" content="'.$baseUri.SITE_TEMPLATE_PATH.'/images/logo.png" />'); ?>
  <? $APPLICATION->AddHeadString('<meta property="og:url" content="'.$curUri.'" />'); ?>

	<link href="<?= SITE_TEMPLATE_PATH ?>/css/reset.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH ?>/css/jquery.fancybox.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH ?>/css/flexslider.css" rel="stylesheet">
	<link href="<?= SITE_TEMPLATE_PATH ?>/css/from_old_template.css" rel="stylesheet">
    
    <!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-1.10.2.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.cookie.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.fancybox.pack.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.flexslider-min.js"></script>
    <? $APPLICATION->ShowHead(false); ?>

    <?
        $numbPage = ($APPLICATION->GetCurPage() == '/' OR defined("ERROR_404")) ? 'first' : 'all';
        if(strpos($_SERVER['PHP_SELF'], 'home') !== false){$numbPage = 'first';}
        $numbPageStr = ' class="'.$numbPage.'"';

        $includeAreasPath = SITE_TEMPLATE_PATH.'/include_areas/';
        $includeAreasPathRoot = $_SERVER['DOCUMENT_ROOT'].$includeAreasPath;

        if(!defined("ERROR_404")){
            $APPLICATION->IncludeFile($includeAreasPath.'top/top_phone_switch.php');
            include($_SERVER["DOCUMENT_ROOT"].'/metatags/addheadstrings.php');
            if(!$ogTitle){ ?>
                <meta property="og:title" content="<? $APPLICATION->ShowTitle(false); ?>" /><?
            }
        }

        include($_SERVER["DOCUMENT_ROOT"].'/zeus_redirect.php');

    ?>

    <? $APPLICATION->ShowPanel(); ?>

</head>
<body<?=$numbPageStr?>>

    <? include($includeAreasPathRoot.'top/top_google_tag_manager.php'); ?>

    <div class="wrapper">

        <header class="header">

            <div class="header_top_wrapper container">
              <div class="content">
                <div class="header_top">

                        <div class="logo s-hidden">
                            <a title="На главную" href="/"><span>ВАЙТ ДЕНТ</span>Клиника эстетической стоматологии</a>
                        </div>
                        <div class="logo logo-mobile xl-hidden l-hidden m-hidden">
                            <a title="На главную" href="/"></a>
                        </div>
                        <div class="top_features m-hidden s-hidden">
                            <? $APPLICATION->IncludeFile($includeAreasPath.'top/top_features.php'); ?>
                        </div>

                        <div class="top_contacts m-hidden s-hidden">
                            <? $APPLICATION->IncludeFile($includeAreasPath.'top/top_contacts_phone.php'); ?>
                        </div>
                        <div class="top_contacts_mobile xl-hidden l-hidden">
                            <? $APPLICATION->IncludeFile($includeAreasPath.'top/top_contacts_phone_mobile.php'); ?>
                        </div>

                </div> <!-- header_top -->
			  </div>
            </div> <!-- header_top_wrapper -->

            <div class="header_menu_wrapper container">
              <div class="header_menu pageBlockInner content">

                <nav class="top-nav s-hidden">
                  <? include($includeAreasPathRoot.'top/top_menu.php'); ?>
                </nav>

                <nav class="top-nav-mobile xl-hidden l-hidden m-hidden">

                  <label for="top-nav-mobile-switcher" class="top-nav-mobile-label">
                    <span>Меню</span>
                  </label>

                  <input id="top-nav-mobile-switcher" hidden type="checkbox">

                  <div class="top-nav-mobile-container">
                    <ul class="top-nav-mobile-services">
                      <li><span class="top-nav-mobile-services-header">Услуги</span>
                        <? include($includeAreasPathRoot.'top/top_menu_services_mobile.php'); ?>
                      </li>
                    </ul>
                    <? include($includeAreasPathRoot.'top/top_menu.php'); ?>
                  </div>

                </nav>

                <? $APPLICATION->IncludeFile($includeAreasPath.'top/top_visitus.php'); ?>
                
              </div> <!-- header_menu -->
            </div> <!-- header_menu_wrapper -->
            <? if($numbPage == 'all'): ?>
                <div class="header_bottom_wrapper container">
                    <div class="header_bottom content">
                        <? $APPLICATION->IncludeFile($includeAreasPath.'top/top_bottom.php'); ?>
                    </div> <!-- header_menu -->
                </div> <!-- header_bottom_wrapper -->
            <? endif; ?>
            <? /* <div style="background-color: #e6eeee; padding: 12px; text-align: center; font-size: 1.25rem">
              <div style="margin: 0 auto; max-width: 980px; background-color: #fcfcfc; padding: 12px;">
                <p>
                  <strong>Режим работы в новогодние праздники:</strong>
                  <br>
                  31 декабря, 1, 2, 3, 4, 5, 7 января клиника не работает.
                </p>
              </div>
            </div> */ ?>
        </header><!-- .header-->

        <div class="middle">

            <div class="container">
                <div class="content">
                    <? if($numbPage == 'all'): ?>
						<div class="block-wrap block-wrap_wrap">
						<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8  block-wrap__item_s-width6 ">
                        <? include($includeAreasPathRoot.'inner/inner_breadcrumbs.php'); ?>
                        <h1><?$APPLICATION->ShowTitle(false)?></h1>
                    <? endif; ?>
