isIE = (navigator.appName == "Microsoft Internet Explorer") ? true : false;
// текущий видимый элемент
g_current_visible = null;
// ширина клиентской области ( null, т.к. документ не загрузился)
g_all_width = null;
// высота клиентской области
g_all_height = null;
// полная высота документа
g_all_scroll = null;

function $( id) {
	return document.getElementById( id);
}

function getElementsArray( element, tag, wantClass) {
	if ( element.getElementsByTagName) {elementArray = element.getElementsByTagName( tag);
		tempArray = new Array();
		if ( elementArray.length > 0) {
			for ( var i in elementArray) {
				wantClassCondition = true;
				if ( wantClass) {
					wantClassCondition = (elementArray[i].className == wantClass);
				}
				if ( elementArray[i] && elementArray[i].parentNode && elementArray[i].parentNode == element) {
					isNodeFlag = 0;
					for ( var j in tempArray) {
						if ( tempArray[j] == elementArray[i]) {
							isNodeFlag = 1;
							break;
						}
					}
					if ( !isNodeFlag && wantClassCondition) {
						tempArray.push( elementArray[i]);
					}
				}
			}
		}
		return tempArray;
	} else {
		alert( "don't supported!!");
	}
}

function copyElementContent( fromElem, toElem) {
	tempArray = new Array();
    
	childArray = fromElem.childNodes;
	for ( var i in childArray) {
		if ( childArray[i].nodeType && childArray[i].nodeType == 1) {
			tempArray[i] = childArray[i];
		}
	}
	for ( var i in tempArray) {
		toElem.appendChild( tempArray[i]);
	}
}

function deleteChildren( node) {
	while ( node.firstChild) {
		node.removeChild(node.firstChild);
	}
}


function showElement( element, soft) {
    soft = soft || false;
    if ( element.style.display == "none" && !soft || element.style.visibility == "hidden" && soft) {
        if ( !soft) {
            element.style.display = "block";
        }
        element.style.visibility = "visible";
    } else {
        if ( !soft) {
            element.style.display = "none";
        }
        element.style.visibility = "hidden";
    }
}

function hideElementInCursor() {
	if ( g_current_visible) {
		g_current_visible.style.display = "none";
	}
}

function closeElementInCursor() {
	var timeOut = window.setTimeout( "hideElementInCursor()", 350);
}

function showElementInCursor( div, event) {
	hideElementInCursor();
	
 	if ( div && div.style.display == "none") {
		g_current_visible = div;
		div.style.display = "block";
	}
	var wwidth = (window.innerWidth)?window.innerWidth: ((document.all)?document.body.offsetWidth:null);
	if (!event) event=window.event;
	x = (event.clientX) ?  event.clientX : ((event.pageX) ? event.pageX : event.screenX);
	y = (event.clientY) ? (event.clientY + 15) : (event.pageY + 15);
	if ( document.documentElement.scrollTop == 0) {
		y += document.body.scrollTop;
	} else {
		y += document.documentElement.scrollTop;
	}
	
	if ( x + div.offsetWidth + 20 >= wwidth) {
		x -= div.offsetWidth;
	}
	div.style.top = y+"px";
	div.style.left = x+"px";
}

function centreElement( element) {
    if ( !g_all_width) {
        g_all_width = (document.body.scrollWidth) ? document.body.scrollWidth : document.documentElement.scrollWidth;
    }
    if ( !g_all_height) {
        g_all_height = ( document.documentElement.offsetHeight < document.body.offsetHeight) ? document.documentElement.offsetHeight: document.body.offsetHeight;
    }
    if ( !g_all_scroll) {
       g_all_scroll = (document.body.scrollHeight) ? document.body.scrollHeight : document.documentElement.scrollHeight;
    }
    
    element.style.left = Math.ceil( g_all_width / 2 - element.offsetWidth / 2);
    element.style.top = Math.ceil( g_all_height / 2 - element.offsetHeight / 2);
}