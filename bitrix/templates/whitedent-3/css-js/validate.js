form = "";
// массив регэкспов для конкретных полей
regExpObj = new Object;
regExpObj["form_email"] = /^[^а-яА-Я@\/.,:;]+@[^а-яА-Я@\/.,:;]+\.[^а-яА-Я0-9@\/,:;]{2,7}$/;
regExpObj["form_phone"] = /^[-+0-9]+$/;

// массив соответствий
validObj = new Object;
validObj["form_confirm"] = "form_passwd";

// массив исключений
exceptionObj = new Object;
//exceptionObj['form_login'] = "1";				// z.b.

function getRadioValue( id) {
	for ( var i = 0; i < id.length; i ++) {
		if ( id[i].checked) {
		    return id[i].value;
		}
	}
	return false;
}
// функция вывода сообщения
function getMessage( text, element) {
    var error = document.getElementById( "error");
	error.innerHTML = text;
	error.style["color"] = "#FF2020";
    document.getElementById( element).style["color"] = "#FF2020";
	if ( form.elements[element].type) {
		form.elements[element].style["background-color"] = "#FFF4F4";
        form.elements[element].style["border"] = "2px solid #FF7575";
        form.elements[element].onfocus = function() {
            document.getElementById( element).style["color"] = "";
			form.elements[element].style["border"] = "";
    		form.elements[element].style["background-color"] = "";
    		form.elements[element].select();
    		error.innerHTML = "";
		}
	} else {
        for ( var i = 0; i < form.elements[element].length; i ++) {
			form.elements[element][i].onclick = function() {
                document.getElementById( element).style["color"] = "";
                error.innerHTML = "";
		    }
		}
	}

}
// функция проверки
function validate( formName) {
	// определяем форму
	form = document.forms[formName];
	if ( typeof( form) == 'undefined') {
		form = document.getElementById( formName);
	}
	if ( typeof( form) == 'undefined') {
		getMessage( "Form is undefined.");
		return false;
	}
	// проверяем поля на заполненность и на соответствие шаблонам
	for ( i = 0; i < form.elements.length; i ++) {
		switch ( form.elements[i].type) {
			case "text":
			    if ( exceptionObj[form.elements[i].name] == undefined) {
				    if ( form.elements[i].value == "") {
						getMessage( "Error! Please, fill in the blanks.", form.elements[i].name);
						return false;
					}
					if ( regExpObj[form.elements[i].name]) {
						if ( !form.elements[i].value.match(regExpObj[form.elements[i].name])) {
							getMessage( "Error! Invalid information.", form.elements[i].name);
							return false;
						}
					}
					if ( validObj[form.elements[i].name]) {
						if ( form.elements[i].value != form.elements[validObj[form.elements[i].name]].value) {
							getMessage( "Error! Invalid information.", form.elements[i].name);
							return false;
						}
					}
				}
			    break;
			case "password":
			    if ( exceptionObj[form.elements[i].name] == undefined) {
				    if ( form.elements[i].value == "") {
						getMessage( "Error! Please, fill in the blanks.", form.elements[i].name);
						return false;
					}
					if ( regExpObj[form.elements[i].name]) {
						if ( !form.elements[i].value.match(regExpObj[form.elements[i].name])) {
							getMessage( "Error! Invalid information.", form.elements[i].name);
							return false;
						}
					}
					if ( validObj[form.elements[i].name]) {
						if ( form.elements[i].value != form.elements[validObj[form.elements[i].name]].value) {
							getMessage( "Error! Invalid information.", form.elements[i].name);
							return false;
						}
					}
				}
			    break;
			case "radio":
				radioObj = form.elements[form.elements[i].name];
                if ( !getRadioValue( radioObj)) {
					getMessage( "Error! Please, fill in the blanks.", form.elements[i].name);
					return false;
				}
			    break;
		}
	}
}
