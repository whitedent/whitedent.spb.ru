//////////////////////////////////////////////////////////////////////////////////////
//																					//
//							Ajax													//
//							by A.St.											//
//							september 09									//
//																					//
//////////////////////////////////////////////////////////////////////////////////////

Ajax = function( onLoadFunc, onChangeStatusFunc, onErrorFunc) {
	this.READY_STATE_UNINITIALIZED = 0;
	this.READY_STATE_LOADING = 1 ;
	this.READY_STATE_L0ADED = 2 ;
	this.READY_STATE_INTERACTIVE = 3;
	this.READY_STATE_COMPLETE = 4;

	this.name = "Ajax";
	this.request = null;																					// определяем объект xmlHttpRequest
	this.url = null;																							// адрес
	this.httpMethod = "GET";																		// дефолтный метод передачи
	this.params = null;																					// параметры

	this.onErrorFunc = this.defaultError;													// дефолтная функция вывода ошибки
	this.onLoadFunc = this.defaultConsole;													// дефолтная функция вывода результата
	this.onChangeStatusFunc = null;															// дефолтная функция вывода результата

	// если заданы обработчики
	if ( onChangeStatusFunc) {
		this.onChangeStatusFunc = onChangeStatusFunc;
	}
	if ( onLoadFunc) {
		this.onLoadFunc = onLoadFunc;
	}
	if ( onErrorFunc) {
		this.onErrorFunc = onErrorFunc;
	}
}

Ajax.prototype = {
	// отправка запроса
	sendRequest: function( url, httpMethod, params) {
		// определение дефолтных параметров
		if ( url) {
			this.url = url;																					// адрес
		} else {
			return false;
		}
		if ( httpMethod) {
			this.httpMethod = httpMethod;														// дефолтный метод передачи
		}
		if ( params) {
			this.params = params;																	// параметры
		}

		// определение объекта XMLHTTP-запроса и его отправка
		this.request = this.initXMLHTTPRequest() ;
		if ( this.request) {
			var loader = this;
			this.request.onreadystatechange = function () {
				loader.onReadyState.call( loader);
			}
			this.request.open( this.httpMethod, this.url, true);
			this.request.setRequestHeader( "Content-Type", "application/x-www-form-urlencoded");
			this.request.send( params);
		}
	},

	// инициализация объекта запроса
	initXMLHTTPRequest: function() {
		var xRequest=null;
		// Инициализация объекта запроса
		if ( window.XMLHttpRequest){
			xRequest = new XMLHttpRequest();
		} else if ( window.ActiveXObject){
			xRequest = new ActiveXObject( "Microsoft.XMLHTTP");
		}
		return xRequest;
	},

	// обработчик изменения состояния
	onReadyState: function() {
		this.state = this.request.readyState;
		if ( this.onChangeStatusFunc) {
			this.onChangeStatusFunc.call( this);
		}
 		this.data = null;
		// Проверка readyState
		if ( this.state == this.READY_STATE_COMPLETE) {
		// Чтение данных ответа
			this.data = this.request.responseText;
			this.onLoadFunc.call( this, this.data);
		}
	},

	// дефолтный обработчик результата
	defaultConsole: function() {
		alert( this.data);
	},
	
	// дефолтный обработчик ошибки
	defaultError: function() {
		alert( "Error!");
	}

}
