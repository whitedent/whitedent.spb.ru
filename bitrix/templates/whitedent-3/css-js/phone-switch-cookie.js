// Подмена номера телефона при переходе с поисковиков
jQuery(document).ready(function($)
{
	if($.cookie('cookie_url_key')){         	//если кука есть - меняем телефон на ее значение
    	switch_phone($.cookie('cookie_url_key'));
	}

	function switch_phone(new_phone){
		var search_doms = ['phone'];
		$.each(search_doms, function(index, value){
			var dom = 'span.'+value;
			if($(dom).length){
				$(dom).html($(dom).html().replace(/\+7 \(812\) 355-41-09, \+7 \(812\) 346-80-14/ig, new_phone));     //замена номера
			}
		});
	}
});

