var __eventHandlers = new Object();

/*
S.A.: добавил коммент, ибо при добавлении метрики выдает Event registration not supported
function addEventListener( instance, eventName, listener) {
    
    // инициализируем объект - элемент массива обработчиков
    if ( !__eventHandlers[instance]) {
        __eventHandlers[instance] = new Object();
    }
    
    // устанавливаем событие
	if ( instance.addEventListener) {
        __eventHandlers[instance][eventName] = listener;
        instance.addEventListener( eventName, __eventHandlers[instance][eventName], false);
	} else if ( instance.attachEvent) {
		__eventHandlers[instance][eventName] = function() {
		    instance.currentTarget = instance;
			listener.call( this, instance);
		}
		instance.attachEvent("on" + eventName, __eventHandlers[instance][eventName]);
	} else {
		alert( "Event registration not supported");
	}
    
    // возвращаем объект
	return {
		instance: instance,
		name: eventName,
		listener: __eventHandlers[instance][eventName]
	}
}
*/

function removeEventListener( instance, eventName) {
	if ( instance.removeEventListener) {
		instance.removeEventListener( eventName, __eventHandlers[instance][eventName], false);
	} else if ( instance.detachEvent) {
	   // эта фигня не работает
	//	instance.detachEvent( "on" + eventName, __eventHandlers[instance][eventName]);
	} else {
		alert( "Event removal not supported");
	}
    delete __eventHandlers[instance][eventName];
}