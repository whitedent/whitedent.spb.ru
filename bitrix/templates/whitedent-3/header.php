<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <title><?$APPLICATION->ShowTitle()?></title>
    <script src="<?=SITE_TEMPLATE_PATH?>/css-js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/css-js/jquery.cookie.js" type="text/javascript"></script>
    <?php
	    if($_COOKIE['cookie_url_key']){ ?>
	        <script src="<?=SITE_TEMPLATE_PATH?>/css-js/phone-switch-cookie.js" type="text/javascript"></script><?php
	    }elseif(strpos($_SERVER['REQUEST_URI'], 'utm_medium=cpc')){ ?>
	        <script src="<?=SITE_TEMPLATE_PATH?>/css-js/phone-switch-context.js" type="text/javascript"></script><?php
	    }else{ ?>
	        <script src="<?=SITE_TEMPLATE_PATH?>/css-js/phone-switch-other.js" type="text/javascript"></script><?php
	    }
    ?>
    <?$APPLICATION->ShowHead(false);?>
    <?include($_SERVER["DOCUMENT_ROOT"].'/zeus_redirect.php');?>
</head>
<body>
<div id="main">
	<div id="top">
	    <div style="height: 210px;">
	        <div class="top_ie" itemscope itemtype="http://schema.org/Organization">
	            <div class="logo"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt=""/></a></div>
	            <div class="company_desc" itemprop="name">Клиника эстетической стоматологии</div>
	            <div class="phone" itemprop="telephone"><?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.header.phone.php"), array(), array("MODE" => "html"));?>
                	<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<span style="color:#018dcb;text-align:center;font-size:14px!important;" itemprop="streetAddress">м. Приморская \ Васильевский остров</span>
					</div>
		    	</div>
	       	</div>
	    </div>
	    <div class="menu">
	        <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.menu.top.php"), array(), array("MODE" => "html"));?>
	   </div>
	</div>
    <div class="sub-menu-top">
        <div class="block-l">
            <ul class="m">
                <li><a href="/ceny" class="price"></a></li>
                
				
                <?/*<li><a href="/photos/">Фото до и после</a></li>*/?>
            </ul>
        </div>
		<div class="block-c"><a id="zapis" href="/note.php" class="btn-note showmap fancybox.iframe"></a></div>
        <div class="block-r">
            <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.header.links.php"), array(), array("MODE" => "html"));?>
        </div>
    </div>
	
	<div class="banner-top2">
	<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.banner.top.php"), array(), array("MODE" => "html"));?>
	</div>


 
<div id ="left-sidebar" class="left-sidebar">
<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.banner.left.php"), array(), array("MODE" => "html"));?>
</div>

<div id ="right-sidebar2" class="right-sidebar2">
<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.banner.left9.php"), array(), array("MODE" => "html"));?>
</div>
	  