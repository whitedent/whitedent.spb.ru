<div class="page">
	<div class="page_ie6">
        <div class="page_all">
            <div class="col-left">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "services-left",
                    Array(
                        "ROOT_MENU_TYPE" => "left",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => ""
                    )
                );?>
                
                <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.banner.left2.php"), array(), array("MODE" => "html"));?>
            </div>
            <div class="col-right">
                <div style="width: 75%;float: left;">
                    <h1 class="red"><?$APPLICATION->ShowTitle(false)?></h1>
                    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "navchain", Array(
                       	    "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                           	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                           	"SITE_ID" => "-",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                       	),
                       	false
                    );?>