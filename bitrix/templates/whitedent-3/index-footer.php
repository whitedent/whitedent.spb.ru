</div>
	<div class="middle_right2">
		<div class="new_index" style="margin-top: 20px;">
            <h2>Отзывы наших клиентов</h2>
            <?$APPLICATION->IncludeComponent("sr.bitrix:medialibrary.view", "main.review", Array(
            	"FOLDERS" => "5",	// Папка медиабиблиотеки
            	"VARIABLE" => "",	// Название переменной
            	"COUNT_IMAGE" => "10",	// Количество картинок
            	"RANDOM" => "N",	// Случайный порядок
            	"PAGE_LINK" => "",	// Ссылка на страницу
            	"PAGE_LINK_TEXT" => "",	// Текст ссылки
            	"ADD_TITLE" => "",	// Добавка к заголовку
            	"TITLE" => "N",	// Устанавливать заголовок
            	"LOAD_JS" => "Y",	// Загружать Fancybox
            	"CACHE_TYPE" => "A",	// Тип кеширования
            	"CACHE_TIME" => "3600",	// Время кеширования (сек.)
            	"RESIZE_MODE" => "Y",	// Сжатие изображений на лету
            	"RESIZE_MODE_W" => "145",	// По ширине
            	"RESIZE_MODE_H" => "108",	// По высоте
            	"WATERMARK_SETTING" => "Y",	// Включить водяные знаки
            	"PAGE_NAV_MODE" => "N",	// Включить постраничную навигацию
            	"ELEMENT_PAGE" => "",	// Количество изображений на странице
            	"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            	"PAGER_SHOW_ALWAYS" => "N",	// Всегда показывать навигацию
            	"PAGER_TITLE" => "",	// Подпись постраничной навигации
            	"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
            	),
            	false
            );?>
        </div>
		<div class="new_index" style="margin-top:-4px;">
			<h2>Новости</h2>
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "news.last", array(
	"IBLOCK_TYPE" => "content",
	"IBLOCK_ID" => "1",
	"NEWS_COUNT" => "2",
	"SORT_BY1" => "ACTIVE_FROM",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_TITLE" => "N",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"PAGER_TEMPLATE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

			<h2>Наши лицензии</h2>
			<div>
				<div class="license"><a href="/images/licenses/sert01.jpg" class="image-open"><img src="/images/licenses/sert01.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/licenses/sert02.jpg" class="image-open"><img src="/images/licenses/sert02.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/licenses/sert03.jpg" class="image-open"><img src="/images/licenses/sert03.jpg" alt=""/></a></div>
				<div style="clear:both"></div>
			</div>
			<h2 style="margin-top: 20px;">Благодарности</h2>
			<div>
				<div class="license"><a href="/images/gratitude/001.jpg" class="image-open"><img src="/images/gratitude/001.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/gratitude/002.jpg" class="image-open"><img src="/images/gratitude/002.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/gratitude/003.jpg" class="image-open"><img src="/images/gratitude/003.jpg" alt=""/></a></div>
				<div style="clear:both"></div>
			</div>
		</div>
        <div class="new_index" style="margin-top: 20px;">
            <h2>Наши специалисты</h2>
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "main.doctors", Array(
	"IBLOCK_TYPE" => "content",	// Тип информационного блока (используется только для проверки)
	"IBLOCK_ID" => "5",	// Код информационного блока
	"NEWS_COUNT" => "1000",	// Количество новостей на странице
	"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
	"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
	"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
	"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
	"FILTER_NAME" => "",	// Фильтр
	"FIELD_CODE" => array(	// Поля
		0 => "",
		1 => "",
	),
	"PROPERTY_CODE" => array(	// Свойства
		0 => "",
		1 => "D_SPECIALTY",
		2 => "",
	),
	"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
	"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
	"CACHE_GROUPS" => "Y",	// Учитывать права доступа
	"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
	"SET_TITLE" => "N",	// Устанавливать заголовок страницы
	"SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
	"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
	"PARENT_SECTION" => "",	// ID раздела
	"PARENT_SECTION_CODE" => "",	// Код раздела
	"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
	"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
	"PAGER_TITLE" => "",	// Название категорий
	"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
	"PAGER_TEMPLATE" => "",	// Название шаблона
	"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	),
	false
);?>
        </div>
<div class="new_index" style="margin-top: 20px;">
<h2>Приглашаем к сотрудничеству</h2>
<p>Одна из лучших стоматологических клиник Санкт-Петербурга приглашает на работу талантливых и опытных стоматологов всех профилей, которые умеют и любят работать! У нас отличные условия, хороший коллектив и мы умеем договариваться. Клиника ВайтДент удобно расположена на Васильевском острове.</p>
	<p><b>Актуальные вакансии</b><br/>
Требуется врач-стоматолог-терапевт с клиентской базой. Заработная плата по договоренности.<br/>
Требуется ассистент стоматолога. Заработная плата по договоренности. Рабочий день с 11 до 21 часа.<br/>
Подробности по телефону <span class="phone">+7 (812) 355-41-09, +7 (812) 346-80-14</span>.</p>
</div>
	</div>
	</div>