$(document).ready(function()
{
    /* добавляем значение в скрытый input c классам secret */





    $.maskfn.definitions['~']='[+-]';
    $('body').delegate('input[type="tel"], input.phone', 'focus', function(){
        $(this).maskfn('+7 (999) 999-99-99');
    });

    $("form.new-review").on("submitJS", function(event){
        ajaxFormSubmit({
            event: event,
            handler: "ajax",
            model: "NewReviewModel",
            redirect: "/otzyvy/sent/",
        });
    });
        $("form.contacts").on("submitJS", function(event){
            ajaxFormSubmit({
                event: event,
                handler: "ajax",
                model: "ContactsModel",
                errorPopupTitle: "Ошибка",
                errorPopupText: "Сообщение не отправлено",
                popupTitle: "Ок",
                popupText: "Спасибо, ваше сообщение принято",
            });
        });




    $("form.consultation_new_question").on("submitJS", function(event){
        ajaxFormSubmit({
            event: event,
            handler: "ajax",
            model: "ConsultationNewQuestionModel",
            errorPopupTitle: "Ошибка",
            errorPopupText: "Сообщение не отправлено",
            redirect: '/patsientam/cons/sent/',
        });
    });

    $("form.consultation-new-registration").on("submitJS", function(event){
        ajaxFormSubmit({
            event: event,
            handler: "ajax",
            model: "ConsultationNewRegistrationModel",
            errorPopupTitle: "Ошибка",
            errorPopupText: "Сообщение не отправлено",
            successAction: '$(thisForm).parent().html("<h3>Спасибо, что доверяете нам здоровье зубов</h3><p>Мы вам перезвоним в ближайшее рабочее время,<br> пн.-пт. с 11 до 21, сб. с 11 до 18</p>");'
        });
    });

    $("form.consultation-new-registration-popup").on("submitJS", function(event){
        ajaxFormSubmit({
            event: event,
            handler: "ajax",
            model: "ConsultationNewRegistrationModel",
            errorPopupTitle: "Ошибка",
            errorPopupText: "Сообщение не отправлено",
            popupTitle: "Спасибо, что доверяете нам здоровье зубов",
            popupText: "Мы вам перезвоним в ближайшее рабочее время, пн.-пт. с 11 до 21, сб. с 11 до 18",
        });
    });

    $("form.callback").on("submitJS", function(event){
        ajaxFormSubmit({
            event: event,
            handler: "ajax",
            model: "CallbackModel",
            errorPopupTitle: "Ошибка",
            errorPopupText: "Сообщение не отправлено",
            successAction: '$(thisForm).parent().html("<h3>Спасибо за ваше обращение</h3><p>Наш администратор перезвонит вам в течении часа</p>");'
        });
    });

});



