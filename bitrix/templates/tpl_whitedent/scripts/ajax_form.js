function ajaxFormSubmit(params)
{
    var event = params['event'];
    var thisForm = event.target;
    var url;
    var dataHandler = params['handler'];
    var successRedirect = params['redirect'];
    var successAction = params['successAction'];
    var popupTitle = params['popupTitle'];
    var popupText = params['popupText'];
    var errorPopupTitle = params['errorPopupTitle'];
    var errorPopupText = params['errorPopupText'];
    var btnCloseText = params['btnCloseText'];
    var formReset = params['formReset'];
    var model = params['model'];
    var message = params['message'];

    var data = new FormData(thisForm);

    if (dataHandler != undefined) {
        url = '/bitrix/templates/tpl_whitedent/ajax/' + dataHandler + '.php';
        if(model){
            url += "?model=" + model;
        }
    }else{
        url = window.location.href;
    }

    jQuery.ajax({
        url: url,
        data: data,
        type: 'POST',
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data){
            if (data.result == "success") {

                if(successRedirect){
                    document.location.href = successRedirect;
                }

                if(formReset){
                    thisForm.reset();
                }

                if(successAction){
                    eval(successAction);
                }

                if(message){
                    var modalBody = $(".form-body", thisForm);
                    if(modalBody.length){
                        $(".form-title", thisForm).html(message.title);
                        modalBody.html(message.text);
                    }
                }

            }else{
                popupTitle = errorPopupTitle;
                if(data.message != undefined){
                    popupText = data.message;
                }else{
                    popupText = errorPopupText;
                }

                var modalBody = $(".form-body", thisForm);
                if(modalBody.length){
                    $(".form-title", thisForm).html("Ошибка");
                    modalBody.html(data.message);
                }

                console.log("Error: " + data.message);
            }

            if(popupText) {
                var objButtonClose = false;
                if(btnCloseText != undefined)
                {
                    objButtonClose = new BX.PopupWindowButton({
                        text: btnCloseText,
                        className: '',
                        events: {
                            click : function(){this.popupWindow.close()}
                        }
                    })
                }

                var oPopup = new BX.PopupWindow('call-offer-form-wrapper', window.body, {
                    autoHide : true,
                    offsetTop : 1,
                    offsetLeft : 0,
                    lightShadow : true,
                    closeIcon : true,
                    closeByEsc : true,
                    overlay: {
                        backgroundColor: 'black', opacity: '80'
                    },
                    buttons: [objButtonClose],
                    titleBar: popupTitle
                });

                oPopup.setContent(popupText);
                oPopup.show();

                thisForm.reset();
            }


        },
        error: function(data, textStatus, errorThrown){
            console.log("Error");
            console.log(data);
        },
        complete: function(data, textStatus){}

    });

}






