jQuery(function($)
{
    var domPriceTitle = $("#price_title");
    var domPricesRows = $(".table-price .table-price__row");
    var highlightedClass = "table-price__row--highlighted";

    domPriceTitle.autocomplete({
        source: getSourcePrices,
        minLength: 1,
        select: onSelectEvent,
        search: function(){makeRowsNotHighlighted(domPricesRows)}
    });

    function getSourcePrices(request, response)
    {
        var prices = $(".table-price__name", domPricesRows);
        var result = [];

        prices.each(function(index, el){
            var priceTitle = $(el).text();
            if(stringContainWord(priceTitle, request.term)){
                result.push({
                    "value": priceTitle,
                    "index": index
                });
            }
        });

        response(result);
    }

    function stringContainWord(haystack, needle)
    {
        haystack = haystack.replace('(', '');
        haystack = haystack.replace(')', '');
        var haystackWords = haystack.split(' ');

        var result = false;

        haystackWords.forEach(function (word) {
            if(stringStartsWith(word, needle)){
                result = true;
                return true;
            }
        });

        return result;
    }

    function stringStartsWith(haystack, needle)
    {
        haystack = haystack.toLowerCase();
        needle = needle.toLowerCase();
        return haystack.lastIndexOf(needle, 0) === 0;
    }

    function onSelectEvent(event, ui)
    {
        var scrollToRow = $(".table-price .table-price__row:eq(" + ui.item.index  + ")");
        var scrollToCellTitle = $(".table-price__name", scrollToRow);

        $([document.documentElement, document.body]).animate(
            {
                scrollTop: scrollToCellTitle.offset().top - 300
            },
            1000,
            function(){
                makeRowHighlighted(scrollToRow);
            }
        );
    }

    function makeRowHighlighted(row)
    {
        row.addClass(highlightedClass);
    }

    function makeRowsNotHighlighted(rows)
    {
        rows.removeClass(highlightedClass);
    }


});
