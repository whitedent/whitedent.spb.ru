$(document).ready(function(){

    var likeButtons = $(".like-button__button");

    likeButtons.on("click", function(e){
        var button = $(e.target).parents(".like-button__button");
        var id = button.data("id");

        if(isLiked(id)){
            alert("Вы уже поставили оценку");
            return false;
        }

        incrementLikes(id)
            .done(function(response){
                if(response.result == "success"){
                    var newLikesCount = response.message;
                    $(".like-button__counter", button).html(newLikesCount);
                    saveLikedIdToCookie(id);
                    addInactiveClass(button);
                    console.log("Поставлен лайк!");
                }else{
                    console.log(response.message);
                }
            })
            .fail(function(response) {
                console.log(response);
            })
        ;

    });

    likeButtons.each(function (index, el){
        var id = $(el).data("id");
        if(isLiked(id)){
            addInactiveClass($(el));
        }
    });


});

function incrementLikes(id)
{
    return $.post(
        "/bitrix/templates/tpl_whitedent/ajax/ajax.php?model=NewLikeModel",
        {"id": id},
        function (response) {
            if(response.result == "error"){
                console.log("Ошибка: " + response.message);
            }
        },
        "json"
    );
}

function addInactiveClass(node)
{
    node.addClass("like-button__button--inactive");
}

function getLikedIdFromCookie()
{
    var likedId = $.cookie("liked_id");
    return likedId ? $.parseJSON(likedId) : {};
}

function isLiked(id)
{
    var likedId = getLikedIdFromCookie();
    if(!likedId){
        return false;
    }

    return (id in likedId);
}

function saveLikedIdToCookie(id)
{
    if(isLiked(id)){
        return false;
    }

    var likedId = getLikedIdFromCookie();

    likedId[id] = true;

    $.cookie("liked_id", JSON.stringify(likedId), {expires: 365, path: "/"});
}
