<? namespace Prices;

class Price
{
	public $id;
	public $title;
	public $value;
	public $oldValue;
	public $note;

	private $fields;
	private $props;

	public function __construct($bxElement)
	{
		$this->fields = $bxElement->GetFields();
		$this->props = $bxElement->GetProperties();

		$this->id = $this->fields["ID"];
		$this->title = $this->getTitle();
		$this->value = $this->getMainPrice();
		$this->oldValue = $this->getValue($this->props["OLD_PRICE"]["VALUE"]);
		$this->note = $this->props["NOTE"]["VALUE"];
	}

	private function getTitle()
	{
		$title = $this->fields["NAME"];

		if(!$this->props["SERVICE"]["VALUE"]){
			return $title;
		}

		$service = getIbElementFields($this->props["SERVICE"]["VALUE"]);

		$title = '<a href="'.$service["DETAIL_PAGE_URL"].'">'.$title.'</a>';

		return $title;
	}

	private function getMainPrice()
	{
		if(!$this->props["PRICES_RANGE"]["VALUE"]){
			return $this->getValue($this->props["PRICE"]["VALUE"]);
		}

		$pricesRangeId = $this->props["PRICES_RANGE"]["VALUE"];
		$pricesRange = $this->getPricesRange($pricesRangeId);

		if(count($pricesRange) == 1 || $this->props["SHOW_ONLY_MIN_PRICES_RANGE"]["VALUE"] == "Y"){
			$price = "от ".$this->getValue(reset($pricesRange));
		}else{
			$price = $this->getValue(reset($pricesRange))."&nbsp;–&nbsp;".$this->getValue(end($pricesRange));
		}

		return $price;
	}

	private function getValue($value)
	{
		if(!$value){
			return "";
		}

		return is_numeric($value) ? self::format($value) : $value;
	}

	private static function format($number): string
	{
		return number_format($number, 0, '.', ' ')." ₽";
	}

	public function isShow(): bool
	{
		return $this->props["HIDE_ON_PRICE_PAGE"]["VALUE"] != "Y";
	}

	private function getPricesRange($pricesRangeId)
	{
		$pricesRange = [];

		$res = \CIBlockElement::GetList(
			["SORT" => "ASC"],
			[
				"IBLOCK_ID" => ID_IBLOCK_PRICES,
				"ID" => $pricesRangeId,
			]
		);

		while($bxElement = $res->GetNextElement()){
			$props = $bxElement->GetProperties();
			$pricesRange[] = (int)$props["PRICE"]["VALUE"];
		}

		asort($pricesRange);

		return $pricesRange;
	}

}
