<? /** @var Prices\Price $price */ ?>
<div class="table-price__row <?= $price->oldValue ? "table-price__row--discount" : "" ?>">

	<div class="table-price__name"><?= $price->title ?></div>

	<? if($price->oldValue): ?>
		<div class="table-price__value table-price__value--old"><?= $price->oldValue ?></div>
	<? endif ?>

	<div class="table-price__value"><?= $price->value ?></div>

</div>