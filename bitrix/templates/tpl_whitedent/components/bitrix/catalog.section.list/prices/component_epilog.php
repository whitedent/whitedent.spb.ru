<?
if(!empty($arResult["SECTION"]["ID"])){

	global $APPLICATION;
	$APPLICATION->SetTitle("Цены: ".$arResult["SECTION"]["NAME"]);

	if(!empty($arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_TITLE"])){
		$APPLICATION->SetPageProperty("title", $arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_TITLE"]);
	}

	if(!empty($arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"])){
		$APPLICATION->SetPageProperty("description", $arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]);
	}

}