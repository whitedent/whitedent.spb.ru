<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? foreach($arResult["SECTIONS"] as $k => $arSection): ?>
    <? $curIterationDepthLevel = (int)$arSection["DEPTH_LEVEL"] ?>

    <? if($curIterationDepthLevel == 1): ?>

        <? if(!empty($arResult["SECTIONS"][$k - 1])): ?>
                    </div> <!-- table-price__body -->
                </div> <!-- block-marked table-price -->
            </div> <!-- page-subsection -->
        <? endif ?>

        <div class="page-subsection">
            <div class="block-marked table-price table-price--padding-right">
                <div class="table-price__header"><?= $arSection["NAME"] ?></div>
                    <div class="table-price__body">
    <? endif ?>

    <? if($curIterationDepthLevel == 2): ?>
        <div class="table-price__row table-price__row--header">
            <div class="table-price__name"><?= $arSection["NAME"] ?></div>
        </div>
    <? endif ?>

    <? if(!empty($arSection["PRICES"])): ?>
        <? foreach ($arSection["PRICES"] as $price): ?>
            <? include __DIR__."/price_item.php" ?>
        <? endforeach ?>
    <? endif ?>

<? endforeach ?>

        </div> <!-- table-price__body -->
    </div> <!-- block-marked table-price -->
</div> <!-- page-subsection -->

<? if($arResult["SECTION"]["DESCRIPTION"]): ?>
    <section class="page-section container">
        <div class="content">
            <?= $arResult["SECTION"]["DESCRIPTION"] ?>
        </div>
    </section>
<? endif ?>
