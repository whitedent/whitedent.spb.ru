<? /** @var array $arResult */

require __DIR__."/Price.php";
require __DIR__."/PricesHelper.php";

$pricesHelper = new Prices\Helper;

$priceCountLimit = 2;
$isPricesHomePage = true;

// Если это страница Раздела цен -> добавляем текущий раздел в список разделов (первым)
if(!empty($arResult["SECTION"]["ID"])){
	array_unshift($arResult["SECTIONS"], $arResult["SECTION"]);
	$priceCountLimit = 0;
	$isPricesHomePage = false;
}

// Наполняем разделы ценами
foreach ($arResult["SECTIONS"] as $k => $section){
	$arResult["SECTIONS"][$k]["PRICES"] = $pricesHelper->getPrices((int)$section["ID"], $priceCountLimit);
}

// Вывести только по паре цен для каждого раздела, не считая подразделы (на Главной странице цен)
if($isPricesHomePage){
	$rootSections = [];
	foreach ($arResult["SECTIONS"] as $k => $section){
		if($section["DEPTH_LEVEL"] == 1) {
			$rootSections[] = $section;
		}else{
			// Если у раздела второго уровня есть цены -> сливаем эти цены родительскому разделу (первого уровня)
			if(!empty($section["PRICES"])){
				$lastRootSectionKey = array_key_last($rootSections);
				$lastRootSection = $rootSections[$lastRootSectionKey];
				$lastRootSection["PRICES"] = array_merge($lastRootSection["PRICES"], $section["PRICES"]);
				$lastRootSection["PRICES"] = array_slice($lastRootSection["PRICES"], 0, $priceCountLimit);
				$rootSections[$lastRootSectionKey] = $lastRootSection;
			}
		}
	}
	$arResult["SECTIONS"] = $rootSections;
}