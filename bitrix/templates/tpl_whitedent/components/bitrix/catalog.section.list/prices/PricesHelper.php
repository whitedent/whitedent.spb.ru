<?
namespace Prices;

class Helper
{
	public function getPrices(int $sectionId, int $priceCountLimit): array
	{
		$prices = [];

		$arNavStartParams = !empty($priceCountLimit) ? ["nTopCount" => $priceCountLimit] : false;

		$res = \CIBlockElement::GetList(
			["SORT" => "ASC"],
			[
				"IBLOCK_ID" => ID_IBLOCK_PRICES,
				"SECTION_ID" => $sectionId,
				"ACTIVE" => "Y",
				"!PROPERTY_HIDE_ON_PRICE_PAGE" => "Y"
			],
			false,
			$arNavStartParams
		);

		while($bxElement = $res->GetNextElement()){
			$prices[] = new Price($bxElement);
		}

		return $prices;
	}

}