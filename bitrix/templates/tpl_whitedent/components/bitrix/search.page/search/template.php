<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<section class="page-section">
	<div class="container">
		<div class="content">

			<h1>Результаты поиска</h1>

            <? include __DIR__."/include/form.php" ?>

            <? include __DIR__."/include/original_query.php" ?>

            <? if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
            <? elseif($arResult["ERROR_CODE"] != 0): ?>
                <? include __DIR__."/include/error.php" ?>
            <? elseif(count($arResult["SEARCH"]) > 0): ?>
                <? include __DIR__."/include/search_list.php" ?>
                <?= $arResult["NAV_STRING"]?>
                <? include __DIR__."/include/sorting.php" ?>
            <? else: ?>
                <? include __DIR__."/include/nothing_to_found.php" ?>
            <? endif ?>
		</div>
	</div>
</section>