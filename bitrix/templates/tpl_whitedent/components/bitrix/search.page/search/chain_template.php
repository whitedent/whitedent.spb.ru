<?
$arChainBody = [];
foreach($arCHAIN as $k => $item)
{
	if(strlen($item["LINK"]) < strlen(SITE_DIR)){
		continue;
	}

	$title = htmlspecialcharsex($item["TITLE"]);

	if($item["LINK"] <> ""){
		$arChainBody[] = '<div class="search-result__path-parent"><a href="'.$item["LINK"].'">'.$title.'</a></div>';
	}
	else{
		$arChainBody[] = '<div class="search-result__path-current">'.$title.'</div>';
	}
}

return implode('<div class="search-result__path-separator"></div>'."\n", $arChainBody);