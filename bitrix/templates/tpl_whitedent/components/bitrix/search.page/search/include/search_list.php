<div class="page-subsection">
    <div class="grid">
        <div class="grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12">
			<? foreach($arResult["SEARCH"] as $arItem): ?>
				<? include __DIR__."/search_item.php" ?>
			<? endforeach ?>
        </div>
    </div>
</div>