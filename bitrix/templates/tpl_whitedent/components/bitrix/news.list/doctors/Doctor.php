<?
namespace DoctorsList;

class Doctor
{
	public $name;
	public $nameFormatted;
	public $image;
	public $speciality;
	public $experience;
	public $text;
	public $url;

	public function __construct($arItem)
	{
		$this->name = $arItem["NAME"];
		$this->nameFormatted = $this->getNameFormatted($arItem["NAME"]);
		$this->image = $this->getImage($arItem["PREVIEW_PICTURE"]);
		$this->speciality = $this->getSpeciality($arItem["PROPERTIES"]["SPECIALITY"]["VALUE"]);
		$this->experience = $arItem["PROPERTIES"]["EXPERIENCE"]["VALUE"];
		$this->text = $arItem["~PREVIEW_TEXT"];
		$this->url = $arItem["DETAIL_PAGE_URL"];
	}

	private function getNameFormatted($name)
	{
		$parts = explode(" ", $name);
		$parts[0] = "<strong>".$parts[0]."</strong>";
		return implode(" ", $parts);
	}

	private function getImage($picture)
	{
		return !empty($picture["SRC"]) ? $picture["SRC"] : null;
	}

	private function getSpeciality($propValue)
	{
		return mb_ucfirst(mb_strtolower(implode(" / ", $propValue), "utf8"), "utf8");
	}

}