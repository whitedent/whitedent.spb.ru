<? /** @var DoctorsList\Doctor $doctor */ ?>
<div class="doctor-item__info">

	<div class="doctor-item__photo hidden-hd hidden-xl hidden-l hidden-m hidden-s">
		<? include __DIR__."/item-photo.php" ?>
	</div>

	<div class="doctor-item__name"><?= $doctor->nameFormatted ?></div>

	<div class="doctor-item__block">

		<div class="doctor-item__occupation"><?= $doctor->speciality ?></div>

		<div class="properties doctor-item__properties">

			<? if($doctor->experience): ?>
                <div class="properties__item properties__item--experience">
                    <div class="properties__name">Стаж работы <?= $doctor->experience ?></div>
                </div>
			<? endif ?>

			<? if($doctor->text): ?>
                <div class="properties__item properties__item--additional">
                    <div class="properties__name"><?= $doctor->text ?></div>
                </div>
			<? endif ?>

		</div>

	</div>

	<a class="doctor-item__button" href="<?= $doctor->url ?>">Подробнее</a>

</div>
