<? /** @var $doctor DoctorsList\Doctor */  ?>
<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
    <div class="doctor-item">
        <div class="grid grid--padding-y">

            <div class="grid__cell grid__cell--s-6 grid__cell--xs-hidden">
                <div class="doctor-item__photo">
                    <? include __DIR__."/item-photo.php" ?>
                </div>
            </div>

            <div class="grid__cell grid__cell--s-6 grid__cell--xs-12">
				<? include __DIR__."/item-info.php" ?>
            </div>

        </div>
    </div>
</div>
