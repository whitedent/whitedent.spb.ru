<? /** @var array $arResult */ ?>
<? if(!$arResult["SECTION"]): ?>
    <div class="page-subsection">
    <?
    $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => "/vrachi/description.html",
        )
    );
    ?>
    </div>
<? else: ?>
    <?
    $fieldCode = "UF_SHORT_DESCRIPTION";
    $iblockId = ID_IBLOCK_DOCTORS;
    $sectionCode = filterCode($_REQUEST["SECTION_CODE"]);
    if($sectionCode){
        $sectionShortDescription = getSectionPropertyValue($fieldCode, $iblockId, $sectionCode);
    }
    ?>
	<? if($sectionShortDescription): ?>
        <div class="page-subsection">
			<?= $sectionShortDescription ?>
        </div>
	<? endif ?>
<? endif ?>