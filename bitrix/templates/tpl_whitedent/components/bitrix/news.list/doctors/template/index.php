<? /** @var array $arResult */ ?>

<? include __DIR__."/short-description.php" ?>
<? include __DIR__."/menu.php" ?>

<div class="page-subsection">
    <div class="grid grid--padding-y">
		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<? $doctor = new DoctorsList\Doctor($arItem) ?>
			<? include __DIR__."/item.php" ?>
        <? endforeach ?>
    </div>
</div>

<? include __DIR__."/full-description.php" ?>

