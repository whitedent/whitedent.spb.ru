<? /** @var array $arResult */ ?>
<? if($arResult["ITEMS"]): ?>
	<? foreach($arResult["ITEMS"] as $arItem): ?>
		<? $review = new Reviews\Review($arItem) ?>
		<? include __DIR__."/item.php" ?>
	<? endforeach ?>
	<?= $arResult["NAV_STRING"] ?>
<? else: ?>
	<div class="page-subsection">
		<p>По заданным условиям фильтра отзывы не найдены</p>
	</div>
<? endif ?>