<? /** @var $review Reviews\Review */  ?>
<div class="review-item__info">

	<!-- <div class="review-item__name"><?= $review->title ?></div> -->

    <? if($review->rating): ?>
    <div class="review-item__stars">
        <?= str_repeat('<div class="review-item__star"></div>'."\n", 5) ?>
    </div>
    <? endif ?>

	<div class="review-item__body"><?= $review->text ?></div>

    <div class="review-item__footer">
		<? if($review->doctor): ?>
            <div class="review-item__doctor">Отзыв о враче:
                <? if($review->doctor["url"]): ?>
                    <a href="<?= $review->doctor["url"] ?>"><?= $review->doctor["name"] ?></a>
                <? else: ?>
                    <span><?= $review->doctor["name"] ?></span>
                <? endif ?>
            </div>
		<? endif ?>
        <? include __DIR__."/item-info-likes.php" ?>
    </div>

</div>