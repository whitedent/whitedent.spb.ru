<? /** @var $review Reviews\Review */  ?>
<div class="like-button review-item__like">
    <div class="like-button__title">Полезный отзыв?</div>
	<div class="like-button__button" data-id="<?= $review->id ?>">
		<a href="#" onclick="return false;">
            <svg xmlns="http://www.w3.org/2000/svg" width="25.653" height="24" viewBox="0 0 25.653 24">
                <use href="/images/like.svg#reviewItemLike"></use>
            </svg>
			<div class="like-button__counter"><?= $review->likes ?></div>
		</a>
	</div>
</div>