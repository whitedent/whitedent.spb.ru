<? /** @var $review Reviews\Review */  ?>
<div class="page-subsection">
    <div class="review-item" data-rating="<?= $review->rating ?>">
        <? include __DIR__."/item-image.php" ?>
        <? include __DIR__."/item-info.php" ?>
    </div>
</div>