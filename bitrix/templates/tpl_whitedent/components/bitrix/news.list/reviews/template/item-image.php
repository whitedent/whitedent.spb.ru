<? /** @var $review Reviews\Review */  ?>
<div class="review-item__image">

    <? if($review->image): ?>
        <a href="<?= $review->image ?>" data-glightbox="">
            <?= PictureTag::renderWithMQ($review->image, "", $review->title, $review->title) ?>
        </a>
	<? endif ?>

	<div class="review-item__author">

        <? if($review->gender): ?>
		<div class="review-item__author-image">
			<img src="/images/consultation/portrait_<?= $review->gender ?>.svg" alt="" width="34" height="34"/><!---->
		</div>
        <? endif ?>

        <? if($review->author): ?>
            <div class="review-item__author-name"><?= $review->author ?></div>
        <? endif ?>

		<div class="review-item__date"><?= $review->date ?></div>

	</div>

</div>
