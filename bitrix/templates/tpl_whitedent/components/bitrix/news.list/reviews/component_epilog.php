<?
global $APPLICATION;

$h1 = null;
$title = null;
$description = null;

$pageNumber = !empty($_GET["PAGEN_1"]) ? $_GET["PAGEN_1"] : false;

if($pageNumber){
	$h1 = $APPLICATION->GetTitle()." – Страница ".$pageNumber;
	$title = $APPLICATION->GetPageProperty("title")." – Страница ".$pageNumber;
	$description = truncateLastPeriod($APPLICATION->GetPageProperty("description")).". Страница ".$pageNumber;
}

if($h1){
	$APPLICATION->SetTitle($h1);
}

if($title){
	$APPLICATION->SetPageProperty("title", $title);
}

if($description){
	$APPLICATION->SetPageProperty("description", $description);
}

