<?php
namespace Doctor\Consultation;

class Question
{
	public $url;
	public $title;
	public $text;
	public $author;
	public $date;

	public function __construct(array $arItem)
	{
		$this->url = $arItem["DETAIL_PAGE_URL"];
		$this->title = $arItem["NAME"];
		$this->text = $arItem["~PREVIEW_TEXT"];
		$this->author = $arItem["PROPERTIES"]["FIO"]["~VALUE"];
		$this->date = FormatDate("d F Y", strtotime($arItem["FIELDS"]["DATE_CREATE"]));
	}

}
