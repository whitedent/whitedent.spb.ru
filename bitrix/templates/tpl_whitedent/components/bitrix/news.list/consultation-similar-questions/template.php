<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<? if($arResult["ITEMS"]): ?>
    <div class="page-subsection">
        <h2>Похожие вопросы</h2>
    </div>

    <div class="page-subsection">
        <div class="block-marked">
            <ul class="list-marked">
				<? foreach($arResult["ITEMS"] as $arItem): ?>
                    <li>
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                    </li>
				<? endforeach ?>
            </ul>
        </div>
    </div>
<? endif ?>
