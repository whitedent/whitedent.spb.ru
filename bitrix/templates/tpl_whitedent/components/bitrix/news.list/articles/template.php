<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include_once __DIR__."/Article.php";
?>
<div class="page-subsection">
    <div class="grid grid--padding-y">
		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<? $article = new ArticlesList\Article($arItem) ?>
			<? include __DIR__."/item.php" ?>
		<? endforeach ?>
    </div>
</div>

<?= $arResult["NAV_STRING"] ?>
