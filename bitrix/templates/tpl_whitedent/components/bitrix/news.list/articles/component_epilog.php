<?
if(!empty($_GET["PAGEN_1"])){

	$pageNum = $_GET["PAGEN_1"];

	$curTitle = $APPLICATION->GetPageProperty("title");
	$APPLICATION->SetPageProperty("title", $curTitle." – страница ".$pageNum);

	$curDescription = $APPLICATION->GetPageProperty("description");
	$curDescription = truncateLastPeriod($curDescription);
	$APPLICATION->SetPageProperty("description", $curDescription.". Страница ".$pageNum);

	$APPLICATION->SetTitle("Статьи – Страница ".$pageNum);
}