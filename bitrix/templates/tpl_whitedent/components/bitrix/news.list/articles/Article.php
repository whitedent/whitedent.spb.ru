<?
namespace ArticlesList;

class Article
{
	public $title;
	public $url;
	public $image;

	public function __construct($arItem)
	{
		$this->title = $arItem["NAME"];
		$this->url = $arItem["DETAIL_PAGE_URL"];
		$this->image = $arItem["PREVIEW_PICTURE"]["SRC"];
	}

}