<? /** @var $article ArticlesList\Article */  ?>
<div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--xs-12">

    <a class="article-item" href="<?= $article->url ?>">

        <div class="article-item__image">
          <img src="<?=$article->image?>" alt="<?=$article->title?>" title="<?=$article->title?>">
        </div>

        <div class="article-item__name"><?= $article->title ?></div>

    </a>

</div>
