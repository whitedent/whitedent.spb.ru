<? /** @var ActionsList\Action $action */  ?>
    <div class="grid grid--nopadding discount-banner" data-id="<?= $action->id ?>">

        <div class="grid__cell grid__cell--m-5 grid__cell--s-hidden grid__cell--xs-hidden discount-banner__image">

            <?= PictureTag::renderWithMQ($action->image, "", $action->title, $action->title) ?>
        </div>

        <div class="grid__cell grid__cell--m-7 grid__cell--s-12 grid__cell--xs-12">
            <div class="discount-banner__body">

                <div class="discount-banner__info">
					<? if($action->expired): ?>
                        <div class="discount-banner__date">До <?= $action->expired ?></div>
					<? endif ?>
                    <div class="discount-banner__name"><?= $action->title ?></div>
                    <div class="discount-banner__additional"><?= $action->text ?></div>
                </div>

                <? include __DIR__."/item-buttons.php" ?>

            </div>
        </div>

    </div>
