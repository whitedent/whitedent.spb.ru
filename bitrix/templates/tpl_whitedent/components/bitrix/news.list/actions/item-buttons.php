<? /** @var ActionsList\Action $action */  ?>
<div class="discount-banner__buttons">

    <a
        class="button discount-banner__button discount-form-open"
        href="#discount-form"
        data-from="<?= $action->title ?>"
        data-elementid="<?= $action->id ?>"
        data-type="discount"
    >Записаться</a>

	<? if($action->url): ?>
		<a class="discount-banner__more arrow" href="<?= $action->url ?>">Подробнее</a>
	<? endif ?>

</div>