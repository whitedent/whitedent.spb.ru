<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include_once __DIR__."/code.php";
?>

<div class="page-subsection">
<? foreach($arResult["ITEMS"] as $arItem): ?>
    <? $action = new ActionsList\Action($arItem) ?>
    <? include __DIR__."/item.php" ?>
<? endforeach ?>
</div>

<?= $arResult["NAV_STRING"] ?>
