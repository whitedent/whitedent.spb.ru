<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {die();}
$this->setFrameMode(true);
include_once __DIR__."/code.php";
?>
<div class="page-subsection">
    <? if($arResult["ITEMS"]): ?>
        <div class="grid grid--padding-y">
            <? foreach($arResult["ITEMS"] as $arItem): ?>
                <? $work = new WorksList\Work($arItem) ?>
                <? include __DIR__."/item.php" ?>
            <? endforeach ?>
        </div>
        <?= $arResult["NAV_STRING"] ?>
    <? else: ?>
        <p>Не найдены работы <?= $arResult["SECTION"] ? "в этом разделе" : "" ?></p>
    <? endif ?>
</div>

<? if($arParams["PARAM_SHOW_DESCRIPTION"] == "Y"): ?>
    <? include __DIR__."/description.php" ?>
<? endif ?>

