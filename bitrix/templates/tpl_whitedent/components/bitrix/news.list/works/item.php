<? /** @var $work WorksList\Work */  ?>
<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
    <div class="block-marked work-item">
        <div class="work-item__name"><?= $work->title ?></div>
        <? include __DIR__."/item-photos.php" ?>
        <? include __DIR__."/item-features.php" ?>
    </div>
</div>