<?
$title = null;
$description = null;
$h1 = null;

$pageNumber = !empty($_GET["PAGEN_1"]) ? $_GET["PAGEN_1"] : false;
$sectionName = !empty($arResult["SECTION"]["PATH"][0]["NAME"]) ? $arResult["SECTION"]["PATH"][0]["NAME"] : false;

// Корневой раздел
if(!$sectionName){
	// Пагинация
	if($pageNumber){
		$h1 = "Наши работы – Страница ".$pageNumber;
		$title = "Стоматология: фото до и после – Страница ".$pageNumber;
		$description = truncateLastPeriod($APPLICATION->GetPageProperty("description")).". Страница ".$pageNumber;
	}
}else{
	// Страница Раздела
	$h1 = $sectionName;
	$title = $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"];
	$description = $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"];

	if($pageNumber){
		$h1 .= " – Страница ".$pageNumber;
		$title .= " – Страница ".$pageNumber;
		$description = truncateLastPeriod($description).". Страница ".$pageNumber;
	}
}

global $APPLICATION;

if($title){
	$APPLICATION->SetPageProperty("title", $title);
}

if($description){
	$APPLICATION->SetPageProperty("description", $description);
}

if($h1){
	$APPLICATION->SetTitle($h1);
}