<?
namespace WorksList;

class Work
{
	public $id;
	public $title;
	public $photoBefore;
	public $photoAfter;
	public $doctor;
	public $services;

	public function __construct($arItem)
	{
		$this->id = $arItem["ID"];
		$this->title = $arItem["NAME"];
		$this->photoBefore = $this->getImage($arItem["PREVIEW_PICTURE"]);
		$this->photoAfter = $this->getImage($arItem["DETAIL_PICTURE"]);
		$this->doctor = $this->getDoctor($arItem["PROPERTIES"]["P_DOCTOR"]["VALUE"]);
		$this->services = $this->getServices($arItem["PROPERTIES"]["SERVICES"]["VALUE"]);
	}

	private function getImage($picture)
	{
		return !empty($picture["SRC"]) ? $picture["SRC"] : null;
	}

	private function getDoctor($doctorId)
	{
		if(empty($doctorId)){
			return false;
		}

		$element = getIbElement($doctorId);
		if(!$element){
			return false;
		}

		return [
			"name" => $element->fields["NAME"],
			"url" => $element->fields["DETAIL_PAGE_URL"]
		];
	}

	private function getServices($servicesId)
	{
		if(!$servicesId){
			return false;
		}

		$sections = [];
		foreach ($servicesId as $serviceId) {
			$res = \CIBlockSection::GetByID($serviceId);
			$sections[] = $res->GetNext();
		}

		if(!$sections){
			return false;
		}

		$services = [];
		foreach ($sections as $section) {
			$services[] = '<a href="'.$section["SECTION_PAGE_URL"].'">'.$section["NAME"].'</a>';
		}

		return implode(", ", $services);
	}

}