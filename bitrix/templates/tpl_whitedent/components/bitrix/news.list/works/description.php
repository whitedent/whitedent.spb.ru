<? if(empty($arResult["SECTION"])): ?>
	<div class="page-subsection">
		<?
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/o-klinike/nashi-raboty/full-description.html",
			)
		);
		?>
	</div>
<? else: ?>
	<? if(!empty($arResult["SECTION"]["PATH"][0]["DESCRIPTION"])): ?>
		<div class="page-subsection">
			<?= $arResult["SECTION"]["PATH"][0]["DESCRIPTION"] ?>
		</div>
	<? endif ?>
<? endif ?>