<? /** @var $slide DoctorsSlider\Slide */  ?>
<li class="splide__slide">
    <div class="doctor-item">
        <div class="grid grid--padding-y">

            <div class="grid__cell grid__cell--s-6 grid__cell--xs-hidden">
                <div class="doctor-item__photo">
                    <?= PictureTag::renderWithMQ($slide->image, "", $slide->name, $slide->name) ?>
                </div>
            </div>

            <div class="grid__cell grid__cell--s-6 grid__cell--xs-12">
                <div class="doctor-item__info">

                    <div class="doctor-item__photo hidden-hd hidden-xl hidden-l hidden-m hidden-s">
                      <?= PictureTag::renderWithMQ($slide->image, "", $slide->name, $slide->name) ?>

                    </div>

                    <div class="doctor-item__name"><?= $slide->nameFormatted ?></div>

                    <div class="doctor-item__block">

                        <div class="doctor-item__occupation"><?= $slide->speciality ?></div>

                        <div class="properties doctor-item__properties">

							<? if($slide->experience): ?>
                                <div class="properties__item properties__item--experience">
                                    <div class="properties__name">Стаж работы <?= $slide->experience ?></div>
                                </div>
							<? endif ?>

							<? if($slide->text): ?>
                                <div class="properties__item properties__item--additional">
                                    <div class="properties__name"><?= $slide->text ?></div>
                                </div>
							<? endif ?>

                        </div>
                    </div>

                    <a class="doctor-item__button" href="<?= $slide->url ?>">Подробнее</a>

                </div>
            </div>

        </div>
    </div>
</li>
