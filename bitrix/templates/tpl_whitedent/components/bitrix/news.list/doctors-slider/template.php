<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include_once __DIR__."/code.php";
?>
<div class="splide__track">
    <ul class="splide__list">
		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<? $slide = new DoctorsSlider\Slide($arItem) ?>
			<? include __DIR__."/item.php" ?>
		<? endforeach ?>
    </ul>
</div>

<div class="splide__arrows">
    <button class="splide__arrow splide__arrow--prev" type="button" aria-controls="splide01-track" aria-label="Go to last slide"></button>
    <button class="splide__arrow splide__arrow--next" type="button" aria-controls="splide01-track" aria-label="Next slide"></button>
</div>


