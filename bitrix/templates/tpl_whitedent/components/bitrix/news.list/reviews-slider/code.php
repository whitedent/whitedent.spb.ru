<?
namespace ReviewsSlider;

class Slide
{
	public $id;
	public $title;
	public $author;
	public $image;
	public $gender;
	public $date;
	public $text;
	public $doctor;
	public $rating;
	public $likes;

	public function __construct($arItem)
	{
		$this->id = $arItem["IBLOCK_ID"]."_".$arItem["ID"];
		$this->title = $arItem["NAME"];
		$this->author = $arItem["PROPERTIES"]["NAME"]["VALUE"];
		$this->image = $this->getImage($arItem["PREVIEW_PICTURE"]);
		$this->gender = $this->getGender($arItem["PROPERTIES"]["GENDER"]["VALUE"]);
		$this->date = $this->getDate($arItem["DATE_CREATE"]);
		$this->text = $arItem["~PREVIEW_TEXT"];
		$this->doctor = $this->getDoctor($arItem["PROPERTIES"]["O_DOCTOR"]["VALUE"]);
		$this->rating = $arItem["PROPERTIES"]["RATING"]["VALUE"];
		$this->likes = $this->getLikes($arItem["PROPERTIES"]["LIKES"]["VALUE"]);
	}

	private function getImage($picture)
	{
		return !empty($picture["SRC"]) ? $picture["SRC"] : null;
	}

	private function getGender($value)
	{
		if($value == "мужчина"){
			return "m";
		}

		if($value == "женщина"){
			return "w";
		}

		return  false;
	}

	private function getDate($date)
	{
		return FormatDate("d.m.Y", MakeTimeStamp($date));
	}

	private function getDoctor($doctorId)
	{
		if(empty($doctorId)){
			return false;
		}

		$element = getIbElement($doctorId);
		if(!$element){
			return false;
		}

		return [
			"name" => $element->fields["NAME"],
			"url" => $element->fields["DETAIL_PAGE_URL"]
		];
	}

	private function getLikes($value)
	{
		return $value ? $value : "";
	}

}