<? /** @var $slide ReviewsSlider\Slide */  ?>
<div class="review-item__info">

	<!--<div class="review-item__name"><?= $slide->title ?></div>-->

    <? if($slide->rating): ?>
    <div class="review-item__stars">
        <?= str_repeat('<div class="review-item__star"></div>'."\n", 5) ?>
    </div>
    <? endif ?>

	<div class="review-item__body"><?= $slide->text ?></div>

    <div class="review-item__footer">
		<? if($slide->doctor): ?>
            <div class="review-item__doctor">Отзыв о враче:
                <a href="<?= $slide->doctor["url"] ?>"><?= $slide->doctor["name"] ?></a>
            </div>
		<? endif ?>
        <? include __DIR__."/item-info-likes.php" ?>
    </div>

</div>