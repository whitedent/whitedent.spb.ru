<? /** @var $slide ReviewsSlider\Slide */  ?>
<div class="review-item__image">

    <? if($slide->image): ?>
        <a href="<?= $slide->image ?>" data-glightbox="">
            <?= PictureTag::renderWithMQ($slide->image, "", $slide->title, $slide->title) ?>
        </a>
	<? endif ?>

	<div class="review-item__author">

        <? if($slide->gender): ?>
            <div class="review-item__author-image">
                <img src="/images/consultation/portrait_<?= $slide->gender ?>.svg" alt="" width="34" height="34"/>
            </div>
        <? endif ?>

        <? if($slide->author): ?>
            <div class="review-item__author-name"><?= $slide->author ?></div>
        <? endif ?>

		<div class="review-item__date"><?= $slide->date ?></div>

	</div>

</div>
