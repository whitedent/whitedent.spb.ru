<? /** @var $slide ReviewsSlider\Slide */  ?>
<li class="splide__slide">
    <div class="review-item" data-rating="<?= $slide->rating ?>">
        <? include __DIR__."/item-image.php" ?>
        <? include __DIR__."/item-info.php" ?>
    </div>
</li>