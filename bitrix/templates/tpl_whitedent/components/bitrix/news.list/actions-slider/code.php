<?
namespace ActionsSlider;

class Slide
{
	public $id;
	public $title;
	public $image;
	public $expired;
	public $text;
	public $url;

	public function __construct($arItem)
	{
		$this->id = $arItem["ID"];
		$this->title = $arItem["PROPERTIES"]["TITLE"]["~VALUE"];
		$this->image = $this->getImage($arItem["PREVIEW_PICTURE"]);
		$this->expired = $this->getExpired($arItem["PROPERTIES"]["EXPIRED"]["VALUE"]);
		$this->text = $arItem["~PREVIEW_TEXT"];
		$this->url = $arItem["DETAIL_TEXT"] ? $arItem["DETAIL_PAGE_URL"] : false;
	}

	private function getImage($picture)
	{
		return !empty($picture["SRC"]) ? $picture["SRC"] : null;
	}

	private function getExpired($propValue)
	{
		if(!$propValue){
			return false;
		}
		return mb_strtolower(FormatDate("d F", MakeTimeStamp($propValue)), "utf8");
	}

}
