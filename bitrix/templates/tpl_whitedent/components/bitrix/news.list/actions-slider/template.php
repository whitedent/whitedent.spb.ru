<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include_once __DIR__."/code.php";
?>
<div class="splide__track">
    <ul class="splide__list">
        <? foreach($arResult["ITEMS"] as $arItem): ?>
            <? $slide = new ActionsSlider\Slide($arItem) ?>
            <? include __DIR__."/item.php" ?>
        <? endforeach ?>
    </ul>
</div>

<? include __DIR__."/arrows.php" ?>