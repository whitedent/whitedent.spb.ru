<? /** @var $slide ActionsSlider\Slide */  ?>
<div class="discount-banner__buttons">
	<a
		class="button discount-banner__button discount-form-open"
		href="#discount-form"
		data-from="<?= $slide->title ?>"
		data-elementid="<?= $slide->id ?>"
		data-type="discount"
	>Записаться</a>
		<? if($slide->url): ?>
	<a class="discount-banner__more arrow" href="<?= $slide->url ?>">Подробнее</a>
<? endif ?>
</div>
