<? /** @var $slide ActionsSlider\Slide */  ?>
<li class="splide__slide">
    <div class="grid grid--nopadding discount-banner">

        <div class="grid__cell grid__cell--m-5 grid__cell--s-hidden grid__cell--xs-hidden discount-banner__image">

            <?= PictureTag::renderWithMQ($slide->image, "", $slide->title, $slide->title) ?>

        </div>

        <div class="grid__cell grid__cell--m-7 grid__cell--s-12 grid__cell--xs-12">
            <div class="discount-banner__body">

                <div class="discount-banner__info">
                    <? if($slide->expired): ?>
                        <div class="discount-banner__date">До <?= $slide->expired ?></div>
                    <? endif ?>
                    <div class="discount-banner__name"><?= $slide->title ?></div>
                    <div class="discount-banner__additional"><?= $slide->text ?></div>
                </div>

				<? include __DIR__."/item-buttons.php" ?>

            </div>
        </div>

    </div>
</li>
