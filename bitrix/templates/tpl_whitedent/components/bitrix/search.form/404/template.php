<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? $this->setFrameMode(true) ?>
<form
    class="grid grid--padding-y"
    action="<?= $arResult["FORM_ACTION"] ?>"
    method="get"
>

    <div class="grid__cell grid__cell--s-7 grid__cell--xs-12">
        <div class="form-input form-input--text">
            <input
                class="form-input__field"
                type="text"
                name="q"
                value=""
                required
            >
        </div>
    </div>

    <div class="grid__cell grid__cell--s-5 grid__cell--xs-12">
        <button class="button button--wide" type="submit">Найти</button>
    </div>

</form>