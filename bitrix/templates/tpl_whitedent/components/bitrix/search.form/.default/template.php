<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? $this->setFrameMode(true) ?>
<form
    class="header-search__form"
    action="<?= $arResult["FORM_ACTION"] ?>"
    method="get"
>

    <button class="header-search__close" type="button">
        <svg width="18.6" height="18.6" viewBox="0 0 18.6 18.6" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="close" stroke="currentColor" stroke-width="2"><line x1="2" y1="2" x2="16" y2="16"></line><line x1="2" y1="16" x2="16" y2="2"></line></g></svg>
    </button>

    <div class="form-input form-input--search header-search__input">
        <input
            class="form-input__field"
            type="search"
            name="q"
            placeholder="Поиск"
            value=""
            required
        >
    </div>

    <button
        class="header-search__submit"
        type="submit"
    >
        <svg xmlns="http://www.w3.org/2000/svg" width="18.6" height="18.6" viewBox="0 0 18.6 18.6"><g transform="translate(0.3 0.3)"><path fill="currentColor" stroke="currentColor" stroke-width=".6px" d="M28.78,27.718l-4.044-4.044a7.718,7.718,0,1,0-1.062,1.065l4.044,4.041a.751.751,0,0,0,1.062-1.062ZM14.332,23.167A6.243,6.243,0,1,1,18.747,25,6.193,6.193,0,0,1,14.332,23.167Z" transform="translate(-11 -11)"/></g></svg>
    </button>

</form>
