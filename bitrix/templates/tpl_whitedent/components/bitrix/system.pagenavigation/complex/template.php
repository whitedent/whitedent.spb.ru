<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<ul class="pagination">

    <li class="pagination__item pagination__item--prev">
        <?if ($arResult["NavPageNomer"] > 2):?>
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">←</a>
        <?elseif($arResult["NavPageNomer"] == 2):?>
            <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">←</a>
        <?else:?>
            <span>←</span>
        <?endif?>
    </li>

    <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
        <li class="pagination__item <?= $arResult["nStartPage"] == $arResult["NavPageNomer"] ? "pagination__item--active" : "" ?>">
            <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                <span><?=$arResult["nStartPage"]?></span>
            <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
                <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
            <?else:?>
                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
            <?endif?>
            <?$arResult["nStartPage"]++?>
        </li>
    <?endwhile?>

    <li class="pagination__item pagination__item--next">
        <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">→</a>
        <? else: ?>
            <span>→</span>
        <? endif ?>
    </li>

</ul>