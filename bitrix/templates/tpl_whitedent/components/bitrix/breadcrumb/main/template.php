<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult)){return "";}

$strReturn = '<div class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">';

$num_items = count($arResult);
for($index = 0, $itemSize = $num_items; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    $name = '<span property="name">'.$title.'</span>';

    $link = $arResult[$index]["LINK"];

    $aLink = '<a property="item" typeof="WebPage" href="'.$link.'" title="'.$title.'">'.$name.'</a>';

	$microDataPosition = '<meta property="position" content="'.($index + 1).'">';

	if($link <> "" && $index != $itemSize - 1){
		$strReturn .= '<div class="breadcrumbs__parent" property="itemListElement" typeof="ListItem">'.$aLink.'</div>';
		$strReturn .= '<div class="breadcrumbs__separator"></div>';

	}else{
		$strReturn .= '<div class="breadcrumbs__current" property="itemListElement" typeof="ListItem">'.$name.'</div>';
	}

    $strReturn .= $microDataPosition;
}

$strReturn .= '</div>';

return $strReturn;