<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use ConsultationList\Question;
include __DIR__."/Question.php";
?>
<div class="page-subsection">
    <div class="consultation-list">
		<? if($arResult["ITEMS"]): ?>
			<? foreach($arResult["ITEMS"] as $arItem): ?>
                <? $question = new Question($arItem) ?>
                <? include __DIR__."/item.php" ?>
			<? endforeach ?>
		<? else: ?>
            <p>Пока нет вопросов</p>
		<? endif ?>
    </div>
</div>

<? if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
	<?=$arResult["NAV_STRING"]?>
<? endif ?>