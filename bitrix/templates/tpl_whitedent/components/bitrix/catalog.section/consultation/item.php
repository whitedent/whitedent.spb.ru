<? /** @var ConsultationList\Question $question */ ?>
<div class="block-marked block-marked--color-grey consultation-list__item">

	<h2 class="consultation-list__title">
		<a href="<?= $question->url ?>"><?= $question->title ?></a>
	</h2>

	<div class="consultation-list__body">

		<? if($question->text): ?>
			<div class="consultation-list__question"><p><?= $question->text ?></p></div>
		<? endif ?>

		<div class="consultation-list__info"><?= $question->author ?></div>

	</div>

</div>