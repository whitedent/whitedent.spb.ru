<?php
namespace ConsultationList;

class Question
{
	public $id;
	public $url;
	public $title;
	public $text;
	public $author;

	public function __construct(array $arItem)
	{
		$this->id = $arItem["ID"];
		$this->url = $arItem["DETAIL_PAGE_URL"];
		$this->title = $arItem["NAME"];
		$this->text = $arItem["~PREVIEW_TEXT"];
		$this->author = $this->getAuthor($arItem);
	}

	private function getAuthor(array $arItem): string
	{
		$info = [];

		if(!empty($arItem["PROPERTIES"]["NAME"]["~VALUE"])){
			$info[] = $arItem["PROPERTIES"]["NAME"]["~VALUE"];
		}

		if(!empty($arItem["PROPERTIES"]["AGE"]["~VALUE"])){
			$yearsVal = $arItem["PROPERTIES"]["AGE"]["~VALUE"];
			$yearsText = getYearsAsText($arItem["PROPERTIES"]["AGE"]["~VALUE"]);
			$info[] = $yearsVal." ".$yearsText;
		}

		$info[] = mb_strtolower(FormatDate("d F Y", strtotime($arItem["DATE_CREATE"])));

		return implode(', ', $info);
	}

}
