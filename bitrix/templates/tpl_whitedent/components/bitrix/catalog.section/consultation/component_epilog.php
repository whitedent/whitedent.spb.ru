<?
$title = null;
$description = null;
$h1 = null;

$pageNumber = !empty($_GET["PAGEN_1"]) ? $_GET["PAGEN_1"] : false;
$sectionName = !empty($arResult["PATH"][0]["NAME"]) ? $arResult["PATH"][0]["NAME"] : false;

// Корневой раздел
if(!$sectionName){
	// Пагинация
	if($pageNumber){
		$h1 = "Консультации – Страница ".$pageNumber;
		$title = "Онлайн-консультация - клиника «ВайтДент» – страница ".$pageNumber;
		$description = "Создали этот раздел, чтобы нашим пациентам было проще узнать причины проблем с зубами, деснами или ротовой полостью в целом. Просто подробно изложите суть вашей проблемы в вопросе, и наши эксперты дадут полный ответ. Страница ".$pageNumber;
	}
}else{
	// Страница Раздела
	$h1 = "Вопросы и ответы: $sectionName";
	$title = $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"];
	$description = $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"];

	if($pageNumber){
		$h1 .= " – Страница ".$pageNumber;
		$title .= " – Страница ".$pageNumber;
		$description = "Обсуждение на тему ".strtolower($sectionName).". Вопросы и ответы специалистов клиники. Страница ".$pageNumber;
	}
}

global $APPLICATION;

if($title){
	$APPLICATION->SetPageProperty("title", $title);
}

if($description){
	$APPLICATION->SetPageProperty("description", $description);
}

if($h1){
	$APPLICATION->SetTitle($h1);
}