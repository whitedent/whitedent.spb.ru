<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)):?>

[
<? $previousLevel = 0 ?>
<? foreach($arResult as $arItem): ?>
<? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
<?= str_repeat("]},", ($previousLevel - $arItem["DEPTH_LEVEL"])) ?>
<? endif ?>
<?
$isActive = false;
if($arItem["SELECTED"]){
$isActive = true;
}
?>
{
"name": "<?= $arItem["TEXT"] ?>",
"href": "<?= $arItem["LINK"] ?>",
<?if($isActive):?>"active": true,<?endif?>
<?  if($arItem["IS_PARENT"]): ?>
"sublist": [
<?  else: ?>
},
<?  endif ?>
<? $previousLevel = $arItem["DEPTH_LEVEL"] ?>
<? endforeach ?>
<? if ($previousLevel > 1)://close last item tags?>
<?= str_repeat("]}", ($previousLevel-1) ) ?>
<? endif ?>
]
<? endif ?>