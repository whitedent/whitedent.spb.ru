<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)):?>
<div class="block-marked consultation-categories">
    <ul class="consultation-categories__list">
        <? $previousLevel = 0 ?>
        <? foreach($arResult as $arItem): ?>
          <?
          $classes = ["consultation-categories__item"];
		  if($arItem["SELECTED"]){
			  $classes[] = "consultation-categories__item--active";
		  }
          ?>
          <li class="<?= implode(" ", $classes) ?>">
              <a class="consultation-categories__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
              <div class="consultation-categories__count"><?= $arItem["PARAMS"]["COUNT"] ?></div>
          </li>
        <? endforeach ?>
    </ul>
</div>
<? endif ?>