<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)):?>
<nav class="page-navigation">

    <ul class="page-navigation__list">
        <? $previousLevel = 0 ?>
        <? foreach($arResult as $arItem): ?>

          <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
            <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])) ?>
          <? endif ?>

          <?
          $classes = ["page-navigation__item"];

		  if($arItem["SELECTED"]){
			  $classes[] = "page-navigation__item--active";
		  }

          if($arItem["IS_PARENT"]){
            $classes[] = "page-navigation__item--parent";
          }

          if($arItem["DEPTH_LEVEL"] > 1){
            $classes[] = "page-navigation__item--sub page-navigation__item--sub-".($arItem["DEPTH_LEVEL"] - 1);
          }

          $classes = implode(" ", $classes);
          ?>
          <li class="<?= $classes ?>">

              <? if($arItem["SELECTED"]): ?>
                  <span><strong><?= $arItem["TEXT"] ?></strong></span>
              <? else: ?>
                  <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
              <? endif ?>


            <?  if($arItem["IS_PARENT"]): ?>
              <ul class="header-navigation__sublist header-navigation__sublist--<?= $arItem["DEPTH_LEVEL"] ?>">
            <?  else: ?>

          </li>
          <?  endif ?>

          <? $previousLevel = $arItem["DEPTH_LEVEL"] ?>

        <? endforeach ?>

        <? if ($previousLevel > 1)://close last item tags?>
            <?= str_repeat("</ul></li>", ($previousLevel-1) ) ?>
        <? endif ?>

    </ul>
</nav>
<? endif ?>