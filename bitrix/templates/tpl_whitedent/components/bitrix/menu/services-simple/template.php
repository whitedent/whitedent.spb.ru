<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<nav class="menu-service">
    <ul class="menu-service__list">

        <?php if (!empty($arResult)) { ?>

            <?php
            $previousLevel = 0;
            foreach ($arResult as $arItem) { ?>
                <?php if ($arItem["DEPTH_LEVEL"] == 1){ ?>
                <li class="menu-service__item menu-service__item--parent">

                    <a href="<?= $arItem["LINK"] ?>" class=""><?= $arItem["TEXT"] ?></a>

                <?}?>

                        <?php if ( $arItem["DEPTH_LEVEL"] == 2  || $arItem["DEPTH_LEVEL"] == 3 ){?>

                        <ul class="menu-service__sublist menu-service__sublist--1">


                        <?php if ($arItem["DEPTH_LEVEL"] == 2){?>
                                    <li class="menu-service__item menu-service__item--sub menu-service__item--sub-1">
                                        <a href="<?= $arItem["LINK"] ?>" class=""><?= $arItem["TEXT"] ?></a>
                        <?php } ?>

                                <?php if ($arItem["DEPTH_LEVEL"] == 3 ){?>
                                        <ul class="menu-service__sublist menu-service__sublist--2">
                                            <li class="menu-service__item menu-service__item--sub menu-service__item--sub-2">
                                                <a href="<?= $arItem["LINK"] ?>" class=""><?= $arItem["TEXT"] ?></a>
                                            </li>
                                        </ul>
                                <?php } ?>

                                    </li>
                        </ul>

                    <?php } ?>

            <?php } ?>
        <?php } ?>
        </li>
    </ul>
</nav>
