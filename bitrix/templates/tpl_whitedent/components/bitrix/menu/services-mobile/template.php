<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)):?>
<nav class="menu-services">

    <ul class="menu-services__list">
        <? $previousLevel = 0 ?>
        <? foreach($arResult as $arItem): ?>

          <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
            <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])) ?>
          <? endif ?>

          <?
          $classes = ["menu-services__item"];

		  if($arItem["SELECTED"]){
			  $classes[] = "menu-services__item--active";
		  }

          if($arItem["IS_PARENT"]){
            $classes[] = "menu-services__item--parent";
          }

          if($arItem["DEPTH_LEVEL"] > 1){
            $classes[] = "menu-services__item--sub menu-services__item--sub-".($arItem["DEPTH_LEVEL"] - 1);
          }

          $classes = implode(" ", $classes);
          ?>
          <li class="<?= $classes ?>">

            <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>

            <?  if($arItem["IS_PARENT"]): ?>
              <ul class="menu-services__sublist menu-services__sublist--<?= $arItem["DEPTH_LEVEL"] ?>">
            <?  else: ?>

          </li>
          <?  endif ?>

          <? $previousLevel = $arItem["DEPTH_LEVEL"] ?>

        <? endforeach ?>

        <? if ($previousLevel > 1)://close last item tags?>
            <?= str_repeat("</ul></li>", ($previousLevel-1) ) ?>
        <? endif ?>

    </ul>
</nav>
<? endif ?>