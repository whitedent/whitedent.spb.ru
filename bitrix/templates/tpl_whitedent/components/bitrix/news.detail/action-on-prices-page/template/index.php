<? /** @var ActionLinked\Action $action */ ?>
<div class="page-subsection">
	<div class="grid grid--align-center grid--nopadding discount-banner discount-banner--no-image">
		<div class="grid__cell grid__cell--xs-12">

			<div class="discount-banner__body">
				<div class="discount-banner__info">
					<div class="discount-banner__name"><?= $action->title ?></div>
					<div class="discount-banner__additional"><?= $action->text ?></div>
				</div>
                <? include __DIR__."/buttons.php" ?>
			</div>

		</div>
	</div>
</div>


