<? /** @var ActionLinked\Action $action */ ?>
<div class="discount-banner__buttons">

	<a class="discount-banner__more arrow" href="<?= $action->url ?>">Подробнее</a>

	<? if($action->expired): ?>
		<div class="discount-banner__date">Акция действует до <?= $action->expired ?></div>
	<? endif ?>

	<a
		class="button discount-banner__button discount-form-open"
		href="#discount-form"
		data-from="<?= $action->title ?>"
		data-elementid="<?= $action->id ?>"
		data-type="discount"
	>Записаться</a>

</div>