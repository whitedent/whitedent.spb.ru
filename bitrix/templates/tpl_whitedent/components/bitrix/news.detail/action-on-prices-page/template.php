<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
$this->setFrameMode(true);
include_once __DIR__."/code/Action.php";
$action = new ActionLinked\Action($arResult);
include_once __DIR__."/template/index.php";
