<?
namespace ActionLinked;

class Action
{
	public $id;
	public $title;
	public $image;
	public $expired;
	public $text;
	public $url;

	public function __construct($arResult)
	{
		$this->id = $arResult["ID"];
		$this->title = $arResult["PROPERTIES"]["TITLE"]["~VALUE"];
		$this->expired = $this->getExpired($arResult["PROPERTIES"]["EXPIRED"]["VALUE"]);
		$this->text = $arResult["~PREVIEW_TEXT"];
		$this->url = $arResult["DETAIL_PAGE_URL"];
	}

	private function getExpired($propValue)
	{
		if(!$propValue){
			return false;
		}
		return mb_strtolower(FormatDate("d F", MakeTimeStamp($propValue)), "utf8");
	}

}