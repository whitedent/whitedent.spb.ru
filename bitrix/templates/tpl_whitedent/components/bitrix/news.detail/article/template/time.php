<? /** @var Articles\Article $article */  ?>
<? if($article->time): ?>
	<div class="page-subsection">
		<div class="block-marked block-icon block-icon--time">
			<div class="block-icon__title">Время прочтения — <?= $article->time ?></div>
		</div>
	</div>
<? endif ?>