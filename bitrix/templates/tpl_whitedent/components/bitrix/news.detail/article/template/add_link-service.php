<? /** @var Articles\Article $article */  ?>
<div class="grid__cell grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">

	<a class="block-marked block-marked--full-height" href="<?= $article->service->url ?>">
		<div class="grid grid--align-center" style="height: 100%;">

			<div class="grid__cell grid__cell--xs-auto">
				<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path fill="currentColor" d="M20,0A20,20,0,1,0,40,20,20,20,0,0,0,20,0Zm0,38.2A18.2,18.2,0,1,1,38.2,20,18.2,18.2,0,0,1,20,38.2Z"/><path fill="currentColor" d="M20.84,16.65H19.06c-.15,0-.23.07-.23.22V27.78a.2.2,0,0,0,.23.22h1.78c.16,0,.24-.08.24-.22V16.87C21.08,16.72,21,16.65,20.84,16.65Z"/><path fill="currentColor" d="M20,12a1.53,1.53,0,0,0-1.08.4,1.47,1.47,0,0,0,0,2.06,1.66,1.66,0,0,0,2.16,0,1.47,1.47,0,0,0,0-2.06A1.53,1.53,0,0,0,20,12Z"/></svg>
			</div>

			<div class="grid__cell grid__cell--xs-9">
				<p><?= $article->service->title ?> — информация об услуге</p>
			</div>

		</div>
	</a>

</div>
