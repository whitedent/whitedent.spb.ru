<? /** @var Articles\Article $article */  ?>
<? /** @var Articles\Author $author */ ?>
<? $author = $article->author ?>
<div class="page-subsection">
	<div class="grid grid--align-center article-author">

        <? if($author->image): ?>
		<div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-6 grid__cell--xs-8 article-author__image">
			<img src="<?= $author->image ?>" alt="<?= $author->name ?>" title="<?= $author->name ?>"/>
		</div>
        <? endif ?>

		<div class="grid__cell grid__cell--l-10 grid__cell--m-9 grid__cell--s-12 grid__cell--xs-12">
			<div class="article-author__body">
				<div class="grid grid--padding-y">
					<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12 article-author__info">

                        <? if($author): ?>
                            <div class="article-author__title">Автор статьи:</div>
                            <div class="article-author__name">
                                <a href="<?= $author->url ?>"><?= $author->name ?></a>
                            </div>

                            <div class="article-author__occupation"><?= $author->speciality ?></div>
                        <? endif ?>

                        <div class="article-author__date">
							Дата публикации: <?= $article->datePublication ?> г.<br/>
							Дата обновления: <?= $article->dateUpdate ?> г.
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>
