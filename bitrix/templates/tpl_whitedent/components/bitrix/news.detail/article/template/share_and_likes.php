<? /** @var Articles\Article $article */  ?>
<div class="page-subsection">
	<div class="article-footer">

        <? if($article->shareText): ?>
		<div class="article-footer__share">
            <?= $article->shareText ?>
		</div>
        <? endif ?>

		<div class="like-button article-footer__like">
			<div class="like-button__title">Полезная статья?</div>
			<div class="like-button__button" data-id="<?= $article->id ?>">
                <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.653" height="24" viewBox="0 0 25.653 24">
                        <use href="/images/like.svg#reviewItemLike"></use>
                    </svg>
                    <div class="like-button__counter"><?= $article->likes ?></div>
                </a>
            </div>
		</div>

	</div>
</div>
