<? /** @var Articles\Article $article */  ?>
<section class="page-section container">
	<div class="content">
		<div class="form-appointment">
			<div class="grid grid--align-center">

				<div class="grid__cell grid__cell--l-8 grid__cell--xs-12">
                    <? include __DIR__."/form-form.php" ?>
				</div>

				<div class="grid__cell grid__cell--l-4 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden form-appointment__image">
                    <img src="/images/form-appointment.jpg" alt="" title=""/>
				</div>

			</div>
		</div>
	</div>
</section>