<? /** @var Articles\Article $article */  ?>
<? /** @var Articles\Article $similarArticle */  ?>
<? if($article->similarArticles): ?>
<section class="page-section container">
	<div class="content">

		<h2>Похожие статьи</h2>

		<div class="page-subsection">
			<div class="grid grid--padding-y">

                <? foreach ($article->similarArticles as $similarArticle): ?>
				<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">

                    <a class="article-item-small article-item-small--small" href="<?= $similarArticle->url ?>">

						<div class="article-item-small__image">
                            <img
                                src="<?= $similarArticle->image ?>"
                                alt="<?= $similarArticle->title ?>"
                                title="<?= $similarArticle->title ?>"
                            />
                        </div>

						<div class="article-item-small__name"><?= $similarArticle->title ?></div>

						<div class="article-item-small__arrow">
                            <svg xmlns="http://www.w3.org/2000/svg" width="80" height="16" viewBox="0 0 80 16" preserveAspectRatio="none"><path fill="currentColor" d="M31.8,12.086,24.956,5.308A1.232,1.232,0,0,0,24.066,5h0a1.267,1.267,0,0,0-.89,2.168l4.656,4.564H-46.554A1.267,1.267,0,0,0-47.821,13a1.267,1.267,0,0,0,1.267,1.267H27.832L23.176,18.83A1.267,1.267,0,0,0,24.066,21a1.232,1.232,0,0,0,.89-.365L31.8,13.855a1.267,1.267,0,0,0,0-1.8Z" transform="translate(47.821 -4.998)"/></svg>
                        </div>
                    </a>

				</div>
                <? endforeach ?>

			</div>
		</div>

	</div>
</section>
<? endif ?>