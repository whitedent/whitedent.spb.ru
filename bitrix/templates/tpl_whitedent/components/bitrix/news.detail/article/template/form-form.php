<? /** @var Articles\Article $article */  ?>
<div class="form-appointment__title"><?= $article->formTitle ?></div>

<form
    class="form-appointment__form consultation-new-registration-popup validated-form submitJS"
    method="post"
>

    <input type="text" name="req_name">

    <div class="form-appointment__input form-appointment__input--name">
        <div class="form-input form-input--text">
            <input class="form-input__field validated required" type="text" name="name" placeholder="Ваше имя">
        </div>
        <div class="validation-message">Обязательное поле</div>
    </div>

    <div class="form-appointment__input form-appointment__input--tel">
        <div class="form-input form-input--tel">
            <input
                class="form-input__field validated required"
                type="tel"
                name="phone"
                placeholder="Ваш телефон"
            >
        </div>
        <div class="validation-message">Напишите номер телефона. Без телефона мы не сможем вам позвонить и назначить время и врача.</div>
    </div>

    <div class="form-appointment__submit">
        <button class="button button--wide" type="submit">Отправить</button>
    </div>

    <div class="form-appointment__input form-appointment__input--agreement">
        <div class="form-input form-input--checkbox">
            <input class="form-input__field" type="checkbox" name="agreement" required checked>
            <label class="form-input__label">Отправляя эту форму Вы соглашаетесь с условиями Обработки персональных данных</label>
        </div>
    </div>

    <? if($article->service): ?>
        <input type="hidden" name="service_id" value="<?= $article->service->id ?>">
    <? endif ?>

</form>