<section class="page-section container">
	<div class="content">
        <? include __DIR__."/title.php" ?>
        <? include __DIR__."/top.php" ?>
        <? include __DIR__."/time.php" ?>
        <? include __DIR__."/text.php" ?>
        <? include __DIR__."/author.php" ?>
        <? include __DIR__."/share_and_likes.php" ?>
	</div>
</section>

<? include __DIR__."/add_links.php" ?>
<? include __DIR__."/similar.php" ?>
<? include __DIR__."/form.php" ?>