<? /** @var Articles\Article $article */  ?>
<div class="page-subsection">
	<div class="block-marked-image block-marked-image--image-to-right">
		<div class="grid grid--par-like">

			<div class="grid__cell grid__cell--l-5 grid__cell--m-5 grid__cell--s-8 grid__cell--xs-12 block-marked-image__image">
				<?= PictureTag::renderWithMQ($article->image, "", $article->title, $article->title) ?>
			</div>

			<? if($article->contentsLinks): ?>
				<div class="grid__cell grid__cell--l-7 grid__cell--m-7 grid__cell--s-12 grid__cell--xs-12 block-marked-image__body">
					<div class="block-marked-image__body-inner">
						<h2 class="small">Содержание</h2>
                        <ul class="list-marked list-marked--arrow">
                            <? foreach ($article->contentsLinks as $contentsLink): ?>
                                <li>
                                    <? if($contentsLink["url"]): ?>
                                        <a href="#<?= $contentsLink["url"] ?>"><?= $contentsLink["title"] ?></a>
                                    <? else: ?>
                                        <a href="#"><strong><?= $contentsLink["title"] ?></strong></a>
									<? endif ?>
                                </li>
                            <? endforeach ?>
                        </ul>
					</div>
				</div>
			<? endif ?>

		</div>
	</div>
</div>
