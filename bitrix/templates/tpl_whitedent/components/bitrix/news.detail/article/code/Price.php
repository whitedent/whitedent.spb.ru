<?
namespace Articles;

class Price
{
	public $title;
	public $url;

	public function __construct(array $fields)
	{
		$this->title = $fields["NAME"];
		$this->url = $fields["SECTION_PAGE_URL"];
	}

}