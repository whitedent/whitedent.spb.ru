<?
namespace Articles;

class Service
{
	public $id;
	public $title;
	public $url;
	public $rootSectionName;

	public function __construct(array $fields)
	{
		$this->id = $fields["ID"];
		$this->title = $fields["NAME"];
		$this->url = $fields["DETAIL_PAGE_URL"];
		$this->rootSectionName = $this->getRootSectionName($fields["IBLOCK_SECTION_ID"]);
	}

	private function getRootSectionName($sectionId)
	{
		$sections = \CIBlockSection::GetNavChain(ID_IBLOCK_SERVICES, $sectionId, ["NAME"], true);
		$rootSection = reset($sections);

		return $rootSection["NAME"];
	}

}