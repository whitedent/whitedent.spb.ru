<?
namespace Articles;

class Article
{
	public $id;
	public $title;
	public $image;
	public $url;
	public $contentsLinks;
	public $time;
	public $text;
	public $author;
	public $datePublication;
	public $dateUpdate;
	public $shareText;
	public $likes;
	public $service;
	public $price;
	public $similarArticles;
	public $formTitle;

	public function loadData(array $arResult)
	{
		$this->id = $arResult["IBLOCK_ID"]."_".$arResult["ID"];
		$this->title = $arResult["NAME"];
		$this->image = $arResult["PREVIEW_PICTURE"]["SRC"];
		$this->time = $this->getTime($arResult["DETAIL_TEXT"]);
		$this->text = $this->getText($arResult["DETAIL_TEXT"], $arResult["PROPERTIES"]["REGISTER_LINK"]["~VALUE"]["TEXT"]);
		$this->contentsLinks = $this->getContentsLinks($this->text);
		$this->author = $this->getAuthor($arResult["PROPERTIES"]["AUTHOR"]["VALUE"]);
		$this->datePublication = $this->getDate($arResult["CREATED_DATE"]);
		$this->dateUpdate = FormatDate("d.m.Y", strtotime($arResult["TIMESTAMP_X"]));
		$this->shareText = $this->getShareText();
		$this->likes = $arResult["PROPERTIES"]["LIKES"]["VALUE"];
		$this->service = $this->getService($arResult["PROPERTIES"]["SERVICE"]["VALUE"]);
		$this->price = $this->getPrice($arResult["PROPERTIES"]["PRICE"]["VALUE"]);
		$this->similarArticles = $this->getSimilarArticles($arResult["PROPERTIES"]["SIMILAR_ARTICLES"]["VALUE"]);
		$this->formTitle = $this->getFormTitle($this->service);
	}

	private function getTime($text): string
	{
		if(!$text){
			return "";
		}

		$textLength = mb_strlen($text, "utf8");
		$seconds = $textLength / 12;

		return FormatDate("idiff",  time() - $seconds); // N минут
	}

	private function getText($text, $registerLinkText): string
	{
		if(strpos($text, '{{ZAPIS}}') !== false){
			$text = str_replace('{{ZAPIS}}', $registerLinkText, $text);
		}

		return $text;
	}

	private function getAuthor($authorId)
	{
		if(!$authorId){
			return null;
		}

		$res = \CIBlockElement::GetList([], ["ID" => $authorId]);

		if(!$bxElement = $res->GetNextElement()){
			return null;
		}

		return new Author($bxElement->GetFields(), $bxElement->GetProperties());
	}

	private function getDate($date)
	{
		return FormatDate("d.m.Y", strtotime(str_replace('.', '-', $date)));
	}

	private function getShareText(): string
	{
		$text = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/share.php");
		return $text ? $text : "";
	}

	private function getService($serviceId)
	{
		if(!$serviceId){
			return null;
		}

		$res = \CIBlockElement::GetList([], ["ID" => $serviceId]);

		if(!$bxElement = $res->GetNextElement()){
			return null;
		}

		return new Service($bxElement->GetFields());
	}

	private function getPrice($priceId)
	{
		if(!$priceId){
			return null;
		}

		$res = \CIBlockSection::GetByID($priceId);

		if(!$bxElement = $res->GetNextElement()){
			return null;
		}

		return new Price($bxElement->GetFields());
	}

	private function getSimilarArticles($articlesId): array
	{
		$articles = [];

		if(!$articlesId){
			return [];
		}

		$res = \CIBlockElement::GetList([], ["ID" => $articlesId]);

		while($bxElement = $res->GetNextElement()){
			$fields = $bxElement->GetFields();
			$article = new self();
			$article->title = $fields["NAME"];
			$article->image = getImageSrc($fields["PREVIEW_PICTURE"]);
			$article->url = $fields["DETAIL_PAGE_URL"];
			$articles[] = $article;
		}

		return $articles;
	}

	private function getContentsLinks(string $text)
	{
		$contentsLinks = [];
		require $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/SimpleHtmlDom/SimpleHtmlDomModel.php";
		require $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/SimpleHtmlDom/SimpleHtmlDomNodeModel.php";

		$simpleHtmlDomModel = new \SimpleHtmlDomModel($text);

		$resH2 = $simpleHtmlDomModel->find("h2");

		if($resH2){
			foreach ($resH2 as $h2){
				$contentsLinks[] = [
					"url" => $h2->id,
					"title" => $h2->plaintext
				];
			}
		}

		return $contentsLinks;
	}

	private function getFormTitle($service): string
	{
		$rootSectionName = !empty($service) ? $service->rootSectionName : "";
		return getRegisterToDoctorFormTitle($rootSectionName);
	}

}