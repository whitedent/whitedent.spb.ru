<?
namespace Articles;

class Author
{
	public $name;
	public $image;
	public $url;
	public $speciality;

	public function __construct(array $fields, array $properties)
	{
		$this->name = $fields["NAME"];
		$this->image = getImageSrc($fields["PREVIEW_PICTURE"]);
		$this->speciality = $this->getSpeciality($properties["SPECIALITY"]["VALUE"]);
		$this->url = $fields["DETAIL_PAGE_URL"];
	}

	private function getSpeciality($propValue)
	{
		return mb_ucfirst(mb_strtolower(implode(" / ", $propValue), "utf8"), "utf8");
	}

}