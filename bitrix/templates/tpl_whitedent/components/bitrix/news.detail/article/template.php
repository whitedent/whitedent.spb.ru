<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include_once __DIR__."/code/Author.php";
include_once __DIR__."/code/Service.php";
include_once __DIR__."/code/Price.php";
include_once __DIR__."/code/Article.php";
$article = new Articles\Article();
$article->loadData($arResult);
include_once __DIR__."/template/index.php";