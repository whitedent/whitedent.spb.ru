<?
namespace Consultation;

class Doctor
{
	public $name;
	public $image;
	public $url;
	public $speciality;
	public $experience;
	public $isActive;

	public function __construct($doctorId)
	{
		$element = getIbElement($doctorId);

		$this->name = $element->fields["NAME"];
		$this->image = getImageSrc($element->fields["PREVIEW_PICTURE"]);
		$this->url = $element->fields["DETAIL_PAGE_URL"];
		$this->speciality = $this->getSpeciality($element->props["SPECIALITY"]["VALUE"]);
		$this->experience = $element->props["EXPERIENCE"]["VALUE"];
		$this->isActive = $element->fields["ACTIVE"] == "Y";
	}


	private function getSpeciality($propValue)
	{
		return mb_ucfirst(mb_strtolower(implode(" / ", $propValue), "utf8"), "utf8");
	}

}