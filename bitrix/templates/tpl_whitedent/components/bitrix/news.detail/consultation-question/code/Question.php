<?
namespace Consultation;

class Question
{
	public $id;
	public $image;
	public $url;
	public $title;
	public $text;
	public $answer;
	public $author;
	public $date;
	public $doctor;

	public function __construct(array $arItem)
	{
		$this->id = $arItem["ID"];
		$this->image = $this->getImage($arItem["PROPERTIES"]["GENDER"]["VALUE"]);
		$this->url = $arItem["DETAIL_PAGE_URL"];
		$this->title = $arItem["NAME"];
		$this->text = $arItem["~PREVIEW_TEXT"];
		$this->answer = $arItem["~DETAIL_TEXT"];
		$this->author = $this->getAuthor($arItem);
		$this->date = $this->getDate($arItem["DATE_CREATE"]);
		$this->doctor = $this->getDoctor($arItem["PROPERTIES"]["RESPONDENT"]["VALUE"]);
	}

	private function getAuthor(array $arItem): string
	{
		$author = [];

		if(!empty($arItem["PROPERTIES"]["NAME"]["~VALUE"])){
			$author[] = $arItem["PROPERTIES"]["NAME"]["~VALUE"];
		}

		if(!empty($arItem["PROPERTIES"]["AGE"]["~VALUE"])){
			$yearsVal = $arItem["PROPERTIES"]["AGE"]["~VALUE"];
			$yearsText = getYearsAsText($arItem["PROPERTIES"]["AGE"]["~VALUE"]);
			$author[] = $yearsVal." ".$yearsText;
		}

		$author = implode(', ', $author);

		return $author;
	}

	private function getImage($propValue)
	{
		if(!$propValue){
			return false;
		}

		if($propValue == "Мужчина"){
			$gender = "m";
		}else{
			$gender = "w";
		}

		return "/images/consultation/portrait_".$gender.".svg";
	}

	private function getDate($date)
	{
		return mb_strtolower(FormatDate("d F Y", strtotime($date)));
	}

	private function getDoctor($doctorId)
	{
		if(!$doctorId){
			return false;
		}

		return new Doctor($doctorId);
	}

}
