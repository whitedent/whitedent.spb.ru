<? /** @var Consultation\Question $question */ ?>
<h2 class="consultation-question__title">Вопрос</h2>

<div class="consultation-question__body">

	<? if($question->image): ?>
		<div class="grid grid--padding-y grid--align-flex-start">
			<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-8">
				<img class="consultation-question__image" src="<?= $question->image ?>"/>
			</div>
			<div class="grid__cell grid__cell--l-9 grid__cell--m-8 grid__cell--xs-12">
				<p><?= $question->text ?></p>
			</div>
		</div>
	<? else: ?>
		<div class="grid grid--padding-y">
			<div class="grid__cell grid__cell--xs-12">
				<p><?= $question->text ?></p>
			</div>
		</div>
	<? endif ?>

</div>

<? include __DIR__."/info.php" ?>
