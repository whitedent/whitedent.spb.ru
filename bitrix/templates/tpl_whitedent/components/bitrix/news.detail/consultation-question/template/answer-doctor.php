<? /** @var Consultation\Question $question */ ?>
<? /** @var Consultation\Doctor $doctor */ ?>
<? $doctor = $question->doctor ?>
<div class="consultation-answer__author">
	<div class="grid grid--padding-y">

        <? if($doctor->image): ?>
		<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-8 consultation-answer__author-image">
			<img src="<?= $doctor->image ?>" alt="<?= $doctor->name ?>"/>
		</div>
        <? endif ?>

		<div class="grid__cell grid__cell--l-9 grid__cell--m-8 grid__cell--xs-12 consultation-answer__author-info">

			<div class="consultation-answer__author-title">Отвечает: </div>

			<div class="consultation-answer__author-name">
                <? if($doctor->isActive): ?>
				    <a href="<?= $doctor->url ?>"><?= $doctor->name ?></a>
                <? else: ?>
                    <span><?= $doctor->name ?></span>
				<? endif ?>
			</div>

			<div class="consultation-answer__author-occupation"><?= $doctor->speciality ?></div>

			<div class="consultation-answer__author-experience"><?= $doctor->experience ?></div>

		</div>

	</div>
</div>
