<? /** @var Consultation\Question $question */ ?>
<div class="block-marked consultation-answer">

	<h2 class="consultation-answer__title">Ответ</h2>

	<div class="consultation-answer__body">
        <p><?= $question->answer ?></p>
	</div>

    <? if($question->doctor): ?>
        <? include __DIR__."/answer-doctor.php" ?>
	<? endif ?>

</div>