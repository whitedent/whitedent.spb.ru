<? /** @var Consultation\Question $question */ ?>
<div class="page-subsection">

	<div class="block-marked block-marked--color-grey consultation-question">
		<? if($question->text): ?>
			<? include __DIR__."/question-with-text.php" ?>
		<? else: ?>
			<? include __DIR__."/question-without-text.php" ?>
		<? endif ?>
	</div>

    <? if($question->answer): ?>
	    <? include __DIR__."/answer.php" ?>
    <? endif ?>

</div>