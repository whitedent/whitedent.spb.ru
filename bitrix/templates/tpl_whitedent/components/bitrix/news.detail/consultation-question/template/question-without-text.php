<? /** @var Consultation\Question $question */ ?>
<? if($question->image): ?>
	<div class="consultation-question__body">
		<div class="grid grid--padding-y grid--align-flex-start">

			<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-8">
				<img class="consultation-question__image" src="<?= $question->image ?>"/>
			</div>

			<div class="grid__cell grid__cell--l-9 grid__cell--m-8 grid__cell--xs-12">
				<? include __DIR__."/info.php" ?>
			</div>

		</div>
	</div>
<? else: ?>
	<? include __DIR__."/info.php" ?>
<? endif ?>