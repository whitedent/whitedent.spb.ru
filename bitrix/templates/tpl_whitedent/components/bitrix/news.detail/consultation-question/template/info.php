<? /** @var Consultation\Question $question */ ?>
<div class="consultation-question__info">
    <div class="consultation-question__author"><?= $question->author ?></div>
    <div class="consultation-question__date"><?= $question->date ?></div>
</div>