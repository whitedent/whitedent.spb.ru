<?
global $APPLICATION;

$text = !empty($arResult["~PREVIEW_TEXT"]) ? $arResult["~PREVIEW_TEXT"] : $arResult["NAME"];
$previewText = getTruncatedDataForSeo($text);
$APPLICATION->SetPageProperty("description", $previewText);