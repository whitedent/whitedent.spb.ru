<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include_once __DIR__."/code/Doctor.php";
include_once __DIR__."/code/Review.php";
include_once __DIR__."/code/Work.php";
include_once __DIR__."/code/Action.php";
$action = new Actions\Action($arResult);
include_once __DIR__."/template/index.php";