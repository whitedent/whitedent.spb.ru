<? /** @var Actions\Review $review */ ?>
<li class="splide__slide">
	<div class="review-item" data-rating="<?= $review->rating ?>">
		<? include __DIR__."/review-image.php" ?>
		<? include __DIR__."/review-info.php" ?>
	</div>
</li>