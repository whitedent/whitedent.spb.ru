<? /** @var Actions\Action $action */ ?>
<? if($action->doctors): ?>
<section class="page-section container">
	<div class="content">

		<h2>Рекомендуемые врачи, оказывающие услугу</h2>

		<div class="page-subsection">
			<div class="splide slider-doctors">

				<div class="splide__track">
					<ul class="splide__list">
                        <? foreach ($action->doctors as $doctor): ?>
                            <? include __DIR__."/doctor.php" ?>
                        <? endforeach ?>
					</ul>
				</div>

				<? include __DIR__."/doctors-arrows.php" ?>

			</div>

		</div>

	</div>
</section>
<? endif ?>