<? /** @var Actions\Doctor $doctor */ ?>
<li class="splide__slide">
	<div class="doctor-item">
		<div class="grid grid--padding-y">

			<div class="grid__cell grid__cell--s-6 grid__cell--xs-hidden">
				<div class="doctor-item__photo">
					<? include __DIR__."/doctor-image.php" ?>
				</div>
			</div>

			<div class="grid__cell grid__cell--s-6 grid__cell--xs-12">
				<? include __DIR__."/doctor-info.php" ?>
			</div>

		</div>
	</div>
</li>