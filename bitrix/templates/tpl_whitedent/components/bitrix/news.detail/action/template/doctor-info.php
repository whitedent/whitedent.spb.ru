<? /** @var Actions\Doctor $doctor */ ?>
<div class="doctor-item__info">

	<div class="doctor-item__photo hidden-hd hidden-xl hidden-l hidden-m hidden-s">
		<? include __DIR__."/doctor-image.php" ?>
	</div>

	<div class="doctor-item__name"><?= $doctor->nameFormatted ?></div>

	<? if($doctor->speciality): ?>
		<div class="doctor-item__block">
			<div class="doctor-item__occupation"><?= $doctor->speciality ?></div>
		</div>
	<? endif ?>

	<a class="doctor-item__button" href="<?= $doctor->url ?>">Подробнее</a>

</div>
