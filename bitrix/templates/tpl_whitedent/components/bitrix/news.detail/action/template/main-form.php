<? /** @var Actions\Action $action */ ?>
<div class="discount-header__form-header">Запишитесь на консультацию</div>

<form
    class="discount-header__form consultation-new-registration-popup validated-form submitJS"
    method="post"
>

    <input type="text" name="req_name">

    <div class="form-input form-input--text">
        <input class="form-input__field" type="text" name="name" required placeholder="ФИО">
    </div>

    <div class="form-input form-input--tel">
		<input class="form-input__field" type="tel" name="phone" required placeholder="Телефон">
	</div>

    <div class="form-input form-input--hidden">
        <input class="form-input__field" type="hidden" name="element_id" value="<?= $action->id ?>">
        <input class="form-input__field" type="hidden" name="page_type" value="discount">
    </div>

	<button class="button" type="submit">Записаться</button>

</form>