<? /** @var Actions\Action $action */ ?>
<section class="page-section container" data-id="id_<?= $action->id ?>">
	<div class="content">
		<? include __DIR__."/main.php" ?>
		<? include __DIR__."/text.php" ?>
	</div>
</section>