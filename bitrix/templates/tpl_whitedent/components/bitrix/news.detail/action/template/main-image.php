<? /** @var Actions\Action $action */ ?>
<? if($action->image): ?>
    <div class="grid__cell grid__cell--l-3 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden discount-header__image">
        <?= PictureTag::renderWithMQ($action->image, '', $action->title, $action->title) ?>
    </div>
<? endif ?>
