<? /** @var Actions\Work $work */ ?>
<div class="grid__cell grid__cell--s-6 grid__cell--xs-12 work-item__element">
	<a
		class="gallery-item"
		href="<?= $work->photoBefore ?>"
		data-glightbox=""
		data-gallery="gallery_<?= $work->id ?>"
	>
		<img src="<?= $work->photoBefore ?>" alt="<?= $work->title ?>" />
	</a>
</div>

<div class="grid__cell grid__cell--s-6 grid__cell--xs-12 work-item__element">
	<a
		class="gallery-item"
		href="<?= $work->photoAfter ?>"
		data-glightbox=""
		data-gallery="gallery_<?= $work->id ?>"
	>
		<img src="<?= $work->photoAfter ?>" alt="<?= $work->title ?>" />
	</a>
</div>