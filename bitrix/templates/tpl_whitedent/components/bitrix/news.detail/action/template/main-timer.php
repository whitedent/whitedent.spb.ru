<? /** @var Actions\Action $action */ ?>
<? if($action->isActual()): ?>
    <div class="discount-header__date">до <?= $action->expiredAsText ?></div>
    <div
        class="countdown discount-header__countdown"
        data-countdown="<?= $action->expiredAsValue ?>"
        id="countdown"
    >
        <div class="countdown__position">
            <div class="countdown__digits--days countdown__digits"></div><span>дней</span>
        </div>
        <div class="countdown__position">
            <div class="countdown__digits--hours countdown__digits"></div><span>часов</span>
        </div>
        <div class="countdown__position">
            <div class="countdown__digits--minutes countdown__digits"></div><span>минут</span>
        </div>
        <div class="countdown__position">
            <div class="countdown__digits--seconds countdown__digits"></div><span>секунд</span>
        </div>
    </div>
<? endif ?>