<? /** @var Actions\Action $action */ ?>
<? if($action->reviews): ?>
<section class="page-section container">
	<div class="content">

        <? include __DIR__."/reviews-top.php" ?>

		<div class="page-subsection">
			<div class="splide">

				<div class="splide__track">
					<ul class="splide__list">
                        <? foreach ($action->reviews as $review): ?>
                            <? include __DIR__."/review.php" ?>
                        <? endforeach ?>
					</ul>
				</div>

				<? include __DIR__."/reviews-arrows.php" ?>

			</div>
		</div>

	</div>
</section>
<? endif ?>