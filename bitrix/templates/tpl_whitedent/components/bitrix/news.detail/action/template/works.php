<? /** @var Actions\Action $action */ ?>
<? if($action->works): ?>
<section class="page-section container">
	<div class="content">

		<h2>Результаты работы</h2>

		<div class="page-subsection">
			<div class="grid grid--padding-y">
                <? foreach ($action->works as $work): ?>
                    <? include __DIR__."/work.php" ?>
                <? endforeach ?>
			</div>
		</div>

	</div>
</section>
<? endif ?>