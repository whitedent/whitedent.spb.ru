<? /** @var Actions\Work $work */ ?>
<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
	<div class="block-marked work-item">

		<div class="work-item__name"><?= $work->title ?></div>

		<div class="grid grid--padding-y work-item__elements">
			<? include __DIR__."/work-photos.php" ?>
		</div>

		<div class="work-item__features">
			<? include __DIR__."/work-features.php" ?>
		</div>

	</div>
</div>