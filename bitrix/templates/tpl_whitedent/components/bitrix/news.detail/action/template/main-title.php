<? /** @var Actions\Action $action */ ?>
<div class="grid__cell grid__cell--l-5 grid__cell--m-6 grid__cell--xs-12 discount-header__body">
	<h1 class="discount-header__name"><?= $action->title ?></h1>
	<div class="discount-header__additional"><?= $action->description ?></div>
</div>