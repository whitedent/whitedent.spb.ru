<div class="page-subsection">
	<div class="discount-header">
		<div class="grid grid--padding-y grid--align-center">

            <? include __DIR__."/main-image.php" ?>

            <? include __DIR__."/main-title.php" ?>

            <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--xs-12 discount-header__sign">
                <? include __DIR__."/main-timer.php" ?>
                <? include __DIR__."/main-form.php" ?>
            </div>

		</div>
	</div>
</div>
