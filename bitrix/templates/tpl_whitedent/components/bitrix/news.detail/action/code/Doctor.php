<?
namespace Actions;

class Doctor
{
	public $name;
	public $nameFormatted;
	public $image;
	public $speciality;
	public $url;

	public function __construct($elementId)
	{
		$element = getIbElement($elementId);

		$this->name = $element->fields["NAME"];
		$this->nameFormatted = $this->getNameFormatted($this->name);
		$this->image = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);
		$this->speciality = $this->getSpeciality($element->props["SPECIALITY"]["VALUE"]);
		$this->url = $element->fields["DETAIL_PAGE_URL"];
	}

	private function getNameFormatted(string $name): string
	{
		$parts = explode(" ", $name);
		$parts[0] = "<strong>".$parts[0]."</strong>";
		return implode(" ", $parts);
	}

	private function getSpeciality($propValue)
	{
		return mb_ucfirst(mb_strtolower(implode(" / ", $propValue), "utf8"), "utf8");
	}

	private function getImageSrc($imageId)
	{
		return !empty($imageId) ? \CFile::GetPath($imageId) : null;
	}

}