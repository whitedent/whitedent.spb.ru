<?
namespace Actions;

class Action
{
	public $id;
	public $image;
	public $title;
	public $expiredAsText;
	public $expiredAsValue;
	public $description;
	public $text;
	public $doctors;
	public $reviews;
	public $works;

	private $expiredTimeStamp;

	public function __construct(array $arResult)
	{
		$this->id = $arResult["ID"];
		$this->image = $arResult["PREVIEW_PICTURE"]["SRC"];
		$this->title = $arResult["PROPERTIES"]["TITLE"]["~VALUE"];
		$this->description = $arResult["~PREVIEW_TEXT"];
		$this->text = $arResult["~DETAIL_TEXT"];
		$this->expiredTimeStamp = MakeTimeStamp($arResult["PROPERTIES"]["EXPIRED"]["VALUE"]);
		$this->expiredAsText = $this->getExpiredAsText($this->expiredTimeStamp);
		$this->expiredAsValue = $this->getExpiredAsValue($this->expiredTimeStamp);
		$this->doctors = $this->getDoctors($arResult["PROPERTIES"]["DOCTORS"]["VALUE"]);
		$this->reviews = $this->getReviews($arResult["PROPERTIES"]["REVIEWS"]["VALUE"]);
		$this->works = $this->getWorks($arResult["PROPERTIES"]["WORKS"]["VALUE"]);
	}

	private function getExpiredAsText($expiredTimeStamp)
	{
		if(!$expiredTimeStamp){
			return false;
		}

		return mb_strtolower(FormatDate("d F", $expiredTimeStamp), "utf8");
	}

	private function getExpiredAsValue($expiredTimeStamp)
	{
		if(!$expiredTimeStamp){
			return false;
		}

		return date("Y-m-d", $expiredTimeStamp);
	}

	public function isActual(): bool
	{
		return $this->expiredTimeStamp && ($this->expiredTimeStamp > time());
	}

	private function getDoctors($doctorsId): array
	{
		if(!$doctorsId){return [];}

		$doctors = [];
		foreach ($doctorsId as $doctorId){
			$doctors[] = new Doctor($doctorId);
		}

		return $doctors;
	}

	private function getReviews($reviewsId): array
	{
		if(!$reviewsId){return [];}

		$reviews = [];
		foreach ($reviewsId as $reviewId){
			$reviews[] = new Review($reviewId);
		}

		return $reviews;
	}

	private function getWorks($worksId): array
	{
		if(!$worksId){return [];}

		$works = [];
		foreach ($worksId as $workId){
			$works[] = new Work($workId);
		}

		return $works;
	}

}