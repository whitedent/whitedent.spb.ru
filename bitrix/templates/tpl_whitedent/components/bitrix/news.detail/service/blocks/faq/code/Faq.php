<?
namespace Medreclama\Service;

class Faq extends Block
{
	public $questions;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		require "Question.php";
		$this->questions = $this->getQuestions($this->arResult["PROPERTIES"]["FAQ"]["VALUE"]);
	}

	private function getQuestions($questionsId): array
	{
		$questions = [];

		$elements = $this->getElements($questionsId);
		/**  @var Element $element */
		foreach ($elements as $element) {
			$questions[] = new Question($element);
		}

		return $questions;
	}

	public function isShow(): bool
	{
		return (bool)$this->questions;
	}

}