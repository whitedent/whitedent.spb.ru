<? /** @var Medreclama\Service\Work $work */ ?>
<? if($work->doctor): ?>
    <div class="work-item__feature">
        <div class="work-item__feature-name">Врач:</div>
        <div class="work-item__feature-value">
            <a href="<?= $work->doctor["url"] ?>"><?= $work->doctor["name"] ?></a>
        </div>
    </div>
<? endif ?>

<? if($work->services): ?>
    <div class="work-item__feature">
        <div class="work-item__feature-name">Услуги:</div>
        <div class="work-item__feature-value"><?= $work->services ?></div>
    </div>
<? endif ?>
