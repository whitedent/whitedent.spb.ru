<?
namespace Medreclama\Service;

class RegisterForm extends Block
{
	public $title;
	public $serviceId;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		$this->title = $this->getTitle($arResult["SECTION"]["PATH"]);
		$this->serviceId = $arResult["ID"];
	}

	private function getTitle($sectionPath): string
	{
		$rootSectionName = !empty($sectionPath[0]) ? $sectionPath[0]["NAME"] : "";
		return getRegisterToDoctorFormTitle($rootSectionName);
	}


}