<? /** @var Medreclama\Service\Articles $block */ ?>
<section class="page-section container">
	<div class="content">

		<h2>Статьи по теме</h2>

		<div class="page-subsection">
			<div class="grid grid--padding-y">
                <? foreach ($block->articles as $article): ?>
                    <? include __DIR__."/article.php" ?>
                <? endforeach ?>
			</div>
		</div>

	</div>
</section>