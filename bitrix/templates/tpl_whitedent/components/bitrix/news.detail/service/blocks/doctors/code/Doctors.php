<?
namespace Medreclama\Service;

class Doctors extends Block
{
	public $doctors;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		require "Doctor.php";
		$this->doctors = $this->getDoctors($this->arResult["PROPERTIES"]["DOCTORS"]["VALUE"]);
	}

	private function getDoctors($doctorsId): array
	{
		$doctors = [];

		$elements = $this->getElements($doctorsId);

		/**  @var Element $element */
		foreach ($elements as $element) {
			$doctor = new Doctor($element);
			$doctors[] = $doctor;
		}

		return $doctors;
	}

	public function isShow(): bool
	{
		return (bool)$this->doctors;
	}

}