<? /** @var Medreclama\Service\Top $block */ ?>
<? /** @var Medreclama\Service\SubItem $subItem */ ?>
<div class="grid__cell grid__cell--l-9 grid__cell--m-8 grid__cell--s-12 grid__cell--xs-12 block-marked-image__body">
    <div class="block-marked-image__body-inner">
        <ul class="grid grid--padding-y grid--padding-small list-marked list-marked--arrow">

            <? foreach ($block->subItems as $subItem): ?>
                <li class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--xs-12">
                    <a href="<?= $subItem->url ?>"><?= $subItem->title ?></a>
                </li>
            <? endforeach ?>

        </ul>
    </div>
</div>
