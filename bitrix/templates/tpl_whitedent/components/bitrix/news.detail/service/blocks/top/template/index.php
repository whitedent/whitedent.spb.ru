<? /** @var Medreclama\Service\Top $block */ ?>
<section class="page-section container">
	<div class="content">

		<h1><?= $block->name ?></h1>

        <? if($block->image OR $block->subItems): ?>
		<div class="page-subsection">
			<div class="block-marked-image block-marked-image--image-to-right">
				<div class="grid grid--par-like">

					<? if($block->image): ?>
                        <? include __DIR__."/image.php" ?>
					<? endif ?>

					<? if($block->subItems): ?>
                        <? include __DIR__."/sub-items.php" ?>
					<? endif ?>

				</div>
			</div>
		</div>
        <? endif ?>

	</div>
</section>
