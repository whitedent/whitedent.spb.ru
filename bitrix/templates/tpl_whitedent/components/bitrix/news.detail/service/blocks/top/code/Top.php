<?
namespace Medreclama\Service;

class Top extends Block
{
	public $name;
	public $image;
	public $subItems;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		require "SubItem.php";

		$this->name = $this->getName();
		$this->image = $arResult["DETAIL_PICTURE"]["SRC"];
		$this->subItems = $this->getSubItems();
	}

	private function getName(): string
	{
		if($this->arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]){
			return $this->arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"];
		}

		return $this->arResult["NAME"];
	}

	private function getSubItems(): array
	{
		$parentSection = end($this->arResult["SECTION"]["PATH"]);
		$parentSectionId = (int)($parentSection["ID"]);

		$arFilter = [
			"IBLOCK_ID" => ID_IBLOCK_SERVICES,
			"ACTIVE" => "Y",
			"SECTION_ID" => $parentSectionId
		];

		$select = ["ID", "IBLOCK_ID", "IBLOCK_TYPE_ID", "NAME", "IBLOCK_SECTION_ID", "SECTION_PAGE_URL", "CODE"];

		$subItems = [];
		$res = \CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, false, $select);
		while($ar_result = $res->GetNext()) {
			$subItems[] = new SubItem($ar_result["NAME"], $ar_result["SECTION_PAGE_URL"]);
		}

		return $subItems;
	}

}