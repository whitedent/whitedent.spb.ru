<? /** @var Medreclama\Service\Action $block */ ?>
<section class="page-section container">
    <div class="content">

        <div class="page-subsection">
            <div class="grid grid--align-center grid--nopadding discount-banner discount-banner--no-image">
                <div class="grid__cell grid__cell--xs-12">
                    <div class="discount-banner__body">

                        <div class="discount-banner__info">
                            <div class="discount-banner__name"><?= $block->title ?></div>
                            <div class="discount-banner__additional"><?= $block->text ?></div>
                        </div>

                        <div class="discount-banner__buttons">
                            <a
                                class="discount-banner__more arrow"
                                href="<?= $block->url ?>"
                            >Подробнее</a>

                            <? if($block->expired): ?>
                                <div class="discount-banner__date">Акция действует до <?= $block->expired ?></div>
                            <? endif ?>

                            <a
                                class="button discount-banner__button discount-form-open"
                                href="#discount-form"
                                data-from="<?= $block->title ?>"
                                data-elementid="<?= $block->id ?>"
                                data-type="discount"
                            >Записаться</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>