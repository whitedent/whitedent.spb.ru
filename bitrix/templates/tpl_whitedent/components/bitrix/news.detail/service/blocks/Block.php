<?
namespace Medreclama\Service;

class Block
{
	protected $arResult;

	public function __construct(array $arResult)
	{
		$this->arResult = $arResult;
	}

	public function render(): string
	{
		if(!$this->isShow()){
			return "";
		}

		$templatePath = $this->getTemplatePath();

		$result = "";

		ob_start();
		$block = $this; // $block - переменная доступная в шаблоне, указывает на класс блока
		require $templatePath;
		$result .= ob_get_clean();

		return $result;
	}

	protected function getElement($elementId): ?Element
	{
		if(!$elementId){
			return null;
		}

		$res = \CIBlockElement::GetList([], ["ID" => $elementId]);

		if(!$bxElement = $res->GetNextElement()){
			return null;
		}

		$element = new Element;
		$element->fields = $bxElement->GetFields();
		$element->props = $bxElement->GetProperties();

		return $element;
	}

	protected function getElements($elementsId): array
	{
		if(empty($elementsId)){
			return [];
		}

		$res = \CIBlockElement::GetList(["id" => $elementsId], ["ID" => $elementsId]);

		$elements = [];

		while($bxElement = $res->GetNextElement()){
			$element = new Element;
			$element->fields = $bxElement->GetFields();
			$element->props = $bxElement->GetProperties();
			$elements[] = $element;
		}

		return $elements;
	}

	protected function getImages($imagesId)
	{
		if(empty($imagesId)){
			return false;
		}

		$images = [];

		foreach ($imagesId as $imageId){
			$images[] = $this->getImageSrc($imageId);
		}

		return $images;
	}

	protected function getImageSrc($imageId)
	{
		return !empty($imageId) ? \CFile::GetPath($imageId) : null;
	}

	public function isShow(): bool
	{
		return true;
	}

	private function getTemplatePath(): string
	{
		$reflection = new \ReflectionClass($this);
		$classFileName = $reflection->getFileName();
		$blockDirectory = dirname(dirname($classFileName));
		$templatePath = $blockDirectory."/template/index.php";

		if(!file_exists($templatePath)){
			throw new \Exception("Не найден файл шаблона для блока: ".$classFileName);
		}

		return $templatePath;
	}

}