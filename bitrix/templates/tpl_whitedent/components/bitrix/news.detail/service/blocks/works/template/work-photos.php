<? /** @var Medreclama\Service\Work $work */ ?>
<div class="grid__cell grid__cell--s-6 grid__cell--xs-12 work-item__element">
	<a
		class="gallery-item"
        href="<?= $work->photoBefore ?>"
		data-glightbox=""
        data-gallery="gallery_<?= $work->id ?>"
	>
				<?= PictureTag::renderWithMQ($work->photoBefore, "", $work->title, $work->title) ?>

	</a>
</div>

<div class="grid__cell grid__cell--s-6 grid__cell--xs-12 work-item__element">
	<a
		class="gallery-item"
        href="<?= $work->photoAfter ?>"
		data-glightbox=""
        data-gallery="gallery_<?= $work->id ?>"
	>
				<?= PictureTag::renderWithMQ($work->photoAfter, "", $work->title, $work->title) ?>

	</a>
</div>
