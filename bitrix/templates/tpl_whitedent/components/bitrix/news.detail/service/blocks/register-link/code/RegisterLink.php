<?
namespace Medreclama\Service;

class RegisterLink extends Block
{
	public $text;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->text = $this->arResult["PROPERTIES"]["REGISTER_LINK"]["~VALUE"]["TEXT"];
	}

}