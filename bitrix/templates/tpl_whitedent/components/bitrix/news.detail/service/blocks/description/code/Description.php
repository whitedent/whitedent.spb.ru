<?
namespace Medreclama\Service;

class Description extends Block
{
	public $text;

	private $helper;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->helper = new Helper($this->arResult);
		$this->text = $this->getText($this->arResult["DETAIL_TEXT"]);
	}

	private function getText($text): string
	{
		if(strpos($text, '{{TSENY}}') !== false){
			$text = str_replace('{{TSENY}}', $this->getPrices(), $text);
		}

		if(strpos($text, '{{ZAPIS}}') !== false){
			$text = str_replace('{{ZAPIS}}', $this->getRegisterLink(), $text);
		}

		return $text;
	}

	private function getPrices(): string
	{
		return $this->helper->getBlock("prices")->render();
	}

	private function getRegisterLink(): string
	{
		return $this->helper->getBlock("register-link")->render();
	}

}