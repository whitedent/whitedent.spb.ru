<?
namespace Medreclama\Service;

class Price
{
	public $title;
	public $value;
	public $oldValue;

	public function __construct(Element $element)
	{
		$this->title = $element->fields["NAME"];
		$this->value = $this->getValue($element->props["PRICE"]["VALUE"]);
		$this->oldValue = $this->getValue($element->props["OLD_PRICE"]["VALUE"]);
	}

	private function getValue($value)
	{
		if(!$value){
			return "";
		}

		return is_numeric($value) ? $this->format($value) : $value;
	}

	private function format($number)
	{
		return number_format($number, 0, '.', ' ')." ₽";
	}

}