<?
namespace Medreclama\Service;

class Question
{
	public $text;
	public $answer;

	public function __construct(Element $element)
	{
		$this->text = $element->fields["PREVIEW_TEXT"];
		$this->answer = $element->fields["DETAIL_TEXT"];
	}

}