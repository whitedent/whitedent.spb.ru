<div class="grid__cell grid__cell--l-12 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
	<div class="block-marked block-icon block-icon--discount block-icon--translate">
		<div class="block-icon__title">Скидки</div>
		<div class="block-icon__body">
			<p>Мы предоставляем скидки и индивидуальные условия в случаях дорогостоящего лечения.</p>
		</div>
	</div>
</div>

<div class="grid__cell grid__cell--l-12 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
	<div class="block-marked block-icon block-icon--credit block-icon--translate">
		<div class="block-icon__title">Кредит и рассрочка</div>
		<div class="block-icon__body">
			<p>Выгодные условия кредитования от Альфа банк. Рассрочка платежа (для постоянных клиентов при длительном лечении).</p>
		</div>
	</div>
</div>