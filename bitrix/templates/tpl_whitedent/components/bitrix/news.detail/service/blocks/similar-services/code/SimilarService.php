<?
namespace Medreclama\Service;

class SimilarService
{
	public $title;
	public $image;
	public $url;

	public function __construct(Element $element)
	{
		$this->title = $element->fields["NAME"];
		$this->image = getImageSrc($element->fields["DETAIL_PICTURE"]);
		$this->url = $element->fields["DETAIL_PAGE_URL"];
	}

}