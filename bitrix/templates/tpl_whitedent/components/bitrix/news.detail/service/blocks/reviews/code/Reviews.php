<?
namespace Medreclama\Service;

class Reviews extends Block
{
	public $reviews;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		require "Review.php";
		$this->reviews = $this->getReviews($this->arResult["PROPERTIES"]["REVIEWS"]["VALUE"]);
	}

	private function getReviews($reviewsId): array
	{
		$reviews = [];

		$elements = $this->getElements($reviewsId);
		/**  @var Element $element */
		foreach ($elements as $element) {
			$review = new Review($element);
			$reviews[] = $review;
		}

		return $reviews;
	}

	public function isShow(): bool
	{
		return (bool)$this->reviews;
	}

}