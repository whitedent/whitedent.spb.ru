<? /** @var Medreclama\Service\Faq $block */ ?>
<? /** @var Medreclama\Service\Question $question */ ?>
<section class="page-section container">
	<div class="content">

		<h2>Часто задаваемые вопросы</h2>

		<div class="page-subsection">
			<div class="block-marked block-marked--background block-marked--background-question">

                <? if($block->questions): ?>
                    <? foreach ($block->questions as $question): ?>
                        <h3><?= $question->text ?></h3>
                        <p><?= $question->answer ?></p>
                    <? endforeach ?>
                <? endif ?>

                <? /*<div class="grid grid--padding-y grid--par-like grid--align-center">
                     <? include __DIR__."/button.php" ?> 
				</div>*/?>

			</div>
		</div>

	</div>
</section>
