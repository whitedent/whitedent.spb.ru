<? /** @var Medreclama\Service\SimilarServices $block */ ?>
<section class="page-section container">
	<div class="content">

		<h2>Похожие процедуры</h2>

		<div class="page-subsection">
			<div class="grid grid--padding-y grid--justify-center">
                <? foreach ($block->similarServices as $similarService): ?>
                    <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--s-6 grid__cell--xs-12">
                        <? include __DIR__."/item.php" ?>
                    </div>
                <? endforeach ?>
			</div>
		</div>

	</div>
</section>