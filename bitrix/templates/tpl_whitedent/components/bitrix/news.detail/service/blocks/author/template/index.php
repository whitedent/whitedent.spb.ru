<? /** @var Medreclama\Service\Author $block */ ?>
<section class="page-section container">
	<div class="content">

        <div class="page-subsection">
            <div class="grid grid--align-center article-author">

                <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-6 grid__cell--xs-8 article-author__image">

										<?= PictureTag::renderWithMQ($block->image, "", $block->name, $block->name) ?>

                </div>

                <div class="grid__cell grid__cell--l-10 grid__cell--m-9 grid__cell--s-12 grid__cell--xs-12">
                    <div class="article-author__body">
                        <div class="grid grid--padding-y">
                            <div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12 article-author__info">
                                <? include __DIR__."/info.php" ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
		</div>

        <? if($block->shareText): ?>
        <div class="page-subsection">
            <div class="article-footer">
                <div class="article-footer__share">
					<?= $block->shareText ?>
                </div>
            </div>
        </div>
        <? endif ?>

	</div>
</section>
