<?
namespace Medreclama\Service;

class Articles extends Block
{
	public $articles;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		require "Article.php";
		$this->articles = $this->getArticles($this->arResult["PROPERTIES"]["ARTICLES"]["VALUE"]);
	}

	private function getArticles($articlesId): array
	{
		$articles = [];

		$elements = $this->getElements($articlesId);
		/**  @var Element $element */
		foreach ($elements as $element) {
			$article = new Article;
			$article->title = $element->fields["NAME"];
			$article->image = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);
			$article->url = $element->fields["DETAIL_PAGE_URL"];
			$articles[] = $article;
		}

		return $articles;
	}

	public function isShow(): bool
	{
		return (bool)$this->articles;
	}

}