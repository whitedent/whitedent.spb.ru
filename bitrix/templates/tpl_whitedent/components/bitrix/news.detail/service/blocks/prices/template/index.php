<? /** @var Medreclama\Service\Prices $block */ ?>
<section class="page-section container">
	<div class="content">

		<h2>Цены</h2>

		<div class="page-subsection">
			<div class="grid grid--padding-y">

				<div class="grid__cell grid__cell--l-9 grid__cell--xs-12">
					<div class="block-marked table-price table-price--padding-right">

                        <? if($block->prices): ?>
                            <div class="table-price__body">
                                <? foreach ($block->prices as $price): ?>
									<? include __DIR__."/price.php" ?>
                                <? endforeach ?>
                            </div>
                            <? include __DIR__."/footer.php" ?>
                        <? endif ?>

					</div>
				</div>

				<div class="grid__cell grid__cell--l-3 grid__cell--xs-12">
					<div class="grid grid--padding-y">
						<? include __DIR__."/description.php" ?>
					</div>
				</div>

			</div>
		</div>

	</div>
</section>