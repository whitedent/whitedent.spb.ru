<? /** @var Medreclama\Service\Doctors $block */ ?>
<section class="page-section container">
	<div class="content">

		<h2>Рекомендуемые врачи, оказывающие услугу</h2>

		<div class="page-subsection">
			<div class="splide slider-doctors">

				<div class="splide__track">
					<ul class="splide__list">
                        <? foreach ($block->doctors as $doctor): ?>
                            <? include __DIR__."/item.php" ?>
                        <? endforeach ?>
					</ul>
				</div>

				<? include __DIR__."/arrows.php" ?>

			</div>
		</div>

	</div>
</section>