<?
namespace Medreclama\Service;

class Price
{
	public $title;
	public $value;
	public $oldValue;

	private $props;

	public function __construct(Element $element)
	{
		$this->props = $element->props;

		$this->title = $element->fields["NAME"];
		//$this->value = $this->getValue($element->props["PRICE"]["VALUE"]);
		$this->value = $this->getMainPrice();
		$this->oldValue = $this->getValue($element->props["OLD_PRICE"]["VALUE"]);
	}

	private function getValue($value)
	{
		if(!$value){
			return "";
		}

		return is_numeric($value) ? $this->format($value) : $value;
	}

	private function getMainPrice()
	{
		if(!$this->props["PRICES_RANGE"]["VALUE"]){
			return $this->getValue($this->props["PRICE"]["VALUE"]);
		}

		$pricesRangeId = $this->props["PRICES_RANGE"]["VALUE"];
		$pricesRange = $this->getPricesRange($pricesRangeId);

		if(count($pricesRange) == 1 || $this->props["SHOW_ONLY_MIN_PRICES_RANGE"]["VALUE"] == "Y"){
			$price = "от ".$this->getValue(reset($pricesRange));
		}else{
			$price = $this->getValue(reset($pricesRange))."&nbsp;–&nbsp;".$this->getValue(end($pricesRange));
		}

		return $price;
	}

	private function getPricesRange($pricesRangeId)
	{
		$pricesRange = [];

		$res = \CIBlockElement::GetList(
			["SORT" => "ASC"],
			[
				"IBLOCK_ID" => ID_IBLOCK_PRICES,
				"ID" => $pricesRangeId,
			]
		);

		while($bxElement = $res->GetNextElement()){
			$props = $bxElement->GetProperties();
			$pricesRange[] = (int)$props["PRICE"]["VALUE"];
		}

		asort($pricesRange);

		return $pricesRange;
	}

	private function format($number)
	{
		return number_format($number, 0, '.', ' ')." ₽";
	}

}
