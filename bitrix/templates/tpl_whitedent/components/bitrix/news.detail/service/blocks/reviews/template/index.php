<? /** @var Medreclama\Service\Reviews $block */ ?>
<section class="page-section container">
	<div class="content">

		<div class="grid grid--padding-y grid--justify-space-between">
            <? include __DIR__."/head.php" ?>
		</div>

		<div class="page-subsection">
			<div class="splide">
                <div class="splide__track">
                    <ul class="splide__list">
						<? foreach ($block->reviews as $review): ?>
							<? include __DIR__."/item.php" ?>
						<? endforeach ?>
                    </ul>
                </div>
                <? include __DIR__."/arrows.php" ?>
			</div>
		</div>

	</div>
</section>