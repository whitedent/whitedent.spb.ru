<? /** @var Medreclama\Service\WhyWe $block */ ?>
<section class="page-section container">
    <div class="content">
        <?= $block->text ?>
    </div>
</section>