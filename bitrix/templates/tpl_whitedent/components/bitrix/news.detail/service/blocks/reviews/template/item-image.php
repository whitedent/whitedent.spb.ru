<? /** @var Medreclama\Service\Review $review */ ?>
<div class="review-item__image">

	<? if($review->image): ?>
	<a href="<?= $review->image ?>" data-glightbox="">

		<?= PictureTag::renderWithMQ($review->image, "", $review->title, $review->title) ?>

	</a>
	<? endif ?>

	<div class="review-item__author">

		<div class="review-item__author-image">
			<img src="/images/consultation/portrait_<?= $review->gender ?>.svg" alt=""/>
		</div>

		<? if($review->author): ?>
			<div class="review-item__author-name"><?= $review->author ?></div>
		<? endif ?>

		<div class="review-item__date"><?= $review->date ?></div>

	</div>

</div>
