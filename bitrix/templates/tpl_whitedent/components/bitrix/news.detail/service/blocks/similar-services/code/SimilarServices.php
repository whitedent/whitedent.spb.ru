<?
namespace Medreclama\Service;

class SimilarServices extends Block
{
	public $similarServices;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		require "SimilarService.php";
		$this->similarServices = $this->getSimilarServices($this->arResult["PROPERTIES"]["SIMILAR_SERVICES"]["VALUE"]);
	}

	private function getSimilarServices($ids): array
	{
		$services = [];

		$elements = $this->getElements($ids);
		/**  @var Element $element */
		foreach ($elements as $element) {
			$service = new SimilarService($element);
			$services[] = $service;
		}

		return $services;
	}

	public function isShow(): bool
	{
		return (bool)$this->similarServices;
	}
}