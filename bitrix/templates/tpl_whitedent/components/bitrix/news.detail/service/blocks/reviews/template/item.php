<? /** @var Medreclama\Service\Review $review */ ?>
<li class="splide__slide">
	<div class="review-item" data-rating="<?= $review->rating ?>">
		<? include __DIR__."/item-image.php" ?>
		<? include __DIR__."/item-info.php" ?>
	</div>
</li>
