<?
namespace Medreclama\Service;

class Author extends Block
{
	public $name;
	public $image;
	public $url;
	public $speciality;
	public $datePublication;
	public $dateUpdate;
	public $shareText;

	private $author;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->author = $this->getAuthor($this->arResult["PROPERTIES"]["AUTHOR"]["VALUE"]);

		if ($this->author) {
			$this->name = $this->author->fields["NAME"];
			$this->image = $this->getImageSrc($this->author->fields["PREVIEW_PICTURE"]);
			$this->speciality = $this->getSpeciality($this->author->props["SPECIALITY"]["VALUE"]);
			$this->url = $this->author->fields["DETAIL_PAGE_URL"];
			$this->datePublication = $this->getDate($this->arResult["CREATED_DATE"]);
			$this->dateUpdate = FormatDate("d.m.Y", strtotime($arResult["TIMESTAMP_X"]));
		}

		$this->shareText = $this->getShareText();
	}

	private function getSpeciality($propValue)
	{
		return mb_ucfirst(mb_strtolower(implode(" / ", $propValue), "utf8"), "utf8");
	}

	private function getAuthor($authorId)
	{
		return $this->getElement((int)$authorId);
	}

	public function isShow(): bool
	{
		return (bool)$this->author;
	}

	private function getDate($date)
	{
		return FormatDate("d.m.Y", strtotime(str_replace('.', '-', $date)));
	}

	private function getShareText(): string
	{
		$text = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/share.php");
		return $text ? $text : "";
	}

}