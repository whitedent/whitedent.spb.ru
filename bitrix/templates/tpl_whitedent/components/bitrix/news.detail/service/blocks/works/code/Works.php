<?
namespace Medreclama\Service;

class Works extends Block
{
	public $works;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		require "Work.php";
		$this->works = $this->getWorks($this->arResult["PROPERTIES"]["WORKS"]["VALUE"]);
	}

	private function getWorks($worksId): array
	{
		$works = [];

		$elements = $this->getElements($worksId);
		/**  @var Element $element */
		foreach ($elements as $element) {
			$work = new Work($element);
			$works[] = $work;
		}

		return $works;
	}

	public function isShow(): bool
	{
		return (bool)$this->works;
	}

}