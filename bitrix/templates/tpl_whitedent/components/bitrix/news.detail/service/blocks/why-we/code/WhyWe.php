<?
namespace Medreclama\Service;

class WhyWe extends Block
{
	public $text;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);
		$this->text = $this->getText();
	}

	private function getText(): string
	{
		$text = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/uslugi/why-we.php");
		return $text ? $text : "";
	}

	public function isShow(): bool
	{
		return (bool)$this->text;
	}

}