<? /** @var Medreclama\Service\Author $block */ ?>
<div class="article-author__title">Автор статьи:</div>

<div class="article-author__name">
	<a href="<?= $block->url ?>"><?= $block->name ?></a>
</div>

<div class="article-author__occupation"><?= $block->speciality ?></div>

<div class="article-author__date">
	Дата публикации: <?= $block->datePublication ?> г.<br/>
	Дата обновления: <?= $block->dateUpdate ?> г.
</div>
