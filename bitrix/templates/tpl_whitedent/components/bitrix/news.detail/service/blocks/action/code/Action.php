<?
namespace Medreclama\Service;

class Action extends Block
{
	public $id;
	public $title;
	public $text;
	public $url;
	public $expired;

	private $action;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		$this->action = $this->getElement($arResult["PROPERTIES"]["ACTION"]["VALUE"]);

		$this->id = $this->action->fields["ID"];
		$this->title = $this->action->props["TITLE"]["~VALUE"];
		$this->text = $this->action->fields["PREVIEW_TEXT"];
		$this->url = $this->action->fields["DETAIL_PAGE_URL"];
		$this->expired = $this->getExpired($this->action->props["EXPIRED"]["VALUE"]);
	}

	private function getExpired($propValue): string
	{
		if(!$propValue){
			return "";
		}

		return mb_strtolower(FormatDate("d F", MakeTimeStamp($propValue)), "utf8");
	}

	public function isShow(): bool
	{
		return (bool)$this->action;
	}
	
}