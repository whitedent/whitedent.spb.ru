<? /** @var Medreclama\Service\RegisterLink $block */ ?>
<section class="page-section container">
	<div class="content">
		<div class="block-marked block-marked--arrow">
            <?= $block->text ?>
		</div>
	</div>
</section>