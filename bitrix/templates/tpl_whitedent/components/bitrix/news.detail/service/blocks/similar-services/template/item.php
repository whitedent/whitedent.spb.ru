<? /** @var Medreclama\Service\SimilarService $similarService */ ?>
<a
	class="block-marked text-marked"
	href="<?= $similarService->url ?>"
>
	<div class="grid grid--align-center grid--height-100 grid--padding-small">

		<div class="grid__cell grid__cell--xs-4">
            <? if($similarService->image): ?>
			<?= PictureTag::renderWithMQ($similarService->image, "", $similarService->title, $similarService->title) ?>

            <? endif ?>
		</div>

		<div class="grid__cell grid__cell--xs-8"><?= $similarService->title ?></div>

	</div>
</a>
