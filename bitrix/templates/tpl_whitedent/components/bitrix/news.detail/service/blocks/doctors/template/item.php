<? /** @var Medreclama\Service\Doctor $doctor */ ?>
<li class="splide__slide">
	<div class="doctor-item">
		<div class="grid grid--padding-y">

			<div class="grid__cell grid__cell--s-6 grid__cell--xs-hidden">
				<? include __DIR__."/photo.php"?>
			</div>

			<div class="grid__cell grid__cell--s-6 grid__cell--xs-12">
				<? include __DIR__."/info.php"?>
			</div>

		</div>
	</div>
</li>