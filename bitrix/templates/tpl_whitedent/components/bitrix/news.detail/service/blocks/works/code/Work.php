<?
namespace Medreclama\Service;

class Work
{
	public $id;
	public $title;
	public $photoBefore;
	public $photoAfter;
	public $doctor;
	public $services;

	public function __construct(Element $element)
	{
		$this->id = $element->fields["ID"];
		$this->title = $element->fields["NAME"];
		$this->photoBefore = getImageSrc($element->fields["PREVIEW_PICTURE"]);
		$this->photoAfter = getImageSrc($element->fields["DETAIL_PICTURE"]);
		$this->doctor = $this->getDoctor($element->props["P_DOCTOR"]["VALUE"]);
		$this->services = $this->getServices($element->props["SERVICES"]["VALUE"]);
	}

	private function getDoctor($doctorId)
	{
		if (empty($doctorId)) {
			return false;
		}

		$element = getIbElement($doctorId);
		if (!$element) {
			return false;
		}

		return [
			"name" => $element->fields["NAME"],
			"url" => $element->fields["DETAIL_PAGE_URL"]
		];
	}

	private function getServices($servicesId)
	{
		if (!$servicesId) {
			return false;
		}

		$sections = [];
		foreach ($servicesId as $serviceId) {
			$res = \CIBlockSection::GetByID($serviceId);
			$sections[] = $res->GetNext();
		}

		if (!$sections) {
			return false;
		}

		$services = [];
		foreach ($sections as $section) {
			$services[] = '<a href="' . $section["SECTION_PAGE_URL"] . '">' . $section["NAME"] . '</a>';
		}

		return implode(", ", $services);
	}

}