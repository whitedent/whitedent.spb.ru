<?
namespace Medreclama\Service;

class Prices extends Block
{
	public $prices;

	public function __construct(array $arResult)
	{
		parent::__construct($arResult);

		require "Price.php";

		$this->prices = $this->getPrices($this->arResult["PROPERTIES"]["PRICES"]["VALUE"]);
	}

	private function getPrices($elementsId): array
	{
		$elements = $this->getElements($elementsId);

		$prices = [];

		/** @var Element $element */
		foreach ($elements as $element){
			if($element->fields["ACTIVE"] == "Y"){
				$prices[] = new Price($element);
			}
		}

		return $prices;
	}

	public function isShow(): bool
	{
		return (bool)$this->prices;
	}
}