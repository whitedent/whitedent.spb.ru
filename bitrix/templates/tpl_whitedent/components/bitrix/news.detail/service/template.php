<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {die();}
use Medreclama\Service\Block;
require_once __DIR__."/Helper.php";
$serviceHelper = new Medreclama\Service\Helper($arResult);

try{
	$blocks = $serviceHelper->getBlocks();
	/** @var Block $block */
	foreach ($blocks as $block){
		echo $block->render();
	}
}catch (Exception $e){ ?>
	<section class="page-section container">
		<div class="content">
			<p>Ошибка на странице</p>
			<? global $USER ?>
			<?= $USER->IsAdmin() ? "<p>".$e->getMessage()."</p>" : "" ?>
		</div>
	</section>
<? } ?>