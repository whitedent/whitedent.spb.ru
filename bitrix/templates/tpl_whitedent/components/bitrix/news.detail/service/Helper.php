<?
namespace Medreclama\Service;

class Helper
{
	private $arResult;

	public function __construct(array $arResult)
	{
		$this->arResult = $arResult;
		require_once __DIR__."/blocks/Block.php";
		require_once __DIR__."/blocks/Element.php";
	}

	public function getBlocks(): array
	{
		$orderedBlocksNames = $this->getOrderedBlocksNames();
		
		$blocks = [];
		foreach ($orderedBlocksNames as $blockName){
			$blocks[] = $this->getBlock($blockName);
		}

		return $blocks;
	}

	private function getOrderedBlocksNames(): array
	{
		return [
			"top",
			"action",
			"description",
			"faq",
			"author",
			"articles",
			"works",
			"reviews",
			"doctors",
			"why-we",
			"register-form",
			"similar-services",
		];
	}

	public function getBlock(string $blockName): Block
	{
		$blockClassName = str_replace(' ', '', ucwords(str_replace('-', ' ', $blockName)));
		$blockClassFilePath = __DIR__."/blocks/".$blockName."/code/".$blockClassName.".php";
		if(!file_exists($blockClassFilePath)){
			throw new \Exception("Не найден файл класса для блока: ".$blockName);
		}
		require $blockClassFilePath;

		$blockClassName = 'Medreclama\\Service\\'.$blockClassName;
		if(!class_exists($blockClassName)){
			throw new \Exception("Не найден класс для блока: ".$blockName);
		}

		return new $blockClassName($this->arResult);
	}

	public function checkUrlErrors()
	{
		global $APPLICATION;
		$curPage = explode('?', $APPLICATION->GetCurPage());
		$curPage = reset($curPage);

		if($curPage != $this->arResult["SECTION_URL"]){
			header("Location: ".$this->arResult["SECTION_URL"], true, 301);
			exit();
		}
	}

}
