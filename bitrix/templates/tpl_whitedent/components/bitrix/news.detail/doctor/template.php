<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include __DIR__."/code/Diploma.php";
include __DIR__."/code/Review.php";
include __DIR__."/code/Doctor.php";
$doctor = new Doctors\Doctor($arResult);
include __DIR__."/template/index.php";