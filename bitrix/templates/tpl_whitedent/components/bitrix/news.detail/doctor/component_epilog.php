<?
global $APPLICATION;

$description = $arResult["EDUCATION"] ? getTruncatedDataForSeo($arResult["EDUCATION"]) : "";

if($description){
	$APPLICATION->SetPageProperty("description", $description);
}
