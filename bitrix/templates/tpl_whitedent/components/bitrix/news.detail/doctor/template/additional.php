<? /** @var Doctors\Doctor $doctor */ ?>
<? if($doctor->additional): ?>
<div class="page-subsection" id="additional">
	<h2>Дополнительная информация</h2>
    <?= $doctor->additional ?>
</div>
<? endif ?>