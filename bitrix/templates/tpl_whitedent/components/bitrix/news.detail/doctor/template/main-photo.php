<? /** @var Doctors\Doctor $doctor */ ?>
<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-8 grid__cell--xs-12 block-marked-image__image">
	<?= PictureTag::renderWithMQ($doctor->image, "", $doctor->name, $doctor->name) ?>
</div>
