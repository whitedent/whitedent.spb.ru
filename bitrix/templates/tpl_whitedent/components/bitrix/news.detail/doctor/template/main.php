<div class="page-subsection">
	<div class="block-marked-image">
		<div class="grid grid--par-like">

            <? include __DIR__."/main-photo.php" ?>

			<div class="grid__cell grid__cell--l-9 grid__cell--m-8 grid__cell--s-12 grid__cell--xs-12 block-marked-image__body">
				<div class="block-marked-image__body-inner">
					<? include __DIR__."/main-speciality.php" ?>
					<? include __DIR__."/main-properties.php" ?>
					<? include __DIR__."/main-buttons.php" ?>
				</div>
			</div>

		</div>
	</div>
</div>