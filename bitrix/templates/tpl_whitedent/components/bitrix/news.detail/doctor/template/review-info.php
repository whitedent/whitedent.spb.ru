<? /** @var Doctors\Review $review */ ?>
<div class="review-item__info">

	<div class="review-item__name"><?= $review->title ?></div>

	<? if($review->rating): ?>
		<div class="review-item__stars">
			<?= str_repeat('<div class="review-item__star"></div>'."\n", 5) ?>
		</div>
	<? endif ?>

	<div class="review-item__body"><?= $review->text ?></div>

	<div class="review-item__footer">
		<? include __DIR__."/review-info-likes.php" ?>
	</div>

</div>