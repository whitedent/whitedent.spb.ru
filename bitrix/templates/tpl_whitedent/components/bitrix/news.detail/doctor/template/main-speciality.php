<? /** @var Doctors\Doctor $doctor */ ?>
<? if($doctor->speciality): ?>
    <div class="page-subsection">
        <h3><?= $doctor->speciality ?></h3>
    </div>
<? endif ?>