<? /** @var Doctors\Doctor $doctor */ ?>
<div class="page-subsection">
	<div class="grid grid--padding-y">

		<div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--xs-12">
            <a
                class="button button--wide discount-form-open"
                href="#discount-form"
                data-from="<?= $doctor->name ?>"
                data-type="doctor"
                data-elementid="<?= $doctor->id ?>"
            >Записаться на прием</a>
		</div>

		<div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--xs-12">
			<a
                class="button button--secondary button--wide"
                href="/patsientam/cons/ask/">Задать вопрос</a>
		</div>

	</div>
</div>