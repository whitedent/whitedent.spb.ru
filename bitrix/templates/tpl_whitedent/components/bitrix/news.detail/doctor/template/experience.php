<? /** @var Doctors\Doctor $doctor */ ?>
<? if($doctor->experienceText): ?>
<div class="page-subsection" id="experience">
	<h2>Опыт работы</h2>
    <?= $doctor->experienceText ?>
</div>
<? endif ?>