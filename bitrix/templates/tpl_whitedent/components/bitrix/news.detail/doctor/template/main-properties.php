<? /** @var Doctors\Doctor $doctor */ ?>
<div class="page-subsection">
	<div class="properties">

        <? if($doctor->experience): ?>
		<div class="properties__item properties__item--experience">
			<div class="properties__name">Стаж работы <?= $doctor->experience ?></div>
		</div>
        <? endif ?>

        <? if($doctor->text): ?>
        <div class="properties__item properties__item--additional">
			<div class="properties__name"><?= $doctor->text ?></div>
		</div>
        <? endif ?>

	</div>
</div>