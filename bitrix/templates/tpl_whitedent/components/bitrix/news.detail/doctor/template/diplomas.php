<? /** @var Doctors\Doctor $doctor */ ?>
<? if($doctor->diplomas): ?>
<div class="page-subsection" id="diplomas">

	<h2>Дипломы и сертификаты</h2>

	<div class="grid grid--par-like grid--padding-y">
        <? foreach ($doctor->diplomas as $diploma): ?>
		<figure class="figure grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--xs-6">
            <a
                class="figure__detail"
                href="<?= $diploma->image ?>"
                data-glightbox=""
                data-gallery="gallery_<?= $doctor->id ?>"
            >
                <img class="figure__image" src="<?= $diploma->image ?>" alt="" title=""/>


            </a>
			<figcaption class="figure__caption"><?= $diploma->title ?></figcaption>
		</figure>
        <? endforeach ?>
	</div>

</div>
<? endif ?>
