<? /** @var Doctors\Doctor $doctor */ ?>
<? if($doctor->isHasFilledProperties()): ?>
<div class="page-subsection">
	<div class="grid grid--cols-12">
		<div class="grid__cell grid__cell--l-9 grid__cell--m-10 grid__cell--xs-12">

			<nav class="page-navigation">
				<ul class="page-navigation__list">
                    <? include __DIR__."/anchors-list.php" ?>
				</ul>
			</nav>
            
		</div>
	</div>
</div>
<? endif ?>