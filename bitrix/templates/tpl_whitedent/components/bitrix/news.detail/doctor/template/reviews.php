<? /** @var Doctors\Doctor $doctor */ ?>
<? if($doctor->reviews): ?>
<div class="page-subsection" id="reviews">
	<div class="grid grid--padding-y grid--justify-space-between">

        <? include __DIR__."/reviews-top.php" ?>

        <? foreach ($doctor->reviews as $review): ?>
			<? include __DIR__."/review.php" ?>
        <? endforeach ?>

	</div>
</div>
<? endif ?>