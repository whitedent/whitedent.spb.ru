<? /** @var Doctors\Doctor $doctor */ ?>
<? if($doctor->education): ?>
	<li class="page-navigation__item">
		<a href="#education">Образование</a>
	</li>
<? endif ?>

<? if($doctor->qualification): ?>
	<li class="page-navigation__item">
		<a href="#qualification">Повышение квалификации</a>
	</li>
<? endif ?>

<? if($doctor->experience): ?>
	<li class="page-navigation__item">
		<a href="#experience">Опыт работы</a>
	</li>
<? endif ?>

<? if($doctor->additional): ?>
	<li class="page-navigation__item">
		<a href="#additional">О себе</a>
	</li>
<? endif ?>

<? if($doctor->diplomas): ?>
	<li class="page-navigation__item">
		<a href="#diplomas">Дипломы</a>
	</li>
<? endif ?>

<? if($doctor->reviews): ?>
	<li class="page-navigation__item">
		<a href="#reviews">Отзывы</a>
	</li>
<? endif ?>