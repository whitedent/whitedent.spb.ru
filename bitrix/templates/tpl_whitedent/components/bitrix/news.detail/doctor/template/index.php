<section class="page-section container">
    <div class="content">
        <? include __DIR__."/name.php" ?>
        <? include __DIR__."/main.php" ?>
        <? include __DIR__."/anchors.php" ?>
        <? include __DIR__."/education.php" ?>
        <? include __DIR__."/qualification.php" ?>
        <? include __DIR__."/experience.php" ?>
        <? include __DIR__."/additional.php" ?>
        <? include __DIR__."/diplomas.php" ?>
        <? include __DIR__."/reviews.php" ?>
    </div>
</section>