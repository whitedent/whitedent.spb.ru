<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$cp = $this->__component; // объект компонента
if (is_object($cp))
{
    $cp->arResult['EDUCATION'] = $arResult["PROPERTIES"]["EDUCATION"]["~VALUE"]["TEXT"];
    $cp->SetResultCacheKeys(array('EDUCATION'));
    $arResult['EDUCATION'] = $cp->arResult['EDUCATION'];
}
?>