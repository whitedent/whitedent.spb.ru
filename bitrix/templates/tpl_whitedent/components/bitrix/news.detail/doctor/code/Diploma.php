<?
namespace Doctors;

class Diploma
{
	public $image;
	public $title;

	public function __construct($elementId)
	{
		$element = getIbElement($elementId);
		$this->image = getImageSrc($element->fields["PREVIEW_PICTURE"]);
		$this->title = $element->fields["PREVIEW_TEXT"];
	}

}