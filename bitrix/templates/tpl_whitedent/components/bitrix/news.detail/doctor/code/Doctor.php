<?
namespace Doctors;

class Doctor
{
	public $id;
	public $name;
	public $nameFormatted;
	public $image;
	public $speciality;
	public $experience;
	public $text;
	public $url;

	public $education;
	public $qualification;
	public $experienceText;
	public $additional;
	public $diplomas;
	public $reviews;

	public function __construct($arResult)
	{
		$this->id = $arResult["ID"];
		$this->name = $arResult["NAME"];
		$this->nameFormatted = $this->getNameFormatted($arResult["NAME"]);
		$this->image = $arResult["DETAIL_PICTURE"]["SRC"];
		$this->speciality = $this->getSpeciality($arResult["PROPERTIES"]["SPECIALITY"]["VALUE"]);
		$this->experience = $arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"];
		$this->text = $arResult["~PREVIEW_TEXT"];
		$this->url = $arResult["DETAIL_PAGE_URL"];

		$this->education = $arResult["PROPERTIES"]["EDUCATION"]["~VALUE"]["TEXT"];
		$this->qualification = $arResult["PROPERTIES"]["QUALIFICATION"]["~VALUE"]["TEXT"];
		$this->experienceText = $arResult["PROPERTIES"]["EXPERIENCE_TEXT"]["~VALUE"]["TEXT"];
		$this->additional = $arResult["PROPERTIES"]["ADDITIONAL"]["~VALUE"]["TEXT"];
		$this->diplomas = $this->getDiplomas($arResult["PROPERTIES"]);
		$this->reviews = $this->getReviews($arResult["PROPERTIES"]["REVIEWS"]["VALUE"]);
	}

	private function getNameFormatted($name)
	{
		$parts = explode(" ", $name);
		$parts[0] = "<strong>".$parts[0]."</strong>";
		return implode(" ", $parts);
	}


	private function getSpeciality($propValue)
	{
		return mb_ucfirst(mb_strtolower(implode(" / ", $propValue), "utf8"), "utf8");
	}

	public function isHasFilledProperties(): bool
	{
		if(
			!$this->education &&
			!$this->qualification &&
			!$this->experience &&
			!$this->additional &&
			!$this->diplomas &&
			!$this->reviews
		){
			return false;
		}

		return true;
	}

	private function getDiplomas($properties)
	{
		$diplomas = [];

		$diplomasHorizontalId = $properties["DIPLOMAS_HORIZONTAL"]["VALUE"];
		$diplomasVerticalId = $properties["DIPLOMAS_VERTICAL"]["VALUE"];

		if($diplomasHorizontalId){
			foreach ($diplomasHorizontalId as $diplomaHorizontalId){
				$diplomas[] = new Diploma($diplomaHorizontalId);
			}
		}

		if($diplomasVerticalId){
			foreach ($diplomasVerticalId as $diplomaVerticalId){
				$diplomas[] = new Diploma($diplomaVerticalId);
			}
		}

		return $diplomas;
	}

	private function getReviews($reviewsId): array
	{
		$reviews = [];

		foreach ($reviewsId as $reviewId) {
			$reviews[] = new Review($reviewId);
		}

		return $reviews;
	}

}