<?
namespace Doctors;

class Review
{
	public $id;
	public $title;
	public $author;
	public $image;
	public $gender;
	public $date;
	public $text;
	public $rating;
	public $likes;

	public function __construct($elementId)
	{
		$element = getIbElement($elementId);

		$this->id = $element->fields["IBLOCK_ID"]."_".$element->fields["ID"];
		$this->title = $element->fields["NAME"];
		$this->author = $element->props["NAME"]["VALUE"];
		$this->image = $this->getImageSrc($element->fields["PREVIEW_PICTURE"]);
		$this->gender = $this->getGender($element->props["GENDER"]["VALUE"]);
		$this->date = $this->getDate($element->fields["DATE_CREATE"]);
		$this->text = $element->fields["~PREVIEW_TEXT"];
		$this->rating = $element->props["RATING"]["VALUE"];
		$this->likes = $this->getLikes($element->props["LIKES"]["VALUE"]);
	}

	private function getGender($value)
	{
		return $value == "мужчина" ? "m" : "w";
	}

	private function getDate($date)
	{
		return FormatDate("d.m.Y", MakeTimeStamp($date));
	}

	private function getLikes($value)
	{
		return $value ? $value : "";
	}

	private function getImageSrc($imageId)
	{
		return !empty($imageId) ? \CFile::GetPath($imageId) : null;
	}

}