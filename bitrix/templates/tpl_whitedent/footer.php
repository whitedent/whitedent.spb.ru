<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
?>
        </main>

        <footer class="footer">
            <div class="container">
                <div class="content">
                    <div class="grid grid--padding-y">
                        <? include __DIR__."/include/footer/menu-o-klinike.php" ?>
                        <? include __DIR__."/include/footer/menu-patsientam.php" ?>
                        <? include __DIR__."/include/footer/logo.php" ?>
                        <? include __DIR__."/include/footer/contacts.php" ?>
                        <? include __DIR__."/include/footer/developer.php" ?>
                        <? include __DIR__."/include/footer/license.php" ?>
                    </div>
                </div>
            </div>
        </footer>

        <? include __DIR__."/include/footer/popups/index.php" ?>
        <? include __DIR__."/include/footer/counters.php" ?>

        <? include __DIR__."/include/styles.php" ?>
        <? include __DIR__."/include/scripts.php" ?>

    </body>

</html>
