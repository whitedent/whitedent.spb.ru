<? /** @var $aMenuLinks array */ ?>
<? include $_SERVER["DOCUMENT_ROOT"]."/patsientam/.top_sub.menu.php" ?>
<div class="grid__cell grid__cell--l-3 grid__cell--m-6 grid__cell--xs-6">
    <div class="footer__menu">
        <nav class="footer-menu">

            <ul class="footer-menu__list">
                <li class="footer-menu__item footer-menu__item--parent">
                    <a href="/patsientam/">Пациентам</a>
                    <ul class="footer-menu__sublist footer-menu__sublist--1">
						<? foreach ($aMenuLinks as $link): ?>
                        <li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-1">
                            <a href="<?= $link[1] ?>"><?= $link[0] ?></a>
                        </li>
                        <? endforeach ?>
                    </ul>
                </li>
            </ul>

        </nav>
    </div>
</div>
