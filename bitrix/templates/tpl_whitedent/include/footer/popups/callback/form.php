<form
	class="discount-form__form callback validated-form submitJS"
	id="callback-form-form"
>
    <?php
    $token = md5(uniqid(rand(), true));

    // Сохраняем токен в сессии или в базе данных для последующей проверки
    $_SESSION['csrf_token'] = $token;

    ?>

	<div class="discount-form__input">
		<div class="form-input form-input--text">
			<label class="form-input__label" for="discount-form-name">Представьтесь</label>
			<input class="form-input__field validated required" type="text" name="name" placeholder="Ваше Ф.И.О." id="discount-form-name">
            <input type="text"  name="csrf_token" value="<?php echo $token?>"  class="secret">

        </div>
		<div class="validation-message">Обязательное поле</div>
	</div>

	<div class="discount-form__input">
		<div class="form-input form-input--tel">
			<label class="form-input__label" for="discount-form-tel">Номер телефона</label>
			<input class="form-input__field validated required" type="tel" name="phone" id="discount-form-tel">
            <input type="text"  name="whitedent_field" value="" class="secret">


        </div>
		<div class="validation-message">Напишите номер телефона. Без телефона мы не сможем вам перезвонить</div>
	</div>

	<div class="discount-form__submit">
		<button class="button" type="submit">Получить обратный звонок</button>
	</div>

</form>
