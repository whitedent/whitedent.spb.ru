<form
    class="discount-form__form consultation-new-registration validated-form submitJS"
    id="discount-form-form"
>
    <input type="hidden"  name="secret" value="" class="secret">


    <div class="form-input form-input--hidden">
		<input class="form-input__field" type="hidden" name="from" id="discount-form-from">
        <input class="form-input__field" type="hidden" name="element_id">
        <input class="form-input__field" type="hidden" name="page_type">
    </div>

	<div class="discount-form__input">
		<div class="form-input form-input--text">
			<label class="form-input__label" for="discount-form-name">Представьтесь</label>
			<input class="form-input__field validated required" type="text" name="name" id="discount-form-name">
		</div>
        <div class="validation-message">Обязательное поле</div>
    </div>

	<div class="discount-form__input">
		<div class="form-input form-input--tel">
			<label class="form-input__label" for="discount-form-tel">Номер телефона</label>
			<input class="form-input__field validated required" type="tel" name="phone" id="discount-form-tel">
		</div>
		<div class="validation-message">Напишите номер телефона. Без телефона мы не сможем вам позвонить и назначить время и врача.</div>
	</div>

	<div class="discount-form__input">
		<div class="form-input form-input--textarea">
			<label class="form-input__label" for="discount-form-comment">Напишите время визита и причину</label>
			<textarea class="form-input__field" name="message" id="discount-form-comment"></textarea>
		</div>
	</div>

	<div class="discount-form__submit">
		<button class="button" type="submit">Записаться на прием</button>
	</div>

    <? if(!empty($_GET["ref"]) && $_GET["ref"] == "comagic"): ?>
        <input type="hidden" name="is_from_comagic" value="true">
    <? endif ?>

</form>
