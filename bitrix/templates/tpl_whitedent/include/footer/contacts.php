<div class="grid__cell grid__cell--l-4 grid__cell--m-8 grid__cell--xs-12">
    <div class="footer__contacts">

        <p class="footer__contacts-title">Клиника Вайтдент</p>

        <div class="footer__phone">
            <a href="tel:+78126129792" class="phone">+7 (812) 612-97-92</a>
        </div>

        <div class="footer__socials">
            <span>Присоединяйтесь к нам</span>
            <div class="link-icons">
                <a class="link-icons__link link-icons__link--vk" href="https://vk.com/spbdent" target="_blank"></a>
                <? /* <a class="link-icons__link link-icons__link--instagram" href="https://www.instagram.com/whitedent_spb/" target="_blank"></a> */ ?>
            </div>
        </div>

        <p>
            <a href="mailto:send@whitedent.spb.ru">send@whitedent.spb.ru</a>
        </p>

        <? /* <div class="footer__button">
            <a
                class="button"
                href="/patsientam/cons/ask/"
            >Задать вопрос в консультацию</a>
        </div> */ ?>

        <div class="footer__agreement">
            <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">Соглашения на обработку персональных данных</a><br>
            <a href="/pravila-ispolzovaniya-sayta/">Правила использования сайта</a>
        </div>

    </div>
</div>
