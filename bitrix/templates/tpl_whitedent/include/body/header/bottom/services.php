<div class="header-services">

	<button class="button header-services__button">
		<svg class="header-services__button-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="16.001" viewBox="0 0 24 16.001"><g transform="translate(0 0.001)"><path fill="currentColor" d="M22.78,6.88H1.22A1.17,1.17,0,0,0,0,8,1.17,1.17,0,0,0,1.22,9.12H22.78A1.17,1.17,0,0,0,24,8a1.17,1.17,0,0,0-1.22-1.12Z"/><path fill="currentColor" d="M1.017,2.24H18.983a.886.886,0,0,0,.7-.307A1.28,1.28,0,0,0,20,1.12a1.28,1.28,0,0,0-.315-.813.886.886,0,0,0-.7-.307H1.017a.886.886,0,0,0-.7.307A1.28,1.28,0,0,0,0,1.12a1.28,1.28,0,0,0,.315.813.886.886,0,0,0,.7.307Z"/><path fill="currentColor" d="M15.347,13.76H.853c-.416,0-.753.5-.753,1.12S.437,16,.853,16H15.347c.416,0,.753-.5.753-1.12S15.763,13.76,15.347,13.76Z"/></g></svg>
		Наши услуги
	</button>

	<div class="header-services__list js-header-services"></div>

</div>
