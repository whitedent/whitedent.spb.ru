<div class="grid grid--nopadding grid--align-center header__navigation">

    <? if(isHomePage()): ?>
        <div class="grid__cell grid__cell--l-3 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden header__search">
			<? include __DIR__."/search-home.php" ?>
        </div>
	<? else: ?>
        <div class="grid__cell grid__cell--l-2 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden">
			<? include __DIR__."/services.php" ?>
        </div>
        <div class="grid__cell grid__cell--l-1 grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden header__search">
			<? include __DIR__."/search-inner.php" ?>
        </div>
	<? endif ?>

    <div class="grid__cell grid__cell--l-auto grid__cell--m-hidden grid__cell--s-hidden grid__cell--xs-hidden header__menu js-header-menu"></div>

    <div class="grid__cell grid__cell--hd-hidden grid__cell--xl-hidden grid__cell--l-hidden grid__cell--m-6 grid__cell--s-6 grid__cell--xs-5 header__mobile-navigation">
		<? include __DIR__."/mobile-menu.php" ?>
    </div>

    <div class="grid__cell grid__cell--hd-hidden grid__cell--xl-hidden grid__cell--l-hidden grid__cell--m-4 grid__cell--s-4 grid__cell--xs-3 header__call-mobile">
		<? include __DIR__."/mobile-call.php" ?>
    </div>

    <div class="grid__cell grid__cell--l-auto grid__cell--m-6 grid__cell--s-6 grid__cell--xs-7 header__button">
		<? include __DIR__."/appointment-registration.php" ?>
    </div>

</div>
