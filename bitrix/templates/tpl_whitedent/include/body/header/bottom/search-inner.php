<div class="header-search">
	<button class="header-search__open" type="button">
		<svg xmlns="http://www.w3.org/2000/svg" width="18.6" height="18.6" viewBox="0 0 18.6 18.6"><g transform="translate(0.3 0.3)"><path fill="currentColor" stroke="currentColor" stroke-width=".6px" d="M28.78,27.718l-4.044-4.044a7.718,7.718,0,1,0-1.062,1.065l4.044,4.041a.751.751,0,0,0,1.062-1.062ZM14.332,23.167A6.243,6.243,0,1,1,18.747,25,6.193,6.193,0,0,1,14.332,23.167Z" transform="translate(-11 -11)"/></g></svg>
	</button>
	<? include __DIR__."/search-component.php" ?>
</div>
