<?
$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"services-mobile",
	Array(
		"ROOT_MENU_TYPE" => "services",	// Тип меню для первого уровня
		"MAX_LEVEL" => "3",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	),
	false
); ?>
<? /* <nav class="menu-services">
	<ul class="menu-services__list">
		<li class="menu-services__item menu-services__item--active menu-services__item--parent"><a href="/">Протезирование</a>
			<ul class="menu-services__sublist menu-services__sublist--1">
				<li class="menu-services__item menu-services__item--parent menu-services__item--sub menu-services__item--sub-1"><a href="/">Зубные коронки</a>
					<ul class="menu-services__sublist menu-services__sublist--2">
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Металлокерамические коронки</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Керамические коронки</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Циркониевые коронки</a>
						</li>
						<li class="menu-services__item menu-services__item--active menu-services__item--sub menu-services__item--sub-2"><a href="/">Временные коронки</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Коронки из диоксида циркония</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Пластмассовые коронки</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Коронки на передние зубы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Металлические коронки</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Безметалловая керамика</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Установка коронки на зуб</a>
						</li>
					</ul>
				</li>
				<li class="menu-services__item menu-services__item--parent menu-services__item--sub menu-services__item--sub-1"><a href="/">Зубные вкладки</a>
					<ul class="menu-services__sublist menu-services__sublist--2">
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Зубные вкладки</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Культевая вкладка</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Керамическая вкладка</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Вкладка под коронку</a>
						</li>
					</ul>
				</li>
				<li class="menu-services__item menu-services__item--parent menu-services__item--sub menu-services__item--sub-1"><a href="/">Зубные протезы</a>
					<ul class="menu-services__sublist menu-services__sublist--2">
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Установка зубных протезов</a>
						</li>
					</ul>
				</li>
				<li class="menu-services__item menu-services__item--parent menu-services__item--sub menu-services__item--sub-1"><a href="/">Съемные протезы</a>
					<ul class="menu-services__sublist menu-services__sublist--2">
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Акриловый протез</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Бюгельные зубные протезы в СПб</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Частично съемный протез</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Гибкие протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Металлокерамические протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Мягкие протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Нейлоновый протез</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Пластмассовые протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Полные протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Протез «Бабочка»</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Протезы Acry Free</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Протезы без нёба</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Протезы на один зуб</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Съемное протезирование</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Силиконовые протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Вставная челюсть</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Протезы без нёба</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Протезы на один зуб</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Съемное протезирование</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Силиконовые протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Вставная челюсть</a>
						</li>
					</ul>
				</li>
				<li class="menu-services__item menu-services__item--parent menu-services__item--sub menu-services__item--sub-1"><a href="/">Несъемные зубные протезы</a>
					<ul class="menu-services__sublist menu-services__sublist--2">
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Несъемное протезирование</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Металлокерамический мост</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Зубной мост</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Мостовидные протезы</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Балочный протез</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Иммедиат-протез</a>
						</li>
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Пластиночный протез</a>
						</li>
					</ul>
				</li>
				<li class="menu-services__item menu-services__item--parent menu-services__item--sub menu-services__item--sub-1"><a href="/">Протезирование на имплантах</a>
					<ul class="menu-services__sublist menu-services__sublist--2">
						<li class="menu-services__item menu-services__item--sub menu-services__item--sub-2"><a href="/">Протезы на имплантах</a>
						</li>
					</ul>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Протезирование при полном отсутствии зубов</a>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Вставить зубы</a>
				</li>
			</ul>
		</li>
		<li class="menu-services__item menu-services__item--active menu-services__item--parent"><a href="/">Имплантация</a>
			<ul class="menu-services__sublist menu-services__sublist--1">
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Зубные коронки</a>
				</li>
				<li class="menu-services__item menu-services__item--active menu-services__item--sub menu-services__item--sub-1"><a href="/">Зубные вкладки</a>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Зубные протезы</a>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Съемные протезы</a>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Несъемные зубные протезы</a>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Протезирование на имплантах</a>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Протезирование при полном отсутствии зубов</a>
				</li>
				<li class="menu-services__item menu-services__item--sub menu-services__item--sub-1"><a href="/">Вставить зубы</a>
				</li>
			</ul>
		</li>
		<li class="menu-services__item"><a href="/">Лечение зубов</a>
		</li>
		<li class="menu-services__item"><a href="/">Лечение десен</a>
		</li>
		<li class="menu-services__item"><a href="/">Хирургия</a>
		</li>
		<li class="menu-services__item"><a href="/">Исправление прикуса</a>
		</li>
		<li class="menu-services__item"><a href="/">Гигиена полости рта</a>
		</li>
		<li class="menu-services__item"><a href="/">Рентгенодиагностика</a>
		</li>
		<li class="menu-services__item"><a href="/">Анестезия</a>
		</li>
		<li class="menu-services__item"><a href="/">Консультация</a>
		</li>
	</ul>
</nav> */ ?>
