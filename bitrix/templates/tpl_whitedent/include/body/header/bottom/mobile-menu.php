<div class="header-navigation-mobile">

    <? include __DIR__."/mobile-menu-button.php" ?>

    <div class="header-navigation-mobile__body">

        <div class="header-navigation-mobile__search">
			<? include __DIR__."/search.php" ?>
        </div>

        <div class="header-navigation-mobile__services">
            <? include __DIR__."/mobile-menu-services.php" ?>
        </div>

        <div class="header-navigation-mobile__main">
            <? include __DIR__."/menu-main-component-mobile.php" ?>
        </div>

    </div>

</div>
