<?
	$pinAddress = strtok($_SERVER["REQUEST_URI"], '?') !== '/kontakty/' ? '/kontakty/#contacts-map' : '#contacts-map';
?>

<a href="<?=$pinAddress?>" class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-hidden grid__cell--xs-hidden header__contacts">
	<div class="header__address">ул. Нахимова, д. 7, корп. 2 (метро Приморская)</div>
	<div class="header__schedule">пн.-пт. 11:00-21:00, сб. 11:00-18:00</div>
</a>
