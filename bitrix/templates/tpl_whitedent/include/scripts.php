<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$scripts = [
	"jquery-3.5.1.min",
	"jquery.maskedinput",
	"jquery.cookie",
	"ajax_form",
	"form_validation",
	"likes",
	"add",
];

if($APPLICATION->GetPageProperty("ADD_TYPE_AHEAD_ASSETS")){
	$scripts[] = "jquery-ui.min";
	$scripts[] = "type_ahead_search";
	Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/jquery-ui.css");
}


foreach($scripts as $script):
    $src = SITE_TEMPLATE_PATH."/scripts/".$script.".js?v=".require "version.php";
    ?>
    <script data-skip-moving="true" src="<?= $src ?>"></script>
<? endforeach ?>
    <script data-skip-moving="true" src="/local/front/template/scripts/script.js?v=<?= require "version.php" ?>"></script>
