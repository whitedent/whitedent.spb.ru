<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$styles = [];

if(isHomePage()){
	$styles[] = "home";
}else{
	$styles[] = "inner";
}

$styles[] = "rest";


foreach($styles as $style): ?>
    <link href="/local/front/template/styles/<?= $style ?>.css?v=<?= require "version.php" ?>" rel="stylesheet">
    <link href="<?=SITE_TEMPLATE_PATH?>/styles/add.css?v=<?= require "version.php" ?>" rel="stylesheet">
<? endforeach ?>
