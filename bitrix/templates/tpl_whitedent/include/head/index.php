<? include(__DIR__."/comagic.php") ?>

<? $APPLICATION->ShowHead() ?>
<? if(isInnerPage()){CJSCore::Init(["popup"]);} ?>

<link href="/local/front/template/styles/core.css?v=<?= require __DIR__."/../version.php" ?>" rel="stylesheet">

<title><? $APPLICATION->ShowTitle() ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />

<link rel="preload" href="/local/front/template/fonts/Mulish-Regular.woff2" as="font" type="font/woff2" crossorigin="anonymous">

<?
$baseUri = ($_SERVER["HTTPS"]) ? "https://" : "http://";
$baseUri .= $_SERVER["SERVER_NAME"];
$curUri = $baseUri.$APPLICATION->GetCurPage();

$APPLICATION->AddHeadString('<meta property="og:site_name" content="whitedent.spb.ru" />');
$APPLICATION->AddHeadString('<meta property="og:type" content="article" />');
$APPLICATION->AddHeadString('<meta property="og:image" content="'.$baseUri.SITE_TEMPLATE_PATH.'/images/logo.png" />');
$APPLICATION->AddHeadString('<meta property="og:url" content="'.$curUri.'" />');
?>
<?php if (strpos($_SERVER['REQUEST_URI'], "?PAGEN") !== false) {?>
    <meta name="robots" content="noindex, follow" />
    <link href="<?php echo $curUri?>" rel="canonical" />

<?php } ?>
<? include($_SERVER["DOCUMENT_ROOT"]."/zeus_redirect.php") ?>
