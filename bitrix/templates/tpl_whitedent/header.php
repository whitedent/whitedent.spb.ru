<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}

?><?if (!$menuPage):?><!DOCTYPE html>
<html lang="ru">

    <head>
        <? include __DIR__."/include/head/index.php" ?>
    </head>
    <body class="page">

        <? include __DIR__."/include/body/top_scripts.php" ?>

        <div id="panel"><? $APPLICATION->ShowPanel() ?></div>

        <? include __DIR__."/include/body/header/index.php" ?>


        <main>

			<? if(isInnerPage()): ?>
                <? include __DIR__."/include/body/breadcrumbs.php"?>
            <? endif ?>
<? endif ?>