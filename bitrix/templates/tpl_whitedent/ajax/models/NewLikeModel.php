<?
class NewLikeModel extends BaseModel
{
	const EVENT_NAME = "";
	const SUBJECT_INIT = "";
	const IBLOCK_TYPE = "content";
	const NAME_FIELD_NAME = "name";
	const MESSAGE_FIELD_NAME = "message";
	const MAKE_IBLOCK_ELEMENT = false;
	const SEND_EMAIL = false;
	const PROP_CODE_LIKES = "LIKES";

	public function doPostAction()
	{
		$iblockId = $this->getIblockIdFromRequestId($this->request["id"]);
		$elementId = $this->getElementIdFromRequestId($this->request["id"]);
		$likesPropertyId = $this->getPropertyId($iblockId, $elementId, self::PROP_CODE_LIKES);

		$curLikesCount = $this->getLikesCount($iblockId, $elementId, $likesPropertyId);
		$newLikesCount = ++$curLikesCount;
		$this->setLikesCount($elementId, $likesPropertyId, $newLikesCount);

		$this->messages = [$newLikesCount];
	}

	private function getLikesCount($iblockId, $elementId, $likesPropertyId)
	{
		$arElementFilter = ["ID" => $elementId];
		$propertyFilter = ["ID" => $likesPropertyId];

		$res = CIBlockElement::GetPropertyValues($iblockId, $arElementFilter, false, $propertyFilter);
		$property = $res->Fetch();

		$likesCount = (int)$property[$likesPropertyId];

		return $likesCount;
	}

	private function setLikesCount($elementId, $likesPropertyId, $likesCount)
	{
		if(!CIBlockElement::SetPropertyValueCode($elementId, $likesPropertyId, $likesCount)){
			throw new \Exception("Ошибка при обновлении количества лайков");
		}

		// Чтобы сбросить кеш для элемента ИБ
		$el = new CIBlockElement;
		$res = $el->Update($elementId, []);

		return true;
	}

	private function getIblockIdFromRequestId($requestId)
	{
		$requestId = explode("_", $requestId);
		$iblockId = (int)reset($requestId);

		if(!$iblockId){
			throw new \Exception("Не найден ID инфоблока");
		}

		return $iblockId;
	}

	private function getElementIdFromRequestId($requestId)
	{
		$requestId = explode("_", $requestId);
		$elementId = (int)end($requestId);

		if(!$elementId){
			throw new \Exception("Не найден ID элемента инфоблока");
		}

		return $elementId;
	}

	private function getPropertyId(int $iblockId, int $elementId, string $propCodeLikes)
	{
		$db_props = CIBlockElement::GetProperty(
			$iblockId,
			$elementId,
			["sort" => "asc"],
			["CODE" => $propCodeLikes]
		);

		if(!$ar_props = $db_props->Fetch()){
			throw new \Exception("Не найден ID свойства в котором хранится количество лайков");
		}

		return (int)$ar_props["ID"];
	}

}



