<?
class CallbackModel extends BaseModel
{

	const EVENT_NAME = "CALLBACK_FORM";
	const SUBJECT_INIT = "Новый запрос звонка";
	const IBLOCK_TYPE = "content";
	const IBLOCK_ID = 18;

	public $name;
	public $phone;
    public $spam = false;


	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = self::IBLOCK_ID;
		}

		$this->name = $request["name"];
		$this->phone = $request["phone"];

		if (empty($this->name)) {throw new Exception("Поле Имя пустое");}
        if (empty($this->phone)) {throw new Exception("Поле Телефон пустое");}
        // Проверяем, отправлен ли User-Agent

        // Проверка, существует ли HTTP_REFERER в массиве $_SERVER
        if (isset($_SERVER['HTTP_REFERER'])) {
            // Получаем значение HTTP_REFERER
            $referer = $_SERVER['HTTP_REFERER'];


            // Проверяем, является ли Referer допустимым (например, вашим сайтом)
            if (strpos($referer, 'whitedent.spb.ru') !== false) {

                // Referer допустим, выполняем нужные действия
                $this->spam = true;

            } else {
                // Referer не допустим
                $this->spam = false;
            }
        } else {
            // HTTP_REFERER отсутствует в запросе
            echo 'HTTP_REFERER отсутствует';
        }
        $parts = explode(" ", $this->name);

        if (count($parts) < 3){

            $this->spam = false;
        }
        if (
            !empty($_POST['whitedent_field'])
            || $_POST['csrf_token'] != $_SESSION['csrf_token']
            || $this->spam ===  false
        ) {
            die();
        }

	}

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
		$subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $subjectForUser,
			"CODE" => $this->getCode($subjectForCode),
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PROPERTY_VALUES" => [
				"NAME" => $this->name,
				"PHONE" => $this->phone,
			]
		];

		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		$this->messages[] = "New IBlock element created, Id = " . $newElementId;

		return $newElementId;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);

		$emailFields["NAME"] = $this->name;
		$emailFields["PHONE"] = $this->phone;

		return $emailFields;
	}

}



