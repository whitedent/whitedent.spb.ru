<?
class NewReviewModel extends BaseModel
{
	const EVENT_NAME = "NEW_FEEDBACK";
	const SUBJECT_INIT = "отзыв";
	const IBLOCK_TYPE = "content";
	const IBLOCK_ID = ID_IBLOCK_FEEDBACK;

	public $title;
	public $text;
	public $rating;
	public $author;
	public $phone;
	public $email;
	public $service;
	public $doctor;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = self::IBLOCK_ID;
		}

		$this->text = $request["text"];
		$this->title = getTruncatedDataForSeo($this->text, 100);
		$this->rating = $request["rating"];
		$this->author = $request["author"];
		$this->phone = $request["phone"];
		$this->email = $request["email"];
		$this->service = $request["service"];
		$this->doctor = $request["doctor"];

		if (empty($this->text)) {throw new Exception("Text is empty");}
		if (empty($this->author)) {throw new Exception("Author is empty");}
		if (empty($this->phone)) {throw new Exception("Phone is empty");}
	}

	public function createIblockElement()
	{
		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $this->title,
			"CODE" => $this->getCode(self::SUBJECT_INIT."_".$this->dateForCode),
			"ACTIVE" => "N",
			"PREVIEW_TEXT" => $this->text,
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PROPERTY_VALUES" => [
				"RATING" => $this->rating,
				"NAME" => $this->author,
				"PHONE" => $this->phone,
				"EMAIL" => $this->email,
				"SERVICE" => $this->service,
				"O_DOCTOR" => $this->doctor,
			]
		];

		$element = new CIBlockElement;
		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		$this->messages[] = "New IBlock element created, Id = " . $newElementId;

		return $newElementId;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);

		$emailFields["TITLE"] = $this->title;
		$emailFields["TEXT"] = $this->text;
		$emailFields["RATING"] = $this->rating;
		$emailFields["NAME"] = $this->author;
		$emailFields["PHONE"] = $this->phone;
		$emailFields["EMAIL"] = $this->email;
		$emailFields["SERVICE"] = $this->service;
		$emailFields["DOCTOR"] = $this->doctor;

		return $emailFields;
	}

}



