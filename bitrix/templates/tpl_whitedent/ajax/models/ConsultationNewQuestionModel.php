<?
class ConsultationNewQuestionModel extends BaseModel
{
	const EVENT_NAME = "CONSULTATION_NEW_QUESTION";
	const SUBJECT_INIT = "Новый вопрос в консультацию";
	const IBLOCK_TYPE = "modulefaq";

	public $question;
	public $subject;
	public $section;
	public $name;
	public $email;
	public $age;
	public $photo;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = ID_IBLOCK_CONSULTATION;
		}

		$this->question = $request["question"];
		$this->subject = $request["subject"];
		$this->section = $request["section"];
		$this->name = $request["name"];
		$this->email = $request["email"];
		$this->age = $request["age"];
		$this->photo = $_FILES["photo"];

		if (empty($this->question)) {throw new Exception("Question is empty");}
		if (empty($this->section)) {throw new Exception("Section is empty");}
		if (empty($this->name)) {throw new Exception("Name is empty");}
	}

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
		$subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $this->subject ? $this->subject : $subjectForUser,
			"CODE" => $this->getCode($subjectForCode),
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PREVIEW_TEXT" => $this->question,
			"IBLOCK_SECTION_ID" => $this->section,
			"PROPERTY_VALUES" => [
				"NAME" => $this->name,
				"EMAIL" => $this->email,
				"AGE" => $this->age,
				"FILE" => $this->photo,
				"STATUS" => 2, // Не отвечен, не опубликован
			]
		];

		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		$this->messages[] = "New IBlock element created, Id = " . $newElementId;

		return $newElementId;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);

		$emailFields["QUESTION"] = $this->question;
		$emailFields["SUBJECT"] = $this->subject;
		$emailFields["SECTION"] = $this->getSectionName($this->section);
		$emailFields["NAME"] = $this->name;
		$emailFields["EMAIL"] = $this->email;
		$emailFields["AGE"] = $this->age;

		return $emailFields;
	}

	public function getEmailFiles($elementId)
	{
		$element = getIbElement($elementId);

		$photoId = $element->props["FILE"]["VALUE"];

		if(empty($photoId)){
			return [];
		}

		return [$photoId];
	}


	private function getSectionName($sectionId): string
	{
		if(!$sectionId){
			return "";
		}

		$res = CIBlockSection::GetByID($sectionId);
		if(!$ar_res = $res->GetNext()){
			return "";
		}

		return $ar_res["NAME"];
	}

}



