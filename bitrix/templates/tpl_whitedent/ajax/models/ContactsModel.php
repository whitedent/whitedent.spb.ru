<?
class ContactsModel extends BaseModel
{
	const EVENT_NAME = "FEEDBACK_FORM";
	const SUBJECT_INIT = "";
	const IBLOCK_TYPE = "";
	const IBLOCK_ID = "";
	const MAKE_IBLOCK_ELEMENT = false;

	public $name;
	public $email;
	public $text;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = self::IBLOCK_ID;
		}

		$this->name = $request["name"];
		$this->email = $request["email"];
		$this->text = $request["message"];

		if (empty($this->name)) {throw new Exception("Name is empty");}
		if (empty($this->email)) {throw new Exception("Email is empty");}
		if (empty($this->text)) {throw new Exception("Text is empty");}
	}

	public function createIblockElement(){}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);

		$emailFields["NAME"] = $this->name;
		$emailFields["EMAIL"] = $this->email;
		$emailFields["TEXT"] = $this->text;

		return $emailFields;
	}

}



