<?
class ConsultationNewRegistrationModel extends BaseModel
{
	const EVENT_NAME = "CONSULTATION_NEW_REGISTRATION";
	const SUBJECT_INIT = "Запись на консультацию";
	const IBLOCK_TYPE = "content";

	const NAME_FIELD_NAME = "name";
	const MESSAGE_FIELD_NAME = "message";

	public $name;
	public $phone;
	public $message;
	public $serviceId;
	public $doctorId;
	public $actionId;
	public $isFromComagic;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = 14; // Записи на консультацию
		}

		$this->name = trim($request["name"]);
		$this->phone = $request["phone"];

		if (empty($this->name)) {throw new Exception("Поле имя пустое");}
		if (empty($this->phone)) {throw new Exception("Поле телефона пустое");}

		$this->message = !empty($request["message"]) ? $request["message"] : null;

		$this->serviceId = !empty($request["service_id"]) ? $request["service_id"] : false;

		if(!empty($request["page_type"]) && !empty($request["element_id"])){
			$type = $request["page_type"];
			if($type == "doctor"){
				$this->doctorId = $request["element_id"];
			}
			if($type == "discount"){
				$this->actionId = $request["element_id"];
			}
		}

		$this->isFromComagic = !empty($request["is_from_comagic"]);
	}

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
		$subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $subjectForUser,
			"CODE" => $this->getCode($subjectForCode),
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PROPERTY_VALUES" => [
				"NAME" => $this->name,
				"PHONE" => $this->phone,
				"MESSAGE" => $this->message,
			]
		];

		if($this->isFromComagic){
			$arElementFields["PROPERTY_VALUES"]["IS_FROM_COMAGIC"] = 1;
		}

		if($this->serviceId){
			$arElementFields["PROPERTY_VALUES"]["SERVICE_ID"] = $this->serviceId;
		}

		if($this->doctorId){
			$arElementFields["PROPERTY_VALUES"]["DOCTOR_ID"] = $this->doctorId;
		}

		if($this->actionId){
			$arElementFields["PROPERTY_VALUES"]["ACTION_ID"] = $this->actionId;
		}

		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		$this->messages[] = "New IBlock element created, Id = " . $newElementId;

		return $newElementId;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);

		$emailFields["NAME"] = $this->name;
		$emailFields["PHONE"] = $this->phone;

		$messages = [];

		if($this->message){
			$messages[] = "Дата визита или другая информация: ".$this->message;
		}

		if($this->serviceId){
			$messages[] = "Записались на прием со страницы услуги: ".$this->getElementName($this->serviceId);
		}

		if($this->doctorId){
			$messages[] = "Записались на прием к врачу: ".$this->getElementName($this->doctorId);
		}

		if($this->actionId){
			$messages[] = "Записались на консультацию по акции: ".$this->getElementName($this->actionId);
		}

		if($this->isFromComagic){
			$messages[] = "Клиент записался с условием скидки в 5%";
		}

		if($messages){
			$emailFields["MESSAGE"] = implode("\n\n", $messages);
		}

		return $emailFields;
	}

	private function getElementName($elementId): string
	{
		$element = getIbElementFields($elementId);
		return $element["NAME"];
	}

}



