<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if(count($arResult["GALLERY"]) > 0):?>
    <?if(isset($arResult["GALLERY"][0])):?>
        <?foreach($arResult["GALLERY"][0] as $gallery):?>
            <?if($gallery["ID"] == $arParams["FOLDER"]):?>
                <?foreach($arResult["GALLERY"][$gallery["ID"]] as $gal):?>
                    <div style="clear: both;"></div>
                    <h2><?=$gal["NAME"]?></h2>
                    <?$APPLICATION->IncludeComponent(
                    	"sr.bitrix:medialibrary.view",
                    	"fancybox-2",
                    	Array(
                    		"FOLDERS" => $gal["ID"],
                    		"VARIABLE" => "",
                    		"COUNT_IMAGE" => "",
                    		"RANDOM" => "N",
                    		"PAGE_LINK" => "",
                    		"PAGE_LINK_TEXT" => "",
                    		"ADD_TITLE" => "",
                    		"TITLE" => "N",
                    		"LOAD_JS" => "Y",
                    		"CACHE_TYPE" => "N",
                    		"CACHE_TIME" => "0",
                    		"RESIZE_MODE" => "Y",
                    		"RESIZE_MODE_W" => "145",
                    		"RESIZE_MODE_H" => "108",
                    		"WATERMARK_SETTING" => "Y",
                    		"PAGE_NAV_MODE" => "N",
                    		"ELEMENT_PAGE" => "5",
                    		"PAGER_SHOW_ALL" => "N",
                    		"PAGER_SHOW_ALWAYS" => "N",
                    		"PAGER_TITLE" => "Фотографии",
                    		"PAGER_TEMPLATE" => ""
                    	),
                        $component
                    );?>
                <?endforeach;?>
            <?endif;?>
        <?endforeach;?>
    <?endif;?>
<?endif;?>