jQuery(document).ready(function() {
    jQuery(".gallery").fancybox({
        prevEffect	: 'none',
  		nextEffect	: 'none',
        closeBtn	: true,
  		helpers	: {
            title	: {
                type: 'inside'
 			},
 			overlay	: {
                opacity : 0.8,
				css : {
				    'background-color' : '#000'
				}
 			},
 			thumbs	: {
				width	: 50,
				height	: 50
 			}
  		}
   	});
});