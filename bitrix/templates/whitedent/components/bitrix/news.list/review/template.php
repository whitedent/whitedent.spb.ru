<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$count = count($arResult["ITEMS"]);
foreach($arResult["ITEMS"] as $i=>$arItem):
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <a name="review-<?=$arItem["ID"]?>"></a>
        <strong>Отзыв от </strong><?=$arItem["NAME"]?><br />
        <?=$arItem["PREVIEW_TEXT"];?>
	</p>
    <?if($i < $count-1):?><hr /><?endif;?>
<?endforeach;?>