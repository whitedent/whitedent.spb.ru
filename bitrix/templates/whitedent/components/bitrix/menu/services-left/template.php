<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$c = count($arResult)?>
<?if(!empty($arResult)):?>
    <?foreach($arResult as $i=>$arItem):?>
        <?if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;?>
        <?if($arItem["SELECTED"] && $arItem["LINK"] != "/" || $APPLICATION->GetCurUri() == $arItem["LINK"]):?>
            <table class="tab_art_menu" style="margin-bottom: 20px;">
				<tbody><tr>
					<td class="art1_1"><img src="<?=SITE_TEMPLATE_PATH?>/images/art_tl.jpg" width="10" height="10" alt=""/></td>
					<td class="art2"></td>
					<td class="art1"><img src="<?=SITE_TEMPLATE_PATH?>/images/art_tr.jpg" width="10" height="10" alt=""/></td>
				</tr>
				<tr>
					<td class="art3"></td>
					<td class="art6" style="color: #B71B30;font-size: 90%;margin: 0px;padding: 0px;padding-left: 2%;"><?=$arItem["TEXT"]?></td>
					<td class="art4"></td>
				</tr>
				<tr>
					<td><img src="<?=SITE_TEMPLATE_PATH?>/images/art_bl.jpg" width="10" height="10" alt=""/></td>
					<td class="art5"></td>
					<td><img src="<?=SITE_TEMPLATE_PATH?>/images/art_br.jpg" width="10" height="10" alt=""/></td>
				</tr>
			</tbody></table>
        <?else:?>
            <p><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></p>
        <?endif?>
    <?endforeach?>
<?endif?>