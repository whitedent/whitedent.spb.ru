jQuery.noConflict();

(function($){
    $(function(){
        $(document).ready(function(){
            $('.showmap').fancybox({
                'width'  : 615,
                'height' : 470,
                autoSize : false
            });
            
            $(".image-open").fancybox({
            	helpers : {
            		title : {
            			type : 'inside'
            		}
            	}
            });

	    	$("#polis-dms").tooltip({placement:"right", html:true});
			$(".tooltip-link").tooltip({placement:"right"});
			$(".tooltip-link-left").tooltip({placement:"left"});
        });
    });
})(jQuery);