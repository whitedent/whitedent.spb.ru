function getOffsetSum(elem) {
    var top=0;
    var left = 0;
    while ( elem) {
        top = top + parseInt(elem.offsetTop)
        left = left + parseInt(elem.offsetLeft)
        elem = elem.offsetParent;
    }
    return {top: top, left: left}
}

function getOffset(elem) {
    if ( elem.getBoundingClientRect()) {
        return getOffsetRect(elem)
    } else {
        return getOffsetSum(elem)
    }
}

g_current_visible = "";
function hideDiv() {
	if ( g_current_visible) {
		g_current_visible.style.display = "none";
	}
}

function closeDiv() {
	var timeOut = window.setTimeout( "hideDiv()", 350);
}

function showDiv( div, event) {
	hideDiv();
	g_current_visible = div;
	
 	if ( div && div.style.display == "none") {
		current_visible = current_name;
		div.style.display = "block";
	}
	var wwidth = (window.innerWidth)?window.innerWidth: ((document.all)?document.body.offsetWidth:null);
	if (!event) event=window.event;
	x = (event.clientX) ?  event.clientX : ((event.pageX) ? event.pageX : event.screenX);
	y = (event.clientY) ? (event.clientY + 15) : (event.pageY + 15);
	if ( document.documentElement.scrollTop == 0) {
		y += document.body.scrollTop;
	} else {
		y += document.documentElement.scrollTop;
	}
	
	if ( x + 300 >= wwidth) {
		x -= 280;
	}
	div.style.top = y+"px";
	div.style.left = x+"px";
}