<?
header("Content-type: text/html; charset=windows-1251");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if($_POST["name"] != "" && $_POST["phone"] != "")
{
    $event              = array();
    $event["NAME"]      = utf8win1251(htmlspecialcharsex(strip_tags($_POST["name"])));
    $event["PHONE"]     = utf8win1251(htmlspecialcharsex(strip_tags($_POST["phone"])));
    $event["MESSAGE"]   = utf8win1251(htmlspecialcharsex(strip_tags($_POST["message"])));
    
    CEvent::SendImmediate("NOTE", "s1", $event, "Y", 9);
}
?>