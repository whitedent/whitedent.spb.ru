<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.banner.top.php"), array(), array("MODE" => "html"));?>
<div class="middle">
	<div class="middle_ie6">
    	<div class="middle_left" style="padding: 0 !important; margin-top: -20px;">
            <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.banner.left.php"), array(), array("MODE" => "html"));?>
            <div style="margin-bottom: 20px;"></div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "header-left",
                Array(
                    "ROOT_MENU_TYPE" => "left",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );?>
            
            <div style="padding: 20px 0px 0px 0px;">
        		<?$APPLICATION->IncludeComponent(
					"primepix:vkontakte.group",
					"",
					Array(
						"ID_GROUP" => "64206144",
						"TYPE_FORM" => "0",
						"WIDTH_FORM" => "200",
						"HEIGHT_FORM" => "290"
					)
				);?>
        	</div>
    	</div>
    	<div class="middle_right">
            <h1 class="red"><?$APPLICATION->ShowTitle(false)?></h1>