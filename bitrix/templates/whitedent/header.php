<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
	<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css-js/fancybox/jquery.fancybox.css" />
    <title><?$APPLICATION->ShowTitle()?></title>
    <script src="<?=SITE_TEMPLATE_PATH?>/css-js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/css-js/jquery.cookie.js" type="text/javascript"></script>
    <?php
	    if($_COOKIE['cookie_url_key']){ ?>
	        <script src="<?=SITE_TEMPLATE_PATH?>/css-js/phone-switch-cookie.js" type="text/javascript"></script><?php
	    }elseif(strpos($_SERVER['REQUEST_URI'], 'utm_medium=cpc')){ ?>
	        <script src="<?=SITE_TEMPLATE_PATH?>/css-js/phone-switch-context.js" type="text/javascript"></script><?php
	    }else{ ?>
	        <script src="<?=SITE_TEMPLATE_PATH?>/css-js/phone-switch-other.js" type="text/javascript"></script><?php
	    }
    ?>	
    <script src="<?=SITE_TEMPLATE_PATH?>/css-js/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/css-js/script.js" type="text/javascript"></script>
	<!--[if lt IE 7]>
          <![if gte IE 5.5]>
          <script type="text/javascript" src="fixpng.js"></script>
               <style type="text/css"> 
                    .iePNG, .fon_text {filter:expression(fixPNG_scale(this));} 
                    IMG {filter:expression(fixPNG_crop(this));} 
                    .iePNG A {position: relative;}
               </style>
          <![endif]>
     <![endif]-->
     <?/*<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/css-js/cons.js"></script>*/?>
     <?$APPLICATION->ShowHead();?>
</head>
<body>
<div id="top">
    <div style="height: 210px;">
        <div class="top_ie">
            <div class="logo"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt=""/></a></div>
            <div class="company_desc">Клиника эстетической стоматологии</div>
            <div class="phone"><?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.header.phone.php"), array(), array("MODE" => "html"));?>
		<span style="color:#018dcb;text-align:center;font-size:14px!important;">м. Приморская \ Васильевский остров</span>
	    </div>
       	</div>
    </div>
    <div class="menu">
        <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.template.menu.top.php"), array(), array("MODE" => "html"));?>
    </div>
    <div class="sub-menu-top">
        <div class="block-l">
            <ul class="m">
                <li><a href="/files/price.doc" class="price" target="_blank">Скачать прайс</a></li>
                <li><a href="/ask/">Задать вопрос</a></li>
                <li><a href="/faq/">Вопросы и ответы</a></li>
				<li><a href="/news/" class="last">Новости</a></li>
                <?/*<li><a href="/photos/">Фото до и после</a></li>*/?>
            </ul>
        </div>
		<div class="block-c"><a href="/note.php" class="btn-note showmap fancybox.iframe"></a></div>
        <div class="block-r">
            <?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/inc.header.links.php"), array(), array("MODE" => "html"));?>
        </div>
    </div>
    <div class="clear"></div>