<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $DB;
$sql = "SELECT ID, NAME FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1'";
$res = $DB->Query($sql);
if(mysql_num_rows($res->result) > 0)
{
    $arFolders[0] = "";
    while($media = mysql_fetch_array($res->result, MYSQL_ASSOC))
        $arFolders[$media["ID"]] = "[".$media["ID"]."] ".substr($media["NAME"], 0, 50);
}

$arTemplateParameters = array(
	"FOLDER" => array(
	   "NAME" => GetMessage("FOLDER"),
	   "TYPE" => "LIST",
	   "VALUES" => $arFolders,
        "MULTIPLE" => "N",
	),
);
?>