<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	global $USER;
	
	if ($APPLICATION->GetCurDir() == "/consultation/")
	{
		if (!empty($arResult['ITEMS']))
		{

			foreach ($arResult['ITEMS'] as $key => $arItem)
			{
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
				$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
				if($arSec = $res->GetNext())
				{
					if ($USER->IsAdmin() && $arItem['PROPERTIES']['STATUS']["VALUE_ENUM_ID"] == '2')
					{
						$class = " unanswered";
					}
					elseif ( ($USER->IsAdmin() && $arItem['PROPERTIES']['STATUS']["VALUE_ENUM_ID"] == '3') || ($USER->IsAdmin() && $arItem['PROPERTIES']['STATUS']["VALUE_ENUM_ID"] == '4'))
					{
						$class = " answered";
					}
					else
					{
						$class = "";
					}
?>
<div class="question_item<?=$class?>">
	<p class="topic"><span>Тема: </span><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem["NAME"]?></a></p>
	<p class="question"><span>Вопрос: </span><?=$arItem["PREVIEW_TEXT"]?></p>
	<p class="category"><span>Категория: </span><a href="<?=$arSec["SECTION_PAGE_URL"]?>"><?=$arSec['NAME']?></a></p> <p class="name"><?=$arItem["PROPERTIES"]["NAME"]["VALUE"] ?></p>
	<p class="date"><span>Дата: </span><?=$arItem["DATE_CREATE"]?></p><p class="detail"><a href="<?=$arItem['DETAIL_PAGE_URL']?>">Смотреть ответ</a></p>
</div>
<?
				}
			}
			echo "<div style='float:left;'>".$arResult["NAV_STRING"]."</div>";
		}
	}
	
?>
