<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
   	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
   	?>
        <?if($arParams["ID"] == $arItem["ID"]):?>
            <table class="tab_art_menu" style="margin-bottom: 20px;" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<tbody><tr>
					<td class="art1_1"><img src="<?=SITE_TEMPLATE_PATH?>/images/art_tl.jpg" width="10" height="10" alt=""/></td>
					<td class="art2"></td>
					<td class="art1"><img src="<?=SITE_TEMPLATE_PATH?>/images/art_tr.jpg" width="10" height="10" alt=""/></td>
				</tr>
				<tr>
					<td class="art3"></td>
					<td class="art6" style="color: #B71B30;font-size: 90%;margin: 0px;padding: 0px;padding-left: 2%;"><?=$arItem["NAME"]?></td>
					<td class="art4"></td>
				</tr>
				<tr>
					<td><img src="<?=SITE_TEMPLATE_PATH?>/images/art_bl.jpg" width="10" height="10" alt=""/></td>
					<td class="art5"></td>
					<td><img src="<?=SITE_TEMPLATE_PATH?>/images/art_br.jpg" width="10" height="10" alt=""/></td>
				</tr>
			</tbody></table>        
        <?else:?>
            <p><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></p>
        <?endif;?>
<?endforeach;?>