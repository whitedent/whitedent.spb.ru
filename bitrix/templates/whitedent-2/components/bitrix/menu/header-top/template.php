<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$c = count($arResult)?>
<?if(!empty($arResult)):?>
    <div>
        <table class="tab_menu">
            <tr>
                <?foreach($arResult as $i=>$arItem):?>
                    <?if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;?>
                    <?if($arItem["SELECTED"] && $arItem["LINK"] != "/" || $APPLICATION->GetCurUri() == $arItem["LINK"]):?>
                        <?if($i == 0):?>
                            <td><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_left_act.png" width="19" height="60"/></td>
                        <?endif;?>
                        <td style="background:url('<?=SITE_TEMPLATE_PATH?>/images/menu_back_act.jpg') 0% 0% repeat-x;width:11%">
                            <a href="<?=$arItem["LINK"]?>" class="white"><?=$arItem["TEXT"]?></a>
                        </td>
                        <?if(($c-1) == $i):?>
                            <td><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_right_act.png" width="19" height="60"/></td>
                        <?else:?>
                            <td><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_vert_act.jpg" width="2" height="60"/></td>
                        <?endif;?>
                    <?else:?>
                        <?if($i == 0):?>
                            <td><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_left_pas.png" width="19" height="60"/></td>
                        <?endif;?>
                        <td style="background:url('<?=SITE_TEMPLATE_PATH?>/images/menu_back_pas.jpg') 0% 0% repeat-x;width:11%">
                            <a href="<?=$arItem["LINK"]?>" class="white"><?=$arItem["TEXT"]?></a>
                        </td>
                        <?if(($c-1) == $i):?>
                            <td><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_right_pas.png" width="19" height="60"/></td>
                        <?else:?>
                            <td><img src="<?=SITE_TEMPLATE_PATH?>/images/menu_vert_pas.jpg" width="2" height="60"/></td>
                        <?endif;?>
                    <?endif?>
                <?endforeach?>
            </tr>
         </table>
    </div>
<?endif?>