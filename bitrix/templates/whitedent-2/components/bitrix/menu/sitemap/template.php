<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult)):?>
<ul class="left-menu">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
    
    <li><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a>
    <?if($arItem["LINK"] == "/articles/"):?>
        <?$APPLICATION->IncludeComponent(
            	"bitrix:news.list",
            	"sitemap",
            	Array(
            		"AJAX_MODE" => "N",
            		"IBLOCK_TYPE" => "content",
            		"IBLOCK_ID" => "2",
            		"NEWS_COUNT" => "10000",
            		"SORT_BY1" => "ACTIVE_FROM",
            		"SORT_ORDER1" => "DESC",
            		"SORT_BY2" => "SORT",
            		"SORT_ORDER2" => "ASC",
            		"FILTER_NAME" => "",
            		"FIELD_CODE" => array(),
            		"PROPERTY_CODE" => array(),
            		"CHECK_DATES" => "Y",
            		"DETAIL_URL" => "/articles/#ID#/",
            		"PREVIEW_TRUNCATE_LEN" => "",
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"SET_TITLE" => "N",
            		"SET_STATUS_404" => "N",
            		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            		"ADD_SECTIONS_CHAIN" => "N",
            		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
            		"PARENT_SECTION" => "",
            		"PARENT_SECTION_CODE" => "",
            		"INCLUDE_SUBSECTIONS" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "36000000",
            		"CACHE_FILTER" => "N",
            		"CACHE_GROUPS" => "Y",
            		"DISPLAY_TOP_PAGER" => "N",
            		"DISPLAY_BOTTOM_PAGER" => "N",
            		"PAGER_TITLE" => "",
            		"PAGER_SHOW_ALWAYS" => "N",
            		"PAGER_TEMPLATE" => "",
            		"PAGER_DESC_NUMBERING" => "N",
            		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            		"PAGER_SHOW_ALL" => "N",
            		"AJAX_OPTION_JUMP" => "N",
            		"AJAX_OPTION_STYLE" => "Y",
            		"AJAX_OPTION_HISTORY" => "N"
            	),
            $component
            );?>
    <?endif;?>
    </li>
	
<?endforeach?>
	<li><a href="/news/">Новости</a>
		<?$APPLICATION->IncludeComponent(
            	"bitrix:news.list",
            	"sitemap",
            	Array(
            		"AJAX_MODE" => "N",
            		"IBLOCK_TYPE" => "content",
            		"IBLOCK_ID" => "1",
            		"NEWS_COUNT" => "1000",
            		"SORT_BY1" => "ACTIVE_FROM",
            		"SORT_ORDER1" => "DESC",
            		"SORT_BY2" => "SORT",
            		"SORT_ORDER2" => "ASC",
            		"FILTER_NAME" => "",
            		"FIELD_CODE" => array(),
            		"PROPERTY_CODE" => array(),
            		"CHECK_DATES" => "Y",
            		"DETAIL_URL" => "/news/#ID#/",
            		"PREVIEW_TRUNCATE_LEN" => "",
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"SET_TITLE" => "N",
            		"SET_STATUS_404" => "N",
            		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            		"ADD_SECTIONS_CHAIN" => "N",
            		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
            		"PARENT_SECTION" => "",
            		"PARENT_SECTION_CODE" => "",
            		"INCLUDE_SUBSECTIONS" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "36000000",
            		"CACHE_FILTER" => "N",
            		"CACHE_GROUPS" => "Y",
            		"DISPLAY_TOP_PAGER" => "N",
            		"DISPLAY_BOTTOM_PAGER" => "N",
            		"PAGER_TITLE" => "",
            		"PAGER_SHOW_ALWAYS" => "N",
            		"PAGER_TEMPLATE" => "",
            		"PAGER_DESC_NUMBERING" => "N",
            		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            		"PAGER_SHOW_ALL" => "N",
            		"AJAX_OPTION_JUMP" => "N",
            		"AJAX_OPTION_STYLE" => "Y",
            		"AJAX_OPTION_HISTORY" => "N"
            	),
            $component
            );?>
	</li>
	<li><a href="/files/price.doc">Скачать прайс</a></li>
	<li><a href="/ask/">Задать вопрос</a></li>
	<li><a href="/faq/">Вопросы и ответы</a></li>

<li><a href="/galileos-comfort/">Компьютерная 3D томография зубов</a></li>
                    <li><a href="/gigiena-polosti-rta/">Гигиена полости рта</a></li>
                    <li><a href="/terapiya/">Терапия</a></li>
                    <li><a href="/hirurgiya/">Хирургия</a></li>
                    <li><a href="/detskaya-stomatologiya/">Детская стоматология</a></li>
                    <li><a href="/implantatsiya/">Имплантация</a></li>
                    <li><a href="/konsultatsii/">Консультации</a></li>
                    <li><a href="/obezbolivaniye/">Обезболивание</a></li>
                    <li><a href="/ortopediya/">Ортопедия</a></li>
                    <li><a href="/zubotekhnicheskaya-laboratoriya-stomatologicheskoy-kliniki/">Зуботехническая лаборатория стоматологической клиники</a></li>
                    




</ul>
<?endif?>