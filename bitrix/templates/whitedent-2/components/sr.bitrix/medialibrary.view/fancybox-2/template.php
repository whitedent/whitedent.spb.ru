<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<?if(count($arResult["ERRORS"]) > 0):?>
    
<?else:?>
    <?if(count($arResult["FOLDER_IMAGE"]) > 0):?>
        <?foreach($arResult["FOLDER_IMAGE"] as $img):?>
            <div class="img-gallery">
                <a href="<?=$img["URL"]?>" class="gallery" rel="gallery" id="<?=$img["RAND_ID"]?>">
                    <img src="<?=$img["URL"]?>"<?=$img["SIZE"]?> id="img_<?=$img["RAND_ID"]?>" title="<?=$arResult["DESCRIPTION"][$img["ID"]];?>" alt="<?=$arResult["DESCRIPTION"][$img["ID"]];?>"/>
                </a>
            </div>       
        <?endforeach;?>
        <div class="clear">&nbsp;</div>
    <?endif;?>
    <?if($arParams["PAGE_NAV_MODE"] == "Y"):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
<?endif;?>