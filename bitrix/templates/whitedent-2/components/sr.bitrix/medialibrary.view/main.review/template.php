<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if(count($arResult["ERRORS"]) > 0):?>
    
<?else:?>
    <?if(count($arResult["FOLDER_IMAGE"]) > 0):?>
    <div class="review-line">
        <?foreach($arResult["FOLDER_IMAGE"] as $img):?>
            <div>
                <a href="<?=$img["URL"]?>" rel="image_group" class="image-open" id="<?=$img["RAND_ID"]?>">
                    <img src="<?=$img["URL"]?>"<?=$img["SIZE"]?> id="img_<?=$img["RAND_ID"]?>" title="<?=$arResult["DESCRIPTION"][$img["ID"]];?>" alt="<?=$arResult["DESCRIPTION"][$img["ID"]];?>"/>
                </a>
            </div>
        <?endforeach;?>
    </div>
    <div style="clear: both;"></div>
    <p><a href="/gallerys/"></a></p>
    <?endif;?>
<?endif;?>