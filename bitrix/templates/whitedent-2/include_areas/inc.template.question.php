<?
define("IBLOCK_ID_DOCTORS", 5);
define("IBLOCK_ID_SERVICES", 8);
define("IBLOCK_ID_PROP_PUBLIC", 1);


CModule::IncludeModule("iblock");
$doctor = intval($_GET["doctor"]);

if($_POST["send"] == 1)
{
    unset($doctor);
    $arError = array();
    $arResult = false;
    
    $name       = htmlspecialcharsbx(strip_tags($_POST["name"]));
    $email      = htmlspecialcharsbx(strip_tags($_POST["email"]));
    $phone      = htmlspecialcharsbx(strip_tags($_POST["phone"]));
    $service    = htmlspecialcharsbx(strip_tags($_POST["service"]));
    $doctor     = htmlspecialcharsbx(strip_tags($_POST["doctor"]));
    $question   = htmlspecialcharsbx(strip_tags($_POST["question"]));
    $public     = strip_tags($_POST["public"]) == "on" ? IBLOCK_ID_PROP_PUBLIC : 0;
    
    if($name == "")
        $arError[] = "Необходимо представиться.";
    if($email == "")
        $arError[] = "Введите ваш электронный адрес.";
    if($question == "")
        $arError[] = "Задайте свой вопрос.";
    
    $arLoadProductArray = array(
        "IBLOCK_SECTION_ID" => $service,
        "IBLOCK_ID"         => IBLOCK_ID_SERVICES,
        "PROPERTY_VALUES"   => array(
            "F_NAME" => $name,
            "F_EMAIL" => $email,
            "F_PHONE" => $phone,
            "F_PUBLIC" => $public,
            "F_QUESTION_DOCTOR" => $doctor
        ),
        "NAME"              => "Вопрос от ".date("d.m.Y H:i"),
        "ACTIVE"            => "N",
        "PREVIEW_TEXT"      => $question
    );
    
    if(count($arError) == 0)
    {
        $el = new CIBlockElement();
        if($PRODUCT_ID = $el->Add($arLoadProductArray))
        {
            $arResult = true;
            
            CEvent::SendImmediate("FAQ", "s1", array(
                    "FAQ_F_NAME" => $name,
                    "FAQ_F_EMAIL" => $email,
                    "FAQ_F_PHONE" => $phone,
                    "FAQ_F_PUBLIC" => $public,
                    "FAQ_F_QUESTION_DOCTOR" => $doctor,
                    "FAQ_QUESTION" => $question
                ),
                "Y",
                8
            );
        }
        else
            $arError[] =  "Возникла ошибка: ".$el->LAST_ERROR;
    }
}

if($arResult == false || count($arError) || !isset($_POST["send"])):
    if(count($arError))
    {
        echo '<p style="color:tomato;">';
        foreach($arError as $err)
            echo $err.'<br/>';
        echo '</p>';
    }
?>
<form class="ask" action="/ask/" method="post">
    <input type="hidden" name="send" value="1"/>
    <table>
        <tr>
            <td style="width: 20px;"><span class="required">*</span></td>
            <td>Представьтесь:</td>
            <td><input type="text" name="name" value="<?=$name?>"/></td>
        </tr>
        <tr>
            <td><span class="required">*</span></td>
            <td>Электронная почта:</td>
            <td><input type="text" name="email" value="<?=$email?>"/></td>
        </tr>
        <tr>
            <td></td>
            <td>Контактный телефон:</td>
            <td><input type="text" name="phone" value="<?=$phone?>"/></td>
        </tr>
        <tr>
            <td><span class="required">*</span></td>
            <td>Услуга:</td>
            <td>
                <select name="service">
                    <option value="0">Общий вопрос</option>
                    <?
                    $arFilter = Array('IBLOCK_ID' => IBLOCK_ID_SERVICES, 'DEPTH_LEVEL' => 1);
                    $db_list = CIBlockSection::GetList(array($by => $order), $arFilter, false);
                    while($ar_result = $db_list->GetNext())
                        echo '<option value="'.$ar_result['ID'].'"'.($ar_result['ID'] == $service ? ' selected="selected"' : '').'>'.$ar_result['NAME'].'</option>';
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Врач:</td>
            <td>
                <select name="doctor">
                    <option value="0">Выбрать</option>
                    <?
                    $arSelect = array("ID", "NAME");
                    $arFilter = array("IBLOCK_ID" => IBLOCK_ID_DOCTORS, "ACTIVE" => "Y");
                    $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 100), $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        echo '<option value="'.$arFields["ID"].'"'.($arFields['ID'] == $doctor ? ' selected="selected"' : '').'>'.$arFields["NAME"].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><span class="required">*</span></td>
            <td>Текст вопроса:</td>
            <td><textarea name="question"></textarea></td>
        </tr>
        <tr>
            <td colspan="3" style="padding-bottom: 10px;"><input type="checkbox" name="public" id="public"<?=($public == IBLOCK_ID_PROP_PUBLIC ? ' checked="checked"' : '')?>/><label for="public">&nbsp;Опубликовать в разделе "<a href="/faq/">Вопросы и ответы</a>". Ваша контактная информация на сайте отображена не будет.</label></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><input type="submit" value="Отправить"/></td>
        </tr>
    </table>
</form>
<?elseif($arResult == true):?>
    <p>Спасибо! Ваш вопрос отправлен!</p>
<?endif;?>