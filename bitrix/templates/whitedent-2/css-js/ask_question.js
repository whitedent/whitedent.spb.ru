g_doctors = new Object();
g_services = new Object();

function showDialog( id) {
    showElement( $( 'shadow_popup'));
    showElement( $( id));
    centreElement( $( id));
}

function set_service( id, flag) {
    g_all_width = g_all_width ? g_all_width : ((document.body.scrollWidth) ? document.body.scrollWidth : document.documentElement.scrollWidth);
	$( "service_form_active").style.left = Math.ceil( g_all_width * 0.06) + 20 + "px";
    if ( !flag) {
		showElement( $( "service_popup"));
		showElement( $( "shadow_popup"));
		if ( $( "service_form_passive").style.visibility == "visible") {
			showElement( $( "service_form_passive"), 1);
			showElement( $( "service_form_active"));
		}
    } else {
		showElement( $( "service_form_passive"), 1);
		showElement( $( "service_form_active"));
    }
    $( "form_service").value = id;
    $( "service_name").innerHTML = g_services[id].name;
}

function set_doctor( id, flag) {
    g_all_width = g_all_width ? g_all_width : ((document.body.scrollWidth) ? document.body.scrollWidth : document.documentElement.scrollWidth);
	$( "doctor_form_active").style.left = Math.ceil( g_all_width * 0.06) + 20 + "px";
    if ( !flag ) {
		showElement( $( "doctor_popup"));
		showElement( $( "shadow_popup"));
		if ( $( "doctor_form_passive").style.visibility == "visible") {
			showElement( $( "doctor_form_passive"), 1);
			showElement( $( "doctor_form_active"));
		}
    } else {
		showElement( $( "doctor_form_passive"), 1);
		showElement( $( "doctor_form_active"));
    }
    $( "form_doctor").value = id;
    $( "doctor_name").innerHTML = g_doctors[id].name;
    $( "doctor_name").href='/doctors.php?id=' + id;
    $( "doctor_job").innerHTML = g_doctors[id].job;
    $( "doctor_src").src = "img.php?src=" + g_doctors[id].src + "&width=25&height=30&normal=1";
}

function resetDoctor() {
	showElement( $( "doctor_form_passive"), 1);
	showElement( $( "doctor_form_active"));
    $( "form_doctor").value = "";
}

function resetService() {
	showElement( $( "service_form_passive"), 1);
	showElement( $( "service_form_active"));
    $( "form_service").value = "";
}