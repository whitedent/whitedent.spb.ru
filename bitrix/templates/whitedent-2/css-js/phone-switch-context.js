// Подмена номера телефона при переходе с поисковиков
jQuery(document).ready(function($)
{
    switch_phone('+7 (812) 346-80-14', true);   //второй номер

	function switch_phone(new_phone, set_cookie){
		var search_doms = ['phone'];
		$.each(search_doms, function(index, value){
			var dom = 'span.'+value;
			if($(dom).length){
				$(dom).html($(dom).html().replace(/\+7 \(812\) 355-41-09, \+7 \(812\) 346-80-14/ig, new_phone));     //замена номера
			}
		});

		if(set_cookie == true){
			$.cookie('cookie_url_key', new_phone, {				// ставим куку
			    expires: 9999,       							// срок хранения 2 недели
			    path: '/',         								// доступна по всему сайту
			});
		}

	}
});

