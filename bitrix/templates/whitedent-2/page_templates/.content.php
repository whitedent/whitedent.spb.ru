<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
IncludeTemplateLangFile(__FILE__);
$TEMPLATE["index-page.php"] = Array("name" => "Главная страница", "sort" => 0);
$TEMPLATE["inner-nocol-page"] = Array("name" => "Внутренняя страница без колонок", "sort" => 1);
$TEMPLATE["inner-one-col-page"] = Array("name" => "Внутренняя страница с 1 колонкой", "sort" => 2);
$TEMPLATE["inner-service-page"] = Array("name" => "Внутренняя страница раздела услуги", "sort" => 3);
$TEMPLATE["inner-two-col-page"] = Array("name" => "Внутренняя страница с 2 колонками", "sort" => 4);
?>