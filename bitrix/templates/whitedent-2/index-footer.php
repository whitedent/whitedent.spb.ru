</div>
	<div class="middle_right2">
      <?if ($APPLICATION->GetCurDir() != "/consultation/") {?>
		<div class="new_index" style="margin-top: 20px;">
            <h2>Отзывы наших клиентов</h2>
            <?$APPLICATION->IncludeComponent("sr.bitrix:medialibrary.view", "main.review", Array(
            	"FOLDERS" => "5",	// Папка медиабиблиотеки
            	"VARIABLE" => "",	// Название переменной
            	"COUNT_IMAGE" => "10",	// Количество картинок
            	"RANDOM" => "N",	// Случайный порядок
            	"PAGE_LINK" => "",	// Ссылка на страницу
            	"PAGE_LINK_TEXT" => "",	// Текст ссылки
            	"ADD_TITLE" => "",	// Добавка к заголовку
            	"TITLE" => "N",	// Устанавливать заголовок
            	"LOAD_JS" => "Y",	// Загружать Fancybox
            	"CACHE_TYPE" => "A",	// Тип кеширования
            	"CACHE_TIME" => "3600",	// Время кеширования (сек.)
            	"RESIZE_MODE" => "Y",	// Сжатие изображений на лету
            	"RESIZE_MODE_W" => "145",	// По ширине
            	"RESIZE_MODE_H" => "108",	// По высоте
            	"WATERMARK_SETTING" => "Y",	// Включить водяные знаки
            	"PAGE_NAV_MODE" => "N",	// Включить постраничную навигацию
            	"ELEMENT_PAGE" => "",	// Количество изображений на странице
            	"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            	"PAGER_SHOW_ALWAYS" => "N",	// Всегда показывать навигацию
            	"PAGER_TITLE" => "",	// Подпись постраничной навигации
            	"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
            	),
            	false
            );?>
        </div>
      <? } ?>
      <br>
      <br>
		<div class="new_index" style="margin-top:-4px;">
                   <?if ($APPLICATION->GetCurDir() != "/consultation/") {?>
			<h2>Новости</h2>
            <?$APPLICATION->IncludeComponent(
            	"bitrix:news.list",
            	"news.last",
            	Array(
            		"AJAX_MODE" => "N",
            		"IBLOCK_TYPE" => "content",
            		"IBLOCK_ID" => "1",
            		"NEWS_COUNT" => "2",
            		"SORT_BY1" => "ACTIVE_FROM",
            		"SORT_ORDER1" => "DESC",
            		"SORT_BY2" => "SORT",
            		"SORT_ORDER2" => "ASC",
            		"FILTER_NAME" => "",
            		"FIELD_CODE" => array(),
            		"PROPERTY_CODE" => array(),
            		"CHECK_DATES" => "Y",
            		"DETAIL_URL" => "/news/#ID#/",
            		"PREVIEW_TRUNCATE_LEN" => "",
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"SET_TITLE" => "N",
            		"SET_STATUS_404" => "N",
            		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            		"ADD_SECTIONS_CHAIN" => "N",
            		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
            		"PARENT_SECTION" => "",
            		"PARENT_SECTION_CODE" => "",
            		"INCLUDE_SUBSECTIONS" => "Y",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "36000000",
            		"CACHE_FILTER" => "N",
            		"CACHE_GROUPS" => "Y",
            		"DISPLAY_TOP_PAGER" => "N",
            		"DISPLAY_BOTTOM_PAGER" => "N",
            		"PAGER_TITLE" => "",
            		"PAGER_SHOW_ALWAYS" => "N",
            		"PAGER_TEMPLATE" => "",
            		"PAGER_DESC_NUMBERING" => "N",
            		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            		"PAGER_SHOW_ALL" => "N",
            		"AJAX_OPTION_JUMP" => "N",
            		"AJAX_OPTION_STYLE" => "Y",
            		"AJAX_OPTION_HISTORY" => "N"
            	),
            false
            );?>
         <? } ?>
			<h2>Наши лицензии</h2>
			<div>
				<div class="license"><a href="/images/licenses/sert01.jpg" class="image-open"><img src="/images/licenses/sert01.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/licenses/sert02.jpg" class="image-open"><img src="/images/licenses/sert02.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/licenses/sert03.jpg" class="image-open"><img src="/images/licenses/sert03.jpg" alt=""/></a></div>
				<div style="clear:both"></div>
			</div>
			<h2 style="margin-top: 20px;">Благодарности</h2>
			<div>
				<div class="license"><a href="/images/gratitude/001.jpg" class="image-open"><img src="/images/gratitude/001.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/gratitude/002.jpg" class="image-open"><img src="/images/gratitude/002.jpg" alt=""/></a></div>
				<div class="license"><a href="/images/gratitude/003.jpg" class="image-open"><img src="/images/gratitude/003.jpg" alt=""/></a></div>
				<div style="clear:both"></div>
			</div>
		</div>
        <div class="new_index" style="margin-top: 20px;">
            <h2>Наши специалисты</h2>
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "main.doctors", array(
	"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "5",
		"NEWS_COUNT" => "1000",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ID",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "D_SPECIALTY",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
        </div>
<div class="new_index" style="margin-top: 20px;">
<h2>Приглашаем к сотрудничеству</h2>
<p>Одна из лучших стоматологических клиник Санкт-Петербурга приглашает на работу талантливых и опытных стоматологов всех профилей, которые умеют и любят работать! У нас отличные условия, хороший коллектив и мы умеем договариваться. Клиника ВайтДент удобно расположена на Васильевском острове.</p>
</div>
	</div>
	</div>