<?php
$MESS["FOLDERS"]            = "Папка медиабиблиотеки";
$MESS["COUNT_IMAGE"]        = "Количество картинок";
$MESS["VARIABLE"]           = "Название переменной";
$MESS["NULL_VALUE_LIST"]    = "Не определять";
$MESS["TITLE"]              = "Устанавливать заголовок";
$MESS["ADD_TITLE"]          = "Добавка к заголовку";
$MESS["LOAD_JS"]            = "Загружать Fancybox";
$MESS["PAGE_LINK"]          = "Ссылка на страницу";
$MESS["PAGE_LINK_TEXT"]     = "Текст ссылки";
$MESS["RESIZE_MODE"]        = "Сжатие изображений на лету";
$MESS["RESIZE_MODE_W"]      = "По ширине";
$MESS["RESIZE_MODE_H"]      = "По высоте";
$MESS["RANDOM"]             = "Случайный порядок";

$MESS["WATERMARK_NAME"]     = "Настройка водяного знака";
$MESS["WATERMARK_SETTING"]  = "Включить водяные знаки";

$MESS["NAVIGATION"]         = "Настройка постраничной навигации";
$MESS["PAGE_NAV_MODE"]      = "Включить постраничную навигацию";
$MESS["ELEMENT_PAGE"]       = "Количество изображений на странице";
$MESS["PAGER_SHOW_ALL"]     = "Показывать ссылку \"Все\"";
$MESS["PAGER_TITLE"]        = "Подпись постраничной навигации";
$MESS["PAGER_TEMPLATE"]     = "Шаблон постраничной навигации";
$MESS["PAGER_SHOW_ALWAYS"]  = "Всегда показывать навигацию";
?>