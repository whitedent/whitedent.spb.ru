<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $DB;
$sql = "SELECT ID, NAME FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1'";
$res = $DB->Query($sql);
if(mysql_num_rows($res->result) > 0)
{
    $arFolders[0] = "[0] ".GetMessage("NULL_VALUE_LIST");
    while($media = mysql_fetch_array($res->result, MYSQL_ASSOC))
        $arFolders[$media["ID"]] = "[".$media["ID"]."] ".substr($media["NAME"], 0, 50);
}

$arComponentParameters = array(
	"GROUPS" => array(
        "NAVIGATION" => array(
			"NAME" => GetMessage("NAVIGATION")
		),
	),
	"PARAMETERS" => array(
		"FOLDERS" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("FOLDERS"),
			"TYPE" => "LIST",
			"VALUES" => $arFolders,
		),
        
        "VARIABLE" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("VARIABLE"),
			"TYPE" => "TEXTBOX",
		),
        
		"COUNT_IMAGE" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("COUNT_IMAGE"),
			"TYPE" => "TEXTBOX",
		),
        
        "RANDOM" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("RANDOM"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
		),
        
        "PAGE_LINK" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("PAGE_LINK"),
			"TYPE" => "TEXTBOX",
		),
        
        "PAGE_LINK_TEXT" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("PAGE_LINK_TEXT"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "Просмотреть все фото",
		),
        
        "ADD_TITLE" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("ADD_TITLE"),
			"TYPE" => "TEXTBOX",
		),
        
        "TITLE" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("TITLE"),
			"TYPE" => "CHECKBOX",
		),
        
        "LOAD_JS" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("LOAD_JS"),
			"TYPE" => "CHECKBOX",
		),
        
        "RESIZE_MODE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("RESIZE_MODE"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
		),
        
        "RESIZE_MODE_W" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("RESIZE_MODE_W"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "145",
		),
        
        "RESIZE_MODE_H" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("RESIZE_MODE_H"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "108",
		),
        
        "WATERMARK_SETTING" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("WATERMARK_SETTING"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
		),
        
        "PAGE_NAV_MODE" => array(
            "PARENT" => "NAVIGATION",
			"NAME" => GetMessage("PAGE_NAV_MODE"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
		),
        "ELEMENT_PAGE" => array(
            "PARENT" => "NAVIGATION",
			"NAME" => GetMessage("ELEMENT_PAGE"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "5",
		),
        "PAGER_SHOW_ALL" => array(
            "PARENT" => "NAVIGATION",
			"NAME" => GetMessage("PAGER_SHOW_ALL"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
		),
        "PAGER_SHOW_ALWAYS" => array(
            "PARENT" => "NAVIGATION",
			"NAME" => GetMessage("PAGER_SHOW_ALWAYS"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
		),
        "PAGER_TITLE" => array(
            "PARENT" => "NAVIGATION",
			"NAME" => GetMessage("PAGER_TITLE"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "Фотографии",
		),
        "PAGER_TEMPLATE" => array(
            "PARENT" => "NAVIGATION",
			"NAME" => GetMessage("PAGER_TEMPLATE"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "",
		),
        
        'CACHE_TIME' => array('DEFAULT'=>3600),
	),
);
?>