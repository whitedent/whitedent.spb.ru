<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<?
CModule::IncludeModule("sr.pack");
//óáèðàåò çàïîìèíàíèå ñòðàíèö
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if($arParams["COUNT_IMAGE"] == 0)
    $arParams["COUNT_IMAGE"] = "";
if($arParams["VARIABLE"] != "" && is_numeric($_REQUEST[$arParams["VARIABLE"]]))
    $arParams["FOLDERS"] = $_REQUEST[$arParams["VARIABLE"]];
if($arParams["FOLDERS"] != "" && is_numeric($arParams["FOLDERS"]))
{
    global $DB;
    if($arParams["TITLE"] == "Y")
    {
        $sql_title = "SELECT NAME FROM b_medialib_collection WHERE ID = '".$arParams["FOLDERS"]."'";
        $result = $DB->Query($sql_title);
        $title = @mysql_fetch_array($result->result, MYSQL_ASSOC);
        if($arParams["ADD_TITLE"] != "")
            $APPLICATION->SetTitle($arParams["ADD_TITLE"]." ".$title["NAME"]);
        else
            $APPLICATION->SetTitle($title["NAME"]);
        $arResult["NAME"] = $title["NAME"];
    }
    //Âûòàñêèâàåì ID êàðòèíîê ýòîé ïàïêè
    $sql_1 = "SELECT ITEM_ID FROM b_medialib_collection_item WHERE COLLECTION_ID = '".$arParams["FOLDERS"]."'";
    //Âûòàñêèâàåì ID ôàéëîâ èç òàáëèöû b_file
    if($arParams["RANDOM"] == "N")
        $sql_2 = "SELECT SOURCE_ID,DESCRIPTION FROM b_medialib_item WHERE ID IN (".$sql_1.") ORDER BY ID DESC";
    else
        if($arParams["COUNT_IMAGE"] != 0 && is_numeric($arParams["COUNT_IMAGE"]))
            $sql_2 = "SELECT SOURCE_ID,NAME FROM b_medialib_item WHERE ID IN (".$sql_1.") ORDER BY RAND()";
        else
            $sql_2 = "SELECT SOURCE_ID,NAME FROM b_medialib_item WHERE ID IN (".$sql_1.") ORDER BY RAND() LIMIT ".$arParams["COUNT_IMAGE"];
    $res = $DB->Query($sql_2);
        
    if($arParams["PAGE_NAV_MODE"] == "Y")
    {
        //Ïîñòðàíè÷íàÿ íàâèãàöèÿ
        $arParams["PAGER_SHOW_ALWAYS"] = ($arParams["PAGER_SHOW_ALWAYS"] == "N" ? false : true);
        $arParams["PAGER_SHOW_ALL"] = ($arParams["PAGER_SHOW_ALL"] == "N" ? false : true);        
        
        $arNavParams = array(
  		    "nPageSize" => $arParams["ELEMENT_PAGE"],
      		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
      		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
       	);
       	$arNavigation = $res->GetNavParams($arNavParams);
        $res->NavStart($arParams["ELEMENT_PAGE"]);
        $res->bShowAll = $arNavParams["bShowAll"];
        $arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
        $arResult["NAV_RESULT"] = $res;
    }

    if($this->StartResultCache(false, $arNavigation))
    {
        //Îáúåêò êëàññà API ìîäóëÿ
        $srMedialib = new SRMedialibrary();
        $srMedialib->setDB($DB);
        if($count = @mysql_num_rows($res->result))
        {
            $iteration = 1;
            while($img = $res->Fetch())
            {
                if(is_numeric($arParams["COUNT_IMAGE"]))
                    if($iteration > $arParams["COUNT_IMAGE"])
                        break;
                $arImg = CFile::GetByID($img["SOURCE_ID"]);
                $arResult["FOLDER_IMAGE"][$iteration] = $arImg->Fetch();
    
                if($arParams["RESIZE_MODE"] == "Y" && $arParams["RESIZE_MODE_W"] != "" && $arParams["RESIZE_MODE_H"] != ""){
					$arResult["FOLDER_IMAGE"][$iteration]["URL_THUMB"] = $srMedialib->resizeImagePhysical($arParams["RESIZE_MODE_W"], $arParams["RESIZE_MODE_H"], $arResult["FOLDER_IMAGE"][$iteration]);
				}

                $arResult["FOLDER_IMAGE"][$iteration]["RAND_ID"] = rand(00000, 99999);
                $arResult["FOLDER_IMAGE"][$iteration]["RAND_ID"] = "link_".$arResult["FOLDER_IMAGE"][$iteration]["RAND_ID"];
                
                if($arParams["WATERMARK_SETTING"] == "Y"){
					$arResult["FOLDER_IMAGE"][$iteration]["URL"] = $srMedialib->createWatermarkImageComponent($arResult["FOLDER_IMAGE"][$iteration]);
				}
                else{
					$arResult["FOLDER_IMAGE"][$iteration]["URL"] = "/upload/".$arResult["FOLDER_IMAGE"][$iteration]["SUBDIR"]."/".$arResult["FOLDER_IMAGE"][$iteration]["FILE_NAME"];
				}
                $arResult["DESCRIPTION"][$img["SOURCE_ID"]] = $img["DESCRIPTION"];
                $iteration++;
            }
        }
        else
        {
            $arResult["ERRORS"][] = GetMessage("NO_IMAGE_FOLDER");
            $this->AbortResultCache();
        }
        
        $this->IncludeComponentTemplate();
    }
}
?>