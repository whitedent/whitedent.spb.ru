<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $DB;

$sql = "SELECT ID, NAME FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1'";
$res = $DB->Query($sql);
if(mysql_num_rows($res->result) > 0)
{
    $arFolders[0] = "[0] ".GetMessage("NULL_VALUE_LIST");
    while($media = mysql_fetch_array($res->result, MYSQL_ASSOC))
        $arFolders[$media["ID"]] = "[".$media["ID"]."] ".$media["NAME"];
}

if($arCurrentValues["FOLDERS"] != 0)
{
    $sql_1 = "SELECT ITEM_ID FROM b_medialib_collection_item WHERE COLLECTION_ID = '".$arCurrentValues["FOLDERS"]."'";
    $sql_2 = "SELECT SOURCE_ID, NAME FROM b_medialib_item WHERE ID IN (".$sql_1.")";
}
else
{
    $sql_2 = "SELECT SOURCE_ID, NAME FROM b_medialib_item WHERE ID > 0";
}

$res_1 = $DB->Query($sql_2);
while($arr = mysql_fetch_array($res_1->result, MYSQL_ASSOC))
    $arImage[$arr["SOURCE_ID"]] = "[".$arr["SOURCE_ID"]."] ".$arr["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(
        "DESIGN" => array(
            "NAME" => GetMessage("DESIGN_NAME"),
        ),
        
        "WATERMARK" => array(
            "NAME" => GetMessage("WATERMARK_NAME"),
        ),
	),
	"PARAMETERS" => array(
		"FOLDERS" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("FOLDERS"),
			"TYPE" => "LIST",
			"VALUES" => $arFolders,
            "REFRESH" => "Y",
		),
        
        "IMAGE" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("IMAGE"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "N",
			"VALUES" => $arImage,
			"REFRESH" => "Y",
		),
        
        "LOAD_JS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("LOAD_JS"),
			"TYPE" => "CHECKBOX",
		),
        
        "DESIGN_STYLE_IMAGE" => array(
            "PARENT" => "DESIGN",
			"NAME" => GetMessage("DESIGN_STYLE_IMAGE"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "img-r",
		),
        
        "DESIGN_STYLE_A" => array(
            "PARENT" => "DESIGN",
			"NAME" => GetMessage("DESIGN_STYLE_A"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "fancybox-image",
		),
        
        "RESIZE_MODE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("RESIZE_MODE"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
		),
        
        "RESIZE_MODE_W" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("RESIZE_MODE_W"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "247",
		),
        
        "RESIZE_MODE_H" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("RESIZE_MODE_H"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "158",
		),
        
        "WATERMARK_SETTING" => array(
            "PARENT" => "WATERMARK",
			"NAME" => GetMessage("WATERMARK_SETTING"),
			"TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
		),
        
        'CACHE_TIME' => array('DEFAULT'=>3600),
	),
);
?>