<?php
$MESS["DESIGN_NAME"]        = "Настройка дизайна компонента";
$MESS["DESIGN_STYLE_IMAGE"] = "Класс стиля для изображения";
$MESS["DESIGN_STYLE_A"]     = "Класс стиля для ссылки";

$MESS["WATERMARK_NAME"]     = "Настройка водяного знака";
$MESS["WATERMARK_SETTING"]  = "Включить водяные знаки";

$MESS["FOLDERS"]            = "Папка медиабиблиотеки";
$MESS["IMAGE"]              = "Изображение";
$MESS["NULL_VALUE_LIST"]    = "Не определять";
$MESS["LOAD_JS"]            = "Загружать Fancybox";
$MESS["RESIZE_MODE"]        = "Сжатие изображений на лету";
$MESS["RESIZE_MODE_W"]      = "По ширине";
$MESS["RESIZE_MODE_H"]      = "По высоте";
?>