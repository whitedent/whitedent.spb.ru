<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $DB;
$sql = "SELECT ID, NAME FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1'";
$res = $DB->Query($sql);
if(mysqli_num_rows($res->result) > 0)
{
    $arFolders[0] = "[0] ".GetMessage("NULL_VALUE_LIST");
    while($media = mysqli_fetch_array($res->result, MYSQLI_ASSOC))
        $arFolders[$media["ID"]] = "[".$media["ID"]."] ".substr($media["NAME"], 0, 50);
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"FOLDERS" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("FOLDERS"),
			"TYPE" => "LIST",
			"VALUES" => $arFolders,
            "MULTIPLE" => "Y",
		),
        "VAR" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("VAR"),
			"TYPE" => "TEXTBOX",
            "DEFAULT" => "ID"
		),
        "GALLERY_ID" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("GALLERY_ID"),
			"TYPE" => "TEXTBOX"
		),
        "DETAIL_URL" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("DETAIL_URL"),
			"TYPE" => "TEXTBOX",
			"DEFAULT" => "/gallery/detail.php?ID=#ID#",
		),
        
        'CACHE_TIME' => array('DEFAULT'=>3600),
	),
);
?>