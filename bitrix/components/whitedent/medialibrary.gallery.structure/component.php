<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$match = array();
preg_match("/#(.*)#/", $arParams["DETAIL_URL"], $match);

if($arParams["FOLDERS"][0] == 0)
    unset($arParams["FOLDERS"][0]);

if($this->StartResultCache(false, array($arParams["FOLDERS"], $arParams["DETAIL_URL"], $match, $arParams["GALLERY_ID"], $_REQUEST[$arParams["VAR"]])))
{
    if((isset($_REQUEST[$arParams["VAR"]]) && is_numeric($_REQUEST[$arParams["VAR"]]) && $_REQUEST[$arParams["VAR"]] > 0) || (is_numeric($arParams["GALLERY_ID"]) && $arParams["GALLERY_ID"] > 0))
    {
        $id = ($_REQUEST[$arParams["VAR"]] != "") ? $_REQUEST[$arParams["VAR"]] : $arParams["GALLERY_ID"];
        $sql = "SELECT ID, NAME, DESCRIPTION, PARENT_ID FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1' AND PARENT_ID = '".$id."'";
    }
    else
        $sql = "SELECT ID, NAME, DESCRIPTION, PARENT_ID FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1'";
    
    $res = $DB->Query($sql);
    if(mysqli_num_rows($res->result) > 0)
    {
        while($media = mysqli_fetch_array($res->result, MYSQLI_ASSOC))
        {
            unset($count);
            $sql_count = "SELECT COUNT(ITEM_ID) FROM b_medialib_collection_item WHERE COLLECTION_ID = '".$media["ID"]."'";
            $res_count = $DB->Query($sql_count);
            $count = mysqli_fetch_array($res_count->result);
            
            $gallerys[$media["PARENT_ID"]][] = array(
                "DETAIL_PAGE_URL"   => str_replace("#$match[1]#", $media["ID"], $arParams["DETAIL_URL"]),
                "ID"                => $media["ID"],
                "NAME"              => $media["NAME"],
                "DESCRIPTION"       => $media["DESCRIPTION"],
                "PARENT_ID"         => $media["PARENT_ID"],
                "COUNT"             => $count[0]
            );
        }
    }
    
    if(($count = count($arParams["FOLDERS"])) > 0)
    {
        foreach($gallerys as $parent=>$gals)
        {
            if(array_search($parent, $arParams["FOLDERS"]) === false)
            {
                foreach($gals as $i=>$gallery)
                {
                    if(array_search($gallery["ID"], $arParams["FOLDERS"]) !== false)
                        unset($gallerys[$parent][$i]);
                }
            }
            else
                unset($gallerys[$parent]);
        }
    }
    
    $arResult["GALLERY"] = $gallerys;
    $arResult["PARENT"] = ($_REQUEST[$arParams["VAR"]] != "") ? $_REQUEST[$arParams["VAR"]] : $arParams["GALLERY_ID"];
    $this->IncludeComponentTemplate();
}
?>