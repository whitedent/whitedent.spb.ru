<?
class ModelElementsPricesRange extends ModelElements
{
    const PROP_ID_PRICES_RANGE = 32;

    public function replaceOrdinaryPriceByPricesRange($elements)
    {
        foreach ($elements as $id => $element) {
            if($element["PRICES_RANGE"]){
                $pricesRange = $element["PRICES_RANGE"];
                $showOnlyMinPricesRange = ($element["SHOW_ONLY_MIN_PRICES_RANGE"] == "Y") ? true : false;
                $elements[$id]["PRICE"] = $this->getPricesRangeAsText($pricesRange, $showOnlyMinPricesRange);
            }
        }

        return $elements;
    }

    private function getPricesRangeAsText($pricesRange, $showOnlyMinPricesRange)
    {
        $elementsFromPricesRange = $this->getElements(["ID" => $pricesRange]);

        $pricesRange = [];
        foreach ($elementsFromPricesRange as $element) {
            $pricesRange[] = $element["PRICE"];
        }

        asort($pricesRange);

        if(count($pricesRange) == 1 || $showOnlyMinPricesRange){
            $text = "от ".reset($pricesRange);
        }else{
            $text = reset($pricesRange)." - ".end($pricesRange);
        }

        return $text;
    }

    public function checkCorrectPricesRange($arFields)
    {
        $currentPriceId = $arFields["ID"];
        $pricesIdInRangeFromRequest = $this->getPricesRangeFromRequestId($arFields);

        if(!$pricesIdInRangeFromRequest){
            return true;
        }

        if(in_array($currentPriceId, $pricesIdInRangeFromRequest)){
            global $APPLICATION;
            $APPLICATION->ThrowException("Вы пытаетесь привязать в диапазон цену которую в данный момент редактируете!");
            return false;
        }

        return true;
    }

    private function getPricesRangeFromRequestId($arFields)
    {
        $pricesId = [];
        $propValuePricesRange = $arFields["PROPERTY_VALUES"][self::PROP_ID_PRICES_RANGE];
        foreach($propValuePricesRange as $price){
            if($price["VALUE"]){
                $pricesId[] = $price["VALUE"];
            }
        }

        return $pricesId;
    }

}
