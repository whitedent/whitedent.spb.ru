<?
class ModelElements
{
    private $controllerClassName;

    public function __construct($controllerClassName = false)
    {
        $this->controllerClassName = $controllerClassName;
    }

    public function getElementsForSections($sections)
    {
        $sectionsId = array_keys($sections);
        $filter = ["SECTION_ID" => $sectionsId];
        $elements = $this->getElements($filter);

        return $elements;
    }

    public function getElementsByElementPropId($serviceElementPropId)
    {
        $filter = ["PROPERTY_SERVICE" => $serviceElementPropId];
        $elements = $this->getElements($filter);

        return $elements;
    }

    public function getElementsById($pricesId)
    {
        $filter = ["ID" => $pricesId];
        $elements = $this->getElements($filter);

        return $elements;
    }

    protected function getElements($filter)
    {
       $arSort = [
            "SORT" => "ASC",
            "ID" => "ASC"
        ];

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "IBLOCK_SECTION_ID"
        ];

        // Фильтр по умолчанию
        $arFilter = [
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
            "ACTIVE" => "Y",
        ];

        // Фильтр из параметра (Id цен или Id разделов цен)
        if($filter){
            $arFilter = array_merge($arFilter, $filter);
        }

        // Чекбокс «Не выводить цену на страницу цен» для вывода только на странице услуги
        if($this->controllerClassName == "ControllerPricesForSection"){
            $arFilter["!PROPERTY_HIDE_ON_PRICE_PAGE"] = "Y";
        }

        $rs_element = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

        $elements = [];
        while($ob = $rs_element->GetNextElement())
        {
            $fields = $ob->GetFields();
            $properties = $ob->GetProperties();
            $element = [
                "ID" => $fields["ID"],
                "NAME" => $fields["NAME"],
                "SECTION_PARENT_ID" => $fields["IBLOCK_SECTION_ID"],
                "PRICE" => $properties["PRICE"]["VALUE"],
                "NOTE" => $properties["NOTE"]["VALUE"],
                "PRICES_RANGE" => $properties["PRICES_RANGE"]["VALUE"],
                "SHOW_ONLY_MIN_PRICES_RANGE" => $properties["SHOW_ONLY_MIN_PRICES_RANGE"]["VALUE"],
				"PRICE_SUM" => $properties["PRICE_SUM"]["VALUE"],
            ];
            $elements[$fields["ID"]] = $element;
        }

        return $elements;
    }

    public function getParentSectionsForElements($elements, $isLinkedBySection)
    {
        if(!$elements){
            return [];
        }

        $parentSectionsId = [];
        foreach($elements as $element){
            $parentSectionsId[] = $element["SECTION_PARENT_ID"];
        }

        $modelSections = new ModelSections();
        $sections = $modelSections->getSections(["priceSectionsId" => $parentSectionsId]);

        // заголовок подкатегории должен выводиться только в том случае, если на страницу выведена вся подкатегория;
        // если выведены только отдельные услуги, должна быть только группировка
        if (!$isLinkedBySection) {
            foreach ($sections as $id => $section) {
                if($section["DEPTH_LEVEL"] == 2){
                    $sections[$id]["NAME"] = "";
                }
            }
        }

        return $sections;
    }

	public function replaceOrdinaryPriceByPricesSum($elements)
	{
		foreach ($elements as $k => $element) {
			if(!empty($element["PRICE_SUM"])){
				$elements[$k]["PRICE"] = $this->getLinkedPricesSum($element["PRICE_SUM"]);
			}
		}

		return $elements;
    }

	private function getLinkedPricesSum($elementsId)
	{
		$linkedElements = $this->getElementsById($elementsId);
		return array_sum(
			array_map(function ($linkedElement){
				return (int)$linkedElement["PRICE"];
			}, $linkedElements)
		);
    }


}
