<?
class ModelSections
{
    public function getSections($filter)
    {
        $arOrder = [
            "LEFT_MARGIN" => "ASC",
            "SORT" => "ASC",
        ];

        $arFilter = array(
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
            "CHECK_PERMISSIONS" => "Y",
            "ACTIVE" => "Y",
        );

        if(!empty($filter["priceSectionCode"])){
            $priceSectionCode = parse_url($filter["priceSectionCode"]);
            $priceSectionCode = $priceSectionCode["path"];

            if(!$priceSection = $this->getSectionBySectionCode($priceSectionCode)){
                CHTTP::SetStatus('404 Not Found');
                throw new Exception("Не найден раздел с ценами");
            }
            $arFilter[">LEFT_MARGIN"] = $priceSection["LEFT_MARGIN"];
            $arFilter["<RIGHT_MARGIN"] = $priceSection["RIGHT_MARGIN"];
        }

        if(!empty($filter["serviceSectionId"])){
            $serviceSectionId = $filter["serviceSectionId"];

            if(!$priceSection = $this->getSectionBySectionProp("UF_SERVICE", $serviceSectionId)){
                CHTTP::SetStatus('404 Not Found');
                throw new Exception("Не найден раздел с ценами");
            }
            $arFilter[">LEFT_MARGIN"] = $priceSection["LEFT_MARGIN"];
            $arFilter["<RIGHT_MARGIN"] = $priceSection["RIGHT_MARGIN"];
        }

        if(!empty($filter["priceSectionsId"])){
            $priceSectionsId = $filter["priceSectionsId"];
            $arFilter["ID"] = $priceSectionsId;
        }

        $bIncCnt = false;

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "IBLOCK_SECTION_ID",
            "NAME",
            "DEPTH_LEVEL",
            "SORT",
        ];

        $rs_section = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect);

        $sections = [];
        while($ar_section = $rs_section->Fetch()) {
            $sectionId = $ar_section["ID"];
            $sectionName = $ar_section["NAME"];
            $sectionParentId = $ar_section["IBLOCK_SECTION_ID"];
            $sectionDepthLevel = $ar_section["DEPTH_LEVEL"];
            $sectionSort = $ar_section["SORT"];
            $sections[$sectionId] = [
                "ID" => $sectionId,
                "NAME" => $sectionName,
                "SECTION_PARENT_ID" => $sectionParentId,
                "DEPTH_LEVEL" => $sectionDepthLevel,
                "SORT" => $sectionSort,
            ];
        }

        if(!$sections){
            throw new Exception("Не найдены Разделы Цен");
        }

        return $sections;
    }

    private function getSectionBySectionCode($sectionCode)
    {
        $arOrder = [];

        $arFilter = array(
            "CODE" => $sectionCode,
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
            "CHECK_PERMISSIONS" => "Y",
            "ACTIVE" => "Y",
        );

        $bIncCnt = false;

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "IBLOCK_SECTION_ID",
            "NAME",
            "LEFT_MARGIN",
            "RIGHT_MARGIN",
        ];

        $rs_section = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect);
        $section = $rs_section->Fetch();

        return $section;
    }

    public function getSectionsCodeBySectionPropValue($propCode, $propValue)
    {
        $arOrder = [];

        $arFilter = array(
            $propCode => $propValue,
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
            "CHECK_PERMISSIONS" => "Y",
            "ACTIVE" => "Y",
        );

        $bIncCnt = false;

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "CODE",
            $propCode
        ];

        $rs_section = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect);

        $sectionsCode = [];
        while($ar_section = $rs_section->Fetch()){
            $sectionsCode[] = $ar_section["CODE"];
        }

        return $sectionsCode;
    }

    public function combineSectionsAndElements($sections, $elements)
    {
        foreach($elements as $elementId => $element){
            $elementSectionParentId = $element["SECTION_PARENT_ID"];
            $sections[$elementSectionParentId]["ELEMENTS"][$elementId] = $element;
        }

        return $sections;
    }

    public function sortSections($sections)
    {
        $sortGroups = [];
        foreach ($sections as $section) {
            $sectionSort = $section["SORT"];
            $sortGroups[$sectionSort][] = $section;
        }

        ksort($sortGroups);

        $sortedSections = [];

        foreach ($sortGroups as $sortGroup){
            foreach ($sortGroup as $section){
                $sectionId = $section["ID"];
                $sortedSections[$sectionId] = $section;
            }
        }

        return $sortedSections;
    }

    public function makeTreeStructure($sections)
    {
        $result = [];

        foreach ($sections as $sectionId => $section) {

            // Если Раздел уже в результирующем массиве - пропуск
            if(!empty($result[$section["ID"]])){
                continue;
            }

            if($section["DEPTH_LEVEL"] == "1"){
                $result[$sectionId] = $section;
            }else{
                $sectionParentId = $section["SECTION_PARENT_ID"];
                $sectionParent = $sections[$sectionParentId];
                $result[$sectionParentId] = $this->getSubSectionsForSection($sectionParent, $sections);
            }
        }

        return $result;
    }

    private function getSubSectionsForSection($sectionParent, $sections)
    {
        $elements = [];
        foreach($sections as $sectionItem){
            if($sectionItem["SECTION_PARENT_ID"] == $sectionParent["ID"]){

                // на страницах Услуг, у разделов 2-го уровня, при по-элементной привязке заголовок может быть пустым
                if($sectionItem["NAME"]){
                    $elements[] = [
                        "IS_SUBSECTION" => true,
                        "NAME" => $sectionItem["NAME"],
                    ];
                }

                $childElements = $sectionItem["ELEMENTS"];

                $elements = array_merge($elements, $childElements);
            }
        }

        if($sectionParent["ELEMENTS"]){
            $sectionParent["ELEMENTS"] = array_merge($sectionParent["ELEMENTS"], $elements);
        }else{
            $sectionParent["ELEMENTS"] = $elements;
        }


        return $sectionParent;
    }
    
    public function mergeFirstLevelSections($sections, $paramsTitle)
    {
        $elements = [];
        foreach($sections as $section){
            $elements = array_merge($elements, $section["ELEMENTS"]);
        }
        $singleItem = [
            "NAME" => $paramsTitle,
            "ELEMENTS" => $elements,
        ];
        $result[] = $singleItem;

        return $result;
    }

    public function getParentSectionsForSections($sections)
    {
        $parentSectionsId = [];
        foreach($sections as $section){
            if($section["SECTION_PARENT_ID"]){
                $parentSectionsId[] = $section["SECTION_PARENT_ID"];
            }
        }

        if(!$parentSectionsId){
            return [];
        }

        $modelSections = new ModelSections();
        $sections = $modelSections->getSections(["priceSectionsId" => $parentSectionsId]);

        return $sections;
    }

    

}
