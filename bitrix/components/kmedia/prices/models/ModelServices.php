<?
class ModelServices
{
    public function getCurrentServiceCode()
    {
        $urlSegments = $this->getCurUrlSegments();

        $lastSectionPartIndex = count($urlSegments) - 1;
        $curServiceCode = $urlSegments[$lastSectionPartIndex];
        $curServiceCode = stripslashes(trim(htmlspecialchars($curServiceCode, ENT_QUOTES)));

        return $curServiceCode;
    }

    public function getParentServiceCodeForCurrentService()
    {
        $urlSegments = $this->getCurUrlSegments();

        $preLastSectionPartIndex = count($urlSegments) - 2;

        if(empty($urlSegments[$preLastSectionPartIndex])){
           return false;
        }

        $parentServiceCode = $urlSegments[$preLastSectionPartIndex];
        $parentServiceCode = stripslashes(trim(htmlspecialchars($parentServiceCode, ENT_QUOTES)));

        return $parentServiceCode;
    }

    private function getCurUrlSegments()
    {
        global $APPLICATION;
        $curUrl = $APPLICATION->GetCurPage();
        $curUrlExploded = explode("/", $curUrl);

        $segments = [];
        foreach ($curUrlExploded as $part) {
            if($part){
                $segments[] = $part;
            }
        }

        return $segments;
    }

    public function getSectionIdBySectionCode($sectionCode, $parentServiceCode = false)
    {
        $arOrder = [];

        $arFilter = [
            "CODE" => $sectionCode,
            "IBLOCK_ID" => ID_IBLOCK_SERVICES,
            "CHECK_PERMISSIONS" => "Y",
            "ACTIVE" => "Y",
        ];

        if($parentServiceCode){
            if($parentServiceId = $this->getSectionIdBySectionCode($parentServiceCode)){
                $arFilter["SECTION_ID"] = $parentServiceId;
            }
        }

        $bIncCnt = false;

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
        ];

        $rs_section = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect);

        if(!$section = $rs_section->Fetch()){
            throw new Exception("Не найден раздел Услуг");
        }

        $sectionId = $section["ID"];

        return $sectionId;
    }

    public function getElementsFromPropLinkedSections($serviceId)
    {
        $modelSections = new ModelSections();
        $modelElements = new ModelElements();

        $linkedPriceSectionsCode = $modelSections->getSectionsCodeBySectionPropValue("UF_SERVICE", $serviceId);

        if(!$linkedPriceSectionsCode){
            return $linkedPriceSectionsCode;
        }

        $sections = [];
        foreach($linkedPriceSectionsCode as $linkedPriceSectionCode){
            $sections += $modelSections->getSections(["priceSectionCode" => $linkedPriceSectionCode]);
        }

        $elements = $modelElements->getElementsForSections($sections);

        return $elements;
    }



}

