<?
class ModelNotes
{
    public static function addNotes($sections)
    {
        foreach($sections as $sectionId => $section){
            $sections[$sectionId] = self::addNotesForSection($section);
        }

        return $sections;
    }

    private static function addNotesForSection($section)
    {
        $notes = [];

        foreach ($section["ELEMENTS"] as $elementId => $element){

            if(!$element["NOTE"]){
                continue;
            }

            $note = $element["NOTE"];
            if(!in_array($note, $notes)){
                $notes[] = $element["NOTE"];
            }

            $noteIndex = self::getNoteIndex($note, $notes);
            $section["ELEMENTS"][$elementId]["NAME"] .= str_repeat("*", $noteIndex);

        }

        if($notes){
            $section["ELEMENTS"][] = [
                "IS_NOTES" => true,
                "NOTES" => self::getNotesFooter($notes),
            ];
        }

        return $section;
    }

    private static function getNoteIndex($note, $notes)
    {
        foreach($notes as $noteIndex => $noteText){
            if($noteText == $note){
                return ($noteIndex + 1);
            }
        }

        return false;
    }

    private static function getNotesFooter($notes)
    {
        $result = "";
        foreach($notes as $noteIndex => $noteText){
            $result .= str_repeat("*", ($noteIndex + 1));
            $result .= $noteText."<br>\n";
        }

        return $result;
    }
}
