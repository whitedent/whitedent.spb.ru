<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult["ERROR"])): ?>
    <p class="error">Ошибка: <?= $arResult["ERROR"] ?></p>
<? else: ?>

    <? if ($arResult["ITEMS"]): ?>
      <? foreach ($arResult["ITEMS"] as $section): // Цикл по Разделам 1-го уровня ?>

        <? if(!$section["ELEMENTS"]){
          continue;
        } ?>

        <? include __DIR__."/price_table.php" ?>

      <? endforeach ?>

    <? else: ?>
<!--        <p>Нет разделов цен</p>-->
    <? endif ?>

<? endif ?>
