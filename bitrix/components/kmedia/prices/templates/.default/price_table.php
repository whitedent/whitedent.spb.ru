<table class="price">
  <tbody>

      <tr>
        <th class="row1"><?= $section["NAME"] ?></th><th style="width: 20%">в рублях</th>
      </tr>

      <? foreach ($section["ELEMENTS"] as $element): ?>
        <tr>
          <? if(!empty($element["IS_NOTES"])): ?>
            <td class="row1" colspan="2"><i><?= $element["NOTES"] ?></i></td>
          <? elseif(!empty($element["IS_SUBSECTION"])): ?>
            <td class="row1" colspan="2"><strong><?= $element["NAME"] ?></strong></td>
          <? else: ?>
            <td class="row1"><?= $element["NAME"] ?></td><td><?= $element["PRICE"] ?></td>
          <? endif ?>
        </tr>
      <? endforeach ?>

</table>
