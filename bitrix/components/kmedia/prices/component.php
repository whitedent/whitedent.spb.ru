<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");

require __DIR__."/includes/include_controllers.php";
require __DIR__."/includes/include_models.php";

//if(!isset($arParams["CACHE_TIME"])){
//    $arParams["CACHE_TIME"] = 3600*24;
//}

global $APPLICATION;
$curPage = $APPLICATION->GetCurPage();

/** @var \CBitrixComponent $this */
if($this->startResultCache(false, $curPage))
{
    // Мы на странице Цен
    if(!empty($arParams["IS_PRICE_PAGE"])){
        try{
            $arResult["ITEMS"] = ControllerPricesForSection::getData();
        }catch (Exception $e){
            $arResult["ERROR"] = $e->getMessage();
        }
    }

    // Мы на странице Услуги
    if(!empty($arParams["IS_SERVICE_PAGE"])){
        try {
            $arResult["ITEMS"] = ControllerPricesForService::getData($arParams["TITLE"]);
        }
        catch(Exception $e) {
            $arResult["ERROR"] = $e->getMessage();
        }
    }

    // Мы получили массив ID цен
    if(!empty($arParams["PRICES_ID"])){
        try {
            $pricesId = $arParams["PRICES_ID"];
            $title = $arParams["TITLE"];
            $arResult["ITEMS"] = ControllerPricesFromIdArray::getData($pricesId, $title);
        }
        catch(Exception $e) {
            $arResult["ERROR"] = $e->getMessage();
        }
    }

    $this->setResultCacheKeys(["ITEMS"]);

    $this->IncludeComponentTemplate();
}