<?
class ControllerPricesFromIdArray
{
    public static function getData($pricesId, $title)
    {
        // Запрос по Элементам
        $modelElements = new ModelElements();
        $elements = $modelElements->getElementsById($pricesId);

		// Если заполнено свойство Цена-сумма для конкретной цены - подменить значение цены
		$elements = $modelElements->replaceOrdinaryPriceByPricesSum($elements);

        $result[] = [
            "ELEMENTS" => $elements,
            "NAME" => $title,
        ];

        $result = ModelNotes::addNotes($result);

        return $result;
    }

}