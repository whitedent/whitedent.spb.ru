<?
class ControllerPricesForService
{
    private static $modelServices;
    private static $modelSections;
    private static $modelElements;
    private static $modelElementsPricesRange;

    public static function getData($paramsTitle)
    {
        self::$modelServices = new ModelServices();
        self::$modelSections = new ModelSections();
        self::$modelElements = new ModelElements();
        self::$modelElementsPricesRange = new ModelElementsPricesRange();

        // Код текущей Услуги (из URL)
        $serviceCode = self::$modelServices->getCurrentServiceCode();

        // Код родительской Услуги для текущей Услуги (из URL)
        $parentServiceCode = self::$modelServices->getParentServiceCodeForCurrentService();

        // Id текущей Услуги (из инфоблока Услуг (зеркального к статическим страницам))
        $serviceId = self::$modelServices->getSectionIdBySectionCode($serviceCode, $parentServiceCode);

        /** Элементы */
        // Элементы из свойства "привязанных Разделов"
        $elementsFromPropLinkedSections = self::$modelServices->getElementsFromPropLinkedSections($serviceId);

        // Элементы из свойства "привязанных Элементов"
        $elementsFromPropLinkedElements = self::$modelElements->getElementsByElementPropId($serviceId);

        $elements = $elementsFromPropLinkedSections + $elementsFromPropLinkedElements;

        // Если указан Диапазон цен для конкретной цены - подменить значение цены
        $elements = self::$modelElementsPricesRange->replaceOrdinaryPriceByPricesRange($elements);

		// Если заполнено свойство Цена-сумма для конкретной цены - подменить значение цены
		// Если был указан Диапазон цен - он не учитывается в значении цены
		$elements = self::$modelElements->replaceOrdinaryPriceByPricesSum($elements);
        /** /Элементы */

        /** Разделы */
        $parentSections = [];

        // Родительские Разделы для Элементов из свойства "привязанных Разделов"
        $parentSections = $parentSections + self::$modelElements->getParentSectionsForElements($elementsFromPropLinkedSections, true);

        // + Родительские Разделы для Элементов из свойства "привязанных Элементов"
        $parentSections = $parentSections + self::$modelElements->getParentSectionsForElements($elementsFromPropLinkedElements, false);

        // Добавить родительские Разделы к Разделам 2-го уровня
        $parentSectionsForSections = self::$modelSections->getParentSectionsForSections($parentSections);

        $sections = $parentSections + $parentSectionsForSections;
        /** /Разделы */

        // Формируем результирующий массив Элементов с разбивкой по род. Разделам
        $sections = self::$modelSections->combineSectionsAndElements($sections, $elements);

        // Добавляем сноски (как последний элемент массива Элементов Раздела)
        $sections = ModelNotes::addNotes($sections);

        // Разделы 2-го уровня и их Элементы сливаем в один одномерный массив
        $sections = self::$modelSections->makeTreeStructure($sections);
        $sections = self::$modelSections->sortSections($sections);

        // На странице Услуг все Разделы 1-го уровня должны быть в одной таблице
        $sections = self::$modelSections->mergeFirstLevelSections($sections, $paramsTitle);

        return $sections;
    }


}