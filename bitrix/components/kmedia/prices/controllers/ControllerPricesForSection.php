<?
use \Bitrix\Main\Application;

class ControllerPricesForSection
{
    public static function getData()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $priceSectionCode = $request->getQuery('price_section_code');

        // Запрос по Разделам
        $modelSections = new ModelSections();
        $sections = $modelSections->getSections(["priceSectionCode" => $priceSectionCode]);

        // Запрос по Элементам
        $modelElements = new ModelElements(self::class);
        $elements = $modelElements->getElementsForSections($sections);

		// Если заполнено свойство Цена-сумма для конкретной цены - подменить значение цены
		$elements = $modelElements->replaceOrdinaryPriceByPricesSum($elements);


		// Формируем результирующий массив Элементов с разбивкой по род. Разделам
        $sections = $modelSections->combineSectionsAndElements($sections, $elements);

        // Добавляем сноски (как последний элемент массива Элементов Раздела)
        $sections = ModelNotes::addNotes($sections);

        // Разделы 2-го уровня и их Элементы сливаем в один одномерный массив
        $sections = $modelSections->makeTreeStructure($sections);

        return $sections;
    }
}