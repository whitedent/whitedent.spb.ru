<?

$MESS['MAIN_MAIL_CONFIRM_MESSAGE_HINT'] = 'Geben Sie diesen Bestдtigungscode in Ihrem Bitrix24 ein, um Ihre E-Mail-Adresse zu bestдtigen.';
$MESS['MAIN_MAIL_CONFIRM_MESSAGE_FAQ_Q1'] = 'Warum soll ich meine E-Mail-Adresse verifizieren?';
$MESS['MAIN_MAIL_CONFIRM_MESSAGE_FAQ_A1'] = 'Es ist erforderlich, dass Sie Ihre E-Mail-Adresse bestдtigen, damit jegliche Manipulationen ausgeschlossen bleiben und damit sichergestellt wird, dass Sie die E-Mail-Adresse, vor der Mails gesendet werden, tatsдchlich besitzen.';
