<?
$MESS["CATALOG_COMPARE"] = "Comparar";
$MESS["CATALOG_BUY"] = "Comprar";
$MESS["CATALOG_ADD"] = "Adicionar ao carrinho";
$MESS["CATALOG_NOT_AVAILABLE"] = "(Nгo disponнvel em estoque)";
$MESS["CATALOG_QUANTITY"] = "Quantidade";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "de #FROM# para #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# e mais";
$MESS["CATALOG_QUANTITY_TO"] = "atй #TO#";
$MESS["CT_BCT_QUANTITY"] = "Quantidade";
$MESS["CATALOG_MORE"] = "Mais";
$MESS["CATALOG_FROM"] = "a partir de";
$MESS["CT_BCT_ELEMENT_DELETE_CONFIRM"] = "Toda a informaзгo relacionada a este registro serб apagado. Continuar mesmo assim?";
?>