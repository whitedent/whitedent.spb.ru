<?
$MESS["CATALOG_COMPARE"] = "Comparar";
$MESS["CT_BCT_ELEMENT_DELETE_CONFIRM"] = "Toda la informaciуn relacionada con eeste record serб eliminada. їDesea continuar de todas maneras?";
$MESS["CT_BCT_TPL_MESS_BTN_BUY"] = "Comprar";
$MESS["CT_BCT_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "No disponible en stock";
$MESS["CT_BCT_TPL_MESS_BTN_DETAIL"] = "Mбs";
$MESS["CT_BCT_TPL_MESS_BTN_SUBSCRIBE"] = "Notificarme cuando haya en stock";
$MESS["ADD_TO_BASKET_OK"] = "Agregar elemento al carrito";
$MESS["CT_BCT_TPL_MESS_PRICE_SIMPLE_MODE"] = "desde #PRICE# hasta #MEASURE#";
$MESS["CT_BCT_TPL_MESS_MEASURE_SIMPLE_MODE"] = "#VALUE# #UNIT#";
$MESS["CT_BCT_TPL_MESS_PRICE_SIMPLE_MODE_SHORT"] = "desde #PRICE#";
$MESS["CT_BCT_TPL_MESS_BTN_ADD_TO_BASKET"] = "Agregar al carrito";
$MESS["CT_BCT_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Ir al carrito de compras";
$MESS["CT_BCT_CATALOG_TITLE_ERROR"] = "Error";
$MESS["CT_BCT_CATALOG_TITLE_BASKET_PROPS"] = "Propiedades de los elementos para pasar al carrito de compras";
$MESS["CT_BCT_CATALOG_BASKET_UNKNOWN_ERROR"] = "Error desconocido al agregar un elemento al carrito";
$MESS["CT_BCT_CATALOG_BTN_MESSAGE_SEND_PROPS"] = "Seleccionar";
$MESS["CT_BCT_CATALOG_BTN_MESSAGE_CLOSE"] = "Cerrar";
$MESS["CT_BCT_CATALOG_BTN_MESSAGE_CLOSE_POPUP"] = "Continuar comprando";
$MESS["CT_BCT_CATALOG_MESS_COMPARE_OK"] = "El producto se ha agregado a la tabla comparativa";
$MESS["CT_BCT_CATALOG_MESS_COMPARE_TITLE"] = "Comparaciуn de producto";
$MESS["CT_BCT_CATALOG_MESS_COMPARE_UNKNOWN_ERROR"] = "Error al agregar el producto a tabla de comparaciуn";
$MESS["CT_BCT_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT"] = "Comparar producto";
$MESS["CT_BCT_TPL_MESS_BTN_COMPARE"] = "Comparar";
?>