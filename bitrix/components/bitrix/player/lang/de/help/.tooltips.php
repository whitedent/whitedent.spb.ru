<?
$MESS["PLAYER_TYPE_TIP"] = "<b>Automatische Erkennung</b>: Wдhlen Sie diese Option, wenn das Format unbekannt ist; <br /><br /><b>WMV Player</b>: unterstьtzt WMV, WMA. Diese Option erfordert Microsoft Silverlight;<br /><br /><b>Flash Player</b>: unterstьtzt FLV7 Video, FLV8 Video etc.; <a target=\"_blank\" href=\"http://code.longtailvideo.com/trac/wiki/FlashFormats#Mediafileformats\">Alle unterstьtzte Formate... </a>.";
$MESS["USE_PLAYLIST_TIP"] = "Um die Abspielliste zu erlauben, wдhlen Sie diese Option und geben Sie den Pfadnamen ein.";
$MESS["PATH_TIP"] = "Pfad zur Datei oder zur Abspielliste (<a target=\"_blank\" href=\"http://code.longtailvideo.com/trac/wiki/FlashFormats#Playlistformats\">Unterstьtzte Playlistformate</a>).  Links zu YouTube werden auch akzeptiert.";
$MESS["HEIGHT_TIP"] = "Hцhe des Playerfensters einschlieЯlich der Abspielliste (wenn sichtbar). ";
$MESS["WIDTH_TIP"] = "Breite des Playerfensters einschlieЯlich der Abspielliste (wen sichtbar). ";
$MESS["PREVIEW_TIP"] = "Pfad zum Vorschaubild. Sie kцnnen den vollen Pfadnamen  eingeben oder nur den Dateinamen, wenn die Datei im aktuellen Ordner liegt.";
$MESS["LOGO_TIP"] = "Pfad zum Bild, das als Copyright-Zeichen im Player-Fenster genutzt wird. Sie kцnnen den vollen Pfadnamen  eingeben oder nur den Dateinamen, wenn die Datei im aktuellen Ordner liegt.";
$MESS["FULLSCREEN_TIP"] = "Wenn diese Option ausgewдhlt ist, wird die Schaltflдche fьr den Vollbildschirmmodus angezeigt.";
$MESS["SKIN_PATH_TIP"] = "Der Standardordner fьr Designs ist <b>/bitrix/components/bitrix/player/mediaplayer/skins</b> (SWF Dateien). Die benutzerdefinierten Designs mьssen in einem Ordner gespeichert werden, der nicht durch das Update-Systemgeдndert wird.";
$MESS["SKIN_TIP"] = "Zeigt alle Designs im angegebenen Ordner an. <b>Standard</b>-Flashdesigns sind immer verfьgbar.";
$MESS["CONTROLBAR_TIP"] = "Die Player-Bedienelemente kцnnen auf folgenden Positionen angezeigt werden: <br/><br/><b>Unten</b>: unter dem Player-Fenster;<br /><br/><b>Oben</b>: ьber dem Player-Fenster;<br /><br/><b>Versteckt</b>: Die Bedienelenente sind unsichtbar.";
$MESS["WMODE_TIP"] = "Integriert den Player in die Seite. Dieser Wert ist ein Attribut des EMBED-Tag.<br/><br/><b>Transparent</b>: Die HTML-Seite ist sichtbar auf allen transparenten Bereichen der abgespielten Bereiche. Dadurch kann die Performance verringert werden.;<br /><br/><b>Normal</b>: entspricht dem Wert <b>Window</b> und startet die Mediadatei im rechteckigen Fenster auf der Webseite. Ermцglicht die beste Performance;<br /><br /><b>Undurchsichtig</b>: entspricht dem <b>Opaque</b>-Wert des <b>Wmode</b>-Attributes.";
$MESS["PLAYLIST_TIP"] = "<b>Bottom</b>: zeigt die Abspielliste unten;<br /><br /><b>Rechts</b>: zeigt die Abspielliste rechts;<br /><br /><b>Versteckt</b> (Nur Flash Player): versteckt die Abspielliste.";
$MESS["PLAYLIST_SIZE_TIP"] = "Playlisthцhe oder -Breite, abhдngig der Position";
$MESS["HIDE_MENU_TIP"] = "Versteckt das Playerkontextmenь auЯer den Punkten Einstellungen und Adobe Flash Player.";
$MESS["AUTOSTART_TIP"] = "Wenn diese Option ausgewдhlt ist, spielt der Player die Dateien automatisch ab, wenn die Seite geladen wird.";
$MESS["REPEAT_TIP"] = "Wenn diese Option ausgewдhlt ist, werden die gewдhlten Dateien stдndig abgespielt.";
$MESS["VOLUME_TIP"] = "Lautstдrke in Prozent einstellen";
$MESS["DISPLAY_CLICK_TIP"] = "Wдhlen Sie die Aktion, die durch das Klicken auf das Player-Fenster ausgefьhrt wird.";
$MESS["MUTE_TIP"] = "Wenn diese Option ausgewдhlt ist, wird der Ton ausgeschaltet. Die Benutzer kцnnen den Ton manuell einschalten.";
$MESS["HIGH_QUALITY_TIP"] = "Maximale Abspielqualitдt einstellen";
$MESS["SHUFFLE_TIP"] = "Wenn diese Option markiert ist, wird die Playliste in zufдlliger Reihenfolge abgespielt. Die angezeigte Reihenfolge bleibt unverдndert.";
$MESS["START_ITEM_TIP"] = "Definiert das Startelement fьr die Playliste.";
$MESS["ADVANCED_MODE_SETTINGS_TIP"] = "Diese Option ist fьr erfahrene Nutzer. Es цffnet die komplette Parameterliste der Komponente. Sie kцnnen die Komponente einstellen und dann die Option ausschalten, damit die Online-Redakteure die Mediadaten im einfachen Modus einstellen kцnnen.";
$MESS["BUFFER_LENGTH_TIP"] = "Die Abspielpufferlдnge in Sekunden. Der Player lдdt eine bestimmte Datenmenge herunter, bevor er anfдngt die Datei abzuspielen.";
$MESS["DOWNLOAD_LINK_TIP"] = "Definiert den Direktlink fьr den Download der Mediadatei.  Dieser Parameter funktioniert nur eine Mediadatei abgespielt wird (keine Abspielliste).";
$MESS["DOWNLOAD_LINK_TARGET_TIP"] = "Definiert das Zielfenster fьr den Link.";
$MESS["SHOW_CONTROLS_TIP"] = "Wenn diese Option ausgewдhlt ist, werden die Playerbedienelemente angezeigt.";
$MESS["SHOW_STOP_TIP"] = "Zeigt die Schaltflдche <b>Stop</b> in den Player-Bedienelementen.";
$MESS["SHOW_DIGITS_TIP"] = "Wenn diese Option ausgewдhlt ist, wird die Abspielzeit angezeigt.";
$MESS["CONTROLS_BGCOLOR_TIP"] = "Definiert die Hintergrundfarbe der Player-Bedienelemente.";
$MESS["CONTROLS_COLOR_TIP"] = "Definiert die Farbe der Player-Bedienelemente.";
$MESS["CONTROLS_OVER_COLOR_TIP"] = "Definiert die Farbe der hervorgehobenen Player-Bedienelemente.";
$MESS["SCREEN_COLOR_TIP"] = "Definiert die Farbe des Players.";
$MESS["PLAYLIST_TYPE_TIP"] = "Wдhlen Sie das Playlistformat.";
$MESS["PLAYLIST_PREVIEW_WIDTH_TIP"] = "Breite des Vorschaubildes in der Abspielliste.";
$MESS["PLAYLIST_PREVIEW_HEIGHT_TIP"] = "Hцhe des Vorschaubildes in der Playliste.";
$MESS["ALLOW_SWF_TIP"] = "Erlaubt das Abspielen von SWF-Dateien. Nicht empfohlen wegen dem hohen Risiko von XSS-Angriffen.";
$MESS["CONTENT_TYPE_TIP"] = "Definiert den Inhaltstyp, der abgespielt wird.";
$MESS["SIZE_TYPE_TIP"] = "<b>Absolute GrцЯe</b>: geben Sie Hцhe und Breite des Player-Fensters an, in Pixel;<br /><b>In Container einfьgen</b>: Player-Fenster wird automatisch an die GrцЯe des ьbergeordneten Containers angepasst;<br /><b>Videodimensionen nutzen</b>: GrцЯe des Player-Fensters wird an den Videorahmen mцglichst angepasst.";
$MESS["PLAYBACK_RATE_TIP"] = "Geben Sie eine Zahl von 0,1 bis 3 an";
$MESS["USE_PLAYLIST_AS_SOURCES_TIP"] = "Der Browser wird die komplette Dateiliste durchlaufen und die erste verfьgbare Datei wiedergeben";
?>