<?
$MESS["ABOUT_TEXT"] = "Bitrix Media Player";
$MESS["PLAYER_PLAYLIST_ADD"] = "Playliste erstellen";
$MESS["PLAYER_PLAYLIST_EDIT"] = "Playliste bearbeiten";
$MESS["ABOUT_LINK"] = "http://www.bitrix.de/products/cms/features/player.php#tab-main-link";
$MESS["INCORRECT_FILE"] = "Ungьltige Datei";
$MESS["INCORRECT_PLAYLIST"] = "Die angegebene Playliste wurde nicht gefunden";
$MESS["SWF_DENIED"] = "Dieser Dateityp wird nicht unterstьtzt.";
$MESS["INCORRECT_PLAYLIST_FORMAT"] = "Playlist-Format ist nicht korrekt";
$MESS["JWPLAYER_DEPRECATED"] = "Diese Playerversion ist veraltet. Nutzen Sie bitte einen .js basierten Player.";
$MESS["PLAYLIST_AUDIO_AND_VIDEO_NOT_SUPPORTED"] = "Player unterstьtzt nicht Video- und Audioeintrдge gleichzeitig in einer Playlist.";
$MESS["PLAYLIST_VIMEO_NOT_SUPPORTED"] = "Player unterstьtzt nicht die Eintrдge im Format vimeo.com in der Playlist.";
$MESS["PLAYLIST_STREAMING_VIDEO_NOT_SUPPORTED"] = "Player unterstьtzt nicht Eintrдge des Streaming-Videos in der Playlist.";
$MESS["NO_SUPPORTED_FILES"] = "Es gibt keine Dateien, die unterstьtzt werden.";
$MESS["PLAYLIST_FILE_NOT_FOUND"] = "wird nicht unterstьtzt";
$MESS["VIDEOJS_NOT_SUPPORTED_MESSAGE"] = "Es wurde keine passende Wiedergabefunktion gefunden.";
?>