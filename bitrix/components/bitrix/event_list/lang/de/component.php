<?
$MESS["MAIN_EVENTLOG_USER_AUTHORIZE"] = "Erfolgreich eingeloggt";
$MESS["MAIN_EVENTLOG_USER_LOGIN"] = "Fehler beim Einloggen";
$MESS["MAIN_EVENTLOG_USER_LOGINBYHASH_FAILED"] = "Einloggen-Fehler mit gespeicherter Authentifizierung";
$MESS["MAIN_EVENTLOG_USER_LOGOUT"] = "Log out";
$MESS["MAIN_EVENTLOG_USER_REGISTER"] = "Registrierung eines neuen Nutzers";
$MESS["MAIN_EVENTLOG_USER_REGISTER_FAIL"] = "Registrierungsfehler";
$MESS["MAIN_EVENTLOG_USER_INFO"] = "Anfrage wegen der Passwortдnderung";
$MESS["MAIN_EVENTLOG_USER_PASSWORD_CHANGED"] = "Nutzerpasswort дndern";
$MESS["MAIN_EVENTLOG_USER_DELETE"] = "Nutzer lцschen";
$MESS["MAIN_EVENTLOG_MP_MODULE_INSTALLED"] = "Eine Marketplace-Lцsung wurde installiert";
$MESS["MAIN_EVENTLOG_MP_MODULE_UNINSTALLED"] = "Eine Marketplace-Lцsung wurde deinstalliert";
$MESS["MAIN_EVENTLOG_MP_MODULE_DELETED"] = "Eine Marketplace-Lцsung wurde gelцscht";
$MESS["MAIN_EVENTLOG_MP_MODULE_DOWNLOADED"] = "Eine Marketplace-Lцsung wurde heruntergeladen";
$MESS["MAIN_EVENTLOG_GROUP"] = "Gruppen des Nutzers geдndert";
$MESS["MAIN_EVENTLOG_GROUP_POLICY"] = "Sicherheitsrichtlinien der Gruppe geдndert";
$MESS["MAIN_EVENTLOG_MODULE"] = "Zugriffsrechte der Gruppe fьr das Modul geдndert";
$MESS["MAIN_EVENTLOG_FILE"] = "Zugriffsrechte fьr Datei geдndert";
$MESS["MAIN_EVENTLOG_TASK"] = "Zugriffslevel geдndert";
?>