<?
//Send email on Insert/Update FAQ
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "sendMailOnUpdateStatus");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "checkCorrectPricesRangeInPriceIB");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "sendMailOnAddStatus");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "checkCorrectPricesRangeInPriceIB");

//AddEventHandler("main", "OnEpilog", "Redirect404");
AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);

$curPage = explode('?', $APPLICATION->GetCurPage());
$curPage = reset($curPage);

define("ID_IBLOCK_ACTIONS", 4);
define("ID_IBLOCK_DOCTORS", 5);
define("ID_IBLOCK_WORKS", 7);
define("ID_IBLOCK_FEEDBACK", 9);
define("ID_IBLOCK_CONSULTATION", 12);
define("ID_IBLOCK_PRICES", 15);
define("ID_IBLOCK_SERVICES", 16);
define("ID_IBLOCK_DIPLOMAS", 17);

if($_SERVER['REMOTE_ADDR'] != "127.0.0.1") {
	$GLOBALS['prod'] = true;
}

include __DIR__."/include/functions.php";
include __DIR__."/include/pages.php";
include __DIR__."/include/SpamHandler.php";
include __DIR__."/include/ServiceSectionsMenuItemsHelper.php";
include __DIR__."/include/ImageResize/ImageResize.php";
include __DIR__."/include/ImageResize/PictureTag.php";

if(isHomePage()){
	include __DIR__."/include/removeSystemAssets.php";
}

function Redirect404()
{
	if(
	!defined('ADMIN_SECTION') &&
	defined("ERROR_404") &&
	file_exists($_SERVER["DOCUMENT_ROOT"]."/404.php"))
	{
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		CHTTP::SetStatus("404 Not Found");
		include($_SERVER["DOCUMENT_ROOT"]."/404.php");
	}
}


function _Check404Error(){
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/header.php';
        include $_SERVER['DOCUMENT_ROOT'].'/404.php';
        include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/footer.php';
    }
}

function dump()
{
	$args = func_get_args();

	echo "\n<pre style=\"border:1px solid #ccc;padding:10px;" .
       "margin:10px;font:14px courier;background:whitesmoke;" .
       "display:block;border-radius:4px;\">\n";

	$trace = debug_backtrace(false);
	$offset = (@$trace[2]['function'] === 'dump_d') ? 2 : 0;

	echo "<span style=\"color:red\">" .
       @$trace[1+$offset]['class'] . "</span>:" .
       "<span style=\"color:blue;\">" .
       @$trace[1+$offset]['function'] . "</span>:" .
       @$trace[0+$offset]['line'] . " " .
       "<span style=\"color:green;\">" .
       @$trace[0+$offset]['file'] . "</span>\n";

	if ( ! empty($args)) {
		call_user_func_array('var_dump', $args);
	}

	echo "</pre>\n";
}

function checkCorrectPricesRangeInPriceIB(&$arFields)
{
    if($arFields["IBLOCK_ID"] == ID_IBLOCK_PRICES){
        $pathComponentPrices = "/bitrix/components/kmedia/prices";
        require $_SERVER["DOCUMENT_ROOT"].$pathComponentPrices."/models/ModelElements.php";
        require $_SERVER["DOCUMENT_ROOT"].$pathComponentPrices."/models/ModelElementsPricesRange.php";
        $model = new ModelElementsPricesRange();
        if(!$model->checkCorrectPricesRange($arFields)){
            return false;
        }
    }
}

function getIbElement($elementId, $isOnlyFields = false)
{
	$res = \CIBlockElement::GetList([], ["ID" => $elementId]);

	if(!$bxElement = $res->GetNextElement()){
		return null;
	}

	$element = new stdClass();
	$element->fields = $bxElement->GetFields();

	if($isOnlyFields){
		return $element->fields;
	}

	$element->props = $bxElement->GetProperties();

	return $element;
}

function getIbElementFields($elementId)
{
	return getIbElement($elementId, true);
}

function getImageSrc($imageId)
{
	return !empty($imageId) ? \CFile::GetPath($imageId) : null;
}

function getSectionPropertyValue($fieldCode, $iblockId, $sectionCode)
{
	$result = false;

	$ufName = [$fieldCode];
	$ufArResult = CIBlockSection::GetList(
		["SORT" => "ASC"],
		["IBLOCK_ID" => $iblockId, "CODE" => $sectionCode],
		false,
		$ufName
	);

	if($ufValue = $ufArResult->GetNext()):
		if(!empty($ufValue[$fieldCode])):
			$result = $ufValue[$fieldCode];
		endif;
	endif;

	return $result;
}

function getReviewsDoctors(): array
{
	$res = \CIBlockElement::GetList(
		["SORT" => "ASC"],
		[
			"IBLOCK_ID" => ID_IBLOCK_FEEDBACK,
			"ACTIVE" => "Y",
			"!PROPERTY_O_DOCTOR" => false,
		],
		false,
		false,
		["PROPERTY_O_DOCTOR"]
	);

	$doctorsId = [];
	while($bxElement = $res->Fetch()){
		$doctorId = $bxElement["PROPERTY_O_DOCTOR_VALUE"];
		if($doctorId && !in_array($doctorId, $doctorsId)){
			$doctorsId[] = $doctorId;
		}
	}

	$res = \CIBlockElement::GetList(
		["SORT" => "ASC"],
		[
			"ID" => $doctorsId,
			"IBLOCK_ID" => ID_IBLOCK_DOCTORS,
			"ACTIVE" => "Y",
		]
	);
	$doctors = [];
	while($bxElement = $res->Fetch()){
		$doctors[] = [
			"id" => $bxElement["ID"],
			"name" => $bxElement["NAME"],
		];
	}

	return $doctors;
}

function getReviewsServices()
{
	$res = \CIBlockElement::GetList(
		["SORT" => "ASC"],
		[
			"IBLOCK_ID" => ID_IBLOCK_FEEDBACK,
			"ACTIVE" => "Y",
			"!PROPERTY_SERVICE" => false,
		],
		false,
		false,
		["PROPERTY_SERVICE"]
	);

	$servicesId = [];
	while($bxElement = $res->Fetch()){
		$serviceId = $bxElement["PROPERTY_SERVICE_VALUE"];
		if($serviceId && !in_array($serviceId, $servicesId)){
			$servicesId[] = $serviceId;
		}
	}

	$res = \CIBlockElement::GetList(
		["SORT" => "ASC"],
		[
			"ID" => $servicesId,
			"IBLOCK_ID" => ID_IBLOCK_SERVICES,
			"ACTIVE" => "Y",
		]
	);
	$services = [];
	while($bxElement = $res->Fetch()){
		$services[] = [
			"id" => $bxElement["ID"],
			"name" => $bxElement["NAME"],
		];
	}

	return $services;
}

function getAllDoctors(): array
{
	$res = \CIBlockElement::GetList(
		["SORT" => "ASC"],
		["IBLOCK_ID" => ID_IBLOCK_DOCTORS, "ACTIVE" => "Y"]
	);

	$doctors = [];
	while($bxElement = $res->Fetch()){
		$doctors[] = [
			"id" => $bxElement["ID"],
			"name" => $bxElement["NAME"],
		];
	}

	return $doctors;
}

function getAllServices(): array
{
	$res = \CIBlockElement::GetList(
		["SORT" => "ASC"],
		["IBLOCK_ID" => ID_IBLOCK_SERVICES, "ACTIVE" => "Y"]
	);

	$services = [];
	while($bxElement = $res->Fetch()){
		$services[] = [
			"id" => $bxElement["ID"],
			"name" => $bxElement["NAME"],
		];
	}

	return $services;
}

function getConsultationSections(): array
{
	$res = CIBlockSection::GetList(
		["SORT" => "ASC"],
		["IBLOCK_ID" => ID_IBLOCK_CONSULTATION, "ACTIVE" => "Y"],
		false,
		["IBLOCK_ID", "ID", "NAME"]
	);

	$sections = [];
	while($arResult = $res->Fetch()){
		$sections[] = [
			"id" => $arResult["ID"],
			"name" => $arResult["NAME"],
		];
	}

	return $sections;
}

// при совпадении символьных кодов для разных элементов
// при формировании урла отдавать приоритет элементу DETAIL_PAGE_URL которого совпадает с текущим URL
function getElementIdByElementCode($code, $iblockId)
{
	$res = \CIBlockElement::GetList(
		["ID" => "ASC"],
		["IBLOCK_ID" => $iblockId, "CODE" => $code]
	);

	global $curPage;

	$elements = [];
	while($bxElement = $res->GetNext()){
		$elements[] = [
			"ID" => $bxElement["ID"],
			"DETAIL_PAGE_URL" => $bxElement["DETAIL_PAGE_URL"]
		];
	}

	if(!$elements){
		return 0;
	}elseif(count($elements) == 1){
		return $elements[0]["ID"];
	}else{
		foreach ($elements as $element){
			if($element["DETAIL_PAGE_URL"] == $curPage){
				return $element["ID"];
			}
		}
		return $elements[0]["ID"];
	}

}
