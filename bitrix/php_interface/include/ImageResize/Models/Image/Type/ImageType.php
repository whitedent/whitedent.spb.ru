<?
namespace Models\Image\Type;

abstract class ImageType
{
	const SOURCE = ""; // Где хранится фото: PREVIEW_TEXT, DETAIL_TEXT или код свойства

	public $src;

	public function __construct(int $imageId)
	{
		$this->src = static::getImageSrc($imageId);
	}

	protected function getImageSrc(int $imageId): ?string
	{
		return getImageSrc($imageId);
	}

}