<?
namespace Models\Image\Type\Action;

use Models\Image\Type\ImageType;

class Preview extends ImageType
{
	const TITLE = "Акции - Анонсовое изображение";
	const SLUG = "ActionPreview";
	const IBLOCK_ID = ID_IBLOCK_ACTIONS;
	const SOURCE = "PREVIEW_PICTURE";
	const SIZES = [
		"L" => [487, 974],
	];
}