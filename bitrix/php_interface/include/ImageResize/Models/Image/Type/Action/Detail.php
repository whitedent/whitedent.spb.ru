<?
namespace Models\Image\Type\Action;

use Models\Image\Type\ImageType;

class Detail extends ImageType
{
	const TITLE = "Акции - Детальное изображение";
	const SLUG = "ActionDetail";
	const IBLOCK_ID = ID_IBLOCK_ACTIONS;
	const SOURCE = "DETAIL_PICTURE";
	const SIZES = [
		"L" => [252, 504],
	];
}