<?
namespace Models\Image\Type\Service;

use Models\Image\Type\ImageType;

class Detail extends ImageType
{
	const TITLE = "Услуги - Детальное изображение";
	const SLUG = "ServiceDetail";
	const IBLOCK_ID = ID_IBLOCK_SERVICES;
	const SOURCE = "DETAIL_PICTURE";
	const SIZES = [
		"XS" => [480, 960, 1440],
		"L" => [268, 536],
	];
}