<?
namespace Models\Image\Type\Doctor;

use Models\Image\Type\ImageType;

class Certificate extends ImageType
{
	const TITLE = "Доктора - Сертификаты";
	const SLUG = "DoctorCertificate";
	const IBLOCK_ID = ID_IBLOCK_DOCTORS;
	const SOURCE = "PROPERTY_D_CERT";
	const SIZES = [
		"XS" => [208, 416, 624],
		"L" => [268, 536],
	];
}