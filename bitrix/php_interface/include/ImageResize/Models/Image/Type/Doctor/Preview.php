<?
namespace Models\Image\Type\Doctor;

use Models\Image\Type\ImageType;

class Preview extends ImageType
{
	const TITLE = "Доктора - Анонсовое изображение";
	const SLUG = "DoctorPreview";
	const IBLOCK_ID = ID_IBLOCK_DOCTORS;
	const SOURCE = "PREVIEW_PICTURE";
	const SIZES = [
		"XS" => [104, 208, 312],
		"L" => [268, 536]
	];
}