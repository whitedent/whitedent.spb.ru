<?
namespace Models\Image\Type\Doctor;

use Models\Image\Type\ImageType;

class Detail extends ImageType
{
	const TITLE = "Доктора - Детальное изображение";
	const SLUG = "DoctorDetail";
	const IBLOCK_ID = ID_IBLOCK_DOCTORS;
	const SOURCE = "DETAIL_PICTURE";
	const SIZES = [
		"XS" => [480, 960, 1440],
		"L" => [268, 536]
	];
}