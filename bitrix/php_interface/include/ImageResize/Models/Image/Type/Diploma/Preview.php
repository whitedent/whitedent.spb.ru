<?
namespace Models\Image\Type\Diploma;

use Models\Image\Type\ImageType;

class Preview extends ImageType
{
	const TITLE = "Дипломы - Анонсовое изображение";
	const SLUG = "DiplomaPreview";
	const IBLOCK_ID = ID_IBLOCK_DIPLOMAS;
	const SOURCE = "PREVIEW_PICTURE";
	const SIZES = [
		"XS" => [208, 416, 624],
		"L" => [268, 536],
	];
}