<?
namespace Models\Image\Type\Work;

use Models\Image\Type\ImageType;

class Detail extends ImageType
{
	const TITLE = "Работы - Детальное изображение";
	const SLUG = "WorkDetail";
	const IBLOCK_ID = ID_IBLOCK_WORKS;
	const SOURCE = "DETAIL_PICTURE";
	const SIZES = [
		"XS" => [415, 830, 1245],
		"L" => [244, 488]
	];
}