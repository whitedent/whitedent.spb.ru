<?
namespace Models\Image\Type\Work;

use Models\Image\Type\ImageType;

class Preview extends ImageType
{
	const TITLE = "Работы - Анонсовое изображение";
	const SLUG = "WorkPreview";
	const IBLOCK_ID = ID_IBLOCK_WORKS;
	const SOURCE = "PREVIEW_PICTURE";
	const SIZES = [
		"XS" => [415, 830, 1245],
		"L" => [244, 488]
	];
}