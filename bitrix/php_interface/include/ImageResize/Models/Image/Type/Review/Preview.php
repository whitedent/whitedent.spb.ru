<?
namespace Models\Image\Type\Review;

use Models\Image\Type\ImageType;

class Preview extends ImageType
{
	const TITLE = "Отзывы - Анонсовое изображение";
	const SLUG = "ReviewPreview";
	const IBLOCK_ID = ID_IBLOCK_FEEDBACK;
	const SOURCE = "PREVIEW_PICTURE";
	const SIZES = [
		"XS" => [224],
		"L" => [200]
	];
}