<?
namespace Models\Image;

use Models\Image\Type\ImageType;
use Models\PathHelper;
use Models\DimensionHelper;

class Resizer
{
	public function makeCopies(ImageType $image): array
	{
		$result = ["src" => $image->src, "copies" => []];

		$image->src = $_SERVER["DOCUMENT_ROOT"].$image->src;

		if(!file_exists($image->src)){
			return $result;
		}

		$width = DimensionHelper::getWidth($image->src);

		foreach($image::SIZES as $sizeName => $sizeValues){
			foreach($sizeValues as $density => $sizeValue){

				$density++;
				$sizeSlug = $sizeName."_".$density;
				$newPath = PathHelper::getNewPath($image->src, $sizeName, $density);
				$webpPath = PathHelper::getWebpPath($newPath);

				if($sizeValue <= $width){
					if($this->saveResizedCopy($image, $sizeValue, $newPath)){
						$result["copies"][] = $sizeSlug;
					}
					$this->saveWebpCopy($newPath, $webpPath);
				}else{
					if($this->saveOriginalCopy($image, $newPath)){
						$result["copies"][] = $sizeSlug;
					}
					$this->saveWebpCopy($newPath, $webpPath);
					break;
				}

			}
		}

		return $result;
	}

	private function saveResizedCopy(ImageType $image, int $newWidth, string $newPath): bool
	{
		if(file_exists($newPath)){
			return false;
		}

		if(self::isImgJpeg($image->src)){
			$gdImage = imagecreatefromjpeg($image->src);
		}elseif(self::isImgPng($image->src)){
			$gdImage = imagecreatefrompng($image->src);
		}else{
			throw new \Exception("Неизвестный тип изображения");
		}

//		$gdImageResized = imagescale($gdImage, $newWidth);
		$gdImageResized = imagescale($gdImage, $newWidth, -1, IMG_BICUBIC);

		if(self::isImgJpeg($image->src)){
			imagejpeg($gdImageResized, $newPath);
		}elseif(self::isImgPng($image->src)){
			imagepng($gdImageResized, $newPath);
		}

		imagedestroy($gdImage);
		imagedestroy($gdImageResized);

		return true;
	}

	private function saveWebpCopy(string $standardPath, string $webpPath)
	{
		if(file_exists($webpPath)){
			return true;
		}

		if($this->isImgPng($standardPath)){
			$gdImage = imagecreatefrompng($standardPath);
			imagepalettetotruecolor($gdImage);
			imagealphablending($gdImage, true);
			imagesavealpha($gdImage, true);
		} elseif($this->isImgJpeg($standardPath)) {
			$gdImage = imagecreatefromjpeg($standardPath);
			imagepalettetotruecolor($gdImage);
		} else {
			return false;
		}

		imagewebp($gdImage, $webpPath, 100);

		//x00 webp generation fix
		$fpr = fopen($webpPath, "a+");
		fwrite($fpr, chr(0x00));
		fclose($fpr);

		imagedestroy($gdImage);

		return true;
	}

	private function saveOriginalCopy(ImageType $image, string $newPath): bool
	{
		if(file_exists($newPath)){
			return false;
		}

		copy($image->src, $newPath);

		return true;
	}

	private static function isImgJpeg(string $path): bool
	{
		return exif_imagetype($path) == IMAGETYPE_JPEG;
	}

	private static function isImgPng(string $path): bool
	{
		return exif_imagetype($path) == IMAGETYPE_PNG;
	}

}
