<?
AddEventHandler(
	"iblock", 
	"OnAfterIBlockElementAdd", 
	["ImageResize", "Run"]
);

AddEventHandler(
	"iblock", 
	"OnAfterIBlockElementUpdate", 
	["ImageResize", "Run"]
);

class ImageResize
{
	public function run(&$arFields)
	{
		if($arFields["IBLOCK_ID"] == ID_IBLOCK_SERVICES){
			self::process(Models\Image\Type\Service\Detail::class, $arFields["ID"]);
		}

		if($arFields["IBLOCK_ID"] == ID_IBLOCK_ACTIONS){
			self::process(Models\Image\Type\Action\Preview::class, $arFields["ID"]);
			self::process(Models\Image\Type\Action\Detail::class, $arFields["ID"]);
		}

		if($arFields["IBLOCK_ID"] == ID_IBLOCK_DOCTORS){
			self::process(Models\Image\Type\Doctor\Certificate::class, $arFields["ID"]);
			self::process(Models\Image\Type\Doctor\Preview::class, $arFields["ID"]);
			self::process(Models\Image\Type\Doctor\Detail::class, $arFields["ID"]);
		}

		if($arFields["IBLOCK_ID"] == ID_IBLOCK_DIPLOMAS){
			self::process(Models\Image\Type\Diploma\Preview::class, $arFields["ID"]);
		}

		if($arFields["IBLOCK_ID"] == ID_IBLOCK_WORKS){
			self::process(Models\Image\Type\Work\Preview::class, $arFields["ID"]);
			self::process(Models\Image\Type\Work\Detail::class, $arFields["ID"]);
		}

		if($arFields["IBLOCK_ID"] == ID_IBLOCK_FEEDBACK){
			self::process(Models\Image\Type\Review\Preview::class, $arFields["ID"]);
		}

	}

	private static function process($modelName, $elementId)
	{
		self::initModels();

		$bx = new Models\BxModel;
		$images = $bx->getImages($modelName, $elementId);
		$resizer = new Models\Image\Resizer;

		foreach ($images as $image){
			$resizer->makeCopies($image);
		}
	}

	private static function initModels()
	{
		$modelsPath = __DIR__."/Models";
		require_once $modelsPath."/Image/Type/ImageType.php";

		$dir = new RecursiveDirectoryIterator($modelsPath);
		foreach (new RecursiveIteratorIterator($dir) as $file) {
			if (!is_dir($file)) {
				if( fnmatch('*.php', $file) ){
					require_once $file;
				}
			}
		}
	}

}