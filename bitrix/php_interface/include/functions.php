<?
function mb_ucfirst($string, $encoding)
{
	$firstChar = mb_substr($string, 0, 1, $encoding);
	$then = mb_substr($string, 1, null, $encoding);
	return mb_strtoupper($firstChar, $encoding) . $then;
}

function filterCode($code)
{
	$codeExplode = explode('/', $code);
	$code = end($codeExplode);
	$codeExplode = explode('?', $code);
	$code = reset($codeExplode);

	return $code;
}

function getTruncatedDataForSeo($text, $charsLimit = 250)
{
	$text = strip_tags($text);
	$text = str_replace(array("\n", "\r", "\n\r", "\r\n", ), '', $text);

	$text = html_entity_decode($text);
	$text = trim($text);
	$text = preg_replace('/[ ]{2,}/', " ", $text);

	if(mb_strlen($text) > $charsLimit){
		$text = mb_substr($text, 0, $charsLimit, 'UTF-8');
		$position = mb_strrpos($text, ' ', 'UTF-8');
		$text = mb_substr($text, 0, $position, 'UTF-8');
	}

	$text = rtrim($text, '.,:-—');
	$text = trim($text);

	return $text;
}

function getYearsAsText($year)
{
	$year = abs($year);
	$t1 = $year % 10;
	$t2 = $year % 100;
	return ($t1 == 1 && $t2 != 11 ? "год" : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? "года" : "лет"));
}

if (! function_exists("array_key_last")) {
	function array_key_last($array) {
		if (!is_array($array) || empty($array)) {
			return NULL;
		}

		return array_keys($array)[count($array)-1];
	}
}

function getRegisterToDoctorFormTitle(string $rootSectionName): string
{
	$title = "Записаться на консультацию";

	$ref = [
		"Компьютерная 3D томография зубов" => "рентгенологу",
		"КТ пазух носа" => "рентгенологу",
		"Лечение зубов" => "стоматологу-терапевту",
		"Лечение десен" => "пародонтологу",
		"Исправление прикуса" => "ортодонту",
		"Хирургические услуги" => "стоматологу-хирургу",
		"Имплантация" => "стоматологу-имплантологу",
		"Протезирование" => "стоматологу-ортопеду",
		"Обезболивание" => "стоматолог-анестезиолог",
		"Гигиена полости рта" => "стоматологу-гигиенисту",
		"Консультации" => "стоматологу-терапевту",
	];

	if(!empty($ref[$rootSectionName])){
		$title .= " к ".$ref[$rootSectionName];
	}else{
		$title .= " к стоматологу-терапевту";
	}

	return $title;
}

function truncateLastPeriod($text)
{
	$length = mb_strlen($text, "UTF-8");
	$lastCharPosition = $length - 1;
	if(mb_strrpos($text, '.', 0, "UTF-8") == $lastCharPosition){
		return mb_substr($text, 0, ($length - 1), "UTF-8");
	}

	return $text;
}