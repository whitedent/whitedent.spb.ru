<?
AddEventHandler("main", "OnEndBufferContent", "deleteKernelJs");
AddEventHandler("main", "OnEndBufferContent", "deleteKernelCss");

function deleteKernelJs(&$content) {
	global $USER, $APPLICATION;
	if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
	if($APPLICATION->GetProperty("save_kernel") == "Y") return;

	$arPatternsToRemove = Array(
		'/<script.+?src=".+?kernel_main\/kernel_main_v1\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/main\/core\/core[^"]+"><\/script\>/',
		'/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
		'/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
		'/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',

		'/<script.+?src=".+?bitrix\/js\/main\/polyfill\/promise\/js\/promise\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/main\/loadext\/loadext\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/main\/loadext\/loadext\.min\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/main\/loadext\/extension\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/main\/loadext\/extension\.min\.js\?\d+"><\/script\>/',

		'/<script.+?src=".+?bitrix\/js\/main\/popup\/dist\/main\.popup\.bundle\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/pull\/protobuf\/protobuf\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/pull\/protobuf\/model\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/rest\/client\/rest\.client\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/pull\/client\/pull\.client\.js\?\d+"><\/script\>/',
		'/<script.+?src=".+?bitrix\/js\/main\/pageobject\/pageobject\.js\?\d+"><\/script\>/',

	);

	$content = preg_replace($arPatternsToRemove, "", $content);
	$content = preg_replace("/\n{2,}/", "\n\n", $content);

	$content = createBxObj().$content;
}

function deleteKernelCss(&$content) {
	global $USER, $APPLICATION;
	if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
	if($APPLICATION->GetProperty("save_kernel") == "Y") return;

	$arPatternsToRemove = Array(
		'/<link.+?href=".+?kernel_main\/kernel_main_v1\.css\?\d+"[^>]+>/',
		'/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',
		'/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/styles.css[^"]+"[^>]+>/',

		'/<link.+?href=".+?bitrix\/js\/ui\/fonts\/opensans\/ui\.font\.opensans\.css\?\d+"[^>]+>/',
		'/<link.+?href=".+?bitrix\/js\/main\/popup\/dist\/main\.popup\.bundle\.css\?\d+"[^>]+>/',
		'/<link.+?href=".+?bitrix\/panel\/main\/popup\.css\?\d+"[^>]+>/',
		'/<link.+?href=".+?bitrix\/templates\/tpl_whitedent\/template_styles\.css\?\d+"[^>]+>/',
	);

	$content = preg_replace($arPatternsToRemove, "", $content);
	$content = preg_replace("/\n{2,}/", "\n\n", $content);
}

function createBxObj()
{
	return "<script>
		class BX{
		  static setJSList(){} 
		  static setCSSList(){} 
		  static ready(){} 
		}
	</script>";
}
