<?
function getCurPage(): string
{
	global $APPLICATION;
	$curPage = explode('?', $APPLICATION->GetCurPage());
	$curPage = reset($curPage);

	return $curPage;
}

function isHomePage(): bool
{
	$curPage = getCurPage();
	return $curPage == "/";
}

function isInnerPage(): bool
{
	return !isHomePage() && !defined("ERROR_404");
}


function isCurPageUrlConsist($pageUrl): bool
{
	return strpos(getCurPage(), $pageUrl) !== false;
}