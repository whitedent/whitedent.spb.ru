<?
class SpamHandler
{
	private $request;

	public function __construct($request)
	{
		$this->request = $request;
	}

	public function isHasSpam($nameFieldName, $messageFieldName)
	{
		if(!empty($this->request["req_name"])){
			return "Ошибка данных";
		}

		if(!empty($this->request[$nameFieldName])){
			$name = $this->request[$nameFieldName];
			if(preg_match('/[0-9a-zA-Z]/', $name)){
				return 'В поле "Имя" могут быть только символы русского алфавита';
			}
		}

		if(!empty($this->request[$messageFieldName]) && $this->isMessageHasBadWords($messageFieldName)){
			if($GLOBALS["prod"]){
				//$this->sendAdminMessageAboutSpamActivity();
			}
			return "Спам активность";
		}

		return false;
	}

	private function isMessageHasBadWords($messageFieldName)
	{
		$badWords = [
			"FormMarketing",
			"владельц",
			"прибыль",
			"прибыли",
			"рассылк",
			"оффер",
			"сотруднич",
			"команда",
			"команды",
			"команду",
			"кампан",
			"аудит",
			"таргетинг",
			"менеджмент",
			"маркетинг",
			"привлекаем",
			"бренд",
			"репутац",
			"копирайтинг",
			"агрегатор",
			"репутаци",
			"рейтинг",
			"отзывик",
			"продвиж",
			"SЕО",
			"Seologys",
			"marketing",
			"парсим",
			"коммерч",
			"крипто",
			"крипта",
			"пожар",
			"инвайтинг",
			"контекст",
			"недвижимост",
			"дизайн",
			"Joomla",
			"разработк",
			"предоставим",
			"предлагаем",
			"маркетплейс",
			"авито",
			"обзвон",
			"мероприятие",
			"парфюм",
			"партнер",
			"вебмастер",
			"запишитесь",
			"аутстаф",
			"предоставляем",
			"мебель",
		];
		$message = $this->request[$messageFieldName];

		foreach ($badWords as $badWord){
			if(stripos($message, $badWord) !== false){
				return true;
			}
		}

		return false;
	}

	private function sendAdminMessageAboutSpamActivity()
	{
		$fields = [
			"NAME" => $this->request["NAME"],
			"MESSAGE" => $this->request["COMMENT"],
			"REQ_NAME" => $this->request["req_name"],
		];
		CEvent::SendImmediate("SPAM_ACTIVITY", "s1", $fields, "N");
	}

}