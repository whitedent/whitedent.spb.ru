<?
class ServiceSectionsMenuItemsHelper
{
	public function getMenuItems()
	{
		CModule::IncludeModule("iblock");

		$rootSections = $this->getRootSections();

		$sections = [];
		foreach ($rootSections as $rootSection){
			$sections[] = $rootSection;
			$innerSections = $this->getInnerSectionsForRootSection($rootSection["ID"]);
			$sections = array_merge($sections, $innerSections);
		}

		$menuItems = $this->makeMenuItems($sections);
		$menuItems = $this->setIsParentAttribute($menuItems);

		return $menuItems;
	}

	private function getRootSections()
	{
		$arOrder = ["SORT" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => ID_IBLOCK_SERVICES,
			"DEPTH_LEVEL" => 1,
			"ACTIVE" => "Y"
		];
		$bIncCnt = false;
		$Select = ["ID", "IBLOCK_ID", "NAME", "SECTION_PAGE_URL", "DEPTH_LEVEL"];
		$NavStartParams = false;
		$rsSections = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

		$sections = [];
		while ($arSection = $rsSections->GetNext()) {
			$sections[] = $arSection;
		}

		return $sections;
	}

	private function getInnerSectionsForRootSection($sectionId)
	{
		$res = CIBlockSection::GetByID($sectionId);
		$section = $res->GetNext();

		$arOrder = ["LEFT_MARGIN" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => ID_IBLOCK_SERVICES,
			'>LEFT_MARGIN' => $section["LEFT_MARGIN"],
			'<RIGHT_MARGIN' => $section["RIGHT_MARGIN"],
			'>DEPTH_LEVEL' => $section["DEPTH_LEVEL"],
			"ACTIVE" => "Y"
		];
		$bIncCnt = false;
		$Select = ["ID", "IBLOCK_ID", "NAME", "DEPTH_LEVEL", "SECTION_PAGE_URL", "LEFT_MARGIN", "RIGHT_MARGIN"];
		$NavStartParams = false;
		$rsSections = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

		$innerSections = [];
		while ($arSection = $rsSections->GetNext())
		{
			$innerSections[] = $arSection;
		}

		return $innerSections;
	}

	private function makeMenuItems($sections)
	{
		$menuItems = [];
		foreach ($sections as $section) {
			$menuItems[] = [
				$section["NAME"],
				$section["SECTION_PAGE_URL"],
				[],
				["FROM_IBLOCK" => ID_IBLOCK_SERVICES, "DEPTH_LEVEL" => $section["DEPTH_LEVEL"]]
			];
		}

		return $menuItems;
	}

	private function setIsParentAttribute(array $menuItems)
	{
		$menuItemsCount = count($menuItems);
		for($i = 0; $i < $menuItemsCount; $i++){

			// если последний элемент массива -> выход
			if(!array_key_exists($i + 1, $menuItems)){
				break;
			}

			$curItemParams = $menuItems[$i][3];
			$nextItemParams = $menuItems[$i + 1][3];

			$curItemDepthLevel = (int)$curItemParams["DEPTH_LEVEL"];
			$nextItemDepthLevel = (int)$nextItemParams["DEPTH_LEVEL"];

			// если след. элемент массива имеет более глубокий уровень вложенности ->
			// значит текущий элемент массива его родитель
			if($curItemDepthLevel < $nextItemDepthLevel){
				$menuItems[$i][3]["IS_PARENT"] = true;
			}
		}

		return $menuItems;
	}

}
