<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sr.pack/prolog.php");
IncludeModuleLangFile(__FILE__);
$APPLICATION->SetTitle(GetMessage("INDEX_SR_PACK_IMAGES"));
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/interface/admin_lib.php");
$isAdmin = $USER->CanDoOperation('edit_other_settings');

CModule::IncludeModule("sr.pack");
$srML = new SRMedialibrary();

//ACTIONS
if(isset($_GET["IID"]) && isset($_GET["action"]) && is_numeric($_GET["IID"]))
{
    $srML->setDB($DB);
    if($_GET["action"] == "create")
        $srML->createWatermarkImage($_GET["IID"]);
    elseif($_GET["action"] == "reload")
        $srML->actionWatermarkImage($_GET["IID"]);
    elseif($_GET["action"] == "delete")
        $srML->actionWatermarkImage($_GET["IID"], "delete");
    elseif($_GET["action"] == "idelete")
        $srML->actionWatermarkImage($_GET["IID"], "idelete");
}

$sTableID = "tbl_list_image";
// инициализация сортировки
$oSort = new CAdminSorting($sTableID, "SORT", "asc");
// инициализация списка
$lAdmin = new CAdminList($sTableID, $oSort);
$lAdmin->bMultipart = true;

$langs = CLang::GetList($by, $order, Array());
//Вытаскиваем информацию
if(is_numeric($_GET['ID']) && isset($_GET['ID']))
    $sql = "SELECT b_file.* 
            FROM b_file, b_medialib_collection_item, b_medialib_item
            WHERE b_file.ID = b_medialib_item.SOURCE_ID 
                AND b_medialib_item.ID = b_medialib_collection_item.ITEM_ID 
                AND b_medialib_collection_item.COLLECTION_ID = '".urlencode($_GET['ID'])."'";
else
    $sql = "SELECT b_file.* 
            FROM b_file, b_medialib_collection_item, b_medialib_item, b_medialib_collection
            WHERE b_file.ID = b_medialib_item.SOURCE_ID 
                AND b_medialib_item.ID = b_medialib_collection_item.ITEM_ID 
                AND b_medialib_collection_item.COLLECTION_ID = b_medialib_collection.ID
                AND b_medialib_collection.ACTIVE = 'Y' 
                AND b_medialib_collection.ML_TYPE = '1'";

$rsData = $DB->Query($sql);
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

// установка строки навигации
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES")));

$lAdmin->AddHeaders(array(
	array("id"=>"ID", "content"=>GetMessage("ID"), "default"=>true),
	array("id"=>"TIMESTAMP_X", "content"=>GetMessage("TIMESTAMP_X"), "default"=>true),
    array("id"=>"FILE_NAME", "content"=>GetMessage("FILE_NAME"), "default"=>true),
    array("id"=>"SUBDIR", "content"=>GetMessage("SUBDIR"), "default"=>true),
	array("id"=>"ORIGINAL_NAME", "content"=>GetMessage("ORIGINAL_NAME"), "default"=>false),
    array("id"=>"WATERMARK", "content"=>GetMessage("WATERMARK"), "default"=>true),
    
    array("id"=>"WIDTH", "content"=>GetMessage("WIDTH"), "default"=>false),
    array("id"=>"HEIGHT", "content"=>GetMessage("HEIGHT"), "default"=>false),
    array("id"=>"FILE_SIZE", "content"=>GetMessage("FILE_SIZE"), "default"=>false),
    array("id"=>"CONTENT_TYPE", "content"=>GetMessage("CONTENT_TYPE"), "default"=>false),
    array("id"=>"WATERMARK_LINK", "content"=>GetMessage("WATERMARK_LINK"), "default"=>false),
));



while($arRes = $rsData->NavNext(true, "f_"))
{
    $arRes["WATERMARK"] = $srML->existsWatermarkImage($arRes["SUBDIR"], $arRes["FILE_NAME"]);
    $nPath = $srML->getImageWatermark($arRes["SUBDIR"], $arRes["FILE_NAME"]);
    if(!is_null($nPath))
        $arRes["WATERMARK_LINK"] = '<a href="'.$nPath.'" target="_blank">'.GetMessage("VIEW_WM_IMAGE").'</a>';
    else
        $arRes["WATERMARK_LINK"] = GetMessage("NO_VIEW_WM_IMAGE");
    $arRes["SUBDIR"] = "/upload/".$arRes["SUBDIR"]."/";
    $arRes["TIMESTAMP_X"] = MakeTimeStamp($arRes["TIMESTAMP_X"], "YYYY.MM.DD HH:MI:SS");
    $arRes["TIMESTAMP_X"] = date("d.m.Y H:i:s", $arRes["TIMESTAMP_X"]);
	$row =& $lAdmin->AddRow($f_ID, $arRes, false, GetMessage("VIEW"));
	$row->AddViewField("ID", '<a href="'.$arRes["SUBDIR"].$arRes["FILE_NAME"].'" title="'.GetMessage("LIST").'" target="_blank">'.$f_ID.'</a>');
	$row->AddInputField("FILE_NAME");
    $row->AddInputField("SUBDIR");
    $row->AddInputField("WIDTH");
    $row->AddInputField("HEIGHT");
    $row->AddInputField("FILE_SIZE");
    $row->AddInputField("CONTENT_TYPE");
	$row->AddInputField("ORIGINAL_NAME");
    $row->AddCheckField("WATERMARK");
    $row->AddViewField("WATERMARK_LINK", $arRes["WATERMARK_LINK"]);
	$arActions = Array();

	if($isAdmin)
	{
	    if($arRes["WATERMARK"] == "N")
		    $arActions[] = array("ICON"=>"new", "TEXT"=>GetMessage("CREATE_WM"), "ACTION"=>$lAdmin->ActionRedirect("sr_pack_list_image.php?lang=".LANGUAGE_ID."&IID=".urlencode($arRes['ID'])."&action=create&ID=".$_GET['ID']));
        if($arRes["WATERMARK"] == "Y")
		    $arActions[] = array("ICON"=>"edit", "TEXT"=>GetMessage("RELOAD_WM"), "ACTION"=>$lAdmin->ActionRedirect("sr_pack_list_image.php?lang=".LANGUAGE_ID."&IID=".urlencode($arRes['ID'])."&action=reload&ID=".$_GET['ID']));
        if($arRes["WATERMARK"] == "Y")
            $arActions[] = array("ICON"=>"delete", "TEXT"=>GetMessage("DELETE_WM"), "ACTION"=>$lAdmin->ActionRedirect("sr_pack_list_image.php?lang=".LANGUAGE_ID."&IID=".urlencode($arRes['ID'])."&action=delete&ID=".$_GET['ID']));
        $arActions[] = array("SEPARATOR"=>true);
        $arActions[] = array("ICON"=>"delete", "TEXT"=>GetMessage("DELETE_IMAGE"), "ACTION"=>$lAdmin->ActionRedirect("sr_pack_list_image.php?lang=".LANGUAGE_ID."&IID=".urlencode($arRes['ID'])."&action=idelete&ID=".$_GET['ID']));
	}

	$row->AddActions($arActions);
}

$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddAdminContextMenu($aContext);
$lAdmin->CheckListMode();

include($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
$lAdmin->DisplayList();
include($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
?>