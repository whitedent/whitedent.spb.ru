<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sr.pack/prolog.php");
IncludeModuleLangFile(__FILE__);
$APPLICATION->SetTitle(GetMessage("INDEX_SR_PACK_LIBRARY"));
$isAdmin = $USER->CanDoOperation('edit_other_settings');

$sTableID = "tbl_list_library";
// инициализация сортировки
$oSort = new CAdminSorting($sTableID, "SORT", "asc");
// инициализация списка
$lAdmin = new CAdminList($sTableID, $oSort);

$langs = CLang::GetList($by, $order, Array());
//Вытаскиваем информацию
$sql = "SELECT ID, NAME FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1'";
$rsData = $DB->Query($sql);
$rsData->NavStart();

// установка строки навигации
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES"), false));

$lAdmin->AddHeaders(array(
	array("id"=>"ID", "content"=>GetMessage("ID"), "default"=>true),
	array("id"=>"NAME", "content"=>GetMessage("NAME"), "default"=>true),
	array("id"=>"COUNT", "content"=>GetMessage("COUNT"), "default"=>true),
));



while($arRes = $rsData->NavNext(true, "f_"))
{
    $sql_1 = "SELECT COUNT(ITEM_ID) FROM b_medialib_collection_item WHERE COLLECTION_ID = '".$arRes["ID"]."'";
    $data = $DB->Query($sql_1);
    $arRes["COUNT"] = mysql_fetch_array($data->result, MYSQL_ASSOC);
    $arRes["COUNT"] = $arRes["COUNT"]["COUNT(ITEM_ID)"];
    
	$row =& $lAdmin->AddRow($f_ID, $arRes, "sr_pack_list_image.php?lang=".LANGUAGE_ID."&ID=".urlencode($arRes['ID']), GetMessage("VIEW"));
	$row->AddViewField("ID", '<a href="fileman_medialib_admin.php?lang='.LANGUAGE_ID.'" title="'.GetMessage("LIST").'">'.$f_ID.'</a>');
	$row->AddInputField("NAME");
	$row->AddInputField("COUNT");
	$arActions = Array();

	$arActions[] = array("ICON"=>"edit", "TEXT"=>GetMessage("VIEW"), "ACTION"=>$lAdmin->ActionRedirect("sr_pack_list_image.php?lang=".LANGUAGE_ID."&ID=".urlencode($arRes['ID'])), "DEFAULT"=>true);

	if($isAdmin)
	{
	    $arActions[] = array("SEPARATOR"=>true);
		$arActions[] = array("ICON"=>"new", "TEXT"=>GetMessage("CREATE_WM"), "ACTION"=>$lAdmin->ActionRedirect("fileman_medialib_admin.php"));
		$arActions[] = array("ICON"=>"edit", "TEXT"=>GetMessage("RELOAD_WM"), "ACTION"=>$lAdmin->ActionRedirect("fileman_medialib_admin.php"));
        $arActions[] = array("ICON"=>"delete", "TEXT"=>GetMessage("DELETE_WM"), "ACTION"=>$lAdmin->ActionRedirect("fileman_medialib_admin.php"));
        $arActions[] = array("SEPARATOR"=>true);
        $arActions[] = array("ICON"=>"delete", "TEXT"=>GetMessage("DELETE_LIBRARY"), "ACTION"=>$lAdmin->ActionRedirect("fileman_medialib_admin.php"));
	}

	$row->AddActions($arActions);
}

$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddAdminContextMenu($aContext);
$lAdmin->CheckListMode();

include($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
$lAdmin->DisplayList();
include($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
?>