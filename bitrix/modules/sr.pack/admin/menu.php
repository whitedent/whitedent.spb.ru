<?
// подключим языковой файл
IncludeModuleLangFile(__FILE__);
// определим права текущего пользователя
$SUP_RIGHT = $APPLICATION->GetGroupRight("sr.pack");
// если доступ не запрещён то
if($SUP_RIGHT > "D")
{
    $aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => "sr_pack",
		"sort" => 540,
		"text" => GetMessage("SUP_M_SRPACK"),
		"url"  => "/bitrix/admin/sr_pack_index.php?lang=".LANG,
		"title"=> GetMessage("SUP_M_SRPACK"),
		"icon" => "sys_menu_icon",
        "page_icon" => "sys_page_icon",
		"items_id" => "menu_sr_pack",
		"items" => array(
			array(
				"text" => GetMessage("SUP_M_LIST_LIBRARY"),
				"url" => "/bitrix/admin/sr_pack_list_library.php?lang=".LANG,
				"title" => GetMessage("SUP_M_SRPACK")
			),
            array(
				"text" => GetMessage("SUP_M_LIST_IMAGE"),
				"url" => "/bitrix/admin/sr_pack_list_image.php?lang=".LANG,
				"title" => GetMessage("SUP_M_SRPACK")
			),
		)
	);
	return $aMenu;
}
?>