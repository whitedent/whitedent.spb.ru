<?
$MESS["INDEX_SR_PACK_LIBRARY"]  = "Список галерей";
$MESS["PAGES"]                  = "Страница";
$MESS["ID"]                     = "ID";
$MESS["NAME"]                   = "Название";
$MESS["COUNT"]                  = "Количество фотографий";
$MESS["LIST"]                   = "Список медиабиблиотек";

$MESS["CREATE_WM"]              = "Создать Watermark";
$MESS["RELOAD_WM"]              = "Обновить Watermark";
$MESS["DELETE_WM"]              = "Удалить Watermark";

$MESS["VIEW"]                   = "Изображения";
$MESS["DELETE_LIBRARY"]         = "Удалить галерею";

$MESS["GROUP_DELETE"]           = "Удалить"
?>