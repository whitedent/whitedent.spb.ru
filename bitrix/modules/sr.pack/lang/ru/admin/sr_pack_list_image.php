<?
$MESS["INDEX_SR_PACK_IMAGES"]   = "Список изображений";

$MESS["ID"]                     = "ID";
$MESS["TIMESTAMP_X"]            = "Дата";
$MESS["FILE_NAME"]              = "Название файла";
$MESS["ORIGINAL_NAME"]          = "Оригинальное название файла";
$MESS["WATERMARK"]              = "Watermark";

$MESS["WIDTH"]                  = "Ширина, px";
$MESS["HEIGHT"]                 = "Высота, px";
$MESS["FILE_SIZE"]              = "Размер";
$MESS["CONTENT_TYPE"]           = "Тип";
$MESS["SUBDIR"]                 = "Папка";
$MESS["WATERMARK_LINK"]         = "Ссылка на изображение";

$MESS["VIEW_WM_IMAGE"]          = "Просмотреть изображение";
$MESS["NO_VIEW_WM_IMAGE"]       = "Нет изображения";

$MESS["CREATE_WM"]              = "Создать Watermark";
$MESS["RELOAD_WM"]              = "Обновить Watermark";
$MESS["DELETE_WM"]              = "Удалить Watermark";
$MESS["DELETE_IMAGE"]           = "Удалить изображение";
?>