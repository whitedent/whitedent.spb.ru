<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"MIN_VERSION" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("MIN_VERSION"),
			"TYPE" => "TEXTBOX",
			"DEFAULT" => "8",
		),
	),
);
?>