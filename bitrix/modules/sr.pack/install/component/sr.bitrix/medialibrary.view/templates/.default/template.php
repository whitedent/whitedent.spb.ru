<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if(count($arResult["ERRORS"]) > 0):?>
    
<?else:?>
    <?if(count($arResult["FOLDER_IMAGE"]) > 0):?>
    <link href="/resources/css-js/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="/resources/css-js/jquery.mousewheel-3.0.4.pack.js"></script>
   	<script type="text/javascript" src="/resources/css-js/jquery.fancybox-1.3.4.pack.js"></script>
    <script>
    $(document).ready(function(){
        $("a[rel=image_group]").fancybox({
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
            'titlePosition' 	: 'over',
            'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">Изображение ' + (currentIndex + 1) + ' / ' + currentArray.length + ($("#img_" + $(currentArray[currentIndex]).attr('id')).attr("title").length ? '&nbsp;- ' + $("#img_" + $(currentArray[currentIndex]).attr('id')).attr("title") : '') + '</span>';
            }
	   });
    });
    </script>
    <div class="photo-line">
        <?foreach($arResult["FOLDER_IMAGE"] as $img):?>
            <div>
                <a href="<?=$img["URL"]?>" rel="image_group" id="<?=$img["RAND_ID"]?>">
                    <img src="<?=$img["URL"]?>"<?=$img["SIZE"]?> id="img_<?=$img["RAND_ID"]?>" title="<?=$arResult["DESCRIPTION"][$img["ID"]];?>" alt="<?=$arResult["DESCRIPTION"][$img["ID"]];?>"/>
                </a>
            </div>
            <div class="photo-separator"></div>         
        <?endforeach;?>
    </div>
    <p>
    <?if($arParams["PAGE_LINK"] == ""):?>
        <a href="/gallery/<?=$arParams["FOLDERS"]?>/">Просмотреть все фото</a>
    <?else:?>
        <a href="<?=$arParams["PAGE_LINK"]?>"><?=$arParams["PAGE_LINK_TEXT"]?></a>
    <?endif;?>
    </p>
    <?endif;?>
    <?if($arParams["PAGE_NAV_MODE"] == "Y"):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
<?endif;?>