<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule("sr.pack");
if($this->StartResultCache($arParams["CACHE_TIME"], $arParams["IMAGE"]))
{
    if($arParams["IMAGE"] != 0 && is_numeric($arParams["IMAGE"]))
    {
        //Получаем информацию о фотке
        $img = CFile::GetByID($arParams["IMAGE"]);
        $img = $img->Fetch();
        //Объект класса API модуля
        $srMedialib = new SRMedialibrary();
        $srMedialib->setDB($DB);
        //Ресайз изображения
        if($arParams["RESIZE_MODE"] == "Y" && $arParams["RESIZE_MODE_W"] != "" && $arParams["RESIZE_MODE_H"] != "")
            $img["SIZE"] = $srMedialib->resizeImage($arParams["RESIZE_MODE_W"], $arParams["RESIZE_MODE_H"], $img);
        //Водные знаки
        if($arParams["WATERMARK_SETTING"] == "Y")
            $img["URL"] = $srMedialib->createWatermarkImageComponent($img);
        else
            $img["URL"] = "/upload/".$img["SUBDIR"]."/".$img["FILE_NAME"];
        //Для ID в HTML
        $img["RAND_ID"] = rand(00000, 99999);
        $img["RAND_ID"] = "link_".$img["RAND_ID"];
        //Информация по фотке
        $sql = "SELECT NAME, DESCRIPTION FROM b_medialib_item WHERE SOURCE_ID = '".$arParams["IMAGE"]."'";
        $result = $DB->Query($sql);
        if($result->result !== false)
            $desc = mysql_fetch_array($result->result, MYSQL_ASSOC);
        //Результат работы скрипта
        $arResult["IMAGE"] = array_merge($img, $desc);
    }
    $this->IncludeComponentTemplate();
}
?>