$(document).ready(function(){
    $("a.fancybox-image").fancybox({
        'transitionIn'		: 'none',
        'transitionOut'		: 'none',
        'titlePosition' 	: 'over',
        'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">' + $("#img_" + $(currentArray[currentIndex]).attr('id')).attr("title") + '</span>';
        }
    });
});