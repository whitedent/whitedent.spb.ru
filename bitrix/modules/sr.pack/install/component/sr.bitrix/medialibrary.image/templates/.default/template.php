<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if(count($arResult["ERRORS"]) > 0):?>
    
<?else:?>
    <?if(count($arResult["IMAGE"]) > 0):?>
        <?if($arParams["LOAD_JS"] == "Y"):?>
            <link href="/resources/css-js/style.css" type="text/css" rel="stylesheet" />
            <script type="text/javascript" src="/resources/css-js/jquery.mousewheel-3.0.4.pack.js"></script>
        	<script type="text/javascript" src="/resources/css-js/jquery.fancybox-1.3.4.pack.js"></script>
        <?endif;?>
        
    <span class="img-border<?=($arParams["DESIGN_STYLE_IMAGE"] != "" ? ' '.$arParams["DESIGN_STYLE_IMAGE"] : '')?>">
        <a href="<?=$arResult["IMAGE"]["URL"];?>"<?=($arParams["DESIGN_STYLE_A"] != "" ? ' class="'.$arParams["DESIGN_STYLE_A"].'"' : '')?> id="<?=$arResult["IMAGE"]["RAND_ID"]?>"><img src="<?=$arResult["IMAGE"]["URL"];?>" id="img_<?=$arResult["IMAGE"]["RAND_ID"]?>" title="<?=$arResult["IMAGE"]["NAME"]?>" border="0" alt="<?=$arResult["IMAGE"]["NAME"]?>" <?=($arParams["DESIGN_STYLE_IMAGE"] != "" ? ' class="'.$arParams["DESIGN_STYLE_IMAGE"].'"' : '')?><?=$arResult["IMAGE"]["SIZE"]?>/></a>
    </span>
    <?endif;?>
<?endif;?>