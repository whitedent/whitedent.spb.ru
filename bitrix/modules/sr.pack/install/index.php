<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-18);
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

class sr_pack extends CModule
{
    public $MODULE_ID = "sr.pack";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
    
    public function __construct()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = GetMessage("SRPACK_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("SRPACK_MODULE_DESC");
	}
    
    public function InstallDB()
    {
        /*CUrlRewriter::Add(
            array(
                "SITE_ID" => "s1",
				"CONDITION" => "#^/medialibrary/image([0-9]+).jpg#",
				"ID" => "",
				"PATH" => "/resources/watermark.php",
				"RULE" => "id=$1"
            )
        );*/
        RegisterModule("sr.pack");
        RegisterModuleDependences("iblock", 
                                  "OnIBlockPropertyBuildList", 
                                  "main", 
                                  "SRBitrix_CIBlockPropertyMediaLibrary", 
                                  "GetUserTypeDescription", 
                                  100, 
                                  "/modules/sr.pack/classes/ml_property.php");
    }
    
    public function UnInstallDB()
    {
        /*CUrlRewriter::Delete(
            array(
                "SITE_ID" => "s1",
				"CONDITION" => "#^/medialibrary/image([0-9]+).jpg#",
				"ID" => "",
				"PATH" => "/resources/watermark.php",
				"RULE" => "id=$1"
            )
        );*/
        UnRegisterModule("sr.pack");
        UnRegisterModuleDependences("iblock", 
                                    "OnIBlockPropertyBuildList", 
                                    "main", 
                                    "SRBitrix_CIBlockPropertyMediaLibrary", 
                                    "GetUserTypeDescription", 
                                    "/modules/sr.pack/classes/ml_property.php");
    }
    
    public function InstallEvents()
	{
		return true;
	}

	public function UnInstallEvents()
	{
		return true;
	}

	public function InstallFiles()
	{
	    CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sr.pack/install/admin', $_SERVER['DOCUMENT_ROOT']."/bitrix/admin");
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sr.pack/install/component', $_SERVER['DOCUMENT_ROOT']."/bitrix/components", true, true);
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sr.pack/install/resources', $_SERVER['DOCUMENT_ROOT']."/resources", true, true);
        return true;
	}

	public function UnInstallFiles()
	{
	    DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sr.pack/install/admin', $_SERVER['DOCUMENT_ROOT']."/bitrix/admin");
        DeleteDirFilesEx("/bitrix/components/sr.bitrix/");
        DeleteDirFilesEx("/resources/");
		return true;
	}

	public function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallDB();
        $this->InstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("SRPACK_INSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/sr.pack/install/step.php");
	}

	public function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallDB();
        $this->UnInstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("SRPACK_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/sr.pack/install/unstep.php");
	}
}

?>