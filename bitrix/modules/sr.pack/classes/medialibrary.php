<?
/**
 * Класс SRMedialibrary. Класс для работы с медиабиблиотеками Битрикса
 * @package sr.pack
 * @author Швыков Роман
 * @copyright 2011 Shvykov Roman
 */
class SRMedialibrary
{
    /**
     * Объект класса CDatabase
     * @var object
     */
    protected $_DB;
    
    /**
     * Установка объекта класса CDatabase для работы с базой данных
     * @param $DB CDatabase Объект класса CDatabase
     * @return void
     */
    public function setDB(CDatabase $DB)
    {
        $this->_DB = $DB;
    }
    
    /**
     * Проверка существования изображения с watermak'ом
     * @param $subdir string Директория
     * @param $name string Имя файла изображения
     * @return string При существовании изображения Y, в противном случае N
     */
    public function existsWatermarkImage($subdir, $name)
    {
        $path = $this->generationPath($subdir, $name);
        if(file_exists($_SERVER["DOCUMENT_ROOT"].$path))
            return "Y";
        else
            return "N";
    }
    
    /**
     * Проверка существования изображения с watermak'ом. Отличие от {@link SRMedialibrary::existsWatermarkImage() existsWatermarkImage()} заключается в возвращаемых значениях.
     * @param $subdir string Директория
     * @param $name string Имя файла изображения
     * @see SRMedialibrary::existsWatermarkImage()
     * @return mixed При существовании изображения возвращает путь до изображения, в противном случае null
     */
    public function getImageWatermark($subdir, $name)
    {
        $path = $this->generationPath($subdir, $name);
        if(file_exists($_SERVER["DOCUMENT_ROOT"].$path))
            return $path;
        else
            return null;
    }
    
    /**
     * Создание watermark'а по ID изображения. ID изображения из таблицы b_file.
     * @param $id int ID изображения
     * @return void
     */
    public function createWatermarkImage($id)
    {
        $res = $this->_DB->Query("SELECT * FROM b_file WHERE ID = '".$id."'");
        $res->NavStart();
        if($res->NavRecordCount > 0)
        {
            $arFile = $res->GetNext();
            $path = $this->getImageWatermark($arFile["SUBDIR"], $arFile["FILE_NAME"]);
            if(!is_null($path))
                $this->watermark($path, $arFile);
            else
            {
                $path = $this->generationPath($arFile["SUBDIR"], $arFile["FILE_NAME"]);
                $this->watermark($path, $arFile);
            }
        }
    }
    
    /**
     * Создание watermark'а по информации из таблицы b_file.
     * @param $arFile array Ассоциативный массив с данными по изображению из таблицы b_file
     * @return string
     */
    public function createWatermarkImageComponent(array $arFile)
    {
        if(count($arFile) > 0)
        {
            $path = $this->generationPath($arFile["SUBDIR"], $arFile["FILE_NAME"]);
            if($this->existsWatermarkImage($arFile["SUBDIR"], $arFile["FILE_NAME"]) == "N")
                $this->watermark($path, $arFile);
        }
        return $path;
    }
    
    /**
     * Действия над изображениями. Возможно обновление watermark'а, удаление, удаление оригинального изображения из базы данных и файловой структуры.
     * @param $id int ID изображения
     * @param $action string Действие которое необходимо произвести. Возможные варианты: reload - обновление watermark'а, delete - удаление watermark'а, idelete - удаление всего что связанно с этим изображением
     * @return void
     */
    public function actionWatermarkImage($id, $action = "reload")
    {
        $res = $this->_DB->Query("SELECT * FROM b_file WHERE ID = '".$id."'");
        $res->NavStart();
        if($res->NavRecordCount > 0)
        {
            $arFile = $res->GetNext();
            $path = $this->generationPath($arFile["SUBDIR"], $arFile["FILE_NAME"]);
            @unlink($_SERVER["DOCUMENT_ROOT"].$path);
            if($action == "reload")
                $this->createWatermarkImage($id);
            elseif($action == "delete")
                @rmdir($_SERVER["DOCUMENT_ROOT"].$this->dirImage($path));
            elseif($action == "idelete")
                $this->deleteImage($id);
        }
    }
    
    /**
     * Удаление изображения. 
     * Удаляется вся информация по изображению из таблиц базы данных: b_medialib_item, b_medialib_collection_item, b_file; а так же оригинал и watermark.
     * @param $id int ID изображения
     * @return void
     */
    public function deleteImage($id)
    {
        $sql_1 = "SELECT ID FROM b_medialib_item WHERE SOURCE_ID = '".$id."'";
        $sql_2 = "DELETE FROM b_medialib_collection_item WHERE ITEM_ID IN(".$sql_1.")";
        $sql_3 = "SELECT * FROM b_file WHERE ID = '".$id."'";
        $sql_4 = "DELETE FROM b_file WHERE ID = '".$id."'";
        
        $this->_DB->Query($sql_2);
        $res = $this->_DB->Query($sql_3);
        $res->NavStart();
        if($res->NavRecordCount > 0)
        {
            $arFile = $res->GetNext();
            $path = $this->generationPath($arFile["SUBDIR"], $arFile["FILE_NAME"]);
            @unlink($_SERVER["DOCUMENT_ROOT"].$path);
            @rmdir($_SERVER["DOCUMENT_ROOT"].$this->dirImage($path));
            @unlink($_SERVER["DOCUMENT_ROOT"]."/upload/".$arFile["SUBDIR"]."/".$arFile["FILE_NAME"]);
            @rmdir($_SERVER["DOCUMENT_ROOT"]."/upload/".$arFile["SUBDIR"]."/");
            $this->_DB->Query($sql_4);
        }
    }
    
    /**
     * Возвращает watermark
     * @return object imagecreatefrompng
     */
    public function getWatermark()
    {
        return imagecreatefrompng($_SERVER["DOCUMENT_ROOT"].'/resources/images/watermark.png');
    }
    
    /**
     * Метод возвращает новые размеры изображения.
     * Функция ресайза на лету.
     * @return string
     */
    public function resizeImage($resize_w, $resize_h, array $arFile)
    {
        $size = getimagesize($_SERVER["DOCUMENT_ROOT"]."/upload/".$arFile["SUBDIR"]."/".$arFile["FILE_NAME"]);        
        $width = $size[0];
        $height = $size[1];
        if($size[0]/$resize_w >= $size[1]/$resize_h)
            $nSize = ' height="'.$resize_h.'"';
        else
            $nSize = ' width="'.$resize_w.'"';
        return $nSize;
    }

    public function resizeImagePhysical($resize_w, $resize_h, array $arFile){
    	$arSize = array("width" => $resize_w, "height" => $resize_h);
    	return CFile::ResizeImageGet($arFile, $arSize, BX_RESIZE_IMAGE_PROPORTIONAL, true);
    }
    
    /**
     * Создание изображения с watermark'ом
     * @param $path string Путь до изображения
     * @param $arFile array Ассоциативный массив с данными по изображению из таблицы b_file
     * @return void
     */
    protected function watermark($path, $arFile)
    {
        $dir = $this->dirImage($path);
        $wm = new watermark();
        $watermark = $this->getWatermark();
        list($i, $type) = explode("/", $arFile["CONTENT_TYPE"]);
        if($type == "jpeg")
            $image = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"]."/upload/".$arFile["SUBDIR"]."/".$arFile["FILE_NAME"]);
        elseif($type == "png")
            $image = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"]."/upload/".$arFile["SUBDIR"]."/".$arFile["FILE_NAME"]);
        $image = $wm->create_watermark($image, $watermark, 100, "left-bottom");
        if(!file_exists($_SERVER["DOCUMENT_ROOT"].$dir."/"))
            @mkdir($_SERVER["DOCUMENT_ROOT"].$dir."/");
        if($type == "jpeg")
            imagejpeg($image, $_SERVER["DOCUMENT_ROOT"].$dir."/".$arFile["FILE_NAME"]);
        elseif($type == "png")
            imagepng($image, $_SERVER["DOCUMENT_ROOT"].$dir."/".$arFile["FILE_NAME"]);
        imagedestroy($image);
        imagedestroy($watermark);
    }
    
    /**
     * Метод генерирует путь до watermark изображения
     * @param $subdir string Директория
     * @param $name string Имя файла изображения
     * @return string
     */
    protected function generationPath($subdir, $name)
    {
        list($i, $folder) = explode("/", $subdir);
        $newSubdir = "medialibrary/".md5($folder);
        return "/resources/".$newSubdir."/".$name;
    }
    
    /**
     * Выводит путь до папки
     * @param $path string Путь до изображения
     * @return mixed Возвращает путь до папки или null если путь до изображения с ошибкой
     */
    protected function dirImage($path)
    {
        if($path)
        {
            $folder = explode("/", $path);
            $count = count($folder);
            $path = "";
            for($i = 0; $i <= $count - 2; $i++)
                $path .= "/".$folder[$i];
            return $path;
        }
        else
            return null;
    }
}
?>