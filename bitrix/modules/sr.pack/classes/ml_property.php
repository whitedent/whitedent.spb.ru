<?
/**
 * @package default
 * @author SR (Швыков Роман roman_shvykov@mail.ru)
 * @version 1, 05.10.2011
 */
if (!class_exists("SRBitrix_CIBlockPropertyMediaLibrary"))
{
    /**
    * Класс для создания пользовательского типа свойства - привязка к медиабиблиотеке
    */
    class SRBitrix_CIBlockPropertyMediaLibrary
    {
        function GetUserTypeDescription()
        {
            return array(
                'PROPERTY_TYPE'         => 'S',
                'USER_TYPE'             => 'SRBitrix_MediaLibrary',
                'DESCRIPTION'           => 'Привязка к медибиблиотеке',
                'GetPropertyFieldHtml'  => array('SRBitrix_CIBlockPropertyMediaLibrary','GetPropertyFieldHtml'),
                'ConvertToDB'           => array('SRBitrix_CIBlockPropertyMediaLibrary','ConvertToDB'),
                'ConvertFromDB'         => array('SRBitrix_CIBlockPropertyMediaLibrary','ConvertFromDB')
            );
        }
        
                
        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
        {
            global $DB;
            $sql = "SELECT ID, NAME FROM b_medialib_collection WHERE ID > 0 AND ACTIVE = 'Y' AND ML_TYPE = '1'";
            $res = $DB->Query($sql);
            $arFolders[] = '<option value="0">[0] Выбор медиабиблиотеки</option>';
            if(mysql_num_rows($res->result) > 0)
            {
                while($media = mysql_fetch_array($res->result, MYSQL_ASSOC))
                {
                    $tmp = "";
                    if($media["ID"] == $value['VALUE'])
                        $tmp = ' selected="selected"';
                    $arFolders[] = '<option value="'.$media["ID"].'"'.$tmp.'>['.$media["ID"].'] '.$media["NAME"].'</option>';
                }
            }
            $return = '<select name="'.$strHTMLControlName['VALUE'].'" id="'.$strHTMLControlName['VALUE'].'">';
            foreach($arFolders as $option)
                $return .= $option;
            $return .= '</select>';
            return  $return;
        }
        
        function ConvertToDB($arProperty, $value)
        {
            $return = array();
            if(is_numeric($value['VALUE']))
                $return['VALUE'] = $value['VALUE'];
            else
                $return['VALUE'] = 0;
            return $return;
        }
        
        function ConvertFromDB($arProperty, $value)
        {
            $return = array();
            if(is_numeric($value['VALUE']))
                $return['VALUE'] = $value['VALUE'];
            else
                $return['VALUE'] = 0;
            return $return;
        }        
    }
}
?>