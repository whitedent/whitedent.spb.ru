<?
$MESS['AUTH_FORGOT_PASSWORD_1'] = 'Jeigu Jыs pamirрote slaptaюodб, бraрykite savo naudotojo vardа arba el. paрto adresа. <br>Elektroniniu paрtu Jums bus iрsiшsta nuoroda, kuriа turлsite paspausti. Paspaudж nuorodа, lukterkite – kitu  elektroniniu paрtu Jums bus iрsiшstas slaptaюodis.';
$MESS['AUTH_SEND_PASSWORD'] = 'Siшsti';
$MESS['AUTH_LOGIN'] = 'Naudotojo vardas';
$MESS['AUTH_PASSWORD'] = 'Slaptaюodis';
$MESS['AUTH_OR'] = 'arba';
$MESS['AUTH_EMAIL'] = 'El. paрtas';
$MESS['AUTH_AUTH'] = 'Autorizacija';
$MESS['AUTH_REGISTER_NEW_USER'] = 'Naujo naudotojo registracija';
$MESS['AUTH_NAME'] = 'Vardas';
$MESS['AUTH_LAST_NAME'] = 'Pavardл';
$MESS['AUTH_LOGIN_MIN'] = 'Naudotojo vardas (3 ar daugiau simboliш)';
$MESS['AUTH_PASSWORD_MIN'] = 'Slaptaюodis (6 ar daugiau simboliш)';
$MESS['AUTH_CONFIRM'] = 'Pakartokite slaptaюodб';
$MESS['AUTH_REGISTER'] = 'Registruotis';
$MESS['AUTH_REQ'] = 'Privalomi laukai';
$MESS['AUTH_PLEASE_AUTH'] = 'Autorizacija';
$MESS['AUTH_AUTHORIZE'] = 'Prisijungti';
$MESS['AUTH_FORGOT_PASSWORD_2'] = 'Pamirрote slaptaюodб?';
$MESS['AUTH_GO'] = 'Naudokitлs';
$MESS['AUTH_GO_AUTH_FORM'] = 'slaptaюodюio priminimo funkcija.';
$MESS['AUTH_REGISTERATION'] = 'Registracija';
$MESS['AUTH_FIRST_ONE'] = 'Jeigu esate naujas naudotojas, uюpildykite';
$MESS['AUTH_REG_FORM'] = 'registracijos formа';
$MESS['AUTH_MESS_1'] = 'Gavж nuorodа, paspauskite jа. Atsivers ';
$MESS['AUTH_CHANGE_FORM'] = ' slaptaюodюio keitimo forma.';
$MESS['AUTH_CHANGE_PASSWORD'] = 'Slaptaюodюio keitimas';
$MESS['AUTH_CHANGE'] = 'Pakeisti slaptaюodб';
$MESS['AUTH_CHECKWORD'] = 'Patikros nuoroda';
$MESS['AUTH_NEW_PASSWORD'] = 'Naujas slaptaюodis';
$MESS['AUTH_NEW_PASSWORD_CONFIRM'] = 'Slaptaюodюio patikrinimas';
$MESS['AUTH_GET_CHECK_STRING'] = 'Iрsiшsti patikros nuorodа';
$MESS['AUTH_SEND'] = 'Iрsiшsti';
$MESS['AUTH_IF_REGISTERED'] = 'Jei esate registruotas sistemos naudotojas, prisijunkite';
$MESS['AUTH_FORGOT_PASSWORD'] = 'Pamirрote slaptaюodб arba norite jб pakeisti?';
$MESS['AUTH_FILL_AUTH_1'] = 'Uюpildykite slaptaюodюio uюklausimo formа. Elektroniniu paрtu Jums bus iрsiшsta patikros nuoroda. Spauskite jа, atsivers ';
$MESS['AUTH_FILL_AUTH_2'] = 'slaptaюodюio keitimo forma';
$MESS['AUTH_FOR_CONTROL'] = 'Norлdami redaguoti savo asmeninж informacijа, turite tapti registruotu sistemos naudotoju,. Praрome registruotis. ';
$MESS['AUTH_GENERATE'] = 'Sukurti naudotojo vardа ir slaptaюodб';
$MESS['AUTH_SEND_LOGIN'] = 'Naudotojo vardas ir patikros nuoroda bus sukurti automatiрkai bei iрsiшsti nurodytu el. paрto adresu.';
$MESS['AUTH_FILL_REGISTER_FORM'] = 'Pildyti registracijos formа';
$MESS['AUTH_YOU_CAN_FILL_REGISTER'] = 'Jыs galite uюpildyti registracijos formа nurodydami savo asmeninius duomenis, naudotojo vardа bei slaptaюodб.';
$MESS['AUTH_REMEMBER_ME'] = 'Atsiminti prisijungimo duomenis рiame kompiuteryje';
$MESS['AUTH_GENERATE_BTN'] = 'Sukurti';
$MESS['AUTH_LOGIN_BUTTON'] = 'Prisijungti';
$MESS['AUTH_LANG'] = 'Tinklapis';
?>