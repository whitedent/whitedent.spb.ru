<?
$MESS ['CATI_NO_DATA_FILE'] = "No data file has been loaded or selected. Cannot import.";
$MESS ['CATI_NO_IBLOCK'] = "No information block has been selected. Cannot export.";
$MESS ['CATI_NO_FILE_FORMAT'] = "Please set the data file format and properties.";
$MESS ['CATI_NO_DELIMITER'] = "Please set the field delimiter.";
$MESS ['CATI_NO_METKI'] = "Please specify the delimiter marks which will define the field boundaries.";
$MESS ['CATI_NO_DATA'] = "The data file does not contain data. Cannot import.";
$MESS ['CATI_SCHEME_EXISTS'] = "A scheme with the same name already exists.";
$MESS ['CATI_NO_FIELDS'] = "Please establish the field correspondence.";
$MESS ['CATI_NOMAME'] = "<дБиБХЄЧиН>";
$MESS ['CATI_LINE_NO'] = "єГГ·Сґ";
$MESS ['CATI_NOIDNAME'] = "Cannot identify product: the name and the unique code are not specified.";
$MESS ['CATI_ERROR_LOADING'] = "Error importing product:";
$MESS ['CATI_TOTAL_ERRS'] = "Total erroneous records:";
$MESS ['CATI_TOTAL_COR1'] = "Correctly imported";
$MESS ['CATI_TOTAL_COR2'] = "records.";
$MESS ['CATI_PAGE_TITLE'] = "Catalog import: step ";
$MESS ['CATI_NEXT_STEP'] = "µиНд»";
$MESS ['CATI_DATA_LOADING'] = "ЎУЕС§вЛЕґўйНБЩЕ";
$MESS ['CATI_DATA_FILE'] = "дїЕмўйНБЩЕ:";
$MESS ['CATI_OR_DATA_FILE'] = "ЛГЧНдїЕмЁТЎаЗзєд«µм:";
$MESS ['CATI_INFOBLOCK'] = "ѕЧй№·Хи»ГРЄТКСБѕС№ём:";
$MESS ['CATI_LOADSCHEME'] = "Import scheme:";
$MESS ['CATI_NOT'] = "дБигЄи";
$MESS ['CATI_DEL'] = "Еє";
$MESS ['CATI_CHOOSE_APPR_FORMAT'] = "Please choose the appropriate data format and set its properties";
$MESS ['CATI_RAZDELITEL'] = "delimited (fields are denoted with a special symbol)";
$MESS ['CATI_FIXED'] = "fixed-width fields";
$MESS ['CATI_RAZDEL1'] = "Delimited";
$MESS ['CATI_RAZDEL_TYPE'] = "Field delimiter";
$MESS ['CATI_TAB'] = "tab";
$MESS ['CATI_TZP'] = "semicolon";
$MESS ['CATI_ZPT'] = "comma";
$MESS ['CATI_SPS'] = "space";
$MESS ['CATI_OTR'] = "НЧи№ж";
$MESS ['CATI_FIRST_NAMES'] = "First row is a header (contains field names)";
$MESS ['CATI_FIX1'] = "Fixed-width fields";
$MESS ['CATI_FIX_MET'] = "Delimiters";
$MESS ['CATI_FIX_MET_DESCR'] = "Enter numbers of field separator columns, one per line";
$MESS ['CATI_DATA_SAMPLES'] = "µСЗНВиТ§ўйНБЩЕ";
$MESS ['CATI_FIELDS_SOOT'] = "Please assign file fields to database fields";
$MESS ['CATI_FI_NAME'] = "ЄЧиН";
$MESS ['CATI_FI_ACTIV'] = "Active";
$MESS ['CATI_FI_ACTIVFROM'] = "Active from";
$MESS ['CATI_FI_ACTIVTO'] = "Active till";
$MESS ['CATI_FI_CATIMG'] = "ГТВЎТГГЩ»АТѕ";
$MESS ['CATI_FI_CATDESCR'] = "ГТВЎТГГТВЕРаНХВґ";
$MESS ['CATI_FI_DETIMG'] = "ГЩ»АТѕ";
$MESS ['CATI_FI_DETDESCR'] = "ГТВЕРаНХВґ";
$MESS ['CATI_FI_UNIXML'] = "Unique ID";
$MESS ['CATI_FI_QUANT'] = "»ГФБТі";
$MESS ['CATI_FI_WEIGHT'] = "№йУЛ№СЎ";
$MESS ['CATI_FI_PROPS'] = "¤ШіКБєСµФ";
$MESS ['CATI_FI_GROUP_LEV'] = "Group of level";
$MESS ['CATI_FI_PRICE_TYPE'] = "»ГРаА·ўН§ГТ¤Т";
$MESS ['CATI_FIELD'] = "Field";
$MESS ['CATI_ADDIT_SETTINGS'] = "Additional settings";
$MESS ['CATI_IMG_PATH'] = "Path ўН§ГЩ»АТѕ";
$MESS ['CATI_IMG_PATH_DESCR'] = "The full path will be built relative to the site root, in the form \"&lt;path to images&gt;/&lt;file name&gt;\"";
$MESS ['CATI_OUTFILE'] = "Existing items that was not found in the file";
$MESS ['CATI_OF_DEACT'] = "deactivate";
$MESS ['CATI_OF_DEL'] = "Еє";
$MESS ['CATI_OF_KEEP'] = "аЎзєдЗйа»з№";
$MESS ['CATI_SAVE_SCHEME_AS'] = "Save settings as scheme";
$MESS ['CATI_SUCCESS'] = "№УаўйТўйНБЩЕКБєЩГімбЕйЗ";
$MESS ['CATI_SU_ALL'] = "Total rows processed:";
$MESS ['CATI_SU_CORR'] = "Absolutely valid:";
$MESS ['CATI_SU_ER'] = "Contain errors:";
$MESS ['CATI_BACK'] = "ЎЕСє";
$MESS ['CATI_NEXT_STEP_F'] = "вЛЕґўйНБЩЕ";
$MESS ['CATI_INFOBLOCK_SELECT'] = "- аЕЧНЎ -";
$MESS ['CATI_SU_KILLED'] = "Obsolete items deleted:";
$MESS ['CATI_SU_HIDED'] = "Obsolete items deactivated:";
$MESS ['CATI_AUTO_REFRESH'] = "If the page was not refreshed automatically, click the link";
$MESS ['CATI_AUTO_REFRESH_STEP'] = "ўСй№µН№µиНд»";
$MESS ['CATI_DEL_LOAD_SCHEME'] = "Delete import scheme?";
$MESS ['CATI_INACTIVE_PRODS'] = "Existing inactive items/groups which are present in the file";
$MESS ['CATI_KEEP_AS_IS'] = "leave as is";
$MESS ['CATI_ACTIVATE_PROD'] = "activate";
$MESS ['CATI_AUTO_STEP_TIME'] = "Step execution time";
$MESS ['CATI_AUTO_STEP_TIME_NOTE'] = "0 - import all at once<br>any positive value: duration of an execution step, in seconds";
$MESS ['CATI_AUTO_REFRESH_CONTINUE'] = "Performing stepwise import...";
$MESS ['ICI_NOT_CSV'] = "The file is not a CSV file";
$MESS ['ICI_TAB1'] = "дїЕмўйНБЩЕ";
$MESS ['ICI_TAB1_ALT'] = "Select data file and information block for import";
$MESS ['ICI_TAB2'] = "Format";
$MESS ['ICI_TAB2_ALT'] = "Select data file format";
$MESS ['ICI_TAB3'] = "Fields";
$MESS ['ICI_TAB3_ALT'] = "Assign fields in data file to database fields";
$MESS ['ICI_TAB4'] = "јЕ";
$MESS ['ICI_TAB4_ALT'] = "јЕўН§ЎТГ№УаўйТўйНБЩЕ";
$MESS ['ICI_OPEN'] = "а»Фґ";
$MESS ['ICI_2_1_STEP'] = "ЎЕСєд»ВС§ўСй№µН№бГЎ";
$MESS ['CATI_FI_SORT'] = "Sort index";
$MESS ['CATI_FI_CATDESCRTYPE'] = "Description type for list views";
$MESS ['CATI_FI_DETDESCRTYPE'] = "»ГРаА·ўН§ГТВЕРаНХВґ";
$MESS ['CATI_FI_CODE'] = "ГЛСКаѕЧиНгЄйЁУ";
$MESS ['CATI_FI_ID'] = "Primary key";
$MESS ['CATI_FG_NAME'] = "ЄЧиНЎЕШиБ";
$MESS ['CATI_FG_UNIXML'] = "Unique ID";
$MESS ['CATI_FG_ACTIV'] = "Active";
$MESS ['CATI_FG_SORT'] = "Sort index";
$MESS ['CATI_FG_DESCR'] = "ГТВЕРаНХВґ";
$MESS ['CATI_FG_DESCRTYPE'] = "»ГРаА·ўН§ГТВЕРаНХВґ";
$MESS ['CATI_FG_CODE'] = "ГЛСКаѕЧиНгЄйЁУ";
?>