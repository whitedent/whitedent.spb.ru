<?
$MESS['IBLOCK_BAD_SECTION'] = 'Neбraрytas skyriaus pavadinimas.';
$MESS['IBLOCK_BAD_BLOCK_TYPE_ID'] = 'Neteisingas bloko tipas.';
$MESS['IBLOCK_BAD_BLOCK_ID'] = 'Neteisingas informacinio bloko kodas.';
$MESS['IBLOCK_BAD_IBLOCK'] = 'Informacinis blokas nerastas arba trыksta prieigos teisiш.';
$MESS['IBLOCK_BAD_ELEMENT'] = 'Elementas nerastas arba trыksta prieigos teisiш.';
$MESS['IBLOCK_BAD_BLOCK_SECTION_ID_PARENT'] = 'Skyriaus bloko kodas nesutampa su bloko virр-skyriaus kodu!';
$MESS['IBLOCK_BAD_BLOCK_SECTION_PARENT'] = 'Neteisingas virр-skyrius!';
$MESS['IBLOCK_BAD_SECTION_NAME'] = 'Neбvestas pavadinimas.';
$MESS['IBLOCK_BAD_BLOC'] = 'Neбvestas bloko tipas';
$MESS['IBLOCK_BAD_ELEMENT_NAME'] = 'Neбvestas pavadinimas.';
$MESS['IBLOCK_BAD_ACTIVE_FROM'] = 'Neteisingas aktyvacijos pradюios datos formatas.';
$MESS['IBLOCK_BAD_ACTIVE_TO'] = 'Neteisingas aktyvacijos pabaigos datos formatas.';
$MESS['IBLOCK_BAD_BLOCK_SECTION_RECURSE'] = 'Neбmanoma perkelti skyriш б save.';
$MESS['IBLOCK_BAD_FIELD'] = 'Privalomas laukas \"#FIELD_NAME#\" neuюpildytas.';
$MESS['IBLOCK_BAD_PROPERTY'] = 'Privaloma savybл \"#PROPERTY#\" neuюpildyta.';
$MESS['IBLOCK_EDIT_TITLE'] = 'Redaguoti';
$MESS['IBLOCK_NEW_TITLE'] = 'Pridлti';
$MESS['IBLOCK_TITLE'] = 'Informaciniai blokai';
$MESS['IBLOCK_BACK_TO_ADMIN'] = 'Grбюti б informaciniш blokш sаraра';
$MESS['IBLOCK_LAST_UPDATE'] = 'Paskutinis atnaujinimas:';
$MESS['IBLOCK_ACTIVE'] = 'Бraрas aktyvus:';
$MESS['IBLOCK_ACTIVE_PERIOD'] = 'Aktyvumo periodas:';
$MESS['IBLOCK_ACTIVE_PERIOD_FROM'] = 'from';
$MESS['IBLOCK_ACTIVE_PERIOD_TO'] = 'iki';
$MESS['IBLOCK_ELEMENT_PREVIEW'] = 'Trumpas apraрymas:';
$MESS['IBLOCK_ELEMENT_DETAIL'] = 'Pilnas apraрymas:';
$MESS['IBLOCK_PICTURE'] = 'Nuotrauka:';
$MESS['IBLOCK_ACCESS_DENIED_STATUS'] = 'Jыs neturite teisiш redaguoti рio бraрo esamam statuse';
$MESS['IBLOCK_PROP_DETAIL'] = '(detaliam redagavimui paspauskyte mygtukа prie sаvybes):';
$MESS['IBLOCK_LID'] = 'Svetainлs:';
$MESS['IBLOCK_NAME'] = 'Pavadinimas:';
$MESS['IBLOCK_SORT'] = 'Rырiavimo indeksas:';
$MESS['IBLOCK_CONTENT'] = 'Aukриiausias lygis';
$MESS['IBLOCK_SECTION'] = 'Skyriai:';
$MESS['IBLOCK_PARENT_SECTION'] = 'Virр-skyrius:';
$MESS['IBLOCK_DESC_TYPE'] = 'Apraрymo tipas:';
$MESS['IBLOCK_DESC_TYPE_TEXT'] = 'tekstas';
$MESS['IBLOCK_DESC_TYPE_HTML'] = 'HTML';
$MESS['IBLOCK_DESCRIPTION'] = 'Apraрymas:';
$MESS['IBLOCK_PERMISSIONS'] = 'Prieigos teisлs:';
$MESS['IBLOCK_PROP_ACTIVE'] = 'Aktyvus';
$MESS['IBLOCK_PROP_DEF_VALUE'] = 'Reikрmл pagal nutylлjimа';
$MESS['IBLOCK_PROP_ROWS'] = 'Eiluиiш';
$MESS['IBLOCK_PROP_COLS'] = 'Stulpeliш';
$MESS['IBLOCK_ACTION'] = 'Veiksmai';
$MESS['IBLOCK_ADD'] = 'Pridлti >>';
$MESS['IBLOCK_RESET'] = 'Iрvalyti';
$MESS['IBLOCK_SAVE_ERROR'] = 'Klaida iрsaugojant бraра #';
$MESS['IBLOCK_DELETE_ERROR'] = 'Рalinimo klaida. Objektas gali bыti naudojamas.';
$MESS['IBLOCK_TYPE_ADMIN_TITLE'] = 'Informaciniш blokш valdymas';
$MESS['IBLOCK_ACCESS_DENIED'] = 'Prieiga uюdrausta!';
$MESS['IBLOCK_F_FILTER'] = 'Filtras';
$MESS['IBLOCK_F_LANG'] = 'Svetainлs:';
$MESS['IBLOCK_F_ACTIVE'] = 'Aktyvus:';
$MESS['IBLOCK_F_SECTION'] = 'Skyrius:';
$MESS['IBLOCK_F_NAME'] = 'Pavadinimas:';
$MESS['IBLOCK_F_SUBMIT'] = 'Бdiegti';
$MESS['IBLOCK_F_DEL'] = 'Iрvalyti';
$MESS['IBLOCK_SAVE'] = 'Iрsaugoti pakeitimus';
$MESS['IBLOCK_DONT_SAVE'] = 'Neiрsaugoti';
$MESS['IBLOCK_ALL'] = '(bet koks)';
$MESS['IBLOCK_YES'] = 'Taip';
$MESS['IBLOCK_NO'] = 'Ne';
$MESS['IBLOCK_APPLY'] = 'Pritaikyti';
$MESS['IBLOCK_ADMIN_TIMESTAMP'] = 'Redaguota';
$MESS['IBLOCK_ADMIN_ACTIVE'] = 'Aktyvus';
$MESS['IBLOCK_ADMIN_LANG'] = 'Tinklapis';
$MESS['IBLOCK_ADMIN_SORT'] = 'Rырiavimas';
$MESS['IBLOCK_ADMIN_NAME'] = 'Pavadinimas';
$MESS['IBLOCK_SECTION_ADD'] = 'Pridлti >>';
$MESS['IBLOCK_SECTION_ADMIN_TIMESTAMP'] = 'Redaguota';
$MESS['IBLOCK_SECTION_ADMIN_ACTIVE'] = 'Aktyvus';
$MESS['IBLOCK_SECTION_ADMIN_SORT'] = 'Rырiavimas';
$MESS['IBLOCK_SECTION_ADMIN_NAME'] = 'Pavadinimas';
$MESS['IBLOCK_SECTION_ADMIN_CHANGE'] = 'Redaguoti';
$MESS['IBLOCK_ELEMENT_ADD'] = 'Pridлti >>';
$MESS['IBLOCK_ELEMENT_ADMIN_TIMESTAMP'] = 'Atnaujinta';
$MESS['IBLOCK_MODIFIED_BY'] = 'Kas redagavo';
$MESS['IBLOCK_LOCK_STATUS'] = 'Ind.';
$MESS['IBLOCK_ELEMENT_ADMIN_ACTIVE'] = 'Aktyvus';
$MESS['IBLOCK_ELEMENT_ADMIN_SORT'] = 'Rырiavimas';
$MESS['IBLOCK_ELEMENT_ADMIN_NAME'] = 'Pavadinimas';
$MESS['IBLOCK_ELEMENT_ADMIN_CHANGE'] = 'Redaguoti';
$MESS['IBLOCK_CHANGE_ALT'] = 'Keisti parametrus';
$MESS['IBLOCK_CHANGE'] = 'Redaguoti';
$MESS['IBLOCK_HISTORY'] = 'Istorija';
$MESS['IBLOCK_HISTORY_ALT'] = 'Pakeitimш istorija';
$MESS['IBLOCK_DELETE_ALT'] = 'Рalinti бraра';
$MESS['IBLOCK_DELETE'] = 'Iрtrinti';
$MESS['IBLOCK_CONFIRM_DEL_MESSAGE'] = 'Bus paрalinta visa informacija susijusi su рiuo бraрu. Tжsti?';
$MESS['IBLOCK_CODE'] = 'Mnemoninis kodas:';
$MESS['IBLOCK_TAGS'] = 'Юymлs:';
$MESS['MENU_SEPARATOR'] = 'Informaciniai blokai';
$MESS['IBLOCK_USE_HTMLEDIT'] = 'Naudoti vizualш HTML redaktoriш';
$MESS['IBLOCK_TYPE_DELETE_ERROR'] = 'Klaida рalinant tipа';
$MESS['IBLOCK_TYPE_SAVE_ERROR'] = 'Klaida iрsaugojant tipа';
$MESS['IBLOCK_TYPE_SAVE_NEW_ERROR'] = 'Klaida iрsaugojant naujа tipа';
$MESS['IBLOCK_TYPE_CODE'] = 'Kodas';
$MESS['IBLOCK_TYPE_CONTAIN_SECTION'] = 'Sudлtyje yra skyriai';
$MESS['IBLOCK_TYPE_DELETE'] = 'Iрtrinti';
$MESS['IBLOCK_TYPE_NEW_CODE'] = 'Naujas kodas';
$MESS['IBLOCK_TYPE_ELEMENTS'] = 'Elementai';
$MESS['IBLOCK_TYPE_ELEMENT'] = 'Elementas';
$MESS['IBLOCK_TYPE_SECTIONS'] = 'Skirsniai';
$MESS['IBLOCK_TYPE_SECTION'] = 'Skyrius';
$MESS['IBLOCK_TYPE_NAME'] = 'Pavadinimas';
$MESS['IBLOCK_TYPE_LANG'] = 'Kalba';
$MESS['IBLOCK_PROPERTIES'] = 'Savybлs';
$MESS['IBLOCK_TYPE_BAD_ID'] = 'Neбvestas informacinio bloko tipo ID';
$MESS['IBLOCK_TYPE_DUBL_ID'] = 'Informacinio bloko tipo ID dubliavimas.';
$MESS['IBLOCK_TYPE_ID_HAS_WRONG_CHARS'] = 'Informacinio bloko tipo ID gali sudaryti tik lotiniрkos raidлs, skaiиiai ir pabraukimo simbolis.';
$MESS['IBLOCK_TYPE_BAD_NAME'] = 'Neбvestas pavadinimas kalbai';
$MESS['IBLOCK_TYPE_BAD_ELEMENT_NAME'] = 'Neбvestas elementш pavadinimas kalbai';
$MESS['IBLOCK_WF_STATUS'] = 'Padлtis:';
$MESS['IBLOCK_STATUS'] = 'Statusas';
$MESS['IBLOCK_CURRENT_STATUS'] = 'Dabartinis statusas:';
$MESS['IBLOCK_ORIGINAL'] = 'originalas';
$MESS['IBLOCK_ORIGINAL_ALT'] = 'Originali versija';
$MESS['IBLOCK_NEW'] = 'nauja';
$MESS['IBLOCK_GREEN_ALT'] = 'prieinama modifikavimui';
$MESS['IBLOCK_YELLOW_ALT'] = 'uюblokuota Jumis (nepamirрkyte atblokuoti)';
$MESS['IBLOCK_RED_ALT'] = 'laikinai uюblokuota (рiuo momentu redaguojama)';
$MESS['IBLOCK_UNLOCK'] = 'atblokuoti';
$MESS['IBLOCK_UNLOCK_ALT'] = 'Atblokuoti бraра';
$MESS['IBLOCK_UNLOCK_CONFIRM'] = 'Ar tikrai norite atblokuoti бraра?';
$MESS['IBLOCK_DATE_LOCK'] = 'Uюblokuota:';
$MESS['IBLOCK_DOCUMENT_LOCKED'] = 'Бraрas laikinai uюblokuotas naudotoju # #ID# (#DATE#)';
$MESS['IBLOCK_CONFIRM_DEL_HISTORY'] = 'Ar tikrai norite paрalinti рб бraра?';
$MESS['IBLOCK_DELETE_HISTORY_ALT'] = 'Рalinti paskutinius pakeitimus';
$MESS['IBLOCK_NEW_RECORD'] = 'dar nebuvo publikuojama';
$MESS['IBLOCK_COMMENTS'] = 'Komentarai';
$MESS['IBLOCK_F_ID'] = 'ID (atskirta kableliais):';
$MESS['IBLOCK_F_TIMESTAMP'] = 'Atnaujinta';
$MESS['IBLOCK_F_MODIFIED_BY'] = 'Kas redagavo:';
$MESS['IBLOCK_F_STATUS'] = 'Padлtis:';
$MESS['IBLOCK_F_SET_FILTER'] = 'Filtras';
$MESS['IBLOCK_F_DEL_FILTER'] = 'Atрaukti';
$MESS['IBLOCK_FILTER_ON'] = '[filtras бjungtas]';
$MESS['IBLOCK_FILTER_OFF'] = '[filtras iрjungtas]';
$MESS['IBLOCK_HISTORY_PAGES'] = 'Бraрш';
$MESS['IBLOCK_VIEW_HISTORY_ALT'] = 'Бraрo perюiыra';
$MESS['IBLOCK_INCORRECT_ELEMENT_ID'] = 'Neteisingas бraрo ID';
$MESS['IBLOCK_HISTORY_PAGE_TITLE'] = 'Pakeitimш istorija бraрo # #ID#';
$MESS['IBLOCK_VIEW_HISTORY'] = 'Perюiыra';
$MESS['IBLOCK_BACK'] = 'Grбюti';
$MESS['IBLOCK_RECORD_ID'] = 'ID:';
$MESS['IBLOCK_RECORD_NAME'] = 'Pavadinimas:';
$MESS['IBLOCK_RECORD_TIMESTAMP'] = 'Atnaujinimo data:';
$MESS['IBLOCK_RECORD_LOCK'] = 'Uюblokuota:';
$MESS['IBLOCK_RECORD_STATUS'] = 'Padлtis:';
$MESS['IBLOCK_RECORD_CURRENT_SETTINGS'] = 'Esami бraрo parametrai';
$MESS['IBLOCK_HISTORY_CHANGES'] = 'Pakeitimш istorija';
$MESS['IBLOCK_RECORD_ORIGINAL'] = 'originalas';
$MESS['IBLOCK_HISTORY_VIEW_PAGE_TITLE'] = 'Бraрo perюiыra # #ID#';
$MESS['IBLOCK_INCORRECT_HISTORY_ID'] = 'Neteisingas бraрo ID';
$MESS['IBLOCK_F_NEW'] = 'Dar nepublikuota:';
$MESS['IBLOCK_F_LOCK_STATUS'] = 'Prieigos indikatorius:';
$MESS['IBLOCK_RED'] = 'Raudona';
$MESS['IBLOCK_YELLOW'] = 'Geltona';
$MESS['IBLOCK_GREEN'] = 'Юalia';
$MESS['IBLOCK_F_DATE_MODIFY'] = 'Pakeista';
$MESS['IBLOCK_CREATED'] = 'Sukurta:';
$MESS['IBLOCK_TOTAL'] = 'Viso';
$MESS['IBLOCK_WRONG_WF_STATUS'] = 'Neteisingas statusas';
$MESS['IBLOCK_PANEL_ADD_ELEMENT'] = 'Pridлti informacinio bloko elementа (tipas - #IBLOCK_TYPE#)';
$MESS['IBLOCK_PANEL_EDIT_ELEMENT'] = 'Redaguoti informacinio bloko elementа (tipas - #IBLOCK_TYPE#)';
$MESS['IBLOCK_PANEL_EDIT_IBLOCK'] = 'Redaguoti informacinб blokа (tipas - #IBLOCK_TYPE#)';
$MESS['IBLOCK_PANEL_HISTORY'] = 'Informacinio bloko elemento istorija (tipas - #IBLOCK_TYPE#)';
$MESS['IBLOCK_PANEL_EDIT_SECTION'] = 'Redaguoti informacinio bloko skyriш (tipas - #IBLOCK_TYPE#)';
$MESS['IBLOCK_PANEL_ADD_SECTION'] = 'Pridлti informacinio bloko skyriш (tipas - #IBLOCK_TYPE#)';
$MESS['MAIN_RESTORE_DEFAULTS'] = 'Pagal nutylлjimа';
$MESS['IBLOCK_PATH2RSS'] = 'Kelias б eksportuojamus RSS failus';
$MESS['IBLOCK_TCATALOG'] = 'Komercinis katalogas';
$MESS['IBLOCK_ELEMENT_PROP_NO'] = '(nenustatyta)';
$MESS['IBLOCK_ELEMENT_PROP_NA'] = '(nenustatyta)';
$MESS['IBLOCK_ELEMENT_EDIT_VIEW'] = 'Perюiыra';
$MESS['IBLOCK_CODE_FIRST_LETTER'] = 'Sаvybлs kodas negali prasidлti nuo skaiиiaus.';
$MESS['IBLOCK_OPTION_USE_RSS'] = 'Naudoti eksportа б RSS:';
$MESS['IBLOCK_EXTERNAL_CODE'] = 'Iрorinis kodas:';
$MESS['IBLOCK_FILTER_FROMTO_ID'] = 'ID (pradinis ir galutinis):';
$MESS['IBLOCK_VALUE_ANY'] = '(bet koks)';
$MESS['IBLOCK_INCLUDING_SUBSECTIONS'] = 'бskaitant poskyrius';
$MESS['IBLOCK_TYPE_DELETE1'] = 'Рalin.';
$MESS['IBLOCK_MARK_FOR_DELETE'] = 'Paюymлti рalinimui';
$MESS['IBLOCK_GROUP_ACTIVITY'] = 'Grupinis aktyvumo apdorojimas';
$MESS['IBLOCK_GROUP_DELETE'] = 'Paюymлti visus рalinimui';
$MESS['IBLOCK_SHOW_LOADING_CODE'] = 'Rodyti kodа iр iрorinio DB рaltinio';
$MESS['IBLOCK_PROP_EDIT'] = 'Detalus sаvybлs redagavimas:';
$MESS['IBLOCK_ELEMENT_PROP_VALUE'] = 'Sаvybiш reikрmлs:';
$MESS['IBLOCK_BAD_EXTERNAL_CODE'] = 'Neбvestas iрorinis kodas';
$MESS['IBLOCK_SHOW_EDIT_NAMES'] = 'Redaguoti pavadinimus sаraрш puslapiuose';
$MESS['IBLOCK_INDEX_TITLE'] = 'Informaciniai blokai';
$MESS['IBLOCK_INDEX_LIST_OF_ELEMENTS'] = 'Atvaizduoti elementш sаraра';
$MESS['IBLOCK_INDEX_CHANGE_PARAMS'] = 'Keisti informacinio bloko parametrus';
$MESS['IBLOCK_INDEX_SHOW_LIST'] = 'Atvaizduoti sаraра';
$MESS['IBLOCK_INDEX_ADD'] = 'Pridлti';
$MESS['IBLOCK_INDEX_ADD_INFOBLOCK'] = 'Pridлti informacinб blokа...';
$MESS['IBLOCK_INDEX_ADD_TYPE_INFOBLOCK'] = 'Pridлti naujа informaciniш blokш tipа...';
$MESS['IBLOCK_EDIT_PARAM_SECTION'] = 'Parametrai';
$MESS['IBLOCK_ICON_HINT'] = 'Informaciniш blokш modulis';
$MESS['IBLOCK_BUT_IB'] = 'Informacinio bloko savybлs';
$MESS['IBLOCK_BUT_IB_TITLE'] = 'Pereiti б informaciniш blokш sаvybiш redagavimа';
$MESS['IBLOCK_COMBINED_LIST_MODE'] = 'Bendra skyriш bei elementш perюiыra';
$MESS['IBLOCK_FIELD_ID'] = 'ID';
$MESS['IBLOCK_FIELD_CODE'] = 'Simbolinis kodas';
$MESS['IBLOCK_FIELD_XML_ID'] = 'Iрorinis kodas';
$MESS['IBLOCK_FIELD_NAME'] = 'Pavadinimas';
$MESS['IBLOCK_FIELD_TAGS'] = 'Юymлs';
$MESS['IBLOCK_FIELD_SORT'] = 'Rырiavimas';
$MESS['IBLOCK_FIELD_PREVIEW_TEXT'] = 'Anonso apraрymas';
$MESS['IBLOCK_FIELD_PREVIEW_TEXT_TYPE'] = 'Anonso apraрymo tipas';
$MESS['IBLOCK_FIELD_PREVIEW_PICTURE'] = 'Perюiыros paveikslлlis';
$MESS['IBLOCK_FIELD_DETAIL_TEXT'] = 'Detalus apraрymas';
$MESS['IBLOCK_FIELD_DETAIL_TEXT_TYPE'] = 'Detalus apraрymo tipas';
$MESS['IBLOCK_FIELD_DETAIL_PICTURE'] = 'Detalios perюiыros paveikslлlis';
$MESS['IBLOCK_FIELD_DATE_ACTIVE_FROM'] = 'Aktyvumo pradюia (data)';
$MESS['IBLOCK_FIELD_ACTIVE_FROM'] = 'Aktyvumo pradюia (laikas)';
$MESS['IBLOCK_FIELD_DATE_ACTIVE_TO'] = 'Aktyvumo pabaiga (data)';
$MESS['IBLOCK_FIELD_ACTIVE_TO'] = 'Aktyvumo pabaiga (laikas)';
$MESS['IBLOCK_FIELD_SHOW_COUNTER'] = 'Parodymш skaiиius';
$MESS['IBLOCK_FIELD_SHOW_COUNTER_START'] = 'Pirmo parodymo data';
$MESS['IBLOCK_FIELD_IBLOCK_TYPE_ID'] = 'Informacinio bloko tipas';
$MESS['IBLOCK_FIELD_IBLOCK_ID'] = 'Informacinio bloko ID';
$MESS['IBLOCK_FIELD_IBLOCK_CODE'] = 'Informacinio bloko simbolinis kodas';
$MESS['IBLOCK_FIELD_IBLOCK_NAME'] = 'Informacinio bloko pavadinimas';
$MESS['IBLOCK_FIELD_IBLOCK_EXTERNAL_ID'] = 'Iрorinis informacinio bloko kodas';
$MESS['IBLOCK_FIELD_DATE_CREATE'] = 'Sukurta';
$MESS['IBLOCK_FIELD_CREATED_BY'] = 'Sukыrл (ID)';
$MESS['IBLOCK_FIELD_CREATED_USER_NAME'] = 'Sukыrл (vardas)';
$MESS['IBLOCK_FIELD_TIMESTAMP_X'] = 'Atnaujinta';
$MESS['IBLOCK_FIELD_MODIFIED_BY'] = 'Redagavo (ID)';
$MESS['IBLOCK_FIELD_USER_NAME'] = 'Redagavo (vardas)';
$MESS['IBLOCK_FIELD_SECTION_ID'] = 'Skyrius';
$MESS['IBLOCK_FIELD_ACTIVE'] = 'Aktyvus:';
$MESS['IBLOCK_FIELD_SECTIONS'] = 'Susiejimas su skyriais';
$MESS['IBLOCK_FIELD_ACTIVE_PERIOD_FROM'] = 'Aktyvumo pradюia';
$MESS['IBLOCK_FIELD_ACTIVE_PERIOD_TO'] = 'Aktyvumo pabaiga';
$MESS['IBLOCK_MENU_MAX_DEPTH'] = 'Maksimalus meniu skyriш lygiш kiekis';
$MESS['IBLOCK_MENU_MAX_SECTIONS'] = 'Maksimalus meniu skyriш kiekis';
$MESS['IBLOCK_CHOOSE_IBLOCK_TYPE'] = '(pasirinkyte tipа)';
$MESS['IBLOCK_CHOOSE_IBLOCK'] = '(pasirinkyte informacinб blokа)';
?>