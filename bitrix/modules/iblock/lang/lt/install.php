<?
$MESS['IBLOCK_INSTALL_NAME'] = 'Informaciniai blokai';
$MESS['IBLOCK_INSTALL_DESCRIPTION'] = 'Informaciniш blokш modulis leidюia valdyti бvairaus pobыdюio informacijа - naujienas, prekiш sаraрus, vakansijas';
$MESS['IBLOCK_INSTALL_TITLE'] = 'Informaciniш blokш modulio diegimas';
$MESS['IBLOCK_INSTALL_DEFAULT'] = 'Pasirinkite informaciniш blokш pavizdюiш tipus.';
$MESS['IBLOCK_INSTALL_NEWS'] = 'Naujienos';
$MESS['IBLOCK_INSTALL_CATALOG'] = 'Prekiш katalogas';
$MESS['IBLOCK_INSTALL_VACANCY'] = 'Vakansijos';
$MESS['IBLOCK_INSTALL_PUBLIC_DIR'] = 'Vieрasis aplankas';
$MESS['IBLOCK_INSTALL_SETUP'] = 'Бdiegti';
$MESS['IBLOCK_INSTALL_COMPLETE_OK'] = 'Diegimas baigtas. Gauti papildomа informacijа kreipkites б pagalbos skyriш.';
$MESS['IBLOCK_INSTALL_COMPLETE_ERROR'] = 'Diegimas baigtas su klaidom';
$MESS['IBLOCK_INSTALL_ERROR'] = 'Diegimo metu бvyko klaidos';
$MESS['IBLOCK_INSTALL_BACK'] = 'Atgal б moduliш administravimа';
$MESS['IBLOCK_UNINSTALL_WARNING'] = 'Dлmesio! Modulis bus paрalintas iр sistemos.';
$MESS['IBLOCK_UNINSTALL_SAVEDATA'] = 'Jыs galite iрsaugoti duomenis duomenш bazлs lentelлse, paюymлkite \"Iрsaugoti lenteles\"';
$MESS['IBLOCK_UNINSTALL_SAVETABLE'] = 'Iрsaugoti lenteles';
$MESS['IBLOCK_UNINSTALL_DEL'] = 'Paрalinti';
$MESS['IBLOCK_UNINSTALL_ERROR'] = 'Nepavyko iрtrinti. Бvyko klaida. :';
$MESS['IBLOCK_UNINSTALL_COMPLETE'] = 'Рalinimas baigtas.';
$MESS['IBLOCK_INSTALL_PUBLIC_SETUP'] = 'Бdiegti';
$MESS['IBLOCK_SITE'] = 'Svetainл';
$MESS['IBLOCK_LINK'] = 'Nuoroda';
$MESS['IBLOCK_DEMO_DIR'] = 'Norлdami perюiыrлti komponenиiш veikimа, spauskite рias nuorodas:';
?>