<?
$MESS['FILEMAN_ED_SAVE'] = 'Iрsaugoti';
$MESS['FILEMAN_ED_CANC'] = 'Atрaukti';
$MESS['FILEMAN_ED_ADD_SNIPPET'] = 'Pridлti snipetа';
$MESS['FILEMAN_ED_EDIT_SNIPPET'] = 'Redaguoti snipetа';
$MESS['FILEMAN_ED_NAME'] = 'Bylos pavadinimas';
$MESS['FILEMAN_ED_TITLE'] = 'Pavadinimas';
$MESS['FILEMAN_ED_DESCRIPTION'] = 'Apraрymas';
$MESS['FILEMAN_ED_TEMPLATE'] = 'Рablonas';
$MESS['FILEMAN_ED_CODE'] = 'Snipeto kodas';
$MESS['FILEMAN_ED_FILE_EXISTS'] = 'Byla tokiu pavadinimu jau egzistuoja. Perraрyti bylа?';
$MESS['FILEMAN_ED_BASE_PARAMS'] = 'Baziniai parametrai';
$MESS['FILEMAN_ED_LOCATION'] = 'Vieta';
$MESS['FILEMAN_ED_ADD_PARAMS'] = 'Papildomi parametrai';
$MESS['FILEMAN_ED_SN_IMAGE'] = 'Paveikslлlis';
$MESS['FILEMAN_ED_FILE_LOCATION'] = 'Vieta';
$MESS['FILEMAN_ED_CREATE_SUBGROUP'] = 'Sukurti naujа grupж';
$MESS['FILEMAN_ED_SUBGROUP_NAME'] = 'Naujos grupлs pavadinimas';
$MESS['FILEMAN_ED_SN_DEL_IMG'] = 'Рalinti paveikslа';
$MESS['FILEMAN_ED_SN_NEW_IMG'] = 'Naujas paveikslas';
$MESS['FILEMAN_ED_WRONG_PARAMS'] = 'Neteisingi snipeto parametrai';
$MESS['FILEMAN_ED_WRONG_PARAM_TITLE'] = 'Neбvestas snipeto pavadinimas arba бvestas neteisingas';
$MESS['FILEMAN_ED_WRONG_PARAM_CODE'] = 'Neбvestas snipeto kodas arba бvestas neteisingas';
$MESS['FILEMAN_ED_WRONG_PARAM_SUBGROUP'] = 'Klaida: pogrupis tokiu pavadinimu jau yra';
$MESS['FILEMAN_ED_WRONG_PARAM_SUBGROUP2'] = 'Snipetш strukыra negali turлti daugiau 2 lygiш';
?>