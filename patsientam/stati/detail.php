<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "В данном разделе нашего сайта Вы можете подчеркнуть самую последнюю информацию в области стоматологии.");
$APPLICATION->SetPageProperty("keywords", "статьи стоматология услуги протезирование костная имплантация");
$APPLICATION->SetPageProperty("title", "Полезная информация о стоматологии – Клиника «ВайтДент»");
$APPLICATION->SetTitle("Статьи наших специалистов");

$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"article",
	Array(
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "content",
		"ELEMENT_CODE" => filterCode($_REQUEST["ELEMENT_CODE"]),
		"FIELD_CODE" => array("PREVIEW_PICTURE", "DETAIL_PICTURE", "CREATED_DATE", "MODIFIED_DATE"),
		"PROPERTY_CODE" => array("CONTENTS", "REGISTER_LINK"),

		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_ID" => "",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");