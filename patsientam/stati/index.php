<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Статьи");
$APPLICATION->SetPageProperty("title", "Полезная информация о стоматологии");
$APPLICATION->SetPageProperty("keywords", "статьи стоматология услуги протезирование костная имплантация");
$APPLICATION->SetPageProperty("description", "В данном разделе нашего сайта Вы можете подчеркнуть самую последнюю информацию в области стоматологии.");
$APPLICATION->AddHeadString('<link rel="canonical" href="https://whitedent.spb.ru/articles/" />');
?>
<section class="page-section container">
	<div class="content">
		<h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
		<? include "index-component.php" ?>
	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");