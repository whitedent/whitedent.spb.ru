<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Варианты оплаты в нашей клинике");
$APPLICATION->SetPageProperty("description", "В стоматологической клинике «Вайтдент» кроме традиционной оплаты наличными принимаются следующие виды оплаты: банковской картой, безналичным расчетом, оплата в кредит.");
?>
<section class="page-section container">
	<div class="content">

		<h1>Варианты оплаты</h1>

		<p>В стоматологической клинике «Вайтдент» кроме традиционной оплаты наличными принимаются следующие виды оплаты:</p>

		<div class="grid grid--padding-y grid--par-like">
			<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
				<div class="block-marked">
					<h2 class="small">Банковские карты</h2>
					<p>Мы принимаем к оплате банковские карты Visa, Mastercard, Maestro, Сберкард, МИР. При этом никакая дополнительная комиссия НЕ взимается.</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
				<div class="block-marked">
					<h2 class="small">Безналичный расчет</h2>
					<p>Оплата наших услуг возможна по договору, с банковского счета юридического или физического лица.</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
				<div class="block-marked">
					<h2 class="small">Оплата в кредит</h2>
					<p>Выгодные условия кредитования от Альфа банк.</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--xs-12">
				<div class="block-marked">
					<h2 class="small">Другие способы оплаты</h2>
					<p>По договорённости с администрацией возможны и другие способы оплаты. Подробности можно узнать по телефону или заказав обратный звонок.</p>
				</div>
			</div>
			<? /* <div class="grid__cell grid__cell--xs-12">
				<div class="block-marked">
					<h2 class="small">Полисы ДМС</h2>
					<p>Если у вас есть полис ДМС от одной из страховых компаний, с которыми мы сотрудничаем, вы можете получить услуги нашей клиники по данным полисам ДМС. Перечень компаний-партнеров, выписывающих страховой полис ДМС на лечение зубов:</p>
					<div class="grid grid--padding-y grid--align-center grid--par-like">
						<div class="grid__cell grid__cell--xs-auto"><img src="/images/about/rosgosstrah.svg" alt="" width="132">
						</div>
						<div class="grid__cell grid__cell--xs-auto"><img src="/images/about/rms.png" alt="" width="132">
						</div>
						<div class="grid__cell grid__cell--xs-auto"><img src="/images/about/reso.svg" alt="" width="132">
						</div>
						<div class="grid__cell grid__cell--xs-auto"><img src="/images/about/allianz.svg" alt="" width="132">
						</div>
						<div class="grid__cell grid__cell--xs-auto"><img src="/images/about/vtb.svg" alt="" width="132">
						</div>
						<div class="grid__cell grid__cell--xs-auto"><img src="/images/about/medexpress.svg" alt="" width="132">
						</div>
						<div class="grid__cell grid__cell--xs-auto"><img src="/images/about/pomosch.png" alt="" width="132">
						</div>
					</div>
				</div>
			</div> */?>
		</div>
		
	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
