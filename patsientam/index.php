<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Информация для пациентов");
$APPLICATION->SetPageProperty("keywords", "вайдент стоматологическая клиника стоматология");
$APPLICATION->SetPageProperty("description", "Полезная информация для пациентов нашей стоматологической клиники: гарантии, кредит, ДМС, варианты оплаты, статьи, налоговый вычет.");
?>
<section class="page-section container">
  <div class="content">
    <h1>Пациентам</h1>
    <div class="page-subsection">
      <div class="block-marked">
        <div class="grid grid--padding-y">
          <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/garantii/">Гарантии</a>
          </div>
          <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/kredit/">Кредит</a>
          </div>
          <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/varianty-oplaty/">Варианты оплаты</a>
          </div>
          <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/pravovaya-informatsiya/">Правовая информация</a>
          </div>
          <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/nalogoviy-vychet/">Налоговый вычет</a>
          </div>
          <?/* <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/dms/">ДМС</a>
          </div> */?>
          <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/stati/">Статьи</a>
          </div>
          <div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"><a href="/patsientam/cons/">Заочная консультация</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
