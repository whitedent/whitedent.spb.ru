<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Добровольное медицинское страхование в стоматологии");
$APPLICATION->SetPageProperty("description", "Мы приглашаем всех, кто имеет полис ДМС, получить высококвалифицированную стоматологическую помощь по полисам добровольного медицинского страхования.");
$APPLICATION->SetPageProperty("robots", "noindex"); 
?>
<section class="page-section container">
    <div class="content">

        <h1>Добровольное медицинское страхование</h1>

        <div class="page-subsection">
            <p>Каждый работающий человек в нашей стране имеет полис обязательного медицинского страхования, который позволяет получить необходимую медицинскую помощь бесплатно в муниципальных клиниках города. Но, к сожалению, не всякая квалифицированная медицинская помощь может быть предоставлена бесплатно.</p>
            <p>В этой ситуации можно защитить себя полисом добровольного медицинского страхования. Это личный вид страхования, который позволяет получить дополнительные услуги в лечебно-профилактических учреждениях, а также индивидуальную современную высокоэффективную медицинскую помощь. Программу ДМС можно выбирать, ориентируясь на состояние личного здоровья и финансовые возможности.</p>
        </div>

        <div class="page-subsection">
            <p>Мы принимаем пациентов застрахованных в следующих компаниях:</p>
            <div class="grid grid--padding-y grid--align-center grid--par-like">
                <div class="grid__cell grid__cell--xs-auto"><img src="/images/about/rosgosstrah.svg" alt="" width="132">
                </div>
                <div class="grid__cell grid__cell--xs-auto"><img src="/images/about/rms.png" alt="" width="132">
                </div>
                <div class="grid__cell grid__cell--xs-auto"><img src="/images/about/reso.svg" alt="" width="132">
                </div>
                <div class="grid__cell grid__cell--xs-auto"><img src="/images/about/allianz.svg" alt="" width="132">
                </div>
                <div class="grid__cell grid__cell--xs-auto"><img src="/images/about/vtb.svg" alt="" width="132">
                </div>
                <div class="grid__cell grid__cell--xs-auto"><img src="/images/about/medexpress.svg" alt="" width="132">
                </div>
                <div class="grid__cell grid__cell--xs-auto"><img src="/images/about/pomosch.png" alt="" width="132">
                </div>
            </div>
        </div>

        <div class="page-subsection">
            <p>Мы приглашаем всех, кто имеет полис ДМС, получить высококвалифицированную стоматологическую помощь по полисам добровольного медицинского страхования.</p>
            <p>Сообщите своей страховой компании о желании обслуживаться в клинике «ВайтДент», и наши сотрудники проведут переговоры с компанией, согласуют условия вашего медицинского обслуживания в клинике.</p>
        </div>

    </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
