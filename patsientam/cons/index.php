<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Консультация");
$APPLICATION->SetPageProperty("description", "Создали этот раздел, чтобы нашим пациентам было проще узнать причины проблем с зубами, деснами или ротовой полостью в целом. Просто подробно изложите суть вашей проблемы в вопросе, и наши эксперты дадут полный ответ.");
$APPLICATION->SetPageProperty("title", "Онлайн-консультация - клиника «ВайтДент»");
$APPLICATION->SetPageProperty("keywords", "консультации врачей вопросы стоматология протезирование имплантация");
?>
<section class="page-section">
    <div class="container">
        <div class="content">

            <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>

            <div class="grid grid--par-like grid--padding-y">

                <div class="grid__cell grid__cell--l-8 grid__cell--m-6 grid__cell--xs-12">
                    <? include __DIR__."/include/button.php" ?>
                    <? /* <? include __DIR__."/include/about.php" ?> */?>
                    <? include __DIR__."/include/component-list.php" ?>
                </div>

                <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--xs-12">
					<? include __DIR__."/include/categories.php" ?>
                </div>

            </div>

        </div>
    </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
