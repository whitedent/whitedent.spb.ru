<? if(empty($_REQUEST["SECTION_CODE"])): ?>
<div class="page-subsection">
	<div class="block-marked">

		<p>В разделе консультаций вы можете задать любой интересующий вас вопрос стоматологу. Вы можете проконсультироваться по поводу:</p>

		<ul class="list-marked list-marked--w-prev-par">
			<li>боли</li>
			<li>внешнего вида зубов</li>
			<li>процесса лечения</li>
			<li>определенной процедуры</li>
		</ul>

		<p>Врач даст полный ответ в самый короткий срок</p>

	</div>
</div>
<? endif ?>