<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Вопрос отправлен - клиника «ВайтДент»");
?>
<div class="container">
	<div class="content">
		<section class="page-section">
			<div class="grid grid--par-like grid--padding-y grid--justify-center">
				<div class="grid__cell grid__cell--m-6 grid__cell--s-10 grid__cell--xs-12">

					<h1 class="text-align--center">Вопрос отправлен</h1>

					<p class="appear text-align--center">Вопрос отправлен, в течение рабочего дня ответим на него. Ответ появится на сайте в разделе "Консультация".</p>
					<p class="appear text-align--center">Если вы указали e-mail, отправим письмо с ответом. Если оставили телефон, позвоним и проконсультируем.</p>
					<p class="appear text-align--center">Можете не ждать ответа и позвонить <strong>+7 (812) 612-97-92</strong></p>
					<p class="appear text-align--center">Режим работы:<br>пн.-пт. 11:00-21:00,<br>сб. 11:00-18:00</p>
				</div>
			</div>
		</section>
	</div>
</div>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
