<?
$arOrder = ["SORT" => "ASC"];
$arFilter = [
	"IBLOCK_ID" => ID_IBLOCK_CONSULTATION,
	"DEPTH_LEVEL" => 1,
	"ACTIVE" => "Y"
];
$bIncCnt = false;
$Select = ["ID", "IBLOCK_ID", "NAME", "SECTION_PAGE_URL"];
$NavStartParams = false;
$rsSections = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

$sections = [];

// Получаем разделы
while ($arSection = $rsSections->GetNext()) {
	$sections[] = [
		"id" => $arSection["ID"],
		"name" => $arSection["NAME"],
		"url" => $arSection["SECTION_PAGE_URL"]
	];
}

// Добавляем кол-во элементов в разделах
global $USER;
foreach ($sections as $k => $section){
	$filter = [
		"IBLOCK_ID" => ID_IBLOCK_CONSULTATION,
		"SECTION_ID" => $section["id"],
		"ACTIVE" => "Y",
	];
	if (!$USER->IsAdmin()) {
		$filter["PROPERTY_STATUS"] = 4;
	}
	$elementsCount = CIBlockElement::GetList(["SORT" => "ASC"], $filter, []);
	$sections[$k]["count"] = $elementsCount;
}

$aMenuLinks = [];
foreach ($sections as $section){
	$aMenuLinks[] = [
		$section["name"],
		$section["url"],
		[],
		["COUNT" => $section["count"]]
	];
}