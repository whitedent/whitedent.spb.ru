<div class="grid__cell grid__cell--xs-12">
	<div class="form-input form-input--textarea">
		<textarea name="question" class="form-input__field" required placeholder="Ваше вопрос"></textarea>
	</div>
</div>

<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
	<div class="form-input form-input--text">
		<input name="subject" class="form-input__field" type="text" placeholder="Тема вопроса">
	</div>
</div>

<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
	<div class="form-input form-input--select">
		<select name="section" class="form-input__field" required>
			<option value="">Выберите категорию</option>
            <? foreach (getConsultationSections() as $section): ?>
                <option value="<?= $section["id"] ?>"><?= $section["name"] ?></option>
			<? endforeach ?>
		</select>
	</div>
</div>

<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
	<div class="form-input form-input--text">
		<input name="name" class="form-input__field" type="text" required placeholder="Имя">
	</div>
</div>

<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
	<div class="form-input form-input--email">
		<input name="email" class="form-input__field" type="email" placeholder="E-mail">
	</div>
</div>

<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
	<div class="form-input form-input--text">
		<input name="age" class="form-input__field" type="text" placeholder="Возраст">
	</div>
</div>