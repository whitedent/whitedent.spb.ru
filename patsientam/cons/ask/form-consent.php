<div class="grid__cell grid__cell--xs-12">
	<div class="form-input form-input--checkbox">
		<input class="form-input__field" type="checkbox" required checked>
		<label class="form-input__label">Нажимая на кнопку, я даю свое согласие на обработку <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">персональных данных</a>.</label>
	</div>
</div>