<form
	class="block-marked consultation_new_question validated-form submitJS"
	enctype="multipart/form-data"
>
	<div class="grid grid--padding-y">
		<? include __DIR__."/form-fields-text.php" ?>
		<? include __DIR__."/form-fields-file.php" ?>
		<? include __DIR__."/form-consent.php" ?>
		<? include __DIR__."/form-submit.php" ?>
	</div>
</form>
