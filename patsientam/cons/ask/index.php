<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Задать вопрос - клиника «ВайтДент»");
$APPLICATION->SetPageProperty("description", "Просто подробно изложите суть вашей проблемы в вопросе, и наши эксперты дадут полный ответ.");
?>
<section class="page-section container">
	<div class="content">

		<h1>Задать вопрос</h1>

		<? include __DIR__."/about.php" ?>

		<div class="page-subsection">
			<? include __DIR__."/form.php" ?>
		</div>

	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");