<div class="grid__cell grid__cell--xs-12">

	<h2 class="small">Прикрепить файл</h2>

	<p>При необходимости вы можете прикрепить файл с фотографией или рентгенографическим снимком к своему вопросу.</p>

    <div class="grid grid--padding-y grid--par-like grid--align-center">

		<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
			<div class="form-input form-input--file">
				<input name="photo" class="form-input__field" type="file" accept="image/*">
			</div>
		</div>

		<div class="grid__cell grid__cell--m-6 grid__cell--xs-12">
			<p>Допустимые форматы: jpg, png, doc.</p>
		</div>
	</div>

</div>