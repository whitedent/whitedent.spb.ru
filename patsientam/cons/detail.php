<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
?>
<section class="page-section">
    <div class="container">
        <div class="content">
            <div class="grid grid--par-like grid--padding-y">

                <div class="grid__cell grid__cell--l-8 grid__cell--xs-12">
                    <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
					<? include __DIR__."/include/component-detail.php" ?>
					<? include __DIR__."/include/similar-questions.php" ?>
					<? include __DIR__."/include/button.php" ?>
                </div>

                <div class="grid__cell grid__cell--l-4 grid__cell--m-6 grid__cell--xs-12">
					<? include __DIR__."/include/categories.php" ?>
                </div>

            </div>
        </div>
    </div>
</section>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
