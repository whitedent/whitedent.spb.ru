<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Гарантии на стоматологическое лечение");
$APPLICATION->SetPageProperty("description", "Читайте в этой статье о гарантиях, которые мы предоставляем. Ознакомьтесь с видами гарантий, их перечнем, особенностями и условиями предоставления.");
?>
<section class="page-section container">
  <div class="content">
    <h1>Гарантии в клинике ВайтДент</h1>
    <div class="page-subsection">
      <p class="text-marked">Стоматологическая клиника ВайтДент предоставляет всем пациентам гарантии на все виды стоматологических услуг.
      </p>
    </div>
    <div class="page-subsection">
      <p>В соответствии с действующим Законом об охране здоровья граждан выделяют два вида гарантий:</p>
      <ul class="list-marked list-marked--w-prev-par">
        <li>безусловные – предоставляемый в любом случае и не подвергаемые какому-либо двусмысленному истолкованию;</li>
        <li>и прогнозируемые – предоставляемые на осязаемый (вещественный) результат стоматологической помощи (выражается гарантийным сроком и сроком службы).</li>
      </ul>
      <p>Мы гарантируем нашим пациентам, безусловно, во всех случаях оказания стоматологической помощи:</p>
      <ul class="list-marked list-marked--w-prev-par">
        <li>предоставление полной, достоверной и доступной по форме информации о состоянии здоровья пациентов (с учетом их прав и желания получать таковую по доброй воле);</li>
        <li>проведение консультации и при необходимости – консилиума;</li>
        <li>оказание стоматологических услуг специалистами, имеющими сертификаты, подтверждающие право на осуществление данного вида медицинской помощи;</li>
        <li>установление полного диагноза;</li>
        <li>составление рекомендуемого или предлагаемого плана лечения;</li>
        <li>использование всех доступных методов и технологий лечения, применяемых в нашей клинике;</li>
        <li>безопасность лечения, которая обеспечивается неукоснительным исполнением санитарно – эпидемиологических мероприятий для профилактики заражения инфекционными заболеваниями, с использованием разрешенных к применению технологий и материалов;</li>
        <li>точную диагностику при наличии должного профессионального уровня специалистов, современных диагностических средств и данных дополнительных обследований;</li>
        <li>соблюдение технологий лечения, что предполагает высокопрофессиональную подготовку врачей, зубных техников и ассистентов, а также специальные средства контроля качества их работы;</li>
        <li>применение безопасных, разрешенных к применению МЗСР материалов, разрешенных к применению на территории РФ, имеющих сертификаты Росстандарта и не утративших сроков годности;</li>
        <li>проведение повторных контрольных осмотров ( по показаниям) после проведенного сложного лечения для предупреждения нежелательных последствий;</li>
        <li>проведение бесплатных профилактических осмотров с частотой, определяемой лечащим стоматологом;</li>
        <li>проведение мероприятий по определению риска повторения или обострения выявленных заболеваний, устранению или снижению степени осложнений, которые могут возникнуть в процессе или после лечения.</li>
      </ul>
      <p>Согласно действующему законодательству, в медицине, в частности в стоматологии, большинство гарантий, влекущих за собой материальные претензии пациентов, относятся не к числу безусловных, а к числу прогнозируемых.</p>
      <p>Такие гарантии не могут быть обеспечены только клиникой, и в частности врачом-стоматологом. Они требуют соответствующих действий от самого пациента и предоставляются при соблюдении ряда условий, ведь в устранении любой медицинской проблемы всегда участвуют, как минимум, двое: сам пациент и его лечащий врач.</p>
      <p>Данная особенность прогнозируемых гарантий является нормой в стоматологиях всего мира.</p>
      <p>Необходимо пояснить, что часть гарантийных обязательств мы переносим от производителя материалов или технологий. Например, компания Nobel Biocare предоставляет пожизненную гарантию физическую целостность импланта (LIFE TIME WARRANTY ) Nobel Replace, Nobel Active, 10 лет на положительную интеграцию и 5-летнию гарантию на коронки и мосты Nobel Procera. Практически во всех остальных случаях ( косметическое восстановление зубов, эстетическая ортодонтия, косметическое протезирование зубов и т.д. ) производители материалов и технологий не могут гарантировать результат эстетической работы, потому что невозможно использовать какие-либо критерии оценки качества выполненной работы.</p>
      <h2>Какие обстоятельства учитывает врач, при определении гарантии?</h2>
      <p>При установлении прогнозируемых гарантий (гарантийного срока и срока службы) врач учитывает и разъясняет обстоятельства, ограничивающие гарантии (если таковые выявлены):</p>
      <ul class="list-marked list-marked--w-prev-par">
        <li>состояние общего здоровья пациента;</li>
        <li>полноту и оббьем выполненного рекомендованного плана лечения;</li>
        <li>клиническая ситуация в полости рта (имеющиеся нарушения, особенности прикуса, состав слюны, повышенная стираемость зубов, прогноз развития или повторения имеющихся заболеваний и др.);</li>
        <li>сложность каждого конкретного случая лечения;</li>
        <li>достоинства и недостатки выбранных технологий и материалов, а так же выбранных вариантов лечения;</li>
        <li>особенности профессиональной деятельности пациента, которые могут негативно сказываться на результат лечения;</li>
        <li>обязательность выполнения индивидуального графика посещения профилактических осмотров и проведения профессиональной гигиены полости рта.</li>
      </ul>
      <h2>При каких условия будут выполняться установленные гарантии?</h2>
      <p>Врач (клиника) сможет выполнять установленные прогнозируемые гарантии в случаях:</p>
      <ul class="list-marked list-marked--w-prev-par">
        <li>если в период действия гарантий у пациента не возникнут (не проявятся) заболевания внутренних органов, а также изменения физиологического состояния организма (вследствие беременности, аутоиммунных состояний и заболеваний соединительнотканной системы, сахарного диабета, онкологии и результатов лечения, приема лекарственных препаратов, вредных внешних воздействий, получение травм, повлекших утрату протезов и реставрации), которые способны негативно повлиять на результаты стоматологического лечения;</li>
        <li>если пациент соблюдает гигиену полости рта и другие указания стоматолога;</li>
        <li>если пациент посещает врача для контроля лечения с частотой, рекомендованной врачом:
          <ul class="list-marked list-marked--w-prev-par">
            <li> <i>Эндодонтическое лечение: </i>1 раз в полгода, в течение гарантийного срока.</li>
            <li> <i>Эстетическая прямая и непрямая реставрации (пломба, еоронка, винир): </i>1 раз в год, в течении гарантийного срока.</li>
            <li> <i>Имплантация: </i>1 раз в полгода, в течение 3-х лет.</li>
          </ul>
        </li>
        <li>если в период лечения у врача пациента не будет проходить схожее лечение у специалиста другой клиники или посещать специалистов попеременно;</li>
        <li>если при вынужденном обращении по не неотложной ситуации в другую клинику пациент предоставит выписку из амбулаторной карты и рентгеновские снимки, фиксирующие результаты вмешательства;</li>
        <li>если выявленные недостатки или дефекты нашей работы будут исправляться специалистами нашей клиники;</li>
        <li>если не возникнут форс-мажорные обстоятельства, способные негативно повлиять на результаты лечения.</li>
      </ul>
      <p>Все установленные врачом и согласованные с пациентом прогнозируемые гарантии на каждую выполненную работу фиксируются в медицинской карте за подписью пациента и врача.</p>
      <h2>Когда врач может отказать в гарантии на результат лечения?</h2>
      <p>Может ли врач, взявший на себя гарантию, отказаться от бесплатного устранения недостатков? По закону да, если пациент нарушает правила эксплуатации услуги, где первым пунктом обычно стоит приход к врачу раз в полгода, а вторым – адекватная гигиена полости рта и т.д. Это не попытка стоматолога снять с себя ответственность, а совершенно необходимые условия сохранения результатов лечения. Разумная гарантия отражает не попытку продать свои услуги, во что бы то ни стало, а реальное положение вещей, основанное на достижение науки.</p>
      <p>Многолетняя положительная репутация клиники на рынке медицинских услуг – вот главная гарантия, позволяющая пациенту чувствовать себя надежно и уверенно. Врач не всегда может гарантировать биологически положительный результат лечения (т.е. излечение), но всегда должен гарантировать безупречность проведенных манипуляций.</p>
      <h2>Что подлежит бесплатному устранению по гарантии?</h2>
      <p>Гарантии могут устанавливаться только на материальный результат лечения, если он существует, в каждом случае индивидуально. Это финансовая ответственность врача (или клиники) перед пациентом на бесплатное устранение возможных недостатков оказанной услуги (лечения).</p>
      <p>В стоматологии гарантии подлежит вещественный результат – изделие (пломба, коронка, протез и т.д.).</p>
      <p>Например, лечение глубокого кариеса до 70% случаев заканчивается пульпитом. В этом случае, приступая к лечению, стоматолог гарантирует правильность своих действий, оценить которые, могут эксперты, но не финансовую ответственность за возможный пульпит.</p>
      <p>В клинике разработаны средние гарантийные сроки каждого изделия (услуги). Однако в каждом конкретно случае срок гарантии может быть увеличен либо уменьшен, по усмотрению врача. Изменение срока гарантии подлежит подробной аргументации, со стороны лечащего доктора, который сможет объяснить главные причины, по которым это происходит.</p>
      <p>Клиника берет на себя ответственность исполнения гарантий только при надлежащем выполнении рекомендации врача и соблюдении графика контрольных посещений. Это рекомендации по ежедневному уходу за ротовой полостью или новой ортопедической конструкцией и так далее.</p>
      <p>На консультации врач в должном объеме освящает вопрос предоставляемых гарантий, независимо от вида планируемой работы. Мы уверены, что успех любого стоматологического вмешательства, особенно когда речь идет об идеальной эстетике напрямую зависит как от желания и добросовестности пациента. Только полное взаимопонимание и доверие позволяет добиться максимального желаемого результата.</p>
      <h2>Ортодонтия</h2>
      <div class="grid grid--padding-y grid--par-like">
        <div class="block-marked grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12">
          <div class="table-wrap">
            <table class="table">
              <colgroup class="table__colgroup table__colgroup--3">
                <col class="table__col"/>
                <col class="table__col"/>
                <col class="table__col"/>
              </colgroup>
              <tr class="table__row">
                <th class="table__header">Виды работ
                </th>
                <td class="table__cell">Гарантийный срок (месяцы)
                </td>
                <td class="table__cell">Срок службы (месяцы)
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Ретенционные аппараты после снятия брекет-системы
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">каппа OSAMU
                </td>
                <td class="table__cell">10
                </td>
                <td class="table__cell">18
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">несъемный ретейнер
                </td>
                <td class="table__cell">12
                </td>
                <td class="table__cell">24
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">аппарат Дерихсвайлера
                </td>
                <td class="table__cell">4
                </td>
                <td class="table__cell">6
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">внеротовые аппараты
                </td>
                <td class="table__cell">12
                </td>
                <td class="table__cell">24
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">съемный протез
                </td>
                <td class="table__cell">8
                </td>
                <td class="table__cell">12
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">съемный аппарат одночелюстной
                </td>
                <td class="table__cell">6
                </td>
                <td class="table__cell">12
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">съемный аппарат двухчелюстной
                </td>
                <td class="table__cell">10
                </td>
                <td class="table__cell">12
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">вестибулярная пластика
                </td>
                <td class="table__cell">15
                </td>
                <td class="table__cell">20
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <h2>Средние гарантийные сроки и сроки службы, действующие в клинике</h2>
      <h3>Терапевтическая стоматология</h3>
      <p>Поставка пломб, эстетическая реставрация (годы, месяцы).</p>
      <div class="grid grid--padding-y grid--par-like">
        <div class="block-marked grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12">
          <div class="table-wrap">
            <table class="table">
              <colgroup class="table__colgroup table__colgroup--2">
                <col class="table__col"/>
                <col class="table__col"/>
              </colgroup>
              <tr class="table__row">
                <th class="table__header" rowspan="2">Виды работ
                </th>
                <th class="table__header text-align--center" colspan="2">Средние сроки (лет)
                </th>
              </tr>
              <tr class="table__row">
                <th class="table__header">гарантийный
                </th>
                <th class="table__header">службы
                </th>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Пломба из композитного светоотверждаемого материала
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес жевательной поверхности зубов
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">3
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес контактной поверхности малых и больших боковых зубов
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">2
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес контактной поверхности резцов и клыков
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">2
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес контактной поверхности резцов с разрушением угла коронки
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">2
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес придесневой области
                </td>
                <td class="table__cell table__cell--empty">
                </td>
                <td class="table__cell table__cell--empty">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">покрытие пломбировочным материалом губной, пришеечной поверхности зубов всех групп(прямое винирование)
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">2
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Пломба из стеклоиномерного цемента(СИЦ)
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">По виду дефекта
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес жевательной поверхности зубов
                </td>
                <td class="table__cell">0.5
                </td>
                <td class="table__cell">1
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес контактной поверхности малых и больших боковых зубов
                </td>
                <td class="table__cell">0.5
                </td>
                <td class="table__cell">1
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес контактной поверхности резцов и клыков
                </td>
                <td class="table__cell">0.5
                </td>
                <td class="table__cell">1
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес контактной поверхности резцов с разрушением угла коронки
                </td>
                <td class="table__cell">1 мес.
                </td>
                <td class="table__cell">0.6
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">кариес в придесневой области
                </td>
                <td class="table__cell">0.5
                </td>
                <td class="table__cell">1
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Наложение герметика
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из композита
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">—
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из стекломерного цемента (СИЦ)
                </td>
                <td class="table__cell">3 мес.
                </td>
                <td class="table__cell">5 мес.
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из компомера
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">2
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Адгезивные протезы
                </td>
                <td class="table__cell">2 мес.
                </td>
                <td class="table__cell">0.5
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <h3>Эндодонтическое лечение</h3>
      <div class="grid grid--padding-y grid--par-like">
        <div class="block-marked grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12">
          <div class="table-wrap">
            <table class="table">
              <colgroup class="table__colgroup table__colgroup--2">
                <col class="table__col"/>
                <col class="table__col"/>
              </colgroup>
              <tr class="table__row">
                <th class="table__header" rowspan="2">Виды работ
                </th>
                <th class="table__header text-align--center" colspan="2">Средние сроки (лет)
                </th>
              </tr>
              <tr class="table__row">
                <th class="table__header">гарантийный
                </th>
                <th class="table__header">службы
                </th>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Первичная эндодонтия
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">Без огр.
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Повторное, после проведенного в другой клинике
                </td>
                <td class="table__cell">—
                </td>
                <td class="table__cell">Без огр.
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <h3>Ортопедическая стоматология</h3>
      <div class="grid grid--padding-y grid--par-like">
        <div class="block-marked grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12">
          <div class="table-wrap">
            <table class="table">
              <colgroup class="table__colgroup table__colgroup--2">
                <col class="table__col"/>
                <col class="table__col"/>
              </colgroup>
              <tr class="table__row">
                <th class="table__header" rowspan="2">Виды работ
                </th>
                <th class="table__header text-align--center" colspan="2">Средние сроки (лет)
                </th>
              </tr>
              <tr class="table__row">
                <th class="table__header">гарантийный
                </th>
                <th class="table__header">службы
                </th>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Вкладки
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из безметалловых материалов
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">5
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из металла
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">6
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из композиционнах материалов
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">5
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">культевые штифтовые металлические
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">10
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">культевые штифтовые неметаллические
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">5
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Виниры
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">5
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Коронки
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из пластмассы (временные)
                </td>
                <td class="table__cell">1 мес.
                </td>
                <td class="table__cell">6 мес.
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из металлопластика
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">6
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Empress (e-Max)
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">3
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Коронки цельнометаллические и металлокерамические
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из неблагородного металла
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">7
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из драгоценного металла
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">7
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Адгезивные протезы
                </td>
                <td class="table__cell">2 мес.
                </td>
                <td class="table__cell">0.5
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Мостовидные протезы металлокерамические
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из неблагородного металла
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">7
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">из драгоценного металла
                </td>
                <td class="table__cell">2
                </td>
                <td class="table__cell">7
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell" colspan="3">Безметалловые мостовидные протезы
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Empress (e-Max)
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">3
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell table__cell--empty" colspan="3">
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Съемное протезирование(полное, частичное, замковое)
                </td>
                <td class="table__cell">1
                </td>
                <td class="table__cell">5
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <h3>Имплантация</h3>
      <div class="grid grid--padding-y grid--par-like">
        <div class="block-marked grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12">
          <div class="table-wrap">
            <table class="table">
              <colgroup class="table__colgroup table__colgroup--2">
                <col class="table__col"/>
                <col class="table__col"/>
              </colgroup>
              <tr class="table__row">
                <th class="table__header" rowspan="2">Виды работ
                </th>
                <th class="table__header text-align--center" colspan="2">Средние сроки (лет)
                </th>
              </tr>
              <tr class="table__row">
                <th class="table__header">гарантийный
                </th>
                <th class="table__header">службы
                </th>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Replace
                </td>
                <td class="table__cell">10
                </td>
                <td class="table__cell">15
                </td>
              </tr>
              <tr class="table__row">
                <td class="table__cell">Adin, MIS, Implay
                </td>
                <td class="table__cell">5
                </td>
                <td class="table__cell">15
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
