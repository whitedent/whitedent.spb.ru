<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Налоговый вычет в стоматологии");
$APPLICATION->SetPageProperty("description", "Ознакомьтесь с документами для получения налогового вычета.");
?>
<section class="page-section container">
	<div class="content">

		<h1>Налоговый вычет</h1>

		<div class="page-subsection">
			<div class="block-marked block-marked--arrow">
				<p class="text-marked text-marked--big">Вы прошли лечение в нашей клинике? Верните 13% от стоимости услуг
				</p>
			</div>
		</div>

		<div class="page-subsection">
			<p class="text-marked">Документы, которые необходимы для получения налогового вычета
			</p>
			<p><strong>Для получения справки от Вайтдент:</strong></p>
			<ul class="list-marked list-marked--w-prev-par">
				<li>Заявление о предоставлении справки в налоговую инспекцию (образец заявления получите у администратора или скачайте здесь)</li>
				<li>Копия паспорта (1-ая страница и прописка)</li>
				<li>Копия договора об оказании услуг (восстановим утерянный экземпляр)</li>
				<li>Копии чеков</li>
				<li>Копия ИНН</li>
			</ul>
		</div>

		<div class="page-subsection">
			<p><strong>Для предоставления в налоговую инспекцию:</strong></p>
			<ul class="list-marked list-marked--w-prev-par">
				<li>Справка от Вайтдент</li>
				<li>Заполненная декларация 3-НДФЛ</li>
				<li>Справка с места работы по форме 2-НДФЛ</li>
				<li>Копия договора об оказании услуг</li>
				<li>Копия паспорта (1-ая страница и прописка)</li>
				<li>Копии чеков</li>
				<li>Копии СНИЛС и ИНН</li>
				<li>Реквизиты счета (не кредитный)</li>
			</ul>
		</div>

	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");