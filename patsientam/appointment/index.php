<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Записаться на прием к стоматологу");
$APPLICATION->SetPageProperty("title", "Записаться на прием к стоматологу");
?>
<section class="page-section container">
	<div class="content">
        <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
	</div>
</section>

<section class="page-section container">
	<div class="content">
        <div class="page-subsection">
            <div class="grid grid--padding-y">
                <div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
					<? include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/include/footer/appointment-popup-form.php" ?>
                </div>
            </div>
        </div>
	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
