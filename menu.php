<?
$menuPage = true;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php")?>

<?
	if ($_GET["type"] === "services") {

	$APPLICATION->IncludeComponent(
		"bitrix:menu",
		"services-desktop",
		Array(
			"ROOT_MENU_TYPE" => "services",	// Тип меню для первого уровня
			"MAX_LEVEL" => "3",	// Уровень вложенности меню
			"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
			"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N",	// Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
			"MENU_CACHE_TYPE" => "N",	// Тип кеширования
			"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
			"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		),
		false
	);
	}

	if ($_GET["type"] === "navigation") {
		$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"main-desktop",
			Array(
					"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "top_sub",	// Тип меню для остальных уровней
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
			),
			false
	);
	}

?>
