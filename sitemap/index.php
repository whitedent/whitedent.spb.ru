<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Карта сайта позволит вам проще ориентироваться на сайте: найти нужные разделы, статьи, в которых изложены ответы на наиболее частые вопросы. Просто выберите интересующую вас ссылку, и перейдите на нужную страницу.");
$APPLICATION->SetTitle("Карта сайта");
?><div class="sitemap">
    <?$APPLICATION->IncludeComponent("bitrix:menu", "sitemap", Array(
        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
        "MAX_LEVEL" => "1",	// Уровень вложенности меню
        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N",	// Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
        "SET_STATUS_404" => "Y"
        ),
        false
    );?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>