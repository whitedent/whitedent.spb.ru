<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Запись на приём к стоматологу - Василеостровский район");
$APPLICATION->SetPageProperty("description", "Приглашаем вас записаться на прием, это можно сделать прямо на нашем сайте. Сэкономьте время, просто заполните форму, и наш менеджер свяжется с вами, чтобы подтвердить актуальность записи.");
$APPLICATION->SetTitle("Запись на приём");
?> 
<script src="/bitrix/templates/whitedent/css-js/jquery-1.10.2.min.js" type="text/javascript"></script>
 
<script>
    $(document).on("click", "#submit", function(){
        var error = 0;
		$('.error').detach();

        if($("#name").val() == "" || $("#name").val() == " ")
        {
       		$("#name").after('<span class="error"><br>Поле "Имя" не заполнено</span>');
            $("#name").css("background", "tomato");
            error++;
        }
        else
            $("#name").css("background", "#fff");
        
        if($("#phone").val() == "" || $("#phone").val() == " ")
        {
        	$("#phone").after('<span class="error"><br>Поле "Телефон" не заполнено</span>');
            $("#phone").css("background", "tomato");
            error++;
        }
        else
            $("#phone").css("background", "#fff");

		if ($("#name").val().match(/[^а-яА-Я ]/g)){
            $("#name").after('<span class="error"><br>В поле "Имя" допустимы только русские буквы и пробелы</span>');
            $("#name").css("background", "tomato");
            error++;
        }

        if ($("#phone").val().match(/[^0-9 ]/g)){
            $("#phone").after('<span class="error"><br>В поле "Телефон" допустимы только цифры и пробелы</span>');
            $("#phone").css("background", "tomato");
            error++;
        }
        
        if(error == 0)
        {
            $.ajax({
                url: "/bitrix/templates/whitedent/ajax/ajax.note.php",
                type: "POST",
                data: {
                    "name":$("#name").val(),
                    "phone":$("#phone").val(),
                    "message":$("#message").val()
                },
                timeout: 5000,
                success: function(data)
                {
                    $(".msg-btn").html('<p style="color:blue;">Спасибо. Ваше сообщение отправлено, наш менеджер свяжется с Вами в ближайшее время.</p>');
                    ga('send','event','Form', 'Send', 'Форма записи на странице');
                }
            });
        }
    });
</script>
 
<style>
    .post-form {margin-bottom: 20px;}
    .post-form td {padding-bottom:5px;}
    .post-form input[type="text"] {width:200px;color:#666;font-size:12px; padding: 1px 3px;}
    .post-form input[type="password"] {width:200px;color:#666;font-size:12px; padding: 1px 3px;}
    .post-form textarea {width: 400px; height:150px;}
	.post-form .error {color:tomato;}
</style>
 
<p>Вы можете отправить заявку нашему менеджеру с пожеланиями на дату и время записи. Наш менеджер свяжется с Вами в ближайшее время.</p>
 
<div class="post-form"> 
  <table> 
    <tbody> 
      <tr> <td><span style="color: rgb(255, 99, 71);">*</span> </td> <td width="180">Ваше имя</td> <td><input type="text" id="name" value="" /></td> </tr>
     
      <tr> <td><span style="color: rgb(255, 99, 71);">*</span> </td> <td>Телефон:</td> <td><input type="text" id="phone" value="" /></td> </tr>
     
      <tr> <td></td> <td>Сообщение</td> <td><textarea id="message"></textarea></td> </tr>
     
      <tr> <td></td> <td></td> <td class="msg-btn"><input type="button" id="submit" value="Отправить" style="margin-left: 0px;" /></td> </tr>
     </tbody>
   </table>
 </div>
 
<h2 class="red">При записи с сайта предоставляем скидку в размере 5%*. 
  <br />
 *при записи c сайта на <font color="#262626">акцию </font>скидка не действует.</h2>
 <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>