<?php
$_SERVER["DOCUMENT_ROOT"] = str_replace('\testing\image_processing\App\Tests\unit', "", __DIR__);

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_NO_ACCELERATOR_RESET", true);
define("CHK_EVENT", true);
define("SITE_ID", "s1");

require_once __DIR__."/../../Models/Image/Type/ImageType.php";

$dir = new RecursiveDirectoryIterator(__DIR__."/../../Models");
foreach (new RecursiveIteratorIterator($dir) as $file) {
	if (!is_dir($file)) {
		if( fnmatch('*.php', $file) ){
			require_once $file;
		}
	}
}

function includeBitrixCore()
{
    $level = ob_get_level();
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
    while (ob_get_level() > $level) {
        ob_end_clean();
    }
    $GLOBALS["prod"] = false;
    define("TEST_MODE", true);
}