<?
use Codeception\Test\Unit;

use Models\BxModel;
use Models\Image\Type\Service\Detail as ServiceDetail;
use Models\Resizer;

class MakeResizedCopiesTest extends Unit
{
    /** @var \UnitTester */
    protected $tester;

    public function testResizing()
    {
		$bx = new BxModel;
		$images = $bx->getImages(ServiceDetail::class, 1438);
		$resizer = new Resizer;

		foreach ($images as $image){
			$resizer->makeCopies($image);
		}
	}

    public function _before(){
    	includeBitrixCore();
    }

}