<?
use Codeception\Test\Unit;

use Models\BxModel;
use Models\Image\Type\Action\Preview as ActionPreview;
use Models\Image\Type\Action\Detail as ActionDetail;
use Models\Image\Type\Diploma\Preview as DiplomaPreview;
use Models\Image\Type\Doctor\Certificate as DoctorCertificate;
use Models\Image\Type\Doctor\Detail as DoctorDetail;
use Models\Image\Type\Doctor\Preview as DoctorPreview;
use Models\Image\Type\Service\Detail as ServiceDetail;
use Models\Image\Type\Work\Detail as WorkDetail;
use Models\Image\Type\Work\Preview as WorkPreview;

class MainTest extends Unit
{
    /** @var \UnitTester */
    protected $tester;

    private $bx;

    public function testGetActionPreviewImageModels()
    {
    	$images = $this->bx->getImages(ActionPreview::class, 1436);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Action\Preview", $images);
		$this->tester->assertEquals("/upload/iblock/5a9/5a9bed58996f63ff5e3cf9a2d07325b5.png", $images[0]->src);

		$images = $this->bx->getImages(ActionPreview::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(3, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Action\Preview", $images);
	}

	public function testGetActionDetailImageModels()
    {
    	$images = $this->bx->getImages(ActionDetail::class, 1436);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Action\Detail", $images);
		$this->tester->assertEquals("/upload/iblock/193/1937d196d66e127d2547c5a71cfe2f29.png", $images[0]->src);

		$images = $this->bx->getImages(ActionDetail::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(2, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Action\Detail", $images);
	}

    public function testGetServiceDetailImageModels()
    {
    	$images = $this->bx->getImages(ServiceDetail::class, 1438);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Service\Detail", $images);
		$this->tester->assertEquals("/upload/iblock/71d/71df25adf4407b035ebf3fcfc48174ba.png", $images[0]->src);

		$images = $this->bx->getImages(ServiceDetail::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(3, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Service\Detail", $images);
	}

    public function testGetDoctorCertificateImageModels()
    {
		$images = $this->bx->getImages(DoctorCertificate::class, 24);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(2, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Doctor\Certificate", $images);
		$this->tester->assertEquals("/upload/iblock/a6d/a6d79cdee212a5cfbf5ff121c3a45805.jpg", $images[0]->src);
		$this->tester->assertEquals("/upload/iblock/d3e/d3e50356c23f71b6a471d5f59fe44efa.jpg", $images[1]->src);

		$images = $this->bx->getImages(DoctorCertificate::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(11, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Doctor\Certificate", $images);
		$this->tester->assertEquals("/upload/iblock/8f5/8f5d58a8604f877fddd93ad3852f1ae0.jpg", $images[0]->src);
	}

	public function testGetDiplomaPreviewImageModels()
    {
		$images = $this->bx->getImages(DiplomaPreview::class, 1441);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Diploma\Preview", $images);
		$this->tester->assertEquals("/upload/iblock/c68/c68e9228a1cbcad7e369b1be8d9e837f.png", $images[0]->src);

		$images = $this->bx->getImages(DiplomaPreview::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(8, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Diploma\Preview", $images);
		$this->tester->assertEquals("/upload/iblock/c68/c68e9228a1cbcad7e369b1be8d9e837f.png", $images[0]->src);
		$this->tester->assertEquals("/upload/iblock/ec4/ec4f3687dccf761f4a6820234fbba99e.png", $images[1]->src);
		$this->tester->assertEquals("/upload/iblock/a54/a54f4a8fe93d3d3f49d98bf4768f2aa6.png", $images[2]->src);
		$this->tester->assertEquals("/upload/iblock/6b2/6b2e3c55ed3fceb575cdc72aaabce88a.png", $images[3]->src);
		$this->tester->assertEquals("/upload/iblock/c68/c68e9228a1cbcad7e369b1be8d9e837f.png", $images[0]->src);
	}

	public function testGetDoctorDetailImageModels()
    {
		$images = $this->bx->getImages(DoctorDetail::class, 24);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Doctor\Detail", $images);
		$this->tester->assertEquals("/upload/iblock/1fb/1fb32bd29e735c55d0161e30f007a7a6.jpg", $images[0]->src);

		$images = $this->bx->getImages(DoctorDetail::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(2, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Doctor\Detail", $images);
	}

	public function testGetDoctorPreviewImageModels()
    {
		$images = $this->bx->getImages(DoctorPreview::class, 24);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Doctor\Preview", $images);
		$this->tester->assertEquals("/upload/iblock/adf/adf36336b5c65dd0f26e04f23862ae84.jpg", $images[0]->src);

		$images = $this->bx->getImages(DoctorPreview::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(19, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Doctor\Preview", $images);
	}

	public function testGetWorkDetailImageModels()
    {
		$images = $this->bx->getImages(WorkDetail::class, 198);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Work\Detail", $images);
		$this->tester->assertEquals("/upload/iblock/310/3105cd5c4a5beffc5d5ab9941e7e22bc.JPG", $images[0]->src);

		$images = $this->bx->getImages(WorkDetail::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(16, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Work\Detail", $images);
	}

	public function testGetWorkPreviewImageModels()
    {
		$images = $this->bx->getImages(WorkPreview::class, 198);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(1, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Work\Preview", $images);
		$this->tester->assertEquals("/upload/iblock/0ba/0ba05eac810cdf7da0639627da25d970.JPG", $images[0]->src);

		$images = $this->bx->getImages(WorkPreview::class);
		$this->tester->assertNotEmpty($images);
		$this->tester->assertCount(16, $images);
		$this->tester->assertContainsOnlyInstancesOf("Models\Image\Type\Work\Preview", $images);
	}

    public function _before(){
    	includeBitrixCore();
    	$this->bx = new BxModel;
    }

}