<?
namespace Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use System\BaseController;
use System\Helper;
use Models\BxModel;
use Models\Image\Resizer;

use Models\Image\Type\Action\Detail as ActionDetail;
use Models\Image\Type\Action\Preview as ActionPreview;
use Models\Image\Type\Diploma\Preview as DiplomaPreview;
use Models\Image\Type\Doctor\Certificate as DoctorCertificate;
use Models\Image\Type\Doctor\Detail as DoctorDetail;
use Models\Image\Type\Doctor\Preview as DoctorPreview;
use Models\Image\Type\Service\Detail as ServiceDetail;
use Models\Image\Type\Work\Detail as WorkDetail;
use Models\Image\Type\Work\Preview as WorkPreview;
use Models\Image\Type\Review\Preview as ReviewPreview;

use Models\Reporter\Text as TextReporter;
use Models\Reporter\Html as HtmlReporter;
use Models\DiskWriter;

class Controller extends BaseController
{
	public function processImageActionDetail(){$this->processImage(ActionDetail::class);}
	public function processImageActionPreview(){$this->processImage(ActionPreview::class);}
	public function processImageDiplomaPreview(){$this->processImage(DiplomaPreview::class);}
	public function processImageDoctorCertificate(){$this->processImage(DoctorCertificate::class);}
	public function processImageDoctorDetail(){$this->processImage(DoctorDetail::class);}
	public function processImageDoctorPreview(){$this->processImage(DoctorPreview::class);}
	public function processImageServiceDetail(){$this->processImage(ServiceDetail::class);}
	public function processImageWorkDetail(){$this->processImage(WorkDetail::class);}
	public function processImageWorkPreview(){$this->processImage(WorkPreview::class);}
	public function processImageReviewPreview(){$this->processImage(ReviewPreview::class);}

    private function processImage(string $className): string
    {
    	$this->initExternalModels();

		$bxModel = new BxModel;
		$resizer = new Resizer;

    	$images = $bxModel->getImages($className);

    	$results = [];
    	foreach ($images as $image){
    		try{
				$result = $resizer->makeCopies($image);
				$results[] = $result;
			}catch (\Exception $e){
    			die($e->getMessage());
			}
		}

		$textReporter = new TextReporter;
		$textReport = $textReporter->getReport($results);

		$diskWriter = new DiskWriter;
		$arrLogPath = $diskWriter->save($className::SLUG, $textReport);

		$htmlReporter = new HtmlReporter;
		$htmlReport = $htmlReporter->getReport($className::TITLE, $results, $arrLogPath);

		return $this->render($htmlReport);
    }

	private function initExternalModels()
	{
		$modelsPath = $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/ImageResize/Models";
		require_once $modelsPath."/Image/Type/ImageType.php";

		$dir = new \RecursiveDirectoryIterator($modelsPath);
		foreach (new \RecursiveIteratorIterator($dir) as $file) {
			if (!is_dir($file)) {
				if( fnmatch('*.php', $file) ){
					require_once $file;
				}
			}
		}
	}

}
