<?
use System\Helper;

$controller = [
	["processImageActionDetail", "Акции - Детальное изображение"],
	["processImageActionPreview", "Акции - Анонсовое изображение"],
	["divider"],
	["processImageDiplomaPreview", "Дипломы - Анонсовое изображение"],
	["divider"],
	["processImageDoctorCertificate", "Доктора - Сертификаты"],
	["processImageDoctorDetail", "Доктора - Детальное изображение"],
	["processImageDoctorPreview", "Доктора - Анонсовое изображение"],
	["divider"],
	["processImageServiceDetail", "Услуги - Детальное изображение"],
	["divider"],
	["processImageWorkDetail", "Работы - Детальное изображение"],
	["processImageWorkPreview", "Работы - Анонсовое изображение"],
	["divider"],
	["processImageReviewPreview", "Отзывы - Анонсовое изображение"],
];
Helper::renderControllers($appRoot, $controller, "Обработать изображения");