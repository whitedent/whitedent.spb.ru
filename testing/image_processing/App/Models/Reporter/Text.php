<?
namespace Models\Reporter;

class Text
{
    public function getReport(array $results): string
    {
		if(empty($results)){
			return "Не были переданы изображения для обработки";
		}

		$string = "";

		$countResizedImages = array_reduce(
			$results,
			function(int $countResizedImages, array $result): int {
				$countResizedImages += count($result["copies"]);
				return $countResizedImages;
			},
			0
		);

		$string .= "Обработано изображений: ".count($results)."\n\n";

		if(!$countResizedImages){
			$string .= "Изображения в новом размере созданы не были"."\n\n";
		}else{
			$string .= "Созданно изображений в новом размере: ".$countResizedImages."\n\n";
		}

		foreach ($results as $result){
			$item = $result["src"]." (".count($result["copies"]).")";
			if($result["copies"]){
				$item .= ": ".implode(", ", $result["copies"]);
			}
			$string .= $item."\n\n";
		}

        return $string;
    }

}