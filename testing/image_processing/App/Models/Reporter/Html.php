<?
namespace Models\Reporter;

use System\Helper;

class Html
{
    public function getReport(string $title, array $results, $arrLogPath): string
    {
        if(empty($results)){
			$message = "<p>Не были переданы изображения для обработки</p>";
            return Helper::panelSucces($message, "Готово");
        }

		$countResizedImages = array_reduce(
			$results,
			function(int $countResizedImages, array $result): int {
				$countResizedImages += count($result["copies"]);
				return $countResizedImages;
			},
			0
		);

        $message = "<p>Обработано изображений: |".count($results)."|</p>";

        if(!$countResizedImages){
			$message.= "<p>Изображения в новом размере созданы не были</p>";
		}else{
			$message.= "<p>Созданно изображений в новом размере: |".$countResizedImages."|</p>";
		}

		$logLink = '<a href="'.$arrLogPath["url"].'" target="_blank">'.$arrLogPath["title"].'</a>';
		$message .= "<p>Подробности в логе (|".$logLink."|)</p>";

        return Helper::panelInfo($message, $title." - Готово");
    }

}