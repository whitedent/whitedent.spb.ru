<?
namespace Models;

class DiskWriter
{
    public function save(string $slug, string $string): array
    {
        $path = "/testing/image_processing/logs/";
        $fileName = "result-".$slug."-".date("Y-m-d_H-i-s").".txt";
        $fullPath = $_SERVER["DOCUMENT_ROOT"].$path.$fileName;

        if(file_put_contents($fullPath, $string) === false){
            throw new \Exception("Ошибка во время сохранения лога в |".$fileName."|");
        }

        return ["title" => $fileName, "url" => $path.$fileName];
    }

}