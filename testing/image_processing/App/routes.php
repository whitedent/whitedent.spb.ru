<?
use Controllers\Controller;

$app->get("/processImageActionDetail", Controllers\Controller::class.":processImageActionDetail");
$app->get("/processImageActionPreview", Controllers\Controller::class.":processImageActionPreview");
$app->get("/processImageDiplomaPreview", Controllers\Controller::class.":processImageDiplomaPreview");
$app->get("/processImageDoctorCertificate", Controllers\Controller::class.":processImageDoctorCertificate");
$app->get("/processImageDoctorDetail", Controllers\Controller::class.":processImageDoctorDetail");
$app->get("/processImageDoctorPreview", Controllers\Controller::class.":processImageDoctorPreview");
$app->get("/processImageServiceDetail", Controllers\Controller::class.":processImageServiceDetail");
$app->get("/processImageWorkDetail", Controllers\Controller::class.":processImageWorkDetail");
$app->get("/processImageWorkPreview", Controllers\Controller::class.":processImageWorkPreview");
$app->get("/processImageReviewPreview", Controllers\Controller::class.":processImageReviewPreview");