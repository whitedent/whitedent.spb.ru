<?
namespace System;

use Exceptions\AbortException;
use Exception;

class Result
{
    private $data;
    private $title;
    private $type;

    function __construct($data, $title, $type = "info")
    {
        $this->data = $data;
        $this->title = $title;
        $this->type = $type;
    }

    public static function get($resultObj)
    {
        if($resultObj instanceof AbortException){
            return Helper::message($resultObj->getMessage(), "danger", true);
        }

        if($resultObj instanceof Exception){
            $message = "|".$resultObj->getMessage()."|";
            $message .= "<br>|File:| ".$resultObj->getFile();
            $message .= "<br>|Line:| ".$resultObj->getLine();
            return Helper::panel($message, "Ошибка", "danger", true);
        }

        return Helper::panel($resultObj->data, $resultObj->title, $resultObj->type, true);
    }

    public static function show($resultObj)
    {
        echo self::get($resultObj);
    }

}
