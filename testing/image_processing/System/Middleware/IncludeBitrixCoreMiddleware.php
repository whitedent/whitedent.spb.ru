<?
namespace Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class IncludeBitrixCoreMiddleware
{
    public function __invoke(Request $slimRequest, Response $slimResponse, callable $next)
    {
        $bitrixCore = $_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php";

        if(is_file($bitrixCore)){
            ob_start();
            require($bitrixCore);
            ob_end_clean();
        }

//        error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
        error_reporting(E_ALL ^ E_STRICT ^ E_DEPRECATED ^ E_NOTICE);

        $slimResponse = $next($slimRequest, $slimResponse);

        return $slimResponse;
    }

}