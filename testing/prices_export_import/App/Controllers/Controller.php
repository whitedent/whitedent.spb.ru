<?
namespace Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use System\BaseController;
use System\Helper;

use Models\Price\Model as PriceModel;
use Models\FileStorage;

class Controller extends BaseController
{
	public function savePrices()
	{
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();

		$fileStorage = new FileStorage;

		$fileStorage->write($prices);

		$href = '/testing/prices_export_import/App/Resources/prices_export.php';
		$file = '<a href="'.$href.'" target="_blank">файле</a>';

		return $this->render(
			Helper::panelSucces("Цены сохранены в |".$file."|", "Готово")
		);
	}

	public function updatePrices()
	{
		$fileStorage = new FileStorage;
		$prices = $fileStorage->read();

		$priceModel = new PriceModel;
		$priceModel->savePrices($prices);

		return $this->render(
			Helper::panelSucces("Цены успешно обновлены", "Готово")
		);
	}

}
