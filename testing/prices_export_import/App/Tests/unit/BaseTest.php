<?
use Codeception\Test\Unit;
use Models\Price\Model as PriceModel;
use Models\FileStorage;

class BaseTest extends Unit
{
    /** @var \UnitTester */
    protected $tester;

    public function testGetBxPrices()
    {
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();

		$this->tester->assertCount(303, $prices);
		$this->tester->assertContainsOnlyInstancesOf("Models\Price\Element", $prices);
	}

	public function testWriteBxPrices()
    {
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();

		$fileStorage = new FileStorage;

		if(file_exists($fileStorage->path)){
			unlink($fileStorage->path);
		}

		$this->tester->expectThrowable(new Exception("Empty prices array"), function () {
			$fileStorage = new FileStorage;
			$fileStorage->write([]);
		}, $fileStorage);

		$fileStorage->write($prices);

		$this->tester->assertFileExists($fileStorage->path);
	}

	public function testReadBxPrices()
	{
		$fileStorage = new FileStorage;

		if(file_exists($fileStorage->path)){
			unlink($fileStorage->path);
		}

		$this->tester->expectThrowable(new Exception("По пути /testing/prices_export_import/App/Resources/ не найден файл экспорта цен prices_export.php"), function () {
			$fileStorage = new FileStorage;
			$fileStorage->read();
		}, $fileStorage);

		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();
		$fileStorage->write($prices);

		$prices = $fileStorage->read();

		$this->tester->assertCount(303, $prices);
		$this->tester->assertContainsOnlyInstancesOf("Models\Price\Element", $prices);
	}

	public function testSaveBxPrices()
	{
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();

		$fileStorage = new FileStorage;

		if(file_exists($fileStorage->path)){
			unlink($fileStorage->path);
		}

		$fileStorage->write($prices);
		$prices = $fileStorage->read();

		$countOperations = $priceModel->savePrices($prices);
		$this->tester->assertEquals(303, $countOperations);
	}



    public function _before()
	{
    	includeBitrixCore();
    }

}