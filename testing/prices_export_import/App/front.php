<?
use System\Helper;

$controller = [
	["savePrices", "Сохранить цены", "success"],
	["updatePrices", "Обновить цены", "danger"],
];
Helper::renderControllers($appRoot, $controller);