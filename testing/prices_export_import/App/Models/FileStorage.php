<?
namespace Models;

use Exception;

class FileStorage
{
	public $path = __DIR__."/../Resources/prices_export.php";

	public function write(array $prices): bool
	{
		if(!$prices){
			throw new Exception("Empty prices array");
		}

		if(!file_put_contents($this->path, serialize($prices))){
			throw new Exception("Unable to save prices");
		}

		return true;
	}

	public function read(): array
	{
		if(!file_exists($this->path)){
			$path = "/testing/prices_export_import/App/Resources/";
			$message = "По пути ".$path." не найден файл экспорта цен prices_export.php";
			throw new Exception($message);
		}

		$prices = file_get_contents($this->path);
		$prices = unserialize($prices);

		return $prices;
	}

}