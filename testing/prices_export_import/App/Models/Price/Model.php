<?
namespace Models\Price;

use Models\Price\Element as PriceElement;

class Model
{
	public function getPrices(int $limit = 0): array
	{
		\CModule::IncludeModule("iblock");

		$arOrder = [];

        $arFilter = [
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
			"ACTIVE" => "Y"
        ];

        $arGroupBy = false;

        $arNavStartParams = false;
        if($limit){
            $arNavStartParams = ["nTopCount" => $limit];
        }

        $arSelectFields = ["IBLOCK_ID", "ID", "NAME"];

        $res = \CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

        $prices = [];
        while($ob = $res->GetNextElement()){

        	$fields = $ob->GetFields();
        	$props = $ob->GetProperties();

        	$id = (int)$fields["ID"];
        	$value = (int)$props["PRICE"]["VALUE"];

			$prices[] = new PriceElement($id, $value);
        }

        return $prices;
	}

	public function savePrices(array $prices): int
	{
		$counter = 0;
		foreach ($prices as $price) {
			if ($this->isPriceElementExistInDb($price->id)) {
				$this->savePrice($price);
				$counter++;
			}
		}

		return $counter;
	}

	private function savePrice(PriceElement $price): bool
	{
		if(!\CIBlockElement::SetPropertyValueCode($price->id, "PRICE", $price->value)){
			throw new \Exception("Ошибка при обновлении цены");
		}

		return true;
	}

	private function isPriceElementExistInDb(int $id): bool
	{
		$arFilter = [
			"IBLOCK_ID" => ID_IBLOCK_PRICES,
			"ID" => $id
		];

		return (bool)\CIBlockElement::GetList([], $arFilter, [], false, ["IBLOCK_ID", "ID"]);
	}

}

