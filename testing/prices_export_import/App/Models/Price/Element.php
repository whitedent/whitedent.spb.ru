<?
namespace Models\Price;

class Element
{
	public $id;
	public $value;

	public function __construct(string $id, int $value)
	{
		$this->id = (int)$id;
		$this->value = $value;
	}

}