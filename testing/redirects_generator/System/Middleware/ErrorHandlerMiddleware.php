<?
namespace Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use System\BaseController;

class ErrorHandlerMiddleware
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, $exception)
    {
        $response = $response->withStatus(500);

        $baseController = new BaseController($this->container);
        $baseController->render($exception);

        return $response;
    }
}