<?php
namespace Middleware;

use System\BaseController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use System\Result;

class NotFoundHandlerMiddleware
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response)
    {
        $uri = $request->getUri();
        $path = $uri->getPath();

        $response = $response->withStatus(404);

        $result = new Result("Не найден маршрут для комманды: |".$path."|", "Ошибка", "danger");

        $baseController = new BaseController($this->container);
        $baseController->render($result);

        return $response;
    }

}