<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $title ?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?= $baseUrl ?>favicon.ico" />
		<link href="<?= $baseUrl ?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= $baseUrl ?>css/base.css" rel="stylesheet">

		<? if(!empty($style)): ?>
		    <link href="<?= $style ?>" rel="stylesheet">
		<? endif ?>

		<? if($useJquery): ?>
            <script src="<?= $baseUrl ?>js/jquery-3.3.1.min.js"></script>
		<? endif ?>

	</head>

	<body>

		<div class="container">

			<div class="page-header">
				<? if(!empty($appRoot)): ?>
					<h1><a href="<?= $appRoot ?>"><?= $title ?></a></h1>
				<? else: ?>
					<h1><?= $title ?></h1>
				<? endif ?>
			</div>

			<? if(!empty($front)): ?>
			<div class="content">
				<? include $front ?>
			</div>
			<? endif ?>

			<div class="result">
				<? if(!empty($result)): ?>
					<?= $result ?>
				<? endif ?>
			</div>

			<? if(!empty($footer)): ?>
			<div class="bs-callout bs-callout-info">
				<?= System\Helper::beautifyMessage($footer) ?>
			</div>
			<? endif ?>

		</div><!-- /.container -->

  </body>
</html>