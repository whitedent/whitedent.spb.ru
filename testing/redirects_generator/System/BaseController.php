<?
namespace System;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Exceptions\AbortException;
use Exception;

class BaseController
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke()
    {
        return $this->render();
    }

    public function render($result = null)
    {
        $request = $this->container->request;
        $response = $this->container->response;

        $uri =  $request->getUri();
        $basePath = $uri->getBasePath();

        $config = $this->container->get("settings");

        $data = [
            "title" => $this->container["settings"]->get("title"),
            "appRoot" => $basePath."/",
            "baseUrl" => $basePath."/System/template/",
            "front" => __DIR__."/../App/front.php",
            "useJquery" => $config["useJquery"],
            "request" => $request,
        ];

        if($result instanceof AbortException){
            $data["title"] = "Ошибка";
            $data["front"] = null;
        }

        if($result instanceof Exception || $result instanceof Result){
            $result = Result::get($result);
        }

        if($result){
            $data["result"] = $result;
        }

        return $this->container->view->render($response, "index.php", $data);
    }

}