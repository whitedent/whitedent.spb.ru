<?php
use Codeception\Example;

class RedirectsCest
{
	/** @var AcceptanceTester */
	protected $tester;

	public function _before(AcceptanceTester $I)
	{
		$this->tester = $I;
	}

	/**
	 * @dataProvider pageProvider
	 */
	public function runTests(AcceptanceTester $I, Example $example)
	{
		$this->tester->amOnPage($example["from"]);

		if($example["to"] == "410"){
			$this->tester->seeResponseCodeIs(410);
		}else{
			$this->tester->seeResponseCodeIs(200, "Проверка кода ответа");
			$this->tester->seeCurrentUrlEquals($example["to"]);
		}
	}

	protected function pageProvider()
	{
		return array (
  0 => 
  array (
    'from' => '/about/',
    'to' => '/o-klinike/',
  ),
  1 => 
  array (
    'from' => '/about/obraschenie_glavnogo_vracha/',
    'to' => '/o-klinike/obraschenie-direktora/',
  ),
  2 => 
  array (
    'from' => '/about/sotrudnichestvo.php',
    'to' => '/o-klinike/vakansii/',
  ),
  3 => 
  array (
    'from' => '/akcii/',
    'to' => '/o-klinike/akcii/',
  ),
  4 => 
  array (
    'from' => '/articles/',
    'to' => '/patsientam/stati/',
  ),
  5 => 
  array (
    'from' => '/articles/39/',
    'to' => '/patsientam/stati/',
  ),
  6 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/',
    'to' => '/patsientam/stati/',
  ),
  7 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/air-flow-ulybka-uspekha/',
    'to' => '/patsientam/stati/air-flow-ulybka-uspekha/',
  ),
  8 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/antibakterialnyy-agent-povyshaet-effektivnost-zubnoy-pasty/',
    'to' => '/patsientam/stati/',
  ),
  9 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/bakterii-sozdayut-otpechatok-paltsa-v-vashem-rtu/',
    'to' => '/patsientam/stati/',
  ),
  10 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/bazalnaya-implantatsiya/',
    'to' => '/patsientam/stati/bazalnaya-implantatsiya/',
  ),
  11 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/belosnezhnye-zuby-sovety-stomatologa/',
    'to' => '/patsientam/stati/belosnezhnye-zuby-sovety-stomatologa/',
  ),
  12 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/issledovateli-rekomenduyut-sokratit-kolichestvo-sladkikh-pishchevykh-dobavok-dlya-snizheniya-kariesa/',
    'to' => '/patsientam/stati/issledovateli-rekomenduyut-sokratit-kolichestvo-sladkikh-pishchevykh-dobavok-dlya-snizheniya-kariesa/',
  ),
  13 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/istoriya-zubnykh-implantatov/',
    'to' => '/patsientam/stati/istoriya-zubnykh-implantatov/',
  ),
  14 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-chasto-nuzhno-provodit-chistku-naleta-kurilshchika/',
    'to' => '/patsientam/stati/kak-chasto-nuzhno-provodit-chistku-naleta-kurilshchika/',
  ),
  15 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-poluchit-maksimum-ot-vizita-k-stomatologu/',
    'to' => '/patsientam/stati/kak-poluchit-maksimum-ot-vizita-k-stomatologu/',
  ),
  16 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-postavit-diagnoz-po-zubam/',
    'to' => '/patsientam/stati/',
  ),
  17 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-pravilno-chistit-zuby/',
    'to' => '/patsientam/stati/kak-pravilno-chistit-zuby/',
  ),
  18 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-pravilno-ukhazhivat-za-zubnymi-implantami/',
    'to' => '/patsientam/stati/kak-pravilno-ukhazhivat-za-zubnymi-implantami/',
  ),
  19 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-sokhranit-zdorovye-zuby-vashikh-detey-v-etom-godu/',
    'to' => '/patsientam/stati/',
  ),
  20 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-vliyaet-kurenie-na-zdorove-dyesen/',
    'to' => '/patsientam/stati/kak-vliyaet-kurenie-na-zdorove-dyesen/',
  ),
  21 => 
  array (
    'from' => '/articles/obshchaya-informatsiya/kak-zubnye-implantaty-mogut-uluchshit-zdorove-polosti-rta/',
    'to' => '/patsientam/stati/kak-zubnye-implantaty-mogut-uluchshit-zdorove-polosti-rta/',
  ),
  22 => 
  array (
    'from' => '/ask/',
    'to' => '/patsientam/cons/ask/',
  ),
  23 => 
  array (
    'from' => '/callback.php',
    'to' => '410',
  ),
  24 => 
  array (
    'from' => '/ceny/',
    'to' => '/tseny/',
  ),
  25 => 
  array (
    'from' => '/ceny/implantacija-zubov/',
    'to' => '/tseny/implantologiya/',
  ),
  26 => 
  array (
    'from' => '/ceny/kompyuternaya-tomografiya-pazuh-nosa/',
    'to' => '/tseny/rentgenografiya/',
  ),
  27 => 
  array (
    'from' => '/consultation/implantatsiya/implantatsiya-/',
    'to' => '/patsientam/cons/implantatsiya/implantatsiya-/',
  ),
  28 => 
  array (
    'from' => '/consultation/implantatsiya/implantatsiya-2/',
    'to' => '/patsientam/cons/implantatsiya/implantatsiya-2/',
  ),
  29 => 
  array (
    'from' => '/consultation/implantatsiya/implantatsiya-3/',
    'to' => '/patsientam/cons/implantatsiya/implantatsiya-3/',
  ),
  30 => 
  array (
    'from' => '/protezirovanie-zubov/zubnye-koronki/vremennye/',
    'to' => '/uslugi/protezirovanie-zubov/zubnye-koronki/vremennye/',
  ),
  31 => 
  array (
    'from' => '/protezirovanie-zubov/zubnye-protezy/',
    'to' => '/uslugi/protezirovanie-zubov/zubnye-protezy/',
  ),
  32 => 
  array (
    'from' => '/protezirovanie-zubov/zubnye-protezy/ustanovka/',
    'to' => '/uslugi/protezirovanie-zubov/zubnye-protezy/ustanovka/',
  ),
  33 => 
  array (
    'from' => '/protezirovanie-zubov/zubnye-vkladki/',
    'to' => '/uslugi/protezirovanie-zubov/zubnye-vkladki/',
  ),
  34 => 
  array (
    'from' => '/protezirovanie-zubov/zubnye-vkladki/keramicheskaya/',
    'to' => '/uslugi/protezirovanie-zubov/zubnye-vkladki/keramicheskaya/',
  ),
  35 => 
  array (
    'from' => '/protezirovanie-zubov/zubnye-vkladki/kultevaya/',
    'to' => '/uslugi/protezirovanie-zubov/zubnye-vkladki/kultevaya/',
  ),
  36 => 
  array (
    'from' => '/protezirovanie-zubov/zubnye-vkladki/pod-koronku/',
    'to' => '/uslugi/protezirovanie-zubov/zubnye-vkladki/pod-koronku/',
  ),
  37 => 
  array (
    'from' => '/services/',
    'to' => '/uslugi/',
  ),
  38 => 
  array (
    'from' => '/skolko-stoit-vstavit-zuby/',
    'to' => '/',
  ),
  39 => 
  array (
    'from' => '/stomatologiya-kredit/',
    'to' => '/patsientam/kredit/',
  ),
  40 => 
  array (
    'from' => '/terapiya',
    'to' => '/uslugi/terapiya/',
  ),
  41 => 
  array (
    'from' => '/terapiya/',
    'to' => '/uslugi/terapiya/',
  ),
  42 => 
  array (
    'from' => '/terapiya/drugie/',
    'to' => '/uslugi/terapiya/drugie/',
  ),
  43 => 
  array (
    'from' => '/terapiya/drugie/emali-zubov/',
    'to' => '/uslugi/terapiya/drugie/emali-zubov/',
  ),
  44 => 
  array (
    'from' => '/terapiya/drugie/lechenie-periodontita/',
    'to' => '/uslugi/terapiya/drugie/lechenie-periodontita/',
  ),
  45 => 
  array (
    'from' => '/terapiya/lechenie-pulpita/chistka-zubnykh-kanalov/',
    'to' => '/uslugi/terapiya/lechenie-pulpita/chistka-zubnykh-kanalov/',
  ),
  46 => 
  array (
    'from' => '/terapiya/lechenie-pulpita/depulpirovanie/',
    'to' => '/uslugi/terapiya/lechenie-pulpita/depulpirovanie/',
  ),
  47 => 
  array (
    'from' => '/terapiya/lechenie-pulpita/kanalov-zuba/',
    'to' => '/uslugi/terapiya/lechenie-pulpita/kanalov-zuba/',
  ),
  48 => 
  array (
    'from' => '/terapiya/lechenie-pulpita/perelechivanie/',
    'to' => '/uslugi/terapiya/lechenie-pulpita/perelechivanie/',
  ),
  49 => 
  array (
    'from' => '/terapiya/lechenie-pulpita/plombirovka-i-rasplombirovka-kanala/',
    'to' => '/uslugi/terapiya/lechenie-pulpita/plombirovka-i-rasplombirovka-kanala/',
  ),
  50 => 
  array (
    'from' => '/terapiya/lechenie-zubov/',
    'to' => '/uslugi/terapiya/',
  ),
  51 => 
  array (
    'from' => '/terapiya/lechenie-zubov/bez-boli/',
    'to' => '/uslugi/terapiya/bez-boli/',
  ),
  52 => 
  array (
    'from' => '/terapiya/lechenie-zubov/flyusa/',
    'to' => '/uslugi/terapiya/flyusa/',
  ),
  53 => 
  array (
    'from' => '/terapiya/lechenie-zubov/mudrosti/',
    'to' => '/uslugi/terapiya/mudrosti/',
  ),
  54 => 
  array (
    'from' => '/terapiya/lechenie-zubov/pod-sedaciey/',
    'to' => '/uslugi/terapiya/pod-sedaciey/',
  ),
  55 => 
  array (
    'from' => '/ustanovka-zubov/',
    'to' => '/uslugi/hirurgiya/ustanovka-zubov/',
  ),
  56 => 
  array (
    'from' => '/zapis-na-priem/',
    'to' => '/',
  ),
  57 => 
  array (
    'from' => '/xxx/',
    'to' => '/o-klinike/yyy/',
  ),
);
	}

}
?>

