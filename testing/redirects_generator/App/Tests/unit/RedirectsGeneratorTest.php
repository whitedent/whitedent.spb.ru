<?
use Codeception\Test\Unit;
use Models\Redirect;
use Models\Xls;
use Models\Writer\Redirects as WriterRedirects;
use Models\Writer\Tests as WriterTests;

class RedirectsGeneratorTest extends Unit
{
    /** @var \UnitTester */
    protected $tester;

    /** @var $xls Xls */
    private $xls;

    /** @var $redirect Redirect */
    private $redirect;

    public function _before()
    {
        $this->xls = new Xls(__DIR__."/../_data/test.xlsx");
        $this->redirect = new Redirect();
    }

    public function testGettingXlsRefItems()
    {
        $refItems = $this->xls->getRefItems();
        $this->tester->assertIsArray($refItems);
        $this->tester->assertNotEmpty($refItems);
        $this->tester->assertCount(61, $refItems);
    }

	public function testGettingRedirectsItems()
	{
		$refItems = $this->xls->getRefItems();
		$redirects = $this->redirect->getRedirects($refItems);

		$this->tester->assertIsArray($redirects);
		$this->tester->assertNotEmpty($redirects);
		$this->tester->assertContainsOnlyInstancesOf("Models\Redirect", $redirects);
		$this->tester->assertCount(58, $redirects);

		$this->tester->assertEquals("RewriteRule ^about/$ /o-klinike/ [R=301,L]", $redirects[0]->rule);
		$this->tester->assertEquals("RewriteRule ^callback.php$ - [R=410,L]", $redirects[23]->rule);
		$this->tester->assertEquals("RewriteRule ^xxx/$ /o-klinike/yyy/ [R=301,L]", $redirects[57]->rule);
    }

	public function testWriteRedirects()
	{
		$refItems = $this->xls->getRefItems();
		$redirects = $this->redirect->getRedirects($refItems);

		$writerRedirects = new WriterRedirects($redirects);
		$writerRedirects->save();

		$expected = file_get_contents(__DIR__."/../_data/redirects.txt");
		$actual = file_get_contents($writerRedirects->path);
		$this->tester->assertEquals($expected, $actual);
	}

	public function testWriteAcceptanceTests()
	{
		$refItems = $this->xls->getRefItems();
		$redirects = $this->redirect->getRedirects($refItems);

		$writerTests = new WriterTests($redirects);
		$writerTests->save();

		$expected = file_get_contents(__DIR__."/../_data/RedirectsCest.php");
		$actual = file_get_contents($writerTests->path);
		$this->tester->assertEquals($expected, $actual);

	}

}