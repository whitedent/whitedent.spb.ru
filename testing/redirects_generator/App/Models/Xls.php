<?
namespace Models;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Xls
{
    private $filePath;

    public function __construct(string $filePath = null)
    {
        if(!empty($filePath)){
            $this->filePath = $filePath;
        }
    }

    public function getRefItems(): array
    {
        $spreadsheet = $this->getSpreadsheet();

        $worksheets = $this->getWorksheets($spreadsheet);
        $worksheet = $this->geFirstWorksheet($worksheets);

        return $this->getRefItemsFromWorksheet($worksheet);
    }

    private function getSpreadsheet(): Spreadsheet
    {
        $fileName = "ref.xlsx";

        if(empty($this->filePath)){
            $filePath = __DIR__."/../Resources/".$fileName;
        }else{
            $filePath = $this->filePath;
        }

        if(!file_exists($filePath)){
            throw new \Exception("Не найден файл с данными |".$fileName."|");
        }

        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);

        return $reader->load($filePath);
    }

    private function getWorksheets(Spreadsheet $spreadsheet): array
    {
        $worksheets = [];
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            $worksheets[] = $worksheet;
        }

        return $worksheets;
    }

    private function geFirstWorksheet(array $worksheets): Worksheet
    {
        if(empty($worksheets[0])){
			throw new \Exception("В xls-файле нет листов");
		}

		return $worksheets[0];
    }

    private function getRefItemsFromWorksheet(Worksheet $worksheet): array
    {
        $refItems = [];
        $headers = [];

        foreach ($worksheet->getRowIterator() as $rowIndex => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            if($rowIndex == 1){
                foreach ($cellIterator as $cellIndex => $cell) {
                    $cellValue = $cell->getValue();
                    if(!$cellValue){continue;}
                    $headers[$cellIndex] = $cellValue;
                }
            }else{
                $refItem = [];
                foreach ($cellIterator as $cellIndex => $cell) {
                    $cellValue = $cell->getValue();
                    if(!$cellValue OR empty($headers[$cellIndex])){
                        continue;
                    }
                    $refItem[] = $cellValue;
                }
				$refItems[] = $refItem;
            }
        }

        return $refItems;
    }

}