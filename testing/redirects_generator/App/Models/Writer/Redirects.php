<?
namespace Models\Writer;

use Models\Redirect;

class Redirects
{
	public $path;
	private $redirects;

	public function __construct(array $redirects)
	{
		$this->path = __DIR__."/../../Result/redirects.txt";
		$this->redirects = $redirects;
	}

	public function save()
	{
		if(!file_put_contents($this->path, $this->prepare())){
			throw new \Exception("Не удалось сохранить редиректы в файл");
		}

		return true;
	}

	private function prepare(): string
	{
		$string = "";

		/** @var Redirect $redirect*/
		foreach ($this->redirects as $redirect){
			$string .= $redirect->rule."\n";
		}

		return $string;
	}

}
