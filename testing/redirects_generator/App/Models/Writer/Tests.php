<?
namespace Models\Writer;

use Models\Redirect;

class Tests
{
	public $path;
	private $redirects;

	public function __construct(array $redirects)
	{
		$this->path = $_SERVER["DOCUMENT_ROOT"]."/tests/acceptance/RedirectsCest.php";
		$this->redirects = $redirects;
	}

	public function save()
	{
		if(!file_put_contents($this->path, $this->prepare())){
			throw new \Exception("Не удалось сохранить тесты в файл");
		}

		return true;
	}

	private function prepare(): string
	{
		$string = file_get_contents(__DIR__."/../../Resources/RedirectsCestExample.php");

		$redirects = array_map(function (Redirect $redirect){
			return ["from" => $redirect->from, "to" => $redirect->to];
		}, $this->redirects);

		$string = str_replace('RedirectsCestExample', 'RedirectsCest', $string);
		$string = str_replace("'___REPLACE___'", var_export($redirects, true), $string);

		return $string;
	}
}