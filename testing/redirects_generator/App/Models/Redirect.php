<?
namespace Models;

class Redirect
{
	public $from;
	public $to;
	public $rule;

	public function __construct(string $from = "", string $to = "")
	{
		$this->from = $from;
		$this->to = $to;
	}

	public function getRedirects(array $refItems): array
	{
		$redirects = $this->addRedirects($refItems);
		$redirects = $this->rejectIncorrectRedirects($redirects);
		$redirects = $this->addRules($redirects);

		return $redirects;
	}

	private function addRedirects(array $refItems): array
	{
		return array_map(function (array $refItem): Redirect {
			return new self($refItem[0], $refItem[1]);
		}, $refItems);
	}

	private function rejectIncorrectRedirects(array $redirects): array
	{
		return array_filter($redirects, function (Redirect $redirect): bool {
			if(
				($redirect->from == $redirect->to) ||
				(strpos($redirect->from, '%') !== false)
			){
				return false;
			}
			return true;
		});
	}

	private function addRules(array $redirects): array
	{
		return array_map(function (Redirect $redirect): Redirect {
			if($redirect->to == "410"){
				$redirect->rule = $this->getRule410($redirect->from);
			}else{
				$redirect->rule = $this->getRuleStandard($redirect->from, $redirect->to);
			}
			return $redirect;
		}, $redirects);
	}

	private function getRule410(string $from): string
	{
		return "RewriteRule ^".self::trimLeadingSlash($from)."$ - [R=410,L]";
	}

	private function getRuleStandard(string $from, string $to): string
	{
		return "RewriteRule ^".self::trimLeadingSlash($from)."$ ".$to." [R=301,L]";
	}

	private static function trimLeadingSlash(string $url): string
	{
		if(strpos($url, '/') === 0){
			return substr($url, 1);
		}

		return $url;
	}

}