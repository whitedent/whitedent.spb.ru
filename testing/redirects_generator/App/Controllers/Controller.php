<?
namespace Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use System\BaseController;
use System\Helper;
use Models\Xls;
use Models\Redirect;
use Models\Writer\Redirects as WriterRedirects;
use Models\Writer\Tests as WriterTests;

class Controller extends BaseController
{
    public function saveRedirects()
    {
		$xls = new Xls(__DIR__."/../Resources/ref.xlsx");
		$redirect = new Redirect();

		$refItems = $xls->getRefItems();
		$redirects = $redirect->getRedirects($refItems);

		$writerRedirects = new WriterRedirects($redirects);
		$writerRedirects->save();

		return $this->render(Helper::panelSucces("Редиректы сохранены", "Готово"));
    }

    public function saveTests()
    {
		$xls = new Xls(__DIR__."/../Resources/ref.xlsx");
		$redirect = new Redirect();

		$refItems = $xls->getRefItems();
		$redirects = $redirect->getRedirects($refItems);

		$writerTests = new WriterTests($redirects);
		$writerTests->save();

		return $this->render(Helper::panelSucces("Тесты сохранены", "Готово"));
    }

}
