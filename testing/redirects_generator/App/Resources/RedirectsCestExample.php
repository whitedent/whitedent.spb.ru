<?php
use Codeception\Example;

class RedirectsCestExample
{
	/** @var AcceptanceTester */
	protected $tester;

	public function _before(AcceptanceTester $I)
	{
		$this->tester = $I;
	}

	/**
	 * @dataProvider pageProvider
	 */
	public function runTests(AcceptanceTester $I, Example $example)
	{
		$this->tester->amOnPage($example["from"]);

		if($example["to"] == "410"){
			$this->tester->seeResponseCodeIs(410);
		}else{
			$this->tester->seeResponseCodeIs(200, "Проверка кода ответа");
			$this->tester->seeCurrentUrlEquals($example["to"]);
		}
	}

	protected function pageProvider()
	{
		return '___REPLACE___';
	}

}
?>

