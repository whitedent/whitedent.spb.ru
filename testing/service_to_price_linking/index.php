<?
use Middleware\CheckAccessMiddleware;
use Middleware\NotFoundHandlerMiddleware;
use Middleware\ErrorHandlerMiddleware;
use Middleware\NoTrailingSlashMiddleware;
use Middleware\IncludeBitrixCoreMiddleware;

chdir(__DIR__);

require "vendor/autoload.php";
require "System/BaseController.php";

$config = require "App/config.php";

$app = new \Slim\App(["settings" => $config]);
$container = $app->getContainer();

$container["view"] = new \Slim\Views\PhpRenderer("System/template/");

$app->get("/", System\BaseController::class); // корневой маршрут

require "App/routes.php";

$app->add(new NoTrailingSlashMiddleware());

if($config["checkAccess"]){
    $app->add(new CheckAccessMiddleware);
}

$app->add(new IncludeBitrixCoreMiddleware);

$container["notFoundHandler"] = function ($c){return new NotFoundHandlerMiddleware($c);};
$container["errorHandler"] = function ($c){return new ErrorHandlerMiddleware($c);};

error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);

$app->run();