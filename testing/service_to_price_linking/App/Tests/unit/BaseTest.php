<?
use Codeception\Test\Unit;
use Models\Price\Model as PriceModel;
use Models\Service\Model as ServiceModel;

class BaseTest extends Unit
{
    /** @var \UnitTester */
    protected $tester;

    public function testGetBxPrices()
    {
		$priceModel = new PriceModel;

    	$prices = $priceModel->getPrices();

		$this->tester->assertCount(166, $prices);
		$this->tester->assertContainsOnlyInstancesOf("Models\Price\Element", $prices);

		$this->tester->assertArrayHasKey(1178, $prices);
		$this->tester->assertArrayNotHasKey(1258, $prices);
		$this->tester->assertCount(3, $prices[1178]->serviceSections);
		$this->tester->assertEquals(155, $prices[1178]->serviceSections[0]->id);
		$this->tester->assertEquals(157, $prices[1178]->serviceSections[1]->id);
		$this->tester->assertEquals(160, $prices[1178]->serviceSections[2]->id);
		$this->tester->assertEquals("adin", $prices[1178]->serviceSections[0]->code);
		$this->tester->assertEquals("alfa-bio", $prices[1178]->serviceSections[1]->code);
		$this->tester->assertEquals("abatment", $prices[1178]->serviceSections[2]->code);
	}

	public function testGetServicesSections()
	{
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();
		$serviceModel = new ServiceModel;
		$servicesSections = $serviceModel->getServicesSections($prices);

		$this->tester->assertCount(136, $servicesSections);
		$this->tester->assertContainsOnlyInstancesOf("Models\Service\Section", $servicesSections);

		$this->tester->assertArrayHasKey(47, $servicesSections);
		$serviceSection = $servicesSections[47];
		$this->tester->assertEquals(47, $serviceSection->id);
		$this->tester->assertEquals("galileos-comfort", $serviceSection->code);
		$this->tester->assertCount(3, $serviceSection->pricesId);
		$this->tester->assertEquals(1335, $serviceSection->pricesId[0]);
		$this->tester->assertEquals(1336, $serviceSection->pricesId[1]);
		$this->tester->assertEquals(1293, $serviceSection->pricesId[2]);

		$this->tester->assertArrayHasKey(201, $servicesSections);
		$serviceSection = $servicesSections[201];
		$this->tester->assertEquals(201, $serviceSection->id);
		$this->tester->assertEquals("vestibuloplastika", $serviceSection->code);
		$this->tester->assertCount(2, $serviceSection->pricesId);
		$this->tester->assertEquals(938, $serviceSection->pricesId[0]);
		$this->tester->assertEquals(1278, $serviceSection->pricesId[1]);
	}

	public function testIsAllServicesSectionsExists()
	{
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();
		$serviceModel = new ServiceModel;
		$servicesSections = $serviceModel->getServicesSections($prices);
		$this->tester->assertTrue($serviceModel->isAllServicesSectionsExists($servicesSections));
	}

	public function testGetServicesElements()
	{
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();
		$serviceModel = new ServiceModel;

		$servicesSections = $serviceModel->getServicesSections($prices);
		$servicesElements = $serviceModel->getServicesElements($servicesSections);

		$this->tester->assertCount(134, $servicesElements);

		$this->tester->assertArrayHasKey(47, $servicesElements);
		$serviceElement = $servicesElements[47];
		$this->tester->assertEquals(1449, $serviceElement->id);
		$this->tester->assertCount(3, $serviceElement->pricesId);
		$this->tester->assertEquals(1335, $serviceElement->pricesId[0]);
		$this->tester->assertEquals(1336, $serviceElement->pricesId[1]);
		$this->tester->assertEquals(1293, $serviceElement->pricesId[2]);

		$this->tester->assertArrayHasKey(201, $servicesElements);
		$serviceElement = $servicesElements[201];
		$this->tester->assertEquals(1524, $serviceElement->id);
		$this->tester->assertCount(2, $serviceElement->pricesId);
		$this->tester->assertEquals(938, $serviceElement->pricesId[0]);
		$this->tester->assertEquals(1278, $serviceElement->pricesId[1]);
	}

	public function testUpdateServiceElements()
	{
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();
		$serviceModel = new ServiceModel;

		$propPrices = $serviceModel->getServiceElementPropertyValue(1524, "PRICES");
		$this->tester->assertNull($propPrices[0]);
		$propFaq = $serviceModel->getServiceElementPropertyValue(1524, "FAQ");
		$this->tester->assertEquals("1822", $propFaq[0]);
		$this->tester->assertEquals("1823", $propFaq[1]);
		$this->tester->assertEquals("1824", $propFaq[2]);

		$servicesSections = $serviceModel->getServicesSections($prices);
		$servicesElements = $serviceModel->getServicesElements($servicesSections);
		$this->tester->assertTrue($serviceModel->updateServiceElements($servicesElements));

		$propPrices = $serviceModel->getServiceElementPropertyValue(1524, "PRICES");
		$this->tester->assertEquals("938", $propPrices[0]);
		$this->tester->assertEquals("1278", $propPrices[1]);

		$propFaq = $serviceModel->getServiceElementPropertyValue(1524, "FAQ");
		$this->tester->assertEquals("1822", $propFaq[0]);
		$this->tester->assertEquals("1823", $propFaq[1]);
		$this->tester->assertEquals("1824", $propFaq[2]);
	}

    public function _before()
	{
    	includeBitrixCore();
    }

}