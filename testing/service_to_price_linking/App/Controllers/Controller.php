<?
namespace Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use System\BaseController;
use System\Helper;

use Models\Price\Model as PriceModel;
use Models\Service\Model as ServiceModel;

class Controller extends BaseController
{
	public function addServiceToPriceLinking()
	{
		$priceModel = new PriceModel;
		$prices = $priceModel->getPrices();
		$serviceModel = new ServiceModel;

		$servicesSections = $serviceModel->getServicesSections($prices);
		$servicesElements = $serviceModel->getServicesElements($servicesSections);
		$serviceModel->updateServiceElements($servicesElements);

		return $this->render(
			Helper::panelSucces("Дабавлена привязка Цен к Услугам", "Готово")
		);
	}

}
