<?
namespace Models\Price;

class Element
{
	public $id;
	public $name;
	public $serviceSections;

	public function __construct(string $id, string $name)
	{
		$this->id = (int)$id;
		$this->name = $name;
	}

}