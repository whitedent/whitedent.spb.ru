<?
namespace Models\Price;

use Models\Price\Element as PriceElement;
use Models\Service\Section as ServiceSection;

class Model
{
	public function getPrices(int $limit = 0): array
	{
		\CModule::IncludeModule("iblock");

		$arOrder = [];

        $arFilter = [
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
			"ACTIVE" => "Y",
			"!PROPERTY_SERVICE" => false
        ];

        $arGroupBy = false;

        $arNavStartParams = false;
        if($limit){
            $arNavStartParams = ["nTopCount" => $limit];
        }

        $arSelectFields = [
            "IBLOCK_ID",
            "ID",
            "NAME",
        ];

        $res = \CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

        $prices = [];
        while($ob = $res->GetNextElement()){

        	$fields = $ob->GetFields();
        	$props = $ob->GetProperties();

        	$priceId = (int)$fields["ID"];
        	$priceName = $fields["NAME"];
        	$serviceSectionsId = array_map(function($id){
        		return (int)$id;
			}, $props["SERVICE"]["VALUE"]);

        	if(!array_key_exists($priceId, $prices) && !empty($serviceSectionsId)){
				$price = new PriceElement($priceId, $priceName);
				$price->serviceSections = $this->getServiceSections($serviceSectionsId);
				$prices[$priceId] = $price;
			}
        }

        return $prices;
	}

	private function getServiceSections(array $serviceSectionsId): array
	{
		return array_filter(array_map(function (int $sectionId) : ?ServiceSection {
			$resource = \CIBlockSection::GetByID($sectionId);
			$section = $resource->Fetch();
			if(empty($section["CODE"])){
				return null;
			}
			return new ServiceSection((int)$section["ID"], $section["CODE"]);
		}, $serviceSectionsId));
	}

}

