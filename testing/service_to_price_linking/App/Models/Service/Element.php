<?
namespace Models\Service;

class Element
{
	public $id;
	public $pricesId;

	public function __construct(int $id, array $pricesId)
	{
		$this->id = $id;
		$this->pricesId = $pricesId;
	}

}