<?
namespace Models\Service;

class Section
{
	public $id;
	public $code;
	public $pricesId;

	public function __construct(int $id, string $code)
	{
		$this->id = $id;
		$this->code = $code;
	}

}