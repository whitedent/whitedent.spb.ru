<?
namespace Models\Service;
use Models\Service\Section as ServiceSection;
use Models\Service\Element as ServiceElement;
use Models\Price\Element as PriceElement;

class Model
{
	public function getServicesSections(array $prices)
	{
		$servicesSections = [];

		/** @var PriceElement $price*/
		foreach ($prices as $price){
			/** @var ServiceSection $serviceSection*/
			foreach ($price->serviceSections as $serviceSection){
				if(!array_key_exists($serviceSection->id, $servicesSections)){
					$serviceSection->pricesId = [$price->id];
					$servicesSections[$serviceSection->id] = $serviceSection;
				}else{
					$serviceSection = $servicesSections[$serviceSection->id];
					if(!in_array($price->id, $serviceSection->pricesId)){
						$serviceSection->pricesId[] = $price->id;
					}
				}
			}
		}

		return $servicesSections;
	}

	public function isAllServicesSectionsExists(array $servicesSections): bool
	{
		/** @var ServiceSection $serviceSection */
		foreach ($servicesSections as $serviceSection){
			$res = \CIBlockSection::GetByID($serviceSection);
			$bxSection = $res->Fetch();
			if(!$bxSection){
				return false;
			}
		}

		return true;
	}

	public function getAllServicesSectionsWoChildElementWithSameCode(array $servicesSections): array
	{
		$servicesSectionsWoChildElementWithSameCode = [];

		/** @var ServiceSection $serviceSection */
		foreach ($servicesSections as $serviceSection){
			$arOrder = [];
			$arFilter = ["IBLOCK_ID" => ID_IBLOCK_SERVICES, "CODE" => $serviceSection->code];
			$arGroupBy = [];
			$arNavStartParams = false;
			$arSelectFields = ["IBLOCK_ID", "ID", "NAME"];

			$count = \CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
			if(!$count){
				$servicesSectionsWoChildElementWithSameCode[] = $serviceSection;
			}
		}

		return $servicesSectionsWoChildElementWithSameCode;
	}

	public function getServicesElements(array $servicesSections): array
	{
		$servicesElements = [];

		/** @var ServiceSection $serviceSection */
		foreach ($servicesSections as $serviceSection){
			$arOrder = [];
			$arFilter = ["IBLOCK_ID" => ID_IBLOCK_SERVICES, "CODE" => $serviceSection->code];
			$arGroupBy = false;
			$arNavStartParams = false;
			$arSelectFields = ["IBLOCK_ID", "ID", "NAME"];

			$res = \CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
			$element = $res->Fetch();
			if(!$element){
				continue;
			}
			$servicesElements[$serviceSection->id] = new ServiceElement($element["ID"], $serviceSection->pricesId);
		}

		return $servicesElements;
	}

	public function updateServiceElements(array $servicesElements)
	{
		/** @var ServiceElement $serviceElement */
		foreach ($servicesElements as $serviceElement){
			if(!\CIBlockElement::SetPropertyValueCode($serviceElement->id, "PRICES", $serviceElement->pricesId)){
				throw new \Exception("Ошибка при обновлении привязки Цен к Услугам");
			}
		}

		return true;
	}

	public function getServiceElementPropertyValue(int $serviceId, string $propertyCode): array
	{
		$res = \CIBlockElement::GetProperty(ID_IBLOCK_SERVICES, $serviceId, [], ["CODE" => $propertyCode]);

		$values = [];
		while ($ob = $res->GetNext()) {
			$values[] = $ob["VALUE"];
		}

		return $values;
	}

}