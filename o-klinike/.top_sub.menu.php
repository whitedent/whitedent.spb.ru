<?
$aMenuLinks = [
	["Акции", "/o-klinike/akcii/"],
	["Лицензии", "/o-klinike/litsenzii/"],
	["Наши работы", "/o-klinike/nashi-raboty/"],
	["Интерьеры", "/o-klinike/interery/"],
	["Оборудование", "/o-klinike/oborudovanie/"],
	["Вакансии", "/o-klinike/vakansii/"],
	["Обращение директора", "/o-klinike/obraschenie-direktora/"],
	["Меры противодействия COVID", "/o-klinike/covid/"],
	["Зуботехническая лаборатория", "/o-klinike/zubotekhnicheskaya-laboratoriya-stomatologicheskoy-kliniki/"],
];
