<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Наличие собственной зуботехнической лаборатории позволяет нам добиваться высокого качества ортопедического лечения. У нас работают зубные техники с большим опытом работы, используют только современные технологии.");
$APPLICATION->SetTitle("Зуботехническая лаборатория стоматологической клиники");
?><section class="page-section container">
<div class="content">
  <div class="page-subsection">
    <h1>Зуботехническая лаборатория стоматологической клиники</h1>
  <p>Зуботехническая лаборатория стоматологической клиники «ВайтДент» оснащена передовым в Санкт-Петербурге оборудованием. Опыт сотрудников лаборатории и современные материалы позволяют решать самые сложные в задачи в области <a href="/uslugi/protezirovanie-zubov/">протезировании зубов</a>. Благодаря использованию современных и классических, проверенных временем методик, мы предлагаем наиболее эффективные решения. </p>
  <p> Преимущества зуботехнической лаборатории «ВайтДент»: </p>
  <ul class="list-marked">
    <li>индивидуальный подход к каждому пациенту;</li>
    <li>использование безопасных и проверенных временем методик;</li>
    <li>опытные сотрудники с многолетним стажем.</li>
  </ul>
  <p> Одной из основных задач зуботехнической лаборатории стоматологической клиники «ВайтДент» является безопасность пациентов. Инструменты и оборудование лаборатории проходят регулярную стерилизацию и профилактический осмотр. Это позволяет избежать негативного воздействия на здоровье пациентов. </p>

  </div>
  <div class="page-subsection">
    <div class="block-marked block-marked--arrow">
      <p class="text-marked text-marked--big text-marked--bold">Стаж работы зубных техников лаборатории составляет более 10 лет.</p>
      <p>Благодаря современным, классическим методикам, предлагаем эффективное протезирование.</p>
    </div>
  </div>
  <div class="page-subsection">
    <p> Стаж работы зубных техников лаборатории составляет более 10 лет. Их профессионализм подтвержден действующими Сертификатами. Сотрудники зуботехнического подразделения стоматологической клиники «ВайтДент» регулярно повышают квалификацию в ведущих российских и зарубежных учебных центрах. </p>

    <h2>Сертификаты врачей</h2>

    <p> <a href="/upload/iblock/0e3/0e3da56ecb8ad98ece529463596b1530.jpg?1385094076207089" class="link-dashed image-open" data-glightbox data-gallery="sertificate">Сертификат №1</a>
      <br />
     <a href="/upload/iblock/345/345434e4c2f1b34c1415158be3fd5a5f.jpg?1385094076231255" class="link-dashed image-open" data-glightbox data-gallery="sertificate">Сертификат №2</a>
      <br />
     <a href="/upload/iblock/24a/24a9928bac85ef4049206685c2e5d9e8.jpg?1385094076161601" class="link-dashed image-open" data-glightbox data-gallery="sertificate">Сертификат №3</a>
      <br />
     <a href="/upload/iblock/0b1/0b1f94d456cc08b171f83d07698f4de4.jpg?1385094076176117" class="link-dashed image-open" data-glightbox data-gallery="sertificate">Сертификат №4</a> </p>

    <p> В зуботехнической лаборатории особое внимание уделяется имплантологии. Это позволило нам добиться отличных результатов, независимо от того, какую систему выбирает пациент. Изготовленные зубными техниками имплантаты выглядят эстетично и служат дольше установленного срока. По желанию пациента, сотрудники зуботехнической лаборатории изготовят: </p>

    <ul class="list-marked">
      <li>металлокермамические конструкции;</li>
      <li>безметалловые конструкции;</li>
      <li>бюгельные протезы;</li>
      <li>протезы на имплантатах. </li>
    </ul>
  </div>
  <div class="page-subsection">
    <div class="grid grid--par-like">
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6">
        <a href="/resources/medialibrary/5fe8a1fd7ab33a21e76fb8429d31b0e7/e57ee993c398ada14623b9b8bc8a78ac.JPG" data-glightbox data-gallery="laboratory">
            <img src="/resources/medialibrary/5fe8a1fd7ab33a21e76fb8429d31b0e7/e57ee993c398ada14623b9b8bc8a78ac.JPG" title="" alt="">
        </a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6">
        <a href="/resources/medialibrary/d1f255a373a3cef72e03aa9d980c7eca/418c73f538dec62ba3e4fbeef9edcf9c.JPG" data-glightbox data-gallery="laboratory">
            <img src="/resources/medialibrary/d1f255a373a3cef72e03aa9d980c7eca/418c73f538dec62ba3e4fbeef9edcf9c.JPG" title="" alt="">
        </a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6">
        <a href="/resources/medialibrary/ffbbfcd692e84d6b82af1b5c0e6f5446/dff96e9d9268890381f361b95ce516b8.JPG" data-glightbox data-gallery="laboratory">
            <img src="/resources/medialibrary/ffbbfcd692e84d6b82af1b5c0e6f5446/dff96e9d9268890381f361b95ce516b8.JPG" title="" alt="">
        </a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6">
        <a href="/resources/medialibrary/3b11d4ed537ced20e41a9b8a067f5d85/dcc2f1de9ce021e509649f308a4cd47d.JPG" data-glightbox data-gallery="laboratory">
            <img src="/resources/medialibrary/3b11d4ed537ced20e41a9b8a067f5d85/dcc2f1de9ce021e509649f308a4cd47d.JPG" title="" alt="">
        </a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6">
        <a href="/resources/medialibrary/2fdd9ab840b84783be1118feed947715/76e910f5aed1d462b60ba9555c0c32ec.JPG" data-glightbox data-gallery="laboratory">
            <img src="/resources/medialibrary/2fdd9ab840b84783be1118feed947715/76e910f5aed1d462b60ba9555c0c32ec.JPG" title="" alt="">
        </a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6">
        <a href="/resources/medialibrary/5fe8a1fd7ab33a21e76fb8429d31b0e7/e57ee993c398ada14623b9b8bc8a78ac.JPG" data-glightbox data-gallery="laboratory">
            <img src="/resources/medialibrary/5fe8a1fd7ab33a21e76fb8429d31b0e7/e57ee993c398ada14623b9b8bc8a78ac.JPG" title="" alt="">
        </a>
      </div>

    </div>
  </div>
  <div class="page-subsection">
    <p> Чтобы узнать больше о работе зуботехнической лаборатории стоматологической клиники «ВайтДент», приходите к нам в любое удобное для Вас время. Наша лаборатория работает без выходных. </p>
  </div>



</div>
</section>
 <?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
