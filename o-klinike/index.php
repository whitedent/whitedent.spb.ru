<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Мы являемся одной из ведущих стоматологических клиник города Санкт-Петербурга. В нашей клинике работают профессиональные врачи-стоматологии.");
$APPLICATION->SetPageProperty("keywords", "вайдент стоматологическая клиника стоматология");
$APPLICATION->SetPageProperty("title", "Стоматологическая клиника «ВайтДент» - в ногу со временем!");
$APPLICATION->SetTitle("О компании «ВайтДент»");
?><section class="page-section container">
<div class="content">
	<h1>О клинике</h1>
	<div class="page-subsection">
 <nav class="page-navigation">
		<ul class="page-navigation__list">
			<li class="page-navigation__item"><a href="#section-advantages">Преимущества</a> </li>
			<li class="page-navigation__item"><a href="#section-numbers">Цифры</a> </li>
			<li class="page-navigation__item"><a href="#section-methods">Подход к работе</a> </li>
			<?/* <li class="page-navigation__item"><a href="#section-insurance">Обслуживание по ДМС</a> </li> */?>
			<li class="page-navigation__item"><a href="#section-services">Услуги</a> </li>
			<li class="page-navigation__item"><a href="#section-gratitude">Благодарности</a> </li>
		</ul>
 </nav>
	</div>
	<div class="page-subsection">
		<div class="block-marked">
			<p>
				 Стоматологическая клиника «ВайтДент» является перспективным лечебным учреждением, которое занимает достойное место среди ведущих клиник данного направления в&nbsp;Санкт-Петербурге.
			</p>
		</div>
	</div>
</div>
</section> <section class="page-section container" id="section-advantages">
<div class="content">
	<h2>Преимущества клиники Вайтдент</h2>
	<div class="page-subsection">
		<div class="grid grid--padding-y">
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked block-marked--background block-marked--background-expert">
					<h3>Высокая оценка экспертов</h3>
					<p>
						 Входим в&nbsp;топ 10&nbsp;клиник по&nbsp;имплантации по&nbsp;версии <a href="https://www.spb.kp.ru/daily/26339.5/3222258/" target="_blank">«Комсомольской Правды»</a>
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked block-marked--background block-marked--background-microscope">
					<h3>Собственная зуботехническая лаборатория</h3>
					<p>
						 Сокращаем время производства протезов и&nbsp;их&nbsp;стоимость, контролируем качество на&nbsp;всех этапах
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked block-marked--background block-marked--background-guarantees">
					<h3>Гарантии на&nbsp;все виды стоматологических услуг</h3>
					<p>
						 Все установленные врачом и согласованные с пациентом прогнозируемые <a href="/patsientam/garantii/">гарантии</a> на каждую выполненную работу фиксируются в медицинской карте за подписью пациента и врача
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked block-marked--background block-marked--background-specialists">
					<h3>Специалисты во&nbsp;всех сферах</h3>
					<p>
						 Благодаря наличию врачей всех специальностей: терапия, имплантология, хирургия, протезирование, ортодонтия, пародонтология оказываем полный цикл услуг
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked block-marked--background block-marked--background-xray">
					<h3>Современная диагностика</h3>
					<p>
						 Диагностика для постановки диагноза проходит с&nbsp;использованием современной техники: компьютерный томограф GALILEOS фирма SIRONA (Германия)
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked block-marked--background block-marked--background-function">
					<h3>Работаем с&nbsp;2004 года</h3>
					<p>
						 За&nbsp;17&nbsp;лет мы&nbsp;помогли более 10000 пациентов
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked block-marked--background block-marked--background-credit-card">
					<h3>Лечение в&nbsp;кредит и&nbsp;рассрочка</h3>
					<p>
						 Выгодные условия кредитования от&nbsp;Альфа банк. Рассрочка платежа (для постоянных клиентов при длительном лечении)
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</section> <section class="page-section container" id="section-numbers">
<div class="content">
	<h2>Цифры</h2>
	<div class="page-subsection">
		<div class="grid grid--padding-y grid--justify-center">
			<div class="grid__cell grid__cell--l-4 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
				<div class="block-marked block-marked--shadow text-align--center">
					<h3><strong>10000</strong><br>
					 счастливых пациентов</h3>
					<p>
						 За&nbsp;время работы клиники мы помогли тысячам пациентов
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-4 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
				<div class="block-marked block-marked--shadow text-align--center">
					<h3><strong>17</strong><br>
					 лет опыта</h3>
					<p>
						 Наша клиника начала свою работу с&nbsp;2004&nbsp;года
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-4 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
				<div class="block-marked block-marked--shadow text-align--center">
					<h3><strong>11</strong><br>
					 специалистов</h3>
					<p>
						 Профессиональная команда, состоящая из&nbsp;главного врача и&nbsp;10 стоматологов
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</section> <section class="page-section container" id="section-methods">
<div class="content">
	<h2>Подход к&nbsp;работе</h2>
	<div class="page-subsection">
		<div class="grid grid--padding-y">
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked">
					<p>
 <img alt="Работа «в четыре руки»" src="/images/about/01.jpg" class="block-rounded" title="Работа «в четыре руки»">
					</p>
					<p>
						 Работа с&nbsp;пациентами в&nbsp;клинике построена по&nbsp;принципу <strong>«в&nbsp;четыре руки»</strong>. Врач работает в&nbsp;паре с&nbsp;ассистентом, что позволяет проводить <strong>комфортное</strong> и&nbsp;максимально <strong>эффективное</strong> лечение.
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked">
					<p>
 <img alt="Программах антиГЕПАТИТ и антиСПИД" src="/images/about/02.jpg" class="block-rounded" title="Программах антиГЕПАТИТ и антиСПИД">
					</p>
					<p>
						 Клиника участвует в&nbsp;программах антиГЕПАТИТ и&nbsp;антиСПИД. Специальные программы обработки инструментов и&nbsp;стерилизации с помощью современного автоклава MOCOM (ИТАЛИЯ), <strong>проверка качества стерилизации</strong> после каждого цикла.
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked">
					<p>
 <img alt="Современная аппаратура" src="/images/about/03.jpg" class="block-rounded" title="Современная аппаратура">
					</p>
					<p>
						 Оказание стоматологической помощи в&nbsp;центре осуществляется с&nbsp;помощью <strong>современной аппаратуры</strong> (стоматологические установки SDS-2000 (США), физиодиспенсеры фирмы W&amp;H (Австрия) и&nbsp;NSK (Япония),&nbsp;вакуумная система TURBO-SMART ФИРМЫ CATTANI (Италия).
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked">
					<p>
 <img alt="Высокое качество обработки зубов" src="/images/about/04.jpg" class="block-rounded" title="Высокое качество обработки зубов">
					</p>
					<p>
						 Высокое <strong>качество обработки зубов</strong> врачами центра достигается благодаря использованию инструментов ведущих мировых производителей (стоматологические наконечники W&amp;H (Австрия) и&nbsp;NSK (Япония), стоматологические боры и&nbsp;фрезы производство Германия).
					</p>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked">
					<ul class="list-marked">
						<li>При необходимости обезболивания при оказании стоматологической помощи, нами используются современные и&nbsp;безопасные анестетики производства Франции и&nbsp;Германии (Септанест, Артикаин, Эпинефрин, Убистезин), сертифицированные в&nbsp;России и&nbsp;прошедшие клинические исследования в&nbsp;Европе</li>
						<li>Каждый сложный случай рассматривается на&nbsp;общем врачебном собрании. Либо организуем консультации пациентов у&nbsp;ведущих специалистов Санкт-Петербурга (ЛОР врачи, ортопеды, дерматологи, онкологи)</li>
					</ul>
				</div>
			</div>
			<div class="grid__cell grid__cell--l-6 grid__cell--m-6 grid__cell--xs-12">
				<div class="block-marked">
					<p>
 <img alt="Многолетний опыт специалиста нашей зуботехнической лаборатории" src="/images/about/05.jpg" class="block-rounded" title="Многолетний опыт специалиста нашей зуботехнической лаборатории">
					</p>
					<p>
						 Специалист нашей собственной зуботехнической лаборатории имеет многолетний опыт работы по&nbsp;созданию любых протезов: пластмассовых, диоксида циркония, металлокерамических, керамических.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</section> 
<? /* <section class="page-section container" id="section-insurance">
<div class="content">
	<h2>Обслуживание по&nbsp;ДМС</h2>
	<div class="page-subsection">
		<div class="block-marked">
			<p>
				 Мы принимаем пациентов застрахованных в&nbsp;следующих компаниях:
			</p>
			<div class="grid grid--padding-y grid--align-center grid--par-like">
				<div class="grid__cell grid__cell--xs-auto">
 <img width="132" src="/images/about/rosgosstrah.svg" alt="">
				</div>
				<div class="grid__cell grid__cell--xs-auto">
 <img width="132" src="/images/about/rms.png" alt="">
				</div>
				<div class="grid__cell grid__cell--xs-auto">
 <img width="132" src="/images/about/reso.svg" alt="">
				</div>
				<div class="grid__cell grid__cell--xs-auto">
 <img width="132" src="/images/about/allianz.svg" alt="">
				</div>
				<div class="grid__cell grid__cell--xs-auto">
 <img width="132" src="/images/about/vtb.svg" alt="">
				</div>
				<div class="grid__cell grid__cell--xs-auto">
 <img width="132" src="/images/about/medexpress.svg" alt="">
				</div>
				<div class="grid__cell grid__cell--xs-auto">
 <img width="132" src="/images/about/pomosch.png" alt="">
				</div>
			</div>
		</div>
	</div>
</div> */?>
</section> <section class="page-section container" id="section-services">
<div class="content">
	<h2>Широкий спектр стоматологических услуг</h2>
	<div class="page-subsection">
		<div class="block-marked">
			<p>
				 Мы&nbsp;оказываем всестороннюю консультационную и&nbsp;лечебную помощь нашим пациентам.
			</p>
			<div class="grid grid--padding-y grid--par-like">
				<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
					<ul class="list-marked list-marked--check">
						<li><a href="/uslugi/protezirovanie-zubov/">Протезирование зубов</a></li>
						<li><a href="/uslugi/implantatsiya/">Имплантация зубов</a></li>
						<li><a href="/uslugi/terapiya/">Лечение зубов</a></li>
					</ul>
				</div>
				<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
					<ul class="list-marked list-marked--check">
						<li><a href="/uslugi/lechenie-desen/">Лечение десен</a></li>
						<li><a href="/uslugi/hirurgiya/">Хирургическая стоматология</a></li>
						<li><a href="/uslugi/ispravlenie-prikusa/">Исправление прикуса</a></li>
					</ul>
				</div>
				<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
					<ul class="list-marked list-marked--check">
						<li><a href="/uslugi/gigiena-polosti-rta/">Гигиена полости рта</a></li>
						<li><a href="/uslugi/galileos-comfort/">Диагностика зубов</a></li>
						<li><a href="/uslugi/obezbolivaniye/">Анестезия</a></li>
					</ul>
				</div>
				<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
					<ul class="list-marked list-marked--check">
						<li><a href="/uslugi/konsultatsii/">Консультация</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</section> <section class="page-section container" id="section-gratitude">
<div class="content">
	<h2>Благодарности</h2>
	<div class="page-subsection">
		<div class="grid grid--padding-y">
			<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-6">
 <a href="/images/about/thanks-1.jpg" data-glightbox="" data-gallery="thanks"><img src="/images/about/thanks-1-small.jpg" class="block-rounded block-rounded--big" alt="" title=""></a>
			</div>
			<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-6">
 <a href="/images/about/thanks-2.jpg" data-glightbox="" data-gallery="thanks"><img src="/images/about/thanks-2-small.jpg" class="block-rounded block-rounded--big" alt="" title=""></a>
			</div>
			<div class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-6">
 <a href="/images/about/thanks-3.jpg" data-glightbox="" data-gallery="thanks"><img src="/images/about/thanks-3-small.jpg" class="block-rounded block-rounded--big" alt="" title=""></a>
			</div>
		</div>
	</div>
</div>
 </section>
 <?
 require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
