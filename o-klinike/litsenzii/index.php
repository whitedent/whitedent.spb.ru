<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Лицензии");
$APPLICATION->SetPageProperty("description", "Мы работаем в соответствии с требованиями местного законодательства и имеем все необходимые разрешения и лицензию на осуществление медицинской деятельности");
?>
<section class="page-section container">
	<div class="content">

		<h1>Лицензии</h1>

		<div class="page-subsection">
			<div class="block-marked">
				<p>Мы работаем в соответствии с требованиями местного законодательства и имеет все необходимые разрешения и лицензию Л041-01148-78/00297199 от 09.02.2015 г. (бессрочная).</p>
			</div>
		</div>

		<div class="page-subsection">

			<h2>Лицензия на осуществление медицинской деятельности</h2>

			<div class="grid grid--par-like grid--padding-y">
				<div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a href="/images/about/licenzii/license-1.jpg" data-glightbox data-gallery="licenses"><img src="/images/about/licenzii/license-1-small.jpg" alt="" title=""></a>
				</div>
				<div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a href="/images/about/licenzii/license-2.jpg" data-glightbox data-gallery="licenses"><img src="/images/about/licenzii/license-2-small.jpg" alt="" title=""></a>
				</div>
				<div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a href="/images/about/licenzii/license-3.jpg" data-glightbox data-gallery="licenses"><img src="/images/about/licenzii/license-3-small.jpg" alt="" title=""></a>
				</div>
			</div>

			<h2>Сертификат соответствия</h2>

			<div class="grid grid--par-like grid--padding-y">
				<div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a href="/images/about/licenzii/sertificate-1.jpg" data-glightbox data-gallery="sertificates"><img src="/images/about/licenzii/sertificate-1-small.jpg" alt="" title=""></a>
				</div>
				<div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a href="/images/about/licenzii/sertificate-2.jpg" data-glightbox data-gallery="sertificates"><img src="/images/about/licenzii/sertificate-2-small.jpg" alt="" title=""></a>
				</div>
			</div>

		</div>

	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
