<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Вакансии клиники");
$APPLICATION->SetPageProperty("description", "Приглашаем на работу талантливых и опытных стоматологов всех профилей, которые умеют и любят работать!");
?>
<section class="page-section container">
	<div class="content">

		<h1>Вакансии</h1>

		<div class="page-subsection">
			<div class="block-marked block-marked--arrow">
				<p>Стоматологическая клиника «ВайтДент» является <strong>перспективным лечебным учреждением</strong>, которое занимает достойное место <strong>среди ведущих клиник</strong>данного направления в Санкт-Петербурге.</p>
				<p>Наш клиника работает на рынке медицинских услуг c 1989 года. Входим в топ 10 клиник по имплантации по версии <a href="/">«Комсомольской Правды»</a>.</p>
			</div>
		</div>

		<div class="page-subsection">
			<div class="block-marked-image">
				<div class="grid grid--par-like">
					<div class="grid__cell grid__cell--l-5 grid__cell--m-5 grid__cell--s-8 grid__cell--xs-12 block-marked-image__image"><img src="/images/about/vakansii/vakansii.jpg" alt="" title=""/>
					</div>
					<div class="grid__cell grid__cell--l-7 grid__cell--m-7 grid__cell--s-12 grid__cell--xs-12 block-marked-image__body">
						<div class="block-marked-image__body-inner">
							<ul class="list-marked list-marked--arrow">
								<li>Современное оборудование</li>
								<li>Конкурентная заработная плата</li>
								<li>Комфортные условия труда</li>
								<li>«Белая» заработная плата и оформление по ТК</li>
								<li>Профессиональное обучение</li>
								<li>Возможности карьерного роста</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="page-subsection">
			<p>Главная составляющая успеха любой компании — это ее сотрудники. В настоящее время в компании работает 11 человек. Дружный коллектив единомышленников, профессионалов высокого класса — залог плодотворной деятельности компании.</p>
			<p>Приглашаем на работу <strong>талантливых и опытных</strong> стоматологов всех профилей, которые умеют и любят работать!</p>
			<p>У нас отличные условия, хороший коллектив и мы умеем договариваться.</p>
		</div>

		<div class="page-subsection">
			<h2>Актуальные вакансии</h2>
			<ul class="list-marked">
				<li><strong>Врач-стоматолог-терапевт с клиентской базой. Заработная плата по договоренности.</strong></li>
				<li><strong>Ассистент стоматолога. Заработная плата по договоренности. Рабочий день с 11 до 21 часа.</strong></li>
			</ul>
			<p>Если вакансий, соответствующих вашей квалификации, в настоящее время нет, мы поместим ваше резюме в кадровый резерв клиники, и при открытии вакансии, на которую вы претендуете, мы с вами свяжемся.</p>
			<h2>Контакты для соискателей</h2>
			<p>Если sы ищете работу в современной стабильной стоматологической клинике — <strong>свяжитесь с нами</strong>.</p>
			<p>Записаться на собеседование можно по номеру, указанному ниже.</p>
			<p>Также вы можете прислать информацию о себе в свободной форме или резюме на наш e-mail.</p>
		</div>

		<div class="page-subsection">
			<div class="grid grid--cols-12">
				<div class="grid__cell grid__cell--l-5 grid__cell--m-8 grid__cell--xs-12">
					<div class="block-marked text-align--center">
						<p class="text-marked">тел. <span class="phone">+7 (812) 612-97-92</span>
						</p>
						<p>Электропочта: <a href="mailto:send@whitedent.spb.ru">send@whitedent.spb.ru</a></p>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
