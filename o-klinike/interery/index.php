<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интерьеры клиники");
$APPLICATION->SetPageProperty("description", "Здесь вы можете ознакомиться с интерьерами клиники, оснащением стоматологических кабинетов");
?>
<section class="page-section container">
  <div class="content">
    <h1>Интерьеры клиники</h1>
    <div class="page-subsection">
    </div>
    <div class="grid grid--padding-y">
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-1.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-1_small.webp, /images/o-klinike/interery/interiors-1_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-1_small.jpg, /images/o-klinike/interery/interiors-1_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-1_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-2.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-2_small.webp, /images/o-klinike/interery/interiors-2_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-2_small.jpg, /images/o-klinike/interery/interiors-2_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-2_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-3.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-3_small.webp, /images/o-klinike/interery/interiors-3_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-3_small.jpg, /images/o-klinike/interery/interiors-3_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-3_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-4.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-4_small.webp, /images/o-klinike/interery/interiors-4_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-4_small.jpg, /images/o-klinike/interery/interiors-4_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-4_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-5.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-5_small.webp, /images/o-klinike/interery/interiors-5_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-5_small.jpg, /images/o-klinike/interery/interiors-5_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-5_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-6.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-6_small.webp, /images/o-klinike/interery/interiors-6_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-6_small.jpg, /images/o-klinike/interery/interiors-6_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-6_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-7.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-7_small.webp, /images/o-klinike/interery/interiors-7_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-7_small.jpg, /images/o-klinike/interery/interiors-7_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-7_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-8.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-8_small.webp, /images/o-klinike/interery/interiors-8_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-8_small.jpg, /images/o-klinike/interery/interiors-8_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-8_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-9.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-9_small.webp, /images/o-klinike/interery/interiors-9_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-9_small.jpg, /images/o-klinike/interery/interiors-9_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-9_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-10.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-10_small.webp, /images/o-klinike/interery/interiors-10_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-10_small.jpg, /images/o-klinike/interery/interiors-10_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-10_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-11.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-11_small.webp, /images/o-klinike/interery/interiors-11_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-11_small.jpg, /images/o-klinike/interery/interiors-11_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-11_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-12.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-12_small.webp, /images/o-klinike/interery/interiors-12_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-12_small.jpg, /images/o-klinike/interery/interiors-12_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-12_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-13.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-13_small.webp, /images/o-klinike/interery/interiors-13_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-13_small.jpg, /images/o-klinike/interery/interiors-13_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-13_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-14.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-14_small.webp, /images/o-klinike/interery/interiors-14_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-14_small.jpg, /images/o-klinike/interery/interiors-14_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-14_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-15.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-15_small.webp, /images/o-klinike/interery/interiors-15_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-15_small.jpg, /images/o-klinike/interery/interiors-15_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-15_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-16.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-16_small.webp, /images/o-klinike/interery/interiors-16_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-16_small.jpg, /images/o-klinike/interery/interiors-16_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-16_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-17.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-17_small.webp, /images/o-klinike/interery/interiors-17_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-17_small.jpg, /images/o-klinike/interery/interiors-17_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-17_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-18.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-18_small.webp, /images/o-klinike/interery/interiors-18_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-18_small.jpg, /images/o-klinike/interery/interiors-18_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-18_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-19.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-19_small.webp, /images/o-klinike/interery/interiors-19_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-19_small.jpg, /images/o-klinike/interery/interiors-19_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-19_small.jpg"/>
          </picture></a>
      </div>
      <div class="grid__cell grid__cell--l-2 grid__cell--m-3 grid__cell--s-4 grid__cell--xs-6"><a class="gallery-item" href="/images/o-klinike/interery/interiors-20.jpg" data-glightbox data-gallery="interiors">
          <picture>
            <source srcset="/images/o-klinike/interery/interiors-20_small.webp, /images/o-klinike/interery/interiors-20_small@2x.webp 2x" type="image/webp"/>
            <source srcset="/images/o-klinike/interery/interiors-20_small.jpg, /images/o-klinike/interery/interiors-20_small@2x.jpg 2x"/><img src="/images/o-klinike/interery/interiors-20_small.jpg"/>
          </picture></a>
      </div>
    </div>
  </div>
</section>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
