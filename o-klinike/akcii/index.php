<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Все проводимые акции и наличие скидок на стоматологические услуги Вы можете уточнить в данном разделе нашего сайта или обратиться к специалистам контактного центра ВайтДент.");
$APPLICATION->SetPageProperty("keywords", "акции скидки стоматология протезирование");
$APPLICATION->SetPageProperty("title", "Акции и скидки на стоматологические услуги – «ВайтДент»");
$APPLICATION->SetTitle("Акции");
?>
<section class="page-section container">
    <div class="content">
        <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
        <? include __DIR__."/include/top-description.php"?>
        <? include __DIR__."/include/component.php"?>
        <? include __DIR__."/include/bottom-description.php"?>
    </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");