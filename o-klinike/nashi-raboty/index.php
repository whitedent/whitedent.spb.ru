<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши работы");
$APPLICATION->SetPageProperty("title", "Наши работы");
$APPLICATION->SetPageProperty("description", "Ознакомившись с фотографиями до и после стоматологического лечения, вы сможете оценить качество работы врачей клиники и сравнить его с работами специалистов других клиник.");
?>
<section class="page-section container">
	<div class="content">
		<h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
		<? include __DIR__."/description.php" ?>
		<? include __DIR__."/menu.php" ?>
		<? include __DIR__."/component.php" ?>
	</div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");