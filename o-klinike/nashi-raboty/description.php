<? if(!$_GET["SECTION_CODE"]): ?>
    <div class="page-subsection">
		<?
		$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => "/o-klinike/nashi-raboty/short-description.html",
			)
		);
		?>
    </div>
<? else: ?>
	<?
	$fieldCode = "UF_SHORT_DESCRIPTION";
	$iblockId = ID_IBLOCK_WORKS;
	$sectionCode = filterCode($_REQUEST["SECTION_CODE"]);
	if($sectionCode){
		$sectionShortDescription = getSectionPropertyValue($fieldCode, $iblockId, $sectionCode);
	}
	?>
	<? if($sectionShortDescription): ?>
        <div class="page-subsection">
			<?= $sectionShortDescription ?>
        </div>
	<? endif ?>
<? endif ?>
