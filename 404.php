<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ошибка 404 — страница не найдена");
?>
<section class="page-section container">
    <div class="content">

        <h1>Ошибка 404 — страница не найдена</h1>

        <div class="page-subsection">
            <p>Страница не существует или была удалена.</p>
            <p class="text-marked text-marked--big">Страницу не вернуть, но мы можем вернуть вам здоровые зубы и красивую улыбку.</p>
        </div>

        <div class="page-subsection">
            <p>Найдите через поиск по сайту:</p>
            <div class="grid grid--par-like grid--padding-y">
                <div class="grid__cell grid__cell--l-6 grid__cell--m-8 grid__cell--xs-12">
					<?
					$APPLICATION->IncludeComponent(
						"bitrix:search.form",
						"404",
						Array(
							"USE_SUGGEST" => "N",
							"PAGE" => "#SITE_DIR#search/index.php"
						)
					);
					?>
                </div>
            </div>
        </div>

        <div class="page-subsection">
            <p>Возможно, вас заинтресуют наши услуги или узнать больше о стоматологии:</p>
            <nav class="page-navigation">
                <ul class="page-navigation__list">
                    <li class="page-navigation__item"><a href="/">Консультация</a></li>
                    <li class="page-navigation__item"><a href="/">Рентгенодиагностика</a></li>
                    <li class="page-navigation__item"><a href="/">Лечение зубов</a></li>
                    <li class="page-navigation__item"><a href="/">Имплантация зубов</a></li>
                    <li class="page-navigation__item"><a href="/">Протезирование зубов</a></li>
                    <li class="page-navigation__item"><a href="/">Хирургия</a></li>
                    <li class="page-navigation__item"><a href="/">Исправление прикуса</a></li>
                    <li class="page-navigation__item"><a href="/">Гигиена полости рта</a></li>
                    <li class="page-navigation__item"><a href="/">Анестезия</a></li>
                </ul>
            </nav>
        </div>

    </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");