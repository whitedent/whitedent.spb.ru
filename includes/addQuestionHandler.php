<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!empty($_POST)){

/*
	$data = array(
		'secret' => "6LdWQVkUAAAAAOlDJ5fGtX4GVzbgUFlbSwZTz9Xc",
		'response' => $_POST["g-recaptcha-response"]
	);

	$verify = curl_init();
	curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
	curl_setopt($verify, CURLOPT_POST, true);
	curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
	curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
	$response = json_decode(curl_exec($verify));

	if ($response->success != true) {
		$data = array("error" => "error");
		die(json_encode($data));
	}
*/

	//init data
	$name = htmlspecialcharsbx($_POST['NAME']);
	$email = htmlspecialcharsbx($_POST['EMAIL']);
	$topic = htmlspecialcharsbx($_POST['TOPIC']);
	$section = htmlspecialcharsbx($_POST['SECTION']);
	$question = htmlspecialcharsbx($_POST['QUESTION']);
	
	
  CModule::IncludeModule('iblock');
	$topic = get_unique_topic($topic);
	
	
	//code generation
	$arParamsCode = array(
	   "max_len" => 255,  
	   "change_case" => "L",  
	   "replace_space" => '-',  
	   "replace_other" => '-',   
	   "delete_repeat_replace" => true
	);
	 
	$code = CUtil::translit($topic, "ru", $arParamsCode);
	
	$element = new CIBlockElement;
	
	$elementProps = array();
	$elementProps['NAME'] = $name;
	$elementProps['EMAIL'] = $email;
	$elementProps['STATUS'] = array("VALUE" => 2);
	
	if (!empty($_FILES))
	{
		//make file array
	
		$file['name'] = $_FILES['FILE']['name'];
		$file['type'] = $_FILES['FILE']['type']; 
		$file['tmp_name'] = $_FILES['FILE']['tmp_name'];
		$file['size'] = $_FILES['FILE']['size'];
		$file['MODULE_ID'] = 'iblock';
		
		$elementProps['FILE'] = $file;
	}
	
	
	$arElementFields = Array(
		//"MODIFIED_BY"    	=> $GLOBALS['USER']->GetID(), 
		"IBLOCK_SECTION_ID" => intval($section),   
		"IBLOCK_ID"      	=> 12,
		"PROPERTY_VALUES"	=> $elementProps,
		"NAME"           	=> $topic,
		"CODE"				=> $code,
		"ACTIVE"         	=> "Y",
		"PREVIEW_TEXT"   	=> $question
	);



	if ($PRODUCT_ID = $element->Add($arElementFields))
	{
		$data = array("success" => "success");
	}
	else
	{
		$data = array("error" => "error");
	}
	echo json_encode($data);
} else {
	LocalRedirect('/');
}

function get_unique_topic($topicOrig, $topicNew = '', $counter = 1)
{
    $element = new CIBlockElement;

    if($counter == 1){
        $topicNew = $topicOrig;
    }

	//code generation
	$arParamsCode = array(
	   "max_len" => 255,
	   "change_case" => "L",
	   "replace_space" => '-',
	   "replace_other" => '-',
	   "delete_repeat_replace" => true
	);
    $codeNew = CUtil::translit($topicNew, "ru", $arParamsCode);

    $res_1 = $element->GetList(Array(), Array('NAME' => $topicNew));
    $res_2 = $element->GetList(Array(), Array('CODE' => $codeNew));

    if(
        ($res_1->GetNext())
        OR ($res_2->GetNext())
    )
    {
        $counter++;
        $topicTemp = $topicOrig.' ('.$counter.')';
        $topicNew = get_unique_topic($topicOrig, $topicTemp, $counter);
    }

    return $topicNew;
}

?>