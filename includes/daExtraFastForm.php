<?php
/*
Пример вызова:
<?
include($_SERVER['DOCUMENT_ROOT']."/includes/daExtraFastForm.php");
$fastForm = new DaExtraFastForm("Ежедневная уборка помещений");
?>
*/


class DaExtraFastForm
{
// тексты
	private $serviceName;									// название услуги, о которой идет речь на текущей странице
	private $textareaDefaultVal;							// дефолтное значение в текстовом поле, для проверки "а введён ли телефон?"
// почта 
	private $mailTo = "w-dent@yandex.ru";					// емейл, на который шлем сообщение
	private $mailSubject = "ВайтДент - Сообщение в разделе консультаций";// тема отправляемого письма
	
	
	public function __construct($initServiceName)
	{
		$this->serviceName = $initServiceName;
		$this->textareaDefaultVal = "Здравствуйте, меня интересует $this->serviceName. Пожалуйста, перезвоните мне.";
		$this->letsRide();	// запускаем главный мозг
	}
	private function printForm()	// вывод формы
	{ 
		echo "<div style='clear:both;'></div><b>Заказать звонок</b><form id='daeff' action='#daeff-sent' method='POST' style='margin:0;padding:5px;'>
					Сообщение:
<textarea id='daeff-message' name='daeff-message' style='display:block;width:100%;height:80px;margin:0 0 5px 0;padding:5px;' placeholder='$this->textareaDefaultVal'></textarea>

					Телефон:<br />
					<input type='text' value='' name='daeff-phone'  id='daeff-phone' pattern='^[0-9\+\(\) ]+$' title='Допустимы цифры, скобки и пробел' /><br />

					Имя:<br />
					<input type='text' value='' name='daeff-name' id='daeff-name' pattern='^[а-яА-ЯёЁ ]+$' title='Допустимы русские буквы и пробел'/><br />
					<label class=\"form-agreement\">
                    <input type=\"checkbox\" id=\"agreement\" checked required>
                    Нажимая на кнопку, я даю свое согласие на обработку своих персональных данных.
                  </label>
					<p style='padding:0 0 5px 0;margin:0;color:red;display:none;' id='daeff-error'>Нужно заполнить все поля</p>
					<br />
					<input type='submit' value='Отправить' name='daeff-submit' />
				</form>
		";
		
		// ну и скрипты заодно выведем, не делать же под них отдельный метод, ну в самом деле
	
		echo '<script>jQuery(document).ready(function () {	jQuery("#daeff").submit(function(e) {if(!jQuery("#daeff-message").val() || !jQuery("#daeff-phone").val() || !jQuery("#daeff-name").val()){jQuery("#daeff-error").show();	e.preventDefault();}});});</script>';
		
		
	}
	private function printAftermail($success)	// вывод сообщения после отправки письма
	{
		if($success)
			echo "<a name='daeff-sent'></a><p style='color:green'>Ваше сообщение успешно отправлено.</p>";
		else
		{
			echo "<a name='daeff-sent'></a><p style='color:red'>Произошла ошибка при отправке сообщения, пожалуйста, попробуйте еще раз.</p>";
		}
	}
	
	private function letsRide()	// основная логика
	{
		if(isset($_POST['daeff-submit']))	// нажали на кнопку отправки сообщения
		{
			$this->printForm();
			$this->printAftermail($this->sendMail());
		}
		else								// ничего не нажали - просто грузим форму
			$this->printForm();
	}
	
	private function sendMail()
	{
   		$webmaster=$this->mailTo; 
	
		$message = "<strong>Сообщение из формы сверхбыстрой обратной связи:</strong><br />".$_POST['daeff-message']."<br /><strong>Контактный телефон:</strong><br />".$_POST['daeff-phone']."<br /><strong>Имя:</strong><br />".$_POST['daeff-name'];
		$from =  'Форма обратной связи'.'<' . $webmaster . '>';

		$headers = "From: $from\n"; 	
		$headers .= "MIME-Version: 1.0\n" ;	
		$headers .= "Reply-To: $webmaster\n"; 
		$headers .= "Return-Path: <$webmaster>\n";
		$headers .= "Content-Type: text/html;charset=utf-8\n";

		return mail($this->mailTo, $this->mailSubject, $message, $headers);
	}
}

function mime_header_encode($str, $data_charset, $send_charset) {
  if($data_charset != $send_charset) {
    $str = iconv($data_charset, $send_charset, $str);
  }
  return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
}

?>
